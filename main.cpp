/**
 * Aplicação
 * @file main.cpp
 * 
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 * 
 * @version 1.0
 */
#include <cstdlib>
#include <string>
#include <iostream>

using namespace std;

#include "ui/MainUI/MainUI.h"

int main(int argc, const char * argv[]) {
//  comentar bloco try...catch nos testes unitários. Correr testes em modo Debug
    try {
        MainUI ui;
    } catch (SQLException error) {
        cerr << "Error from Database: " << error.getMessage();
        cout << "This application will terminate." << endl;
    }

    return EXIT_SUCCESS;
}

