CREATE OR REPLACE FUNCTION INSERTSOLARSYSTEM 
(NAMESS solarsystem.name%type, 
 MASSSS solarsystem.mass%type,
 EQDIAMSS solarsystem.equatorialdiameter%type) 
RETURN numeric AS 
  idss numeric;
BEGIN
  idss := seq_solarsystem.NEXTVAL;  
  insert into solarsystem(idSolarSystem, name, mass, equatorialDiameter, state) values (idss, NAMESS, MASSSS, EQDIAMSS, 0);
  
  RETURN idss;
END INSERTSOLARSYSTEM;