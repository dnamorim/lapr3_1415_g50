BEGIN
   FOR cur_rec IN (SELECT object_name, object_type
                     FROM user_objects
                    WHERE object_type IN
                             ('TABLE',
                              'VIEW',
                              'PACKAGE',
                              'PROCEDURE',
                              'FUNCTION',
                              'SEQUENCE'
                             ))
   LOOP
      BEGIN
         IF cur_rec.object_type = 'TABLE'
         THEN
            EXECUTE IMMEDIATE    'DROP '
                              || cur_rec.object_type
                              || ' "'
                              || cur_rec.object_name
                              || '" CASCADE CONSTRAINTS';
         ELSE
            EXECUTE IMMEDIATE    'DROP '
                              || cur_rec.object_type
                              || ' "'
                              || cur_rec.object_name
                              || '"';
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line (   'FAILED: DROP '
                                  || cur_rec.object_type
                                  || ' "'
                                  || cur_rec.object_name
                                  || '"'
                                 );
      END;
   END LOOP;
END;
/

CREATE
  TABLE Corps
  (
    ID_Corps       INTEGER NOT NULL ,
    ID_SolarSystem INTEGER NOT NULL ,
    Name           VARCHAR2 (25) ,
    Source         CHAR (1) ,
    Mass           NUMBER ,
    Diameter       NUMBER ,
    Density        NUMBER ,
    Coordinates    VARCHAR2 (50) ,
    Speed          VARCHAR2 (50) ,
    DataReference  VARCHAR2 (50)
  ) ;
ALTER TABLE Corps ADD CONSTRAINT Corps_PK PRIMARY KEY ( ID_Corps,
ID_SolarSystem ) ;

CREATE
  TABLE GravitationalCorps
  (
    ID_Corps       INTEGER NOT NULL ,
    ID_SolarSystem INTEGER NOT NULL
  ) ;
ALTER TABLE GravitationalCorps ADD CONSTRAINT GravitationalCorps_PK PRIMARY KEY
( ID_Corps, ID_SolarSystem ) ;

CREATE
  TABLE GravitationalPropCorps
  (
    ID_Corps       INTEGER NOT NULL ,
    ID_SolarSystem INTEGER NOT NULL ,
    Propulsion     NUMBER
  ) ;
ALTER TABLE GravitationalPropCorps ADD CONSTRAINT GravitationalPropCorps_PK
PRIMARY KEY ( ID_Corps, ID_SolarSystem ) ;

CREATE
  TABLE Orbit
  (
    ID_Corps_Center INTEGER NOT NULL ,
    ID_SolarSystem  INTEGER NOT NULL ,
    ID_Corps_Orbit  INTEGER NOT NULL
  ) ;
ALTER TABLE Orbit ADD CONSTRAINT Orbiting_PK PRIMARY KEY ( ID_Corps_Center,
ID_SolarSystem, ID_Corps_Orbit ) ;

CREATE
  TABLE OrbitalCorps
  (
    ID_Corps                INTEGER NOT NULL ,
    ID_SolarSystem          INTEGER NOT NULL ,
    SemiMajorAxis           INTEGER ,
    Period                  NUMBER ,
    OrbitalSpeed            NUMBER ,
    InclinationAxisToOribit NUMBER ,
    InclinationOrbit        NUMBER ,
    EccentricityOrbit       NUMBER ,
    EscapeVelocity          NUMBER ,
    Perihelion              NUMBER ,
    Aphelion                NUMBER ,
    MeanAnomaly             NUMBER ,
    LongitudePerihelion     NUMBER ,
    LongitudeAscendingNode  NUMBER
  ) ;

ALTER TABLE OrbitalCorps ADD CONSTRAINT OrbitalCorps_PK PRIMARY KEY ( ID_Corps,
ID_SolarSystem ) ;

CREATE
  TABLE ResultsSimulationCorps
  (
    ID_ResultSimulationCorps INTEGER NOT NULL ,
    ID_Simulation       INTEGER NOT NULL ,
    ID_SolarSystem      INTEGER NOT NULL ,
    ID_Corps            INTEGER NOT NULL ,
    TIME                VARCHAR2 (50) ,
    Coordinates         VARCHAR2 (50)
  ) ;
ALTER TABLE ResultsSimulationCorps ADD CONSTRAINT ResultsSimulationCorps_PK PRIMARY KEY (
ID_ResultSimulationCorps, ID_Simulation, ID_SolarSystem, ID_Corps ) ;
ALTER TABLE ResultsSimulationCorps ADD CONSTRAINT ResultsSimulationCorps__UN UNIQUE ( ID_ResultSimulationCorps , ID_Simulation, ID_Corps ) ;

CREATE TABLE ResultsSimulationStar
  (
    ID_ResultSimulationStar NUMBER NOT NULL ,
    ID_Simulation           NUMBER NOT NULL ,
    ID_SolarSystem          NUMBER NOT NULL ,
    Coordinates             VARCHAR2 (50)
  ) ;
ALTER TABLE ResultsSimulationStar ADD CONSTRAINT ResultsSimulationStar_PK PRIMARY KEY ( ID_ResultSimulationStar, ID_Simulation, ID_SolarSystem ) ;
ALTER TABLE ResultsSimulationStar ADD CONSTRAINT ResultsSimulationStar__UN UNIQUE ( ID_ResultSimulationStar , ID_Simulation ) ;

CREATE
  TABLE Simulation
  (
    ID_Simulation  INTEGER NOT NULL ,
    ID_SolarSystem INTEGER NOT NULL ,
    State         CHAR (1) ,
    Run            CHAR (1) ,
    Name           VARCHAR2 (25) ,
    StartTime       VARCHAR2 (50) ,
    TimeStep        VARCHAR2 (50) ,
    EndTime        VARCHAR2 (50)
  ) ;
ALTER TABLE Simulation ADD CONSTRAINT Simulation_PK PRIMARY KEY ( ID_Simulation
, ID_SolarSystem ) ;
ALTER TABLE Simulation ADD CONSTRAINT Simulation__UN UNIQUE ( ID_Simulation ) ;

CREATE
  TABLE SimulationCorps
  (
    ID_Simulation  INTEGER NOT NULL ,
    ID_SolarSystem INTEGER NOT NULL ,
    ID_Corps       INTEGER NOT NULL
  ) ;
ALTER TABLE SimulationCorps ADD CONSTRAINT Simulationcorps_PK PRIMARY KEY (
ID_Simulation, ID_SolarSystem, ID_Corps ) ;

CREATE
  TABLE SolarSystem
  (
    ID_SolarSystem INTEGER NOT NULL ,
    State         CHAR (1) ,
    Name           VARCHAR2 (25) ,
    ID_Star        INTEGER NOT NULL
  ) ;
ALTER TABLE SolarSystem ADD CONSTRAINT SolarSystem_PK PRIMARY KEY (
ID_SolarSystem ) ;

CREATE
  TABLE Star
  (
    ID_Star  INTEGER NOT NULL ,
    Name     VARCHAR2 (25) ,
    Mass     NUMBER ,
    Diameter NUMBER
  ) ;
ALTER TABLE Star ADD CONSTRAINT Star_PK PRIMARY KEY ( ID_Star ) ;

ALTER TABLE Corps ADD CONSTRAINT Corps_SolarSystem_FK FOREIGN KEY (
ID_SolarSystem ) REFERENCES SolarSystem ( ID_SolarSystem ) ON
DELETE CASCADE ;

ALTER TABLE GravitationalPropCorps ADD CONSTRAINT GraviPropCorps_GraviCorps_FK
FOREIGN KEY ( ID_Corps, ID_SolarSystem ) REFERENCES GravitationalCorps (
ID_Corps, ID_SolarSystem ) ON
DELETE CASCADE ;

ALTER TABLE GravitationalCorps ADD CONSTRAINT GravitationalCorps_Corps_FK
FOREIGN KEY ( ID_Corps, ID_SolarSystem ) REFERENCES Corps ( ID_Corps,
ID_SolarSystem ) ON
DELETE CASCADE ;

ALTER TABLE Orbit ADD CONSTRAINT Orbit_Corps_FK FOREIGN KEY ( ID_Corps_Center,
ID_SolarSystem ) REFERENCES Corps ( ID_Corps, ID_SolarSystem ) ON
DELETE CASCADE ;

ALTER TABLE OrbitalCorps ADD CONSTRAINT OrbitalCorps_Corps_FK FOREIGN KEY (
ID_Corps, ID_SolarSystem ) REFERENCES Corps ( ID_Corps, ID_SolarSystem ) ON
DELETE CASCADE ;

ALTER TABLE Orbit ADD CONSTRAINT Orbiting_OrbitalCorps_FK FOREIGN KEY (
ID_Corps_Orbit, ID_SolarSystem ) REFERENCES OrbitalCorps ( ID_Corps,
ID_SolarSystem ) ON
DELETE CASCADE ;

ALTER TABLE ResultsSimulationStar ADD CONSTRAINT ResultsSimulationStar_Simul_FK FOREIGN KEY (
ID_Simulation, ID_SolarSystem ) REFERENCES Simulation ( ID_Simulation, ID_SolarSystem ) ON
DELETE CASCADE ;

ALTER TABLE ResultsSimulationCorps ADD CONSTRAINT ResulSimulCorps_SimulCorps_FK
FOREIGN KEY ( ID_Simulation, ID_SolarSystem, ID_Corps ) REFERENCES
SimulationCorps ( ID_Simulation, ID_SolarSystem, ID_Corps ) ON
DELETE CASCADE ;

ALTER TABLE SimulationCorps ADD CONSTRAINT SimulationCorps_Corps_FK FOREIGN KEY
( ID_Corps, ID_SolarSystem ) REFERENCES Corps ( ID_Corps, ID_SolarSystem ) ON
DELETE CASCADE ;

ALTER TABLE SimulationCorps ADD CONSTRAINT SimulationCorps_Simulation_FK
FOREIGN KEY ( ID_Simulation, ID_SolarSystem ) REFERENCES Simulation (
ID_Simulation, ID_SolarSystem ) ON
DELETE CASCADE ;

ALTER TABLE Simulation ADD CONSTRAINT Simulation_SolarSystem_FK FOREIGN KEY (
ID_SolarSystem ) REFERENCES SolarSystem ( ID_SolarSystem ) ON
DELETE CASCADE ;

ALTER TABLE SolarSystem ADD CONSTRAINT SolarSystem_Star_FK FOREIGN KEY (
ID_Star ) REFERENCES Star ( ID_Star ) ;

CREATE SEQUENCE AUTO_ID_SIMULATION  START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE AUTO_ID_SOLARSYSTEM START WITH 1 INCREMENT BY 1;
CREATE SEQUENCE AUTO_ID_STAR START WITH 1 INCREMENT BY 1;

CREATE or REPLACE FUNCTION containsSolarSystem(id_solarsystem_p SolarSystem.ID_SolarSystem%type)
RETURN INTEGER IS
  ret INTEGER;
BEGIN
  SELECT COUNT(*) INTO ret
  FROM SolarSystem
  WHERE ID_SolarSystem=id_solarsystem_p;
  RETURN RET;
END;
/

CREATE or REPLACE FUNCTION deleteSolarSystem(id_solarsystem_p SolarSystem.ID_SolarSystem%type)
RETURN INTEGER IS
BEGIN
  BEGIN
    DELETE FROM SolarSystem WHERE ID_SolarSystem=id_solarsystem_p;
    RETURN 1;
  EXCEPTION WHEN OTHERS 
        THEN RETURN 0;
  END;
END;
/

CREATE or REPLACE PROCEDURE getSolarSystems(c1 out sys_refcursor) is
BEGIN
  OPEN c1 FOR
    SELECT ss.ID_SolarSystem, ss.State, ss.Name, ss.ID_Star
    FROM SolarSystem ss;
END;
/

CREATE or REPLACE PROCEDURE getSolarSystem(id_solarsystem_p SolarSystem.ID_SolarSystem%type,c1 out sys_refcursor) is
BEGIN
  OPEN c1 FOR
    SELECT ss.ID_SolarSystem, ss.State, ss.Name, ss.ID_Star
    FROM SolarSystem ss
    WHERE ss.ID_SolarSystem=id_solarsystem_p;
END;
/

CREATE or REPLACE PROCEDURE getStar(id_star_p Star.ID_Star%type,c1 out sys_refcursor) is
BEGIN
  OPEN c1 FOR
    SELECT s.ID_Star,s.Name, s.Mass, s.Diameter
    FROM Star s
    WHERE s.ID_Star=id_star_p;
END;
/

CREATE or REPLACE PROCEDURE getCorps(id_solarsystem_p SolarSystem.ID_SolarSystem%type,c1 out sys_refcursor) is
BEGIN
  OPEN c1 FOR
   SELECT c.ID_Corps, c.Name, c.Source, c.Mass, c.Diameter, c.Density, c.Coordinates, c.Speed, c.DataReference
    FROM Corps c
    WHERE  c.ID_SolarSystem=id_solarsystem_p
      AND c.ID_Corps NOT IN(SELECT oc.ID_Corps FROM OrbitalCorps oc WHERE oc.ID_Corps=c.ID_Corps AND oc.ID_SolarSystem=id_solarsystem_p)
      AND c.ID_Corps NOT IN(SELECT gc.ID_Corps FROM GravitationalCorps gc WHERE gc.ID_Corps=c.ID_Corps AND gc.ID_SolarSystem=id_solarsystem_p)
      AND c.ID_Corps NOT IN(SELECT gpc.ID_Corps FROM GravitationalPropCorps gpc WHERE gpc.ID_Corps=c.ID_Corps AND gpc.ID_SolarSystem=id_solarsystem_p);
END;
/

CREATE or REPLACE PROCEDURE getOrbitCorps(id_solarsystem_p SolarSystem.ID_SolarSystem%type,c1 out sys_refcursor) is
BEGIN
  OPEN c1 FOR
    SELECT c.ID_Corps, c.Name, c.Source, c.Mass, c.Diameter, c.Density, c.Coordinates, c.Speed, c.DataReference
          ,oc.SemiMajorAxis,oc.Period,oc.OrbitalSpeed,oc.InclinationAxisToOribit,oc.InclinationOrbit,oc.EccentricityOrbit,oc.EscapeVelocity,oc.Perihelion,oc.Aphelion,oc.MeanAnomaly,oc.LongitudePerihelion,oc.LongitudeAscendingNode
    FROM OrbitalCorps oc, Corps c
    WHERE oc.ID_Corps=c.ID_Corps
          AND oc.ID_SolarSystem=c.ID_SolarSystem
          AND oc.ID_SolarSystem=id_solarsystem_p;
END;
/

CREATE or REPLACE PROCEDURE getGravitationalCorps(id_solarsystem_p SolarSystem.ID_SolarSystem%type,c1 out sys_refcursor) is
BEGIN
  OPEN c1 FOR
    SELECT c.ID_Corps, c.Name, c.Source, c.Mass, c.Diameter, c.Density, c.Coordinates, c.Speed, c.DataReference
    FROM GravitationalCorps gc, Corps c
    WHERE 
        gc.ID_Corps=c.ID_Corps AND gc.ID_SolarSystem=c.ID_SolarSystem
        AND c.ID_Corps NOT IN(SELECT gpc.ID_Corps FROM GravitationalPropCorps gpc WHERE gpc.ID_Corps=c.ID_Corps AND gpc.ID_SolarSystem=id_solarsystem_p)
        AND c.ID_SolarSystem=id_solarsystem_p;
END;
/

CREATE or REPLACE PROCEDURE getGravitationalPropCorps(id_solarsystem_p SolarSystem.ID_SolarSystem%type,c1 out sys_refcursor) is
BEGIN
  OPEN c1 FOR
   SELECT c.ID_Corps, c.Name, c.Source, c.Mass, c.Diameter, c.Density, c.Coordinates, c.Speed, c.DataReference
          ,gpc.Propulsion
   FROM GravitationalPropCorps gpc, GravitationalCorps gc,Corps c
   WHERE gpc.ID_Corps=gc.ID_Corps AND gpc.ID_Corps=c.ID_Corps
        AND gpc.ID_SolarSystem=gc.ID_SolarSystem AND gc.ID_SolarSystem=c.ID_SolarSystem
   AND c.ID_SolarSystem = id_solarsystem_p;
END;
/

CREATE or REPLACE PROCEDURE getOrbit(id_solarsystem_p SolarSystem.ID_SolarSystem%type,c1 out sys_refcursor) is
BEGIN
  OPEN c1 FOR
   SELECT o.ID_Corps_Center, o.ID_Corps_Orbit, o.ID_SolarSystem
   FROM Orbit o
   WHERE o.ID_SolarSystem = id_solarsystem_p;
END;
/

CREATE or REPLACE FUNCTION insertStar(id_star_p Star.ID_Star%type,name_p Star.Name%type,mass_p Star.Mass%type, diameter_p Star.Diameter%type)
RETURN Star.ID_Star%type IS
BEGIN
 INSERT INTO Star(ID_Star, Name, Mass, Diameter) VALUES (id_star_p,name_p,mass_p,diameter_p);
 RETURN id_star_p;
END;
/

CREATE or REPLACE FUNCTION insertSolarSystem(id_solarsystem_p SolarSystem.ID_SolarSystem%type,state_p SolarSystem.State%type,name_p SolarSystem.Name%type,id_star_p SolarSystem.ID_Star%type)
RETURN SolarSystem.ID_SolarSystem%type IS
BEGIN
  INSERT INTO SolarSystem(ID_SolarSystem,State, Name, ID_Star) VALUES (id_solarsystem_p,state_p,name_p,id_star_p);
 RETURN id_solarsystem_p;
END;
/

CREATE or REPLACE FUNCTION insertCorps(id_corps_p Corps.ID_Corps%type, id_solarsystem_p Corps.ID_SolarSystem%type, name_p Corps.Name%type, source_p Corps.Source%type, mass_p Corps.Mass%type, diameter_p Corps.Diameter%type, density_p Corps.Density%type, coordinates Corps.Coordinates%type, speed_p Corps.Speed%type, datareference_p Corps.DataReference%type)
RETURN Corps.ID_Corps%type IS
BEGIN
  INSERT INTO Corps(ID_Corps, ID_SolarSystem, Name, Source, Mass, Diameter, Density, Coordinates, Speed, DataReference) VALUES (id_corps_p, id_solarsystem_p, name_p, source_p, mass_p, diameter_p, density_p, coordinates, speed_p, datareference_p); 
 RETURN id_corps_p;
END;
/

CREATE or REPLACE FUNCTION insertGravitationalCorps(id_corps_p GravitationalCorps.ID_Corps%type, id_solarsystem_p GravitationalCorps.ID_SolarSystem%type)
RETURN GravitationalCorps.ID_Corps%type IS
BEGIN
  INSERT INTO GravitationalCorps(ID_Corps, ID_SolarSystem) VALUES (id_corps_p, id_solarsystem_p);  
 RETURN id_corps_p;
END;
/

CREATE or REPLACE FUNCTION insertGravitationalPropCorps(id_corps_p GravitationalPropCorps.ID_Corps%type, id_solarsystem_p GravitationalPropCorps.ID_SolarSystem%type, propulsion_p GravitationalPropCorps.Propulsion%type)
RETURN GravitationalPropCorps.ID_Corps%type IS
BEGIN
  INSERT INTO GravitationalPropCorps(ID_Corps, ID_SolarSystem, Propulsion) VALUES (id_corps_p, id_solarsystem_p, propulsion_p);  
 RETURN id_corps_p;
END;
/

CREATE or REPLACE FUNCTION insertOrbitalCorps(id_corps_p OrbitalCorps.ID_Corps%type,id_solarsystem_p OrbitalCorps.ID_SolarSystem%type, semimajoraxis_p OrbitalCorps.SemiMajorAxis%type,period_p OrbitalCorps.Period%type,orbitalspeed_p OrbitalCorps.OrbitalSpeed%type,inclinationaxistooribit_p OrbitalCorps.InclinationAxisToOribit%type,inclinationorbit_p OrbitalCorps.InclinationOrbit%type,eccentricityorbit_p OrbitalCorps.EccentricityOrbit%type,escapevelocity_p OrbitalCorps.EscapeVelocity%type,Perihelion OrbitalCorps.Perihelion%type,aphelion_p OrbitalCorps.Aphelion%type,meananomaly_p OrbitalCorps.MeanAnomaly%type,longitudeperihelion_p OrbitalCorps.LongitudePerihelion%type,longitudeascendingnode_p OrbitalCorps.LongitudeAscendingNode%type)
RETURN OrbitalCorps.ID_Corps%type IS
BEGIN
  INSERT INTO OrbitalCorps(ID_Corps,ID_SolarSystem, SemiMajorAxis,Period,OrbitalSpeed,InclinationAxisToOribit,InclinationOrbit,EccentricityOrbit,EscapeVelocity,Perihelion,Aphelion,MeanAnomaly,LongitudePerihelion,LongitudeAscendingNode)
  VALUES(id_corps_p,id_solarsystem_p, semimajoraxis_p,period_p,orbitalspeed_p,inclinationaxistooribit_p,inclinationorbit_p,eccentricityorbit_p,escapevelocity_p,Perihelion,aphelion_p,meananomaly_p,longitudeperihelion_p,longitudeascendingnode_p);
 RETURN id_corps_p;
END;
/

CREATE or REPLACE FUNCTION insertOrbit(id_corps_center_p Orbit.ID_Corps_Center%type,id_solarsystem_p Orbit.ID_SolarSystem%type, id_corps_orbit_p Orbit.ID_Corps_Center%type)
RETURN OrbitalCorps.ID_Corps%type IS
BEGIN
  INSERT INTO Orbit(ID_Corps_Center, ID_SolarSystem, ID_Corps_Orbit) VALUES (id_corps_center_p,id_solarsystem_p,id_corps_orbit_p);
 RETURN id_corps_orbit_p;
END;
/

CREATE or REPLACE FUNCTION updateStateSolarSystem(id_solarsystem_p SolarSystem.ID_SolarSystem%type,state_p SolarSystem.State%type)
RETURN SolarSystem.State%type IS
BEGIN
  UPDATE SolarSystem
    SET State=state_p
    WHERE ID_SolarSystem=id_solarsystem_p; 
    RETURN state_p;
END;
/

CREATE or REPLACE FUNCTION containsSimulation(id_simulation_p Simulation.ID_Simulation%type)
RETURN INTEGER IS
  ret INTEGER;
BEGIN
  SELECT COUNT(*) INTO ret
  FROM Simulation
  WHERE ID_Simulation=id_simulation_p;
  RETURN RET;
END;
/

CREATE or REPLACE FUNCTION containsSimulationByIDSSystem(id_solarsystem_p SolarSystem.ID_SolarSystem%type)
RETURN INTEGER IS
  ret INTEGER;
BEGIN
  SELECT COUNT(*) INTO ret
  FROM Simulation
  WHERE ID_SolarSystem=id_solarsystem_p;
  RETURN RET;
END;
/

CREATE or REPLACE PROCEDURE getSimulations(c1 out sys_refcursor) is
BEGIN
  OPEN c1 FOR
    SELECT s.ID_Simulation,s.ID_SolarSystem, s.State, s.Run, s.Name , s.StartTime, s.TimeStep,s.EndTime
    FROM Simulation s;
END;
/

CREATE or REPLACE PROCEDURE getSimulation(id_simulation_p Simulation.ID_Simulation%type,c1 out sys_refcursor) is
BEGIN
  OPEN c1 FOR
    SELECT s.ID_Simulation,s.ID_SolarSystem, s.State, s.Run, s.Name , s.StartTime, s.TimeStep,s.EndTime
    FROM Simulation s
    WHERE s.ID_Simulation=id_simulation_p;
END;
/

CREATE or REPLACE PROCEDURE getSimulationCorps(id_simulation_p Simulation.ID_Simulation%type,c1 out sys_refcursor) is
BEGIN
  OPEN c1 FOR
    SELECT sc.ID_Simulation, sc.ID_SolarSystem, sc.ID_Corps
    FROM SimulationCorps sc
    WHERE sc.ID_Simulation=id_simulation_p;
END;
/

CREATE or REPLACE PROCEDURE getResultsSimulationCorps(id_simulation_p ResultsSimulationCorps.ID_Simulation%type,c1 out sys_refcursor) is
BEGIN
  OPEN c1 FOR
    SELECT rsc.ID_ResultSimulationCorps,rsc.ID_Simulation,rsc.ID_SolarSystem, rsc.ID_Corps, rsc.TIME, rsc.Coordinates
    FROM ResultsSimulationCorps rsc
    WHERE rsc.ID_Simulation=id_simulation_p
    ORDER BY rsc.ID_ResultSimulationCorps ASC;
END;
/

CREATE or REPLACE PROCEDURE getResultsSimulationCorpsPos(id_simulation_p ResultsSimulationCorps.ID_Simulation%type,id_resultsimulationcorps_p ResultsSimulationCorps.ID_ResultSimulationCorps%type,c1 out sys_refcursor) is
BEGIN
  OPEN c1 FOR
    SELECT rsc.ID_ResultSimulationCorps,rsc.ID_Simulation,rsc.ID_SolarSystem, rsc.ID_Corps, rsc.TIME, rsc.Coordinates
    FROM ResultsSimulationCorps rsc
    WHERE rsc.ID_Simulation=id_simulation_p
          and rsc.ID_ResultSimulationCorps=id_resultsimulationcorps_p;
END;
/

CREATE or REPLACE PROCEDURE getResultsSimulationStar(id_simulation_p ResultsSimulationStar.ID_Simulation%type,c1 out sys_refcursor) is
BEGIN
  OPEN c1 FOR
    SELECT rss.ID_ResultSimulationStar,rss.ID_Simulation,rss.ID_SolarSystem, rss.Coordinates
    FROM ResultsSimulationStar rss
    WHERE rss.ID_Simulation=id_simulation_p
    ORDER BY rss.ID_ResultSimulationStar ASC;
END;
/

CREATE or REPLACE FUNCTION deleteSimulation(id_simulation_p Simulation.ID_Simulation%type)
RETURN INTEGER IS
BEGIN
  BEGIN
    DELETE FROM Simulation WHERE ID_Simulation=id_simulation_p;
    RETURN 1;
  EXCEPTION WHEN OTHERS 
        THEN RETURN 0;
  END;
END;
/

CREATE or REPLACE FUNCTION insertSimulation(id_simulation_p Simulation.ID_Simulation%type, id_solarsystem_p Simulation.ID_SolarSystem%type, state_p Simulation.State%type, run_p  Simulation.Run%type, name_p  Simulation.Name%type, starttime_p Simulation.StartTime%type, timestep_p Simulation.TimeStep%type,endtime_p Simulation.EndTime%type )
RETURN Simulation.ID_Simulation%type IS
BEGIN
  INSERT INTO Simulation(ID_Simulation,ID_SolarSystem, State, Run, Name, StartTime, TimeStep,EndTime)
   VALUES (id_simulation_p,id_solarsystem_p,state_p,run_p,name_p,starttime_p,timestep_p,endtime_p);
 RETURN id_simulation_p;
END;
/

CREATE or REPLACE FUNCTION insertSimulationCorps(id_simulation_p SimulationCorps.ID_Simulation%type, id_solarsystem_p SimulationCorps.ID_SolarSystem%type, id_corps_p SimulationCorps.ID_Corps%type)
RETURN Simulation.ID_Simulation%type IS
BEGIN
  INSERT INTO SimulationCorps(ID_Simulation,ID_SolarSystem, ID_Corps)
   VALUES (id_simulation_p,id_solarsystem_p,id_corps_p);
 RETURN id_simulation_p;
END;
/

CREATE or REPLACE FUNCTION insertResultsSimulationCorps(id_resultsimulationcorps_p ResultsSimulationCorps.ID_ResultSimulationCorps%type, id_simulation_p ResultsSimulationCorps.ID_Simulation%type,id_solarsystem_p ResultsSimulationCorps.ID_SolarSystem%type,id_corps_p ResultsSimulationCorps.ID_Corps%type,time_p ResultsSimulationCorps.TIME%type,coordinates_p ResultsSimulationCorps.Coordinates%type)
RETURN ResultsSimulationCorps.ID_ResultSimulationCorps%type IS
BEGIN
  INSERT INTO ResultsSimulationCorps(ID_ResultSimulationCorps,ID_Simulation, ID_SolarSystem,ID_Corps,TIME,Coordinates)
   VALUES (id_resultsimulationcorps_p,id_simulation_p,id_solarsystem_p,id_corps_p,time_p,coordinates_p);
 RETURN id_resultsimulationcorps_p;
END;
/

CREATE or REPLACE FUNCTION insertResultsSimulationStar(id_resultsimulationstar_p ResultsSimulationStar.ID_ResultSimulationStar%type, id_simulation_p ResultsSimulationStar.ID_Simulation%type,id_solarsystem_p ResultsSimulationStar.ID_SolarSystem%type,coordinates_p ResultsSimulationStar.Coordinates%type)
RETURN ResultsSimulationStar.ID_ResultSimulationStar%type IS
BEGIN
  INSERT INTO ResultsSimulationStar(ID_ResultSimulationStar,ID_Simulation, ID_SolarSystem,Coordinates)
   VALUES (id_resultsimulationstar_p,id_simulation_p,id_solarsystem_p,coordinates_p);
 RETURN id_resultsimulationstar_p;
END;
/

CREATE or REPLACE FUNCTION updateStateSimulation(id_simulation_p Simulation.ID_Simulation%type,state_p Simulation.State%type)
RETURN Simulation.State%type IS
BEGIN
  UPDATE Simulation
    SET State=state_p
    WHERE ID_Simulation=id_simulation_p; 
    RETURN state_p;
END;
/

CREATE or REPLACE FUNCTION loadAPP
RETURN INTEGER IS
BEGIN
  BEGIN
   
    RETURN 1;
  EXCEPTION WHEN OTHERS 
        THEN RETURN 0;
  END;
END;
/

CREATE or REPLACE FUNCTION saveAPP
RETURN INTEGER IS
BEGIN
  BEGIN
    DELETE FROM Simulation WHERE State=0;
    DELETE FROM SolarSystem ss WHERE ss.State=0 and ss.ID_SolarSystem NOT IN(SELECT s.ID_SolarSystem FROM Simulation s WHERE s.ID_SolarSystem=ss.ID_SolarSystem);
    RETURN 1;
  EXCEPTION WHEN OTHERS 
        THEN RETURN 0;
  END;
END;
/

create or replace FUNCTION updateSimulationRun(id_simulation_p Simulation.ID_Simulation%type,run_p Simulation.State%type)
RETURN Simulation.State%type IS
BEGIN
  UPDATE Simulation
    SET Run=run_p
    WHERE ID_Simulation=id_simulation_p; 
    RETURN run_p;
END;
/

COMMIT;