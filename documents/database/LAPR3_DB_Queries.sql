
-- State: 
--  0: criado sistema solar
--  1: sistema com corpos associados e pronto a ser utilizado
--  2: inactivo e com backup de valores para simulações ocorridas
create table solarsystem(
  idsolarsystem numeric(5) constraint pk_solarsystem primary key,
  name varchar2(100) not null,
  mass binary_double not null,
  equatorialDiameter binary_double not null,
  state numeric(1) not null
);

alter table solarsystem add constraint ck_solarsystem_state check(state between 1 and 3);
create sequence seq_solarsystem start with 1 increment by 1;

create table orbitalcorps(
  idorbital numeric(5) constraint pk_orbitalcorps primary key,
  idsolarsystem numeric(5) constraint fk_orbitalcorps_idsolarsystem references solarsystem(idsolarsystem) not null,
  revolutionPeriod binary_double not null,
  orbitalSpeed binary_double not null,
  inclinationAxisOrbit BINARY_DOUBLE not null,
  equatorialDiameter BINARY_DOUBLE not null,
  mass BINARY_DOUBLE not null,
  density BINARY_DOUBLE not null,
  escapeVelocity binary_double not null,
  semimajorAxis binary_double not null,
  orbitEccentricity binary_double not null,
  orbitInclination binary_double not null,
  perihelion binary_double not null,
  aphelion binary_double not null,
  px binary_double not null,
  py binary_double not null,
  pz binary_double not null,
  vx binary_double not null,
  vy binary_double not null,
  vz binary_double not null,
  refdate date not null
);

create sequence seq_orbitalcorps start with 1 increment by 1;

