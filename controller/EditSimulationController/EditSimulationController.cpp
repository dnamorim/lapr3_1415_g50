#include "EditSimulationController.h"

EditSimulationController::EditSimulationController():Controller(),tempSimulation(NULL) {
}

EditSimulationController::EditSimulationController(const EditSimulationController& copy):Controller(copy) {
}

EditSimulationController::~EditSimulationController() {
    if(!this->dataLayer->autoDestroy() && this->tempSimulation!=NULL){
        delete this->tempSimulation;
    }
}

map<int, string> EditSimulationController::getSimulations() {
    return this->dataLayer->getSimulationsNotRun();
}

bool EditSimulationController::loadSimulation(const int& id) {
    return (this->tempSimulation=this->dataLayer->loadSimulation(id))!=NULL;
}

Simulation& EditSimulationController::getSimulation(){
    return *this->tempSimulation;
}

bool EditSimulationController::save() {
    if ((this->tempSimulation=this->dataLayer->saveSimulation(*this->tempSimulation))!=NULL) {
        this->success = true;
    }
    return this->success;
}