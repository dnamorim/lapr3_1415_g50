#ifndef EDITSIMULATIONCONTROLLER_H
#define	EDITSIMULATIONCONTROLLER_H

/**
 * Editar Simulação Controller - UC7
 * @file EditSimulationController.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 11 de Janeiro de 2015, 17:10 por Gilberto Pereira <1130489@isep.ipp.pt>
 *  -Estrutura Basica do Controller
 */

#include <iostream>
#include <string>
using namespace std;

#include "../Controller.h"
#include "../../model/Simulation/Simulation.h"

class EditSimulationController:public Controller {
private:
    Simulation* tempSimulation;
public:
    /**
     * Construir Instáncia de EditSimulationController(COMPLETO)
     */
    EditSimulationController();
    
    /**
     * Construir Instáncia de EditSimulationController(CÓPIA)
     * @param copy Instáncia de EditSimulationController a Cópiar
     */
    EditSimulationController(const EditSimulationController& copy);
    
    /**
     * Destrotor da Instáncia de EditSolarSystemController
     */
    virtual ~EditSimulationController();
    
    /**
     * Consultar Lista de Simulações
     * @return Lista de Simulações
     */
    map<int, string> getSimulations();
    
    /**
     * Carregar Simulação
     * @param id ID da Simulação
     * @return Sucesso da Operação
     */
    bool loadSimulation(const int& id);
    
    /**
     * Consultar Simulação Activa
     * @return Simulação
     */
    Simulation& getSimulation();
    
    /**
     * Guardar Simulação Editada
     * @return Sucesso da Operação
     */
    bool save();

};

#endif	/* EDITSIMULATIONCONTROLLER_H */

