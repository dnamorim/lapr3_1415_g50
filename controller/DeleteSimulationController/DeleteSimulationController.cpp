#include "DeleteSimulationController.h"

DeleteSimulationController::DeleteSimulationController() : Controller(),tempSimulation(NULL){
}

DeleteSimulationController::DeleteSimulationController(const DeleteSimulationController& copy) : Controller(copy), tempSimulation(copy.tempSimulation) {
}

DeleteSimulationController::~DeleteSimulationController() {
    if(!this->dataLayer->autoDestroy() && this->tempSimulation!=NULL){
        delete this->tempSimulation;
    }
}

map<int, string> DeleteSimulationController::getSimulations() {
    return this->dataLayer->getSimulationsActive();
}

bool DeleteSimulationController::loadSimulation(const int& id) {
    return (this->tempSimulation=this->dataLayer->loadSimulation(id))!=NULL;
}

Simulation& DeleteSimulationController::getSimulation() {
    return *this->tempSimulation;
}

bool DeleteSimulationController::del() {
    if (this->dataLayer->deleteSimulation(this->tempSimulation->getID())) {
        this->success = true;
    }
    return this->success;
}

