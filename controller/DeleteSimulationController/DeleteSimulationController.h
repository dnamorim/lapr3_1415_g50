#ifndef DELETESIMULATIONCONTROLLER_H
#define	DELETESIMULATIONCONTROLLER_H
/**
 * Apagar Simulação Controller - UC8
 * @file DeleteSimulationController.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 9 de Janeiro de 2015, 15:45 por Ricardo Rodrigues <1120703@isep.ipp.pt>
 *  -Estrutura Basica do Controller
 */

#include <iostream>
#include <string>
#include <map>

using namespace std;

#include "../Controller.h"
#include "../../model/Simulation/Simulation.h"

class DeleteSimulationController: public Controller {
private:
    /* Simulador Temporario */
    Simulation* tempSimulation;
public:
    /**
     * Construir Instância de DeleteSimulationController(COMPLETO)
     */
    DeleteSimulationController();
    
    /**
     * Construir Instância de DeleteSimulationController(CÓPIA)
     * @param copy Instância de DeleteSimulationController a Cópiar
     */
    DeleteSimulationController(const DeleteSimulationController& copy);
    
    /**
     * Destrotor da Instância de DeleteSimulationController
     */
    virtual ~DeleteSimulationController();
    
    /**
     * Consultar Lista de Simuladores
     * @return Lista de Simuladores
     */
    map<int, string> getSimulations();
    
    /**
     * Carregar Simulação
     * @param id ID da Simulação
     * @return Sucesso da Operação
     */
    bool loadSimulation(const int& id);
    
    /**
     * Consultar Simulação
     * @return Simulação
     */
    Simulation& getSimulation();

    /**
     * Apagar Simulação
     * @return Sucesso da Operação
     */
    bool del();
    
};

#endif	/* DELETESIMULATIONCONTROLLER_H */

