#include "CreateSolarSystemController.h"

CreateSolarSystemController::CreateSolarSystemController() : Controller(),tempSolarSystem(NULL) {
    this->tempSolarSystem=new SolarSystem();
}

CreateSolarSystemController::CreateSolarSystemController(const CreateSolarSystemController& copy) : Controller(copy), tempSolarSystem(copy.tempSolarSystem) {
}

CreateSolarSystemController::~CreateSolarSystemController() {
    if(!this->dataLayer->autoDestroy() && !this->success && this->tempSolarSystem!=NULL){
        delete this->tempSolarSystem;
    }
}

string CreateSolarSystemController::getNameSolarSystem() {
    return this->tempSolarSystem->getName();
}

void CreateSolarSystemController::setNameSolarSystem(string name) {
    this->tempSolarSystem->setName(name);
}

SolarSystem& CreateSolarSystemController::getSolarSystem() {
    return *this->tempSolarSystem;
}

Star& CreateSolarSystemController::getStar() {
    return this->tempSolarSystem->getStar();
}

bool CreateSolarSystemController::importPlanets(string file) {
    vector<OrbitalCorps*> vecPlanets;
    if (Import::importPlanets(file, vecPlanets)) {
        for (vector<OrbitalCorps*>::iterator it = vecPlanets.begin(); it != vecPlanets.end(); ++it) {
            this->tempSolarSystem->addBody((Corps&)**it);
            delete *it;
        }
        return true;
    }
    return false;
}

bool CreateSolarSystemController::save() {
    if ((this->tempSolarSystem=this->dataLayer->saveSolarSystem(*this->tempSolarSystem))!=NULL) {
        this->solarSystem = this->tempSolarSystem;
        this->success = true;
    }
    return this->success;
}