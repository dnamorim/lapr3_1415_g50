#ifndef CREATESOLARSYSTEMCONTROLLER_H
#define CREATESOLARSYSTEMCONTROLLER_H
/**
 * Criar Sistema Solar Controller - UC2
 * @file CreateSolarSystemController.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 18 de Dezembro de 2014, 01:45 por Duarte Amorim <1130674@isep.ipp.pt>
 *  -Estrutura Basica do Controller
 * 
 * >Modificado a 7 de Janeiro de 2014, 22:10 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Sistema Solar Temporario
 */

#include <iostream>
#include <string>

using namespace std;

#include "../Controller.h"
#include "../../model/SolarSystem/SolarSystem.h"
#include "../../model/Star/Star.h"
#include "../../model/Import/Import.h"

class CreateSolarSystemController : public Controller {
private:
    /* Sistema Solar Temporario para Importar */
    SolarSystem* tempSolarSystem;
public:

    /**
     * Construir Instância de CreateSolarSystemController(COMPLETO)
     */
    CreateSolarSystemController();

    /**
     * Construir Instância de CreateSolarSystemController(CÓPIA)
     * @param copy Instância de CreateSolarSystemController a Cópiar
     */
    CreateSolarSystemController(const CreateSolarSystemController& copy);

    /**
     * Destrotor da Instância de CreateSolarSystemController
     */
    virtual ~CreateSolarSystemController();

    /**
     * Consultar Nome do Sistema Solar
     * @return Nome
     */
    string getNameSolarSystem();

    /**
     * Alterar Nome do Sistema Solar
     * @param name
     */
    void setNameSolarSystem(string name);

    /**
     * Consultar Sistema Solar
     * @return Sistema Solar
     */
    SolarSystem& getSolarSystem();

    /**
     * Consultar Estrela do Sistema Solar
     * @return Estrela
     */
    Star& getStar();

    /**
     * Importar Planetas no Sistema Solar
     * @param file path/nome do ficheiro
     * @return Sucesso da Operação
     */
    bool importPlanets(string file);

    /**
     * Guardar Sistema Solar
     * @return Sucesso da Operação
     */
    bool save();
};

#endif /* CREATESOLARSYSTEMCONTROLLER_H */
