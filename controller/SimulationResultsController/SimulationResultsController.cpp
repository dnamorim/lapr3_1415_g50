#include "SimulationResultsController.h"

SimulationResultsController::SimulationResultsController():Controller(),tempSimulation(NULL) {
}

SimulationResultsController::SimulationResultsController(const SimulationResultsController& copy):Controller(copy),tempSimulation(copy.tempSimulation) {
}

SimulationResultsController::~SimulationResultsController() {
    if(!this->dataLayer->autoDestroy() && this->tempSimulation!=NULL){
        delete this->tempSimulation;
    }
}

map<int, string> SimulationResultsController::getSimulations(){
    return this->dataLayer->getSimulationsRun();
}

bool SimulationResultsController::loadSimulation(const int& id) {
    return (this->tempSimulation=this->dataLayer->loadSimulation(id))!=NULL;
}

Simulation& SimulationResultsController::getSimulation(){
    return *this->tempSimulation;
}

bool SimulationResultsController::expor(string file){
    return Export::exportSimulation(file,*this->tempSimulation);
}