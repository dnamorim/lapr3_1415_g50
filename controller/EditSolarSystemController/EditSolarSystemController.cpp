#include "EditSolarSystemController.h"

EditSolarSystemController::EditSolarSystemController():Controller(),tempSolarSystem(NULL) {
    if(this->canRun()){
        this->tempSolarSystem=new SolarSystem(*this->solarSystem);
    }
}

EditSolarSystemController::EditSolarSystemController(const EditSolarSystemController& copy):Controller(copy) {
}

EditSolarSystemController::~EditSolarSystemController() {
    if(!this->dataLayer->autoDestroy() && !this->success && this->tempSolarSystem!=NULL){
        delete this->tempSolarSystem;
    }
}

bool EditSolarSystemController::canRun() const{
    return Controller::canRun() && this->solarSystem!=NULL;
}

SolarSystem& EditSolarSystemController::getSolarSystem() {
    return *this->tempSolarSystem;
}

bool EditSolarSystemController::save() {
    if ((this->tempSolarSystem=this->dataLayer->saveSolarSystem(*this->tempSolarSystem))!=NULL) {
        this->solarSystem = this->tempSolarSystem;
        this->success = true;
    }
    return this->success;
}