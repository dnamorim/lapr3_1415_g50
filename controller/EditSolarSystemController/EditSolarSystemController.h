#ifndef EDITSOLARSYSTEMCONTROLLER_H
#define	EDITSOLARSYSTEMCONTROLLER_H

/**
 * Editar Sistema Solar Controller - UC4
 * @file EditSolarSystemController.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 11 de Janeiro de 2015, 15:10 por Gilberto Pereira <1130489@isep.ipp.pt>
 *  -Estrutura Basica do Controller
 */

#include <iostream>
#include <string>
using namespace std;

#include "../Controller.h"
#include "../../model/SolarSystem/SolarSystem.h"

class EditSolarSystemController : public Controller {
private:
    SolarSystem* tempSolarSystem;
public:
    
    /**
     * Construir Instáncia de EditSolarSystemController(COMPLETO)
     */
    EditSolarSystemController();
    
    /**
     * Construir Instáncia de EditSolarSystemController(CÓPIA)
     * @param copy Instáncia de EditSolarSystemController a Cópiar
     */
    EditSolarSystemController(const EditSolarSystemController& copy);
    
    /**
     * Destrotor da Instáncia de EditSolarSystemController
     */
    virtual ~EditSolarSystemController();
    
    /**
     * Verificar se é Possivel Correr Controller
     * @return true se for possivel correr Controller, caso contrário retorna false
     */
    virtual bool canRun() const;
    
    /**
     * Carregar Sistema Solar
     * @param id ID do Sistema Solar
     * @return Sucesso da Operação
     */
    bool loadSolarSystem(const int& id);
    
    /**
     * Consultar Sistema Solar
     * @return Sistema Solar
     */
    SolarSystem& getSolarSystem();
    
    /**
     * Atribuir Sistema Solar Selecionado ao Sistema Solar Activo
     * @return Sucesso da Operação
     */
    bool save();

};

#endif	/* EDITSOLARSYSTEMCONTROLLER_H */

