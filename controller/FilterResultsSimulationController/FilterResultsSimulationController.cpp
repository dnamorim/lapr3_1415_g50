#include "FilterResultsSimulationController.h"

FilterResultsSimulationController::FilterResultsSimulationController():Controller(),tempSimulation(NULL) {
}

FilterResultsSimulationController::FilterResultsSimulationController(const FilterResultsSimulationController& copy):Controller(copy),tempSimulation(copy.tempSimulation)  {
}

FilterResultsSimulationController::~FilterResultsSimulationController() {
    if(!this->dataLayer->autoDestroy() && this->tempSimulation!=NULL){
        delete this->tempSimulation;
    }
}

map<int, string> FilterResultsSimulationController::getSimulations(){
    return this->dataLayer->getSimulationsRun();
}

bool FilterResultsSimulationController::loadSimulation(const int& id) {
    return (this->tempSimulation=this->dataLayer->loadSimulation(id))!=NULL;
}

Simulation& FilterResultsSimulationController::getSimulation(){
    return *this->tempSimulation;
}

bool FilterResultsSimulationController::config(Timer& startTime,Timer& endTime,Timer& timeStep,vector<int>& addCorps){
    if(startTime<this->tempSimulation->getStartTime() || this->tempSimulation->getEndTime()>endTime){
        return false;
    }
    vector<Corps*> corps_add;
    int pos=0;
    vector<int>::iterator it_f;
    vector<Corps*> corps_sim = this->tempSimulation->getCorps();
    for(int i=0;i<corps_sim.size();i++){
        it_f = find (addCorps.begin(), addCorps.end(), pos);
        if(it_f!=addCorps.end()){
            corps_add.push_back(corps_sim[i]);
        }
    }
    vector<vector<XYZ>> position_add;
    vector<Timer> times_add;
    vector<Timer> time =this->tempSimulation->getSteps();
    this->tempSimulation->setCorps(corps_add);
    for (Timer steps = startTime; steps < endTime; steps+=timeStep) {
        for(int i=0;i<times_add.size();i++){
            if(times_add[i]==steps){
                times_add.push_back(steps);
                vector<XYZ> temp_add_position;
                vector<XYZ> temp_position=this->tempSimulation->getPositions(i);
                temp_add_position.push_back(temp_position[0]);
                for(int j=1;j<temp_position.size();j++){
                    it_f = find (addCorps.begin(), addCorps.end(), j-1);
                    if(it_f!=addCorps.end()){
                        temp_add_position.push_back(temp_position[j]);
                    }
                }
                position_add.push_back(temp_add_position);
            }

        }
    }
    if(times_add.size()<=0){
        return false;
    }
    this->tempSimulation->setCorps(corps_add);
    this->tempSimulation->setPositions(position_add);
    this->tempSimulation->setSteps(times_add);
    this->tempSimulation->setstartTime(times_add[0]);
    this->tempSimulation->setEndTime(times_add[times_add.size()-1]);
    this->tempSimulation->setTimeStep(timeStep);
    return true;
}

bool FilterResultsSimulationController::expor(string file){
    return Export::exportSimulation(file,*this->tempSimulation);
}