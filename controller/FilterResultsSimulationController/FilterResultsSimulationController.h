#ifndef FILTERRESULTSSIMULATIONCONTROLLER_H
#define	FILTERRESULTSSIMULATIONCONTROLLER_H
/**
 * Aprecentar e Filtrar Resultados da Simulação Controller - UC11
 * @file FilterResultsSimulationController.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 9 de Janeiro de 2015, 01:45 por Ricardo Rodrigues <1120703@isep.ipp.pt>
 *  -Estrutura Basica do Controller
 *  -Sistema Solar Temporario
 */

#include <iostream>
#include <string>
#include <map>

using namespace std;

#include "../Controller.h"
#include "../../model/Simulation/Simulation.h"
#include "../../model/Export/Export.h"

class FilterResultsSimulationController: public Controller {
private:
    Simulation* tempSimulation;
public:
    FilterResultsSimulationController();
    FilterResultsSimulationController(const FilterResultsSimulationController& copy);
    virtual ~FilterResultsSimulationController();
    
    /**
     * Consultar Lista de Simulações
     * @return Lista de Simulações
     */
    map<int, string> getSimulations();
    
    /**
     * Carregar Simulação
     * @param id ID do Simulação
     * @return Sucesso da Operação
     */
    bool loadSimulation(const int& id);
    
    /**
     * Consultar Sistema Solar
     * @return Sistema Solar
     */
    Simulation& getSimulation();
    
    bool config(Timer& startTime,Timer& endTime,Timer& timeStep,vector<int>& addCorps);
    
    /**
     * Exportar Simulação
     * @param file Ficheiro para Exportar
     */
    bool expor(string file);
};

#endif	/* FILTERRESULTSSIMULATIONCONTROLLER_H */

