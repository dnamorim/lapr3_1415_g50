#include "RunSimulationController.h"

RunSimulationController::RunSimulationController():Controller(),tempSimulation(NULL) {
    
}

RunSimulationController::RunSimulationController(const RunSimulationController& copy): Controller(copy) {
}

RunSimulationController::~RunSimulationController() {
    if(!this->dataLayer->autoDestroy() && this->tempSimulation!=NULL){
        delete this->tempSimulation;
    }
}

map<int, string> RunSimulationController::getSimulations() {
    return this->dataLayer->getSimulationsNotRun();
}

bool RunSimulationController::loadSimulation(const int& id) {
    return (this->tempSimulation=this->dataLayer->loadSimulation(id))!=NULL;
}

Simulation& RunSimulationController::getSimulation(){
    return *this->tempSimulation;
}

bool RunSimulationController::run(){
    return this->tempSimulation->run();
}

bool RunSimulationController::save() {
    if ((this->tempSimulation=this->dataLayer->saveSimulationResults(*this->tempSimulation))!=NULL) {
        this->success = true;
    }
    return this->success;
}

