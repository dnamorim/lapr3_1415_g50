#ifndef RUNSIMULATIONCONTROLLER_H
#define	RUNSIMULATIONCONTROLLER_H
/**
 * Correr Simulação Controller - UC9
 * @file RunSimulationController.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 10 de Janeiro de 2015, 01:45 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Estrutura Basica do Controller
 *  -Simulação Temporaria
 */

#include <iostream>

using namespace std;

#include "../Controller.h"
#include "../../model/Simulation/Simulation.h"

class RunSimulationController: public Controller {
private:
    Simulation* tempSimulation;
public:
    /**
     * Construir Instância de RunSimulationController(COMPLETO)
     */
    RunSimulationController();
    
    /**
     * Construir Instância de RunSimulationController(CÓPIA)
     * @param copy Instância de RunSimulationController a Cópiar
     */
    RunSimulationController(const RunSimulationController& copy);
    
    /**
     * Destrotor da Instância de RunSimulationController
     */
    virtual ~RunSimulationController();
    
    /**
     * Consultar Lista de Simulações
     * @return Lista de Simulações
     */
    map<int, string> getSimulations();
    
    /**
     * Carregar Simulação
     * @param id ID da Simulação
     * @return Sucesso da Operação
     */
    bool loadSimulation(const int& id);
    
    /**
     * Consultar Simulação
     * @return Simulação
     */
    Simulation& getSimulation();
    
    /**
     * Correr Simulação
     * @return Sucesso da Operação
     */
    bool run();
    
    /**
     * Guardar Resultados da Simulação
     * @return Sucesso da Operação
     */
    bool save();

};

#endif	/* RUNSIMULATIONCONTROLLER_H */

