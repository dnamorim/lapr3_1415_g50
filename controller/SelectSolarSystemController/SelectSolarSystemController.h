#ifndef SELECTSOLARSYSTEMCONTROLLER_H
#define	SELECTSOLARSYSTEMCONTROLLER_H
/**
 * Criar Sistema Solar Controller - UC1
 * @file SelectSolarSystemController.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 9 de Janeiro de 2015, 01:45 por Ricardo Rodrigues <1120703@isep.ipp.pt>
 *  -Estrutura Basica do Controller
 *  -Sistema Solar Temporario
 */

#include <iostream>
#include <string>
#include <map>

using namespace std;

#include "../Controller.h"
#include "../../model/SolarSystem/SolarSystem.h"

class SelectSolarSystemController : public Controller {
private:
    /* Sistema Solar Temporario Selecionado */
    SolarSystem* tempSolarSystem;
public:
    /**
     * Construir Instância de SelectSolarSystemController(COMPLETO)
     */
    SelectSolarSystemController();
    
    /**
     * Construir Instância de SelectSolarSystemController(CÓPIA)
     * @param copy Instância de SelectSolarSystemController a Cópiar
     */
    SelectSolarSystemController(const SelectSolarSystemController& copy);
    
    /**
     * Destrotor da Instância de SelectSolarSystemController
     */
    virtual ~SelectSolarSystemController();
    
    /**
     * Consultar Lista de Sistemas Solares
     * @return Lista de Sistemas Solares
     */
    map<int, string> getSolarSystems();
    
    /**
     * Carregar Sistema Solar
     * @param id ID do Sistema Solar
     * @return Sucesso da Operação
     */
    bool loadSolarSystem(const int& id);

    /**
     * Consultar Sistema Solar
     * @return Sistema Solar
     */
    SolarSystem& getSolarSystem();
    
    /**
     * Atribuir Sistema Solar Selecionado ao Sistema Solar Activo
     * @return Sucesso da Operação
     */
    bool save();
 
};

#endif	/* SELECTSOLARSYSTEMCONTROLLER_H */

