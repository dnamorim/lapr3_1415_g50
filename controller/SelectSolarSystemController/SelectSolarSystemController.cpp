#include "SelectSolarSystemController.h"

SelectSolarSystemController::SelectSolarSystemController() : Controller(),tempSolarSystem(NULL) {
}

SelectSolarSystemController::SelectSolarSystemController(const SelectSolarSystemController& copy) : Controller(copy), tempSolarSystem(copy.tempSolarSystem) {
}

SelectSolarSystemController::~SelectSolarSystemController() {
    if(!this->dataLayer->autoDestroy() && !this->success && this->tempSolarSystem!=NULL){
        delete this->tempSolarSystem;
    }
}

map<int, string> SelectSolarSystemController::getSolarSystems(){
    return this->dataLayer->getSolarSystemsActive();
}

bool SelectSolarSystemController::loadSolarSystem(const int& id) {
    return (this->tempSolarSystem=this->dataLayer->loadSolarSystem(id))!=NULL;
}

SolarSystem& SelectSolarSystemController::getSolarSystem() {
    return *this->tempSolarSystem;
}

bool SelectSolarSystemController::save() {
    if (this->tempSolarSystem!=NULL) {
        this->solarSystem = this->tempSolarSystem;
        this->success = true;
    }
    return this->success;
}