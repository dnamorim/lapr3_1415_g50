#ifndef COPYSIMULATIONCONTROLLER_H
#define	COPYSIMULATIONCONTROLLER_H
/**
 * Criar Nova Simulação a partir de Existente Controller - UC6
 * @file CopySimulationController.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 18 de Dezembro de 2014, 01:45 por Duarte Amorim <1130674@isep.ipp.pt>
 *  -Estrutura Basica do Controller
 *  -Simulação Temporaria
 */

#include <iostream>
#include <string>
#include <map>

using namespace std;

#include "../Controller.h"
#include "../../model/Simulation/Simulation.h"

class CopySimulationController: public Controller {
private:
    /* Simulação Temporaria para Copiar */
    Simulation* tempSimulation;
public:
    /**
     * Construir Instância de CopySimulationController(COMPLETO)
     */
    CopySimulationController();
    
    /**
     * Construir Instância de CopySimulationController(CÓPIA)
     * @param copy Instância de CopySimulationController a Cópiar
     */
    CopySimulationController(const CopySimulationController& copy);
    
    /**
     * Destrotor da Instância de CreateSolarSystemController
     */
    virtual ~CopySimulationController();
    
    /**
     * Consultar Lista de Simulações
     * @return Lista de Simulações
     */
    map<int, string> getSimulations();
    
    /**
     * Carregar Simulação
     * @param id ID da Simulação
     * @return Sucesso da Operação
     */
    bool loadSimulation(const int& id);

    /**
     * Consultar Simulação
     * @return Simulação
     */
    Simulation& getSimulation();
    
    /**
     * Guardar Simulação Copiada
     * @return Sucesso da Operação
     */
    bool save();

};

#endif	/* COPYSIMULATIONCONTROLLER_H */

