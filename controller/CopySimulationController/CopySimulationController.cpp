#include "CopySimulationController.h"

CopySimulationController::CopySimulationController():Controller(),tempSimulation(NULL) {
}

CopySimulationController::CopySimulationController(const CopySimulationController& copy) : Controller(copy), tempSimulation(copy.tempSimulation) {
}

CopySimulationController::~CopySimulationController() {
    if(!this->dataLayer->autoDestroy() && this->tempSimulation!=NULL){
        delete this->tempSimulation;
    }
}

map<int, string> CopySimulationController::getSimulations() {
    return this->dataLayer->getSimulationsActive();
}

bool CopySimulationController::loadSimulation(const int& id) {
    return (this->tempSimulation=this->dataLayer->loadSimulation(id))!=NULL;
}

Simulation& CopySimulationController::getSimulation() {
    return *this->tempSimulation;
}

bool CopySimulationController::save() {
    this->tempSimulation->setDefine(false);
    if ((this->tempSimulation=this->dataLayer->saveSimulation(*this->tempSimulation))!=NULL) {
        this->success = true;
    }
    return this->success;
}

