#include "Controller.h"

DataLayer* Controller::dataLayer=NULL;
SolarSystem* Controller::solarSystem=NULL;
Simulation* Controller::simulation=NULL;

Controller::Controller():success(false) {
}

Controller::Controller(const Controller& copy):success(copy.success) {
    
}

Controller::~Controller() {
}

bool Controller::Success() const{
    return this->success;
}

bool Controller::canRun() const{
    return (Controller::dataLayer!=NULL);
}

void Controller::showData(ostream &out){
    if(Controller::dataLayer!=NULL){
        out << "<<DataLayer: Define>>"<<endl;
        out << *Controller::dataLayer;
    }else{
        out << "<<DataLayer: Not Define>>"<< endl;
    }
    if(Controller::solarSystem!=NULL){
        out << "<<Solar System: Define>>" << endl;
        out << "ID: " << Controller::solarSystem->getID()<<endl;
        out << "EDITED: " << ((Controller::solarSystem->edited()==true)?"TRUE":"FALSE") << endl;
        out << *Controller::solarSystem<<endl;
    }else{
        out << "<<Solar System: Not Define>>" << endl;
    }
    if(Controller::simulation!=NULL){
        out << "<<Simulation: Define>>" << endl;
        out << "ID: " << Controller::simulation->getID()<<endl;
        out << "EDITED: " << ((Controller::simulation->edited()==true)?"TRUE":"FALSE") << endl;
        out << *Controller::simulation<<endl;
    }else{
        out << "<<Simulation: Not Define>>" << endl;
    }
    
}