#ifndef CREATENEWSIMULATIONCONTROLLER_H
#define CREATENEWSIMULATIONCONTROLLER_H

/**
 * Criar Nova Simulação Controller - UC5
 * @file MainUI.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 19 de Dezembro de 2014, às 12:10 por Duarte Amorim <1130674@isep.ipp.pt>
 *  -Estrutura Basica do Controller
 *
 * >Modificado a 11 de Janeiro de 2015, 15:03 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Simulação Temporaria; canRun(); save();
 */

#include <iostream>
#include <string>

using namespace std;

#include "../Controller.h"
#include "../../model/Simulation/Simulation.h"

class CreateNewSimulationControlller : public Controller {
private:
    /* Simulação Temporaria */
    Simulation* tempSimulation;
public:
    /**
     * Construir Instância de CreateNewSimulationControlller(COMPLETO)
     */
    CreateNewSimulationControlller();
    
    /**
     * Construir Instância de CreateNewSimulationControlller(CÓPIA)
     * @param copy Instância de CreateNewSimulationControlller a Cópiar
     */
    CreateNewSimulationControlller(const CreateNewSimulationControlller& copy);
    
    /**
     * Destrotor da Instância de CreateSolarSystemController
     */
    virtual ~CreateNewSimulationControlller();
    
    
    /**
     * Verificar se é Possivel Correr Controller
     * @return true se for possivel correr Controller, caso contrário retorna false
     */
    virtual bool canRun() const;
    
    /**
     * Consultar Simulação
     * @return Simulação
     */
    Simulation& getSimulation();
    
    /**
     * Guardar Simulação
     * @return Sucesso da Operação
     */
    bool save();
};

#endif /* CREATENEWSIMULATIONCONTROLLER_H */
