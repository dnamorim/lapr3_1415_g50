#include "CreateNewSimulationController.h"

CreateNewSimulationControlller::CreateNewSimulationControlller() : Controller(),tempSimulation(NULL) {
    if(this->canRun()){
        this->tempSimulation=new Simulation(this->solarSystem);
    }
}

CreateNewSimulationControlller::CreateNewSimulationControlller(const CreateNewSimulationControlller& copy) : Controller(copy), tempSimulation(copy.tempSimulation) {
}

CreateNewSimulationControlller::~CreateNewSimulationControlller() {
    if(!this->dataLayer->autoDestroy() && this->tempSimulation!=NULL){
        delete this->tempSimulation;
    }
}

bool CreateNewSimulationControlller::canRun() const{
    return Controller::canRun() && this->solarSystem!=NULL;
}

Simulation& CreateNewSimulationControlller::getSimulation(){
    return *this->tempSimulation;
}

bool CreateNewSimulationControlller::save() {
    if ((this->tempSimulation=this->dataLayer->saveSimulation(*this->tempSimulation))!=NULL) {
        this->success = true;
    }
    return this->success;
}