#ifndef CONTROLLER_H
#define	CONTROLLER_H
/**
 * (ESQUELETO)Controller
 * @file Controller.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 16 de Dezembro de 2014 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Esqueleto Básico do Controller
 * 
 * >Modificado a 7 de Janeiro de 2015, 21:10 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -canRun(), Success()
 */
#define DEBUG_CONTROLLER_VAR
#include <iostream>
#include <cstddef>

using namespace std;

#include "../datalayer/DataLayer.h"
#include "../model/SolarSystem/SolarSystem.h"
#include "../model/Simulation/Simulation.h"

class Controller {
public:
    /* Data Layer dos Controllers */
    static DataLayer* dataLayer;
    /* Sistema Solar Activo */
    static SolarSystem* solarSystem;
    /* Simulação Activa */
    static Simulation* simulation;
protected:
    /* Estado de Sucesso do Controller */
    bool success; 
public:
    /**
     * Construir Instância de Controller(COMPLETO)
     */
    Controller();
    
    /**
     * Construir Instância de Controller(CÓPIA)
     * @param copy Instância de Controller a Cópiar
     */
    Controller(const Controller& copy);
    
    /**
     * Destrotor da Instância de Controller
     */
    virtual ~Controller();
    
    /**
     * Sucesso do Constroller
     * @return Sucesso da Operação
     */
    bool Success() const;
    
    /**
     * Verificar se é Possivel Correr Controller
     * @return true se for possivel correr Controller, caso contrário retorna false
     */
    virtual bool canRun() const;
    
    /**
     * Apresentar Variáveis do Controller
     */
    static void showData(ostream &out = cout);
};

#endif	/* CONTROLLER_H */

