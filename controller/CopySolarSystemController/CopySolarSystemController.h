#ifndef COPYSOLARSYSTEMCONTROLLER_H
#define	COPYSOLARSYSTEMCONTROLLER_H
/**
 * Criar Nova Simulação a partir de Existente Controller - UC6
 * @file CopySimulationController.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 10 de Janeiro de 2015, 21:45 por Sara Freitas <1130489@isep.ipp.pt>
 *  -Estrutura Basica do Controller
 *  -Simulação Temporaria
 */

#include <iostream>
#include <string>
#include <map>
using namespace std;

#include "../Controller.h"
#include "../../model/SolarSystem/SolarSystem.h"

class CopySolarSystemController : public Controller {
private:
		/* Sistema Solar Temporario Selecionado */
		SolarSystem* tempSolarSystem;
public:
	
    /**
	* Construir Instáncia de CopySolarSystemController(COMPLETO)
	*/
    CopySolarSystemController();
	
    /**
	* Construir Instáncia de CopySolarSystemController(CÓPIA)
	* @param copy Instáncia de CopySolarSystemController a Cópiar
	*/
    CopySolarSystemController(const CopySolarSystemController& orig);
	
    /**
	* Destrotor da Instáncia de CopySolarSystemController
	*/
	virtual ~CopySolarSystemController();
	/**
	* Consultar Lista de Sistemas Solares
	* @return Lista de Sistemas Solares
	*/
	map<int, string> getSolarSystems();
	/**
	* Carregar Sistema Solar
	* @param id ID do Sistema Solar
	* @return Sucesso da Operação
	*/
	bool loadSolarSystem(const int& id);

	/**
	* Consultar Sistema Solar
	* @return Sistema Solar
	*/
	SolarSystem& getSolarSystem();
	
	/**
	* Atribuir Sistema Solar Copiado ao Sistema Solar Activo
	* @return Sucesso da Operação
	*/
	bool save();
	


};

#endif	/* COPYSOLARSYSTEMCONTROLLER_H */

