#include "CopySolarSystemController.h"

CopySolarSystemController::CopySolarSystemController() : Controller(),tempSolarSystem(NULL) {
}

CopySolarSystemController::CopySolarSystemController(const CopySolarSystemController& copy) : Controller(copy), tempSolarSystem(copy.tempSolarSystem) {

}

CopySolarSystemController::~CopySolarSystemController() {
    if(!this->dataLayer->autoDestroy() && !this->success && this->tempSolarSystem!=NULL){
        delete this->tempSolarSystem;
    }
}

map<int, string> CopySolarSystemController::getSolarSystems(){
	return this->dataLayer->getSolarSystemsActive();
}

bool CopySolarSystemController::loadSolarSystem(const int& id) {
  return (this->tempSolarSystem=this->dataLayer->loadSolarSystem(id))!=NULL;
}

SolarSystem& CopySolarSystemController::getSolarSystem() {
	return *this->tempSolarSystem;
}

bool CopySolarSystemController::save() {
    this->tempSolarSystem->setDefine(false);
    if ((this->tempSolarSystem=this->dataLayer->saveSolarSystem(*this->tempSolarSystem))!=NULL) {
        this->solarSystem = this->tempSolarSystem;
        this->success = true;
    }
    return this->success;
}