#ifndef SIMULATION_H
#define SIMULATION_H
/**
 * Simulador
 * @file Simulation.h
 * 
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 * 
 * @version 1.0
 * >Criado a 18 de Dezembro de 2014, 22:10 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  - Definição de Atributos
 * 
 * >Modificado 11 de Janeiro de 2015, 20:10 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -ESQUELETO do Simulador
 * 
 * >Modificado a 12 de Janeiro de 2015, 01:20 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -readStream(add e remove corpos)
 */

#include <iostream>
#include <vector>
#include <iterator>
#include <string>
#include <ctime>
#include <math.h>

using namespace std;

#include "../../utils/ID/ID.h"
#include "../SolarSystem/SolarSystem.h"
#include "../Corps/Corps.h"
#include "../../utils/Timer/Timer.h"
#include "../../utils/XYZ/XYZ.h"

class Simulation:public ID {
private:
    /* Sistema Solar da Simulação */
    SolarSystem* solarSystem;
    /* Nome da Simulação */
    string name;
    /* Data Inicial da Simulação */
    Timer startTime;
    /* Data Final da Simulação */
    Timer endTime;
    /* Step da Simulação */
    Timer timeStep;
    /*Corpos da Simulação */
    vector<Corps*> corps;
    /* Estado da Simulação */
    bool runSimulation;
    /* Posições dos Corpos nos Diferentes Times Steps da Simulação */
    vector<vector<XYZ>> positions;
    /* Velocidades dos Corpos nos Diferentes Times Steps da Simulação */
    vector<vector<XYZ>> velocities;
    /* Steps da Simulação */
    vector<Timer> steps;
    
    static XYZ graviticalForce(double mass1, double mass2, XYZ p1, XYZ p2);
    XYZ getOldVelocities(int indexCorps, long nrTimestep);
    XYZ getOldPositions(int indexCorps, long nrTimestep);
public:
    /* Nome Pré-Defenido */
    static string NAME;
    /* Estado Pré-Defenido */
    static bool RUNSIMULATION;
    /* Data Inicial Pré-Definida */
    static Timer STARTTIME;
    /* Data Final Pré-Definida */
    static Timer ENDTIME;
    /* Step Pré-Definida */
    static Timer TIMESTEP;
    /* Lista de Corpos Pré-Defenida */
    static vector<Corps *> CORPS;
    /* Posições dos Corpos nos Diferentes Times Steps Pré-Defenida*/
    static vector<vector<XYZ>> POSITIONS;
    /* Steps Pré-Defenidos */
    static vector<Timer> STEPS;
    /* Constante Gravitacional */
    static double GRAVITATIONALCONSTANT;
    
    Simulation(SolarSystem* solarSystem,Timer startTime=Simulation::STARTTIME, Timer endTime=Simulation::ENDTIME, Timer timeStep=Simulation::TIMESTEP,string name=Simulation::NAME, const vector<Corps*>& corps=Simulation::CORPS, const vector<vector<XYZ>>& positions=Simulation::POSITIONS,
                const vector<Timer>& steps=Simulation::STEPS,bool runSimulation=Simulation::RUNSIMULATION);

    Simulation(const Simulation& copy);
    
    virtual ~Simulation();
    
    virtual Simulation* clone() const;
    
    SolarSystem& getSolarSystem() const;
    void setSolarSystem(SolarSystem* solarSystem);
  
    bool getRun() const;
    void setRun(bool runSimulation);
    
    Timer getStartTime() const;
    Timer& getStartTime();
    void setstartTime(Timer startTime=Simulation::STARTTIME);
    
    Timer getEndTime() const;
    Timer& getEndTime();
    void setEndTime(Timer endTime=Simulation::ENDTIME);
    
    Timer getTimeStep() const;
    Timer& getTimeStep();
    void setTimeStep(Timer timeStep=Simulation::TIMESTEP);
    
    string getName() const;
    void setName(string name=Simulation::NAME);
    
    vector<Corps*> getCorps() const;
    void setCorps(const vector<Corps*>& corps=Simulation::CORPS);
    
    vector<vector<XYZ>> getPositions() const;
    vector<XYZ> getPositions(const int& pos) const;
    void setPositions(const vector<vector<XYZ>>& positions=Simulation::POSITIONS);
    
    vector<Timer> getSteps() const;
    void setSteps(const vector<Timer>& steps=Simulation::STEPS);
    
    bool validate() const;
    
    void clearCorps();
    void clearPositions();
    void clearSteps();
    
    bool validateCorps(const Corps& corps) const;
                
    long numberCorps() const;
    
    bool containsCorps(const Corps& corps) const;
    bool containsCorps(const int& pos) const;
                      
    int indexCorps(const Corps& corps) const;
    
    Corps& getCorps(const int& pos) const;
    
    int addCorpss(const vector<Corps*>& corpss);
    bool addCorps(Corps& corps);
    bool addCorps(const int& pos, Corps& corps);
    
    bool removeCorps(const int& pos);
    bool removeCorps(const Corps& corps);

    
    /**
     * Consultar um Corpo da Lista de Corpos do Simulador
     * @param pos Posição do Corpo
     * @return Corpo
     */
    Corps& operator[](const int& pos) const;
    
    /**
     * Adicionar um Corpo a Lista de Corpos do Simulador
     * @param corps Novo Corpo
     * @return Sistema Solar
     */
    Simulation& operator+=(Corps& corps);
    
    /**
     * Remover um Corpo da Lista de Corpos do Simulador
     * @param corps Corpo
     * @return Sistema Solar
     */
    Simulation& operator-=(const Corps& corps);

    /**
     * Correr Simulação
     * @return Sucesso da Operação
     */
    bool run();
    
    /**
     * Verificar se a Simulação foi Executada
     * @return Sucesso da Operação
     */
    bool alreadyRun() const;
    
    /**
     * Sobrecarga de Operador==(Comparação de Instâncias)
     * @param op Simulador a Comparar
     * @return true se forem iguais, caso contrário retorna false
     */
    virtual bool operator==(const Simulation &op) const;
    
    /**
     * Representação Textual do Simulador
     * @param out Stream de Output
     */
    virtual void writeStream(ostream& out) const;
    
    /**
     * Representação Textual do Apontador do Simulador
     * @param out Stream de Output
     */
    virtual void writePointerStream(ostream& out) const;
    
    /**
     * Leitura Textual da Instância do Simulador
     * @param in Stream de Input
     * @param out Stream de Output
     */
    virtual void readStream(istream &in, ostream& out);
    
      /**
     * Sobrecarga de Operador<<(StreamOUT)
     * @param out Stream de Output
     * @param simulation Simulador para Output
     * @return StreamOUT
     */
    friend ostream& operator<<(ostream &out, const Simulation &simulation);
    
    
    /**
     * Sobrecarga de Operador<<*(StreamOUT)
     * @param out Stream de Output
     * @param simulation Simulador para Output
     * @return StreamOUT
     */
    friend ostream& operator<<(ostream &out, const Simulation *simulation);
    
    /**
     * Sobrecarga de Operador>>(StreamIN)
     * @param out Stream de Input
     * @param simulation Simulador para Output
     * @return StreamIN
     */
    friend istream& operator>>(istream &in, Simulation &simulation);
};

#endif /* SIMULATION_H */

