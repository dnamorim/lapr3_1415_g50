#include "Simulation.h"
#include "../Export/Export.h"

string Simulation::NAME="unknown";
bool Simulation::RUNSIMULATION=false;
Timer Simulation::STARTTIME;
Timer Simulation::ENDTIME;
Timer Simulation::TIMESTEP;
double Simulation::GRAVITATIONALCONSTANT = 6.674e-11;

vector<Corps *> Simulation::CORPS = [] {
    vector<Corps *> v;
    return v;
}();
vector<vector<XYZ>> Simulation::POSITIONS= [] {
    vector<vector<XYZ>> v;
    return v;
}();

vector<Timer> Simulation::STEPS= [] {
    vector<Timer> v;
    return v;
}();


Simulation::Simulation(SolarSystem* solarSystem,Timer startTime, Timer endTime, Timer timeStep,string name,const vector<Corps*>& corps, const vector<vector<XYZ>>& positions,const vector<Timer>& steps,bool runSimulation):solarSystem(solarSystem), startTime(startTime), endTime(endTime), timeStep(timeStep), name(name), corps(), positions(positions), steps(steps),runSimulation(runSimulation) {
    this->setCorps(corps);
}

Simulation::Simulation(const Simulation& copy):ID(copy),solarSystem(copy.solarSystem), startTime(copy.startTime), endTime(copy.endTime), timeStep(copy.timeStep), name(copy.name), corps(copy.corps),positions(copy.positions),steps(copy.steps),runSimulation(copy.runSimulation) {
    
}

Simulation::~Simulation() {
}

Simulation* Simulation::clone() const{
    return new Simulation(*this);
}

SolarSystem& Simulation::getSolarSystem() const{
    return *this->solarSystem;
}

void Simulation::setSolarSystem(SolarSystem* solarSystem){
    if(solarSystem!=this->solarSystem){
        vector<Corps*> corps=solarSystem->getAllBodys();
        vector<Corps*> new_corps;
        for(vector<Corps*>::iterator it=corps.begin();it!=corps.end();++it){
            if(this->containsCorps(**it)){
                new_corps.push_back(*it);
            }
        }
    this->solarSystem=solarSystem;
    this->setCorps(new_corps);
    this->edit=true;
    }
}

bool Simulation::getRun() const{
    return this->runSimulation;
}

void Simulation::setRun(bool runSimulation){
    this->runSimulation=runSimulation;
}

Timer Simulation::getStartTime() const {
    return this->startTime;
}

Timer& Simulation::getStartTime() {
    this->edit=true;
    return this->startTime;
}

void Simulation::setstartTime(Timer startTime) {
    this->startTime = startTime;
    this->edit = true;
}

Timer Simulation::getEndTime() const {
    return this->endTime;
}

Timer& Simulation::getEndTime() {
    this->edit=true;
    return this->endTime;
}

void Simulation::setEndTime(Timer endTime) {
    this->endTime = endTime;
    this->edit = true;
}

Timer Simulation::getTimeStep() const {
    return this->timeStep;
}

Timer& Simulation::getTimeStep() {
    this->edit=true;
    return this->timeStep;
}

void Simulation::setTimeStep(Timer timeStep) {
    this->timeStep = timeStep;
    this->edit = true;
}

string Simulation::getName() const{
    return this->name;
}

void Simulation::setName(string name){
    this->name=name;
    this->edit=true;
}

vector<Corps*> Simulation::getCorps() const{
    return this->corps;
}

void Simulation::setCorps(const vector<Corps*>& corps){
    this->clearCorps();
    this->addCorpss(corps);
}

vector<vector<XYZ>> Simulation::getPositions() const{
    return this->positions;
}

vector<XYZ> Simulation::getPositions(const int& pos) const{
    return this->positions[pos];
}

void Simulation::setPositions(const vector<vector<XYZ>>& positions){
    this->positions=positions;
    this->edit=true;
}

vector<Timer> Simulation::getSteps() const{
    return this->steps;
}

void Simulation::setSteps(const vector<Timer>& steps){
    this->steps=steps;
    this->edit=true;
}

bool Simulation::validate() const{
    if(this->startTime < this->endTime) {
        return true;
    }
    return false;
}

void Simulation::clearCorps(){
    this->corps.clear();
    this->edit=true;
}

void Simulation::clearPositions(){
    this->positions.clear();
    this->edit=true;
}

void Simulation::clearSteps(){
    this->steps.clear();
    this->edit=true;
}

bool Simulation::validateCorps(const Corps& corps) const {
    return (!this->containsCorps(corps) && this->solarSystem->containsAllBody(corps));
}

long Simulation::numberCorps() const {
    return this->corps.size();
}
            
bool Simulation::containsCorps(const Corps& corps) const {
    for (vector<Corps*>::const_iterator it = this->corps.begin(); it != this->corps.end(); ++it) {
        if (*(*it) == corps) {
            return true;
        }
    }
    return false;
}
            
bool Simulation::containsCorps(const int& pos) const {
    return pos >= 0 && this->corps.size() - 1 <= pos;
}

int Simulation::indexCorps(const Corps& corps) const {
    int pos = 0;
    for (vector<Corps*>::const_iterator it = this->corps.begin(); it != this->corps.end(); ++it, pos++) {
        if (*(*it) == corps) {
            return pos;
        }
    }
    return -1;
}

Corps& Simulation::getCorps(const int& pos) const {
    return *this->corps[pos];
}

int Simulation::addCorpss(const vector<Corps*>& corpss) {
    int sucessos = 0;
    for (vector<Corps*>::const_iterator it = corpss.begin(); it != corpss.end(); ++it) {
        if (this->addCorps((Corps&)**it)) {
            sucessos++;
        }
    }
    return sucessos;
}

bool Simulation::addCorps(Corps& corps) {
    if (!this->validateCorps(corps))
        return false;
    this->edit = true;
    this->corps.push_back(&corps);
    return true;
}

bool Simulation::addCorps(const int& pos, Corps& corps) {
    if (pos < 0 || pos>this->corps.size() || !this->validateCorps(corps))
        return false;
    this->edit = true;
    this->corps[pos] = &corps;
    return true;
}

bool Simulation::removeCorps(const int& pos) {
    if (pos < 0 || pos>this->corps.size())
        return false;
    this->edit = true;
    this->corps.erase(this->corps.begin() + pos);
    return true;
}

bool Simulation::removeCorps(const Corps& corps) {
    if (!this->containsCorps(corps))
        return false;
    this->edit = true;
    this->corps.erase(this->corps.begin() + this->indexCorps(corps));
    return true;
}

bool Simulation::run(){
    long timestepSecs = this->timeStep.getSecondsFromTime();
    this->clearPositions();
    this->clearSteps();
    
    vector<XYZ> tempXYZpositions;
    vector<XYZ> tempXYZvelocities;
    
    Star starSS = this->solarSystem->getStar();
    XYZ v0, p0;
    double m;
    long nrTimestep=0;
    for (Timer actualSimulationTime = this->startTime; actualSimulationTime < this->endTime; actualSimulationTime+=timeStep) {
        tempXYZpositions.clear();
        tempXYZvelocities.clear();

        //  Sun Values Default
        tempXYZpositions.push_back(XYZ(0,0,0));
        tempXYZvelocities.push_back(XYZ(0,0,0));
        
        for (int i=0; i < this->corps.size(); i++) {
            v0 = getOldVelocities(i, nrTimestep);
            p0 = getOldPositions(i, nrTimestep);
            
            //Sol
            XYZ forceSun = graviticalForce(corps[i]->getMass(), starSS.getMass(), p0, XYZ(0,0,0));
            double fx = forceSun.getX();
            double fy = forceSun.getY();
            double fz = forceSun.getZ();
            
            //total force
            for (int j = 0; j < corps.size(); j++) {
                if (i != j) {
                    XYZ fc = graviticalForce(corps[i]->getMass(), corps[j]->getMass(), p0, corps[j]->getCoordinates());
                    fx += fc.getX();
                    fy += fc.getY();
                    fz += fc.getZ();
                }
            }
            XYZ f(fx, fy, fz);
            
            //calculate corps accelaration
            XYZ a;
            m = corps[i]->getMass();
            a.setX(f.getX()/ m);
            a.setY(f.getY()/ m);
            a.setZ(f.getZ()/ m);
            
            //calculate corps velocities
            XYZ v;
            v.setX(v0.getX() + (a.getX() * timestepSecs));
            v.setY(v0.getY() + (a.getY() * timestepSecs));
            v.setZ(v0.getZ() + (a.getZ() * timestepSecs));
            
            //calcular a posicao do corpo
            XYZ p;
            p.setX(p0.getX() + v0.getX() * timestepSecs + ((0.5 * a.getX()) *(timestepSecs * timestepSecs)));
            p.setY(p0.getY() + v0.getY() * timestepSecs + ((0.5 * a.getY()) *(timestepSecs * timestepSecs)));
            p.setZ(p0.getZ() + v0.getZ() * timestepSecs + ((0.5 * a.getZ()) *(timestepSecs * timestepSecs)));

            tempXYZpositions.push_back(p);
            tempXYZvelocities.push_back(v);
        }
        
        //  Adicionar Timestep
        this->steps.push_back(actualSimulationTime);
        this->positions.push_back(tempXYZpositions);
        this->velocities.push_back(tempXYZvelocities);
        nrTimestep++;
    }
    
    this->runSimulation=true;
   return this->runSimulation;
}

XYZ Simulation::graviticalForce(double mass1, double mass2, XYZ p1, XYZ p2) {
    double distance = sqrt(pow(p2.getX() - p1.getX(),2) + pow(p2.getY() - p1.getY(),2) + pow(p2.getZ() - p1.getZ(),2));
    
    // calculate unit vectors
    double ux = p1.getX() - p2.getX();
    double uy = p1.getY() - p2.getY();
    double uz = p1.getZ() - p2.getZ();
    
    // calculate force magnitude
    double magnitudeForce = (-1) * Simulation::GRAVITATIONALCONSTANT * (mass1 * mass2) / pow(distance, 2);
    
    //o vetor força gravitica
    XYZ fg(0,0,0);
    fg.setX(magnitudeForce * (ux/distance));
    fg.setY(magnitudeForce * (uy/distance));
    fg.setZ(magnitudeForce * (uz/distance));
    
    return fg;
}

XYZ Simulation::getOldVelocities(int indexCorps, long nrTimestep) {
    XYZ vel;
    
    if(nrTimestep == 0) {
        vel = this->corps[indexCorps]->getSpeed();
    } else {
        vel = this->velocities[nrTimestep-1][indexCorps+1];
    }
    
    return vel;
}

XYZ Simulation::getOldPositions(int indexCorps, long nrTimestep) {
    XYZ vel;
    
    if(nrTimestep == 0) {
        vel = this->corps[indexCorps]->getCoordinates();
    } else {
        vel = this->positions[nrTimestep-1][indexCorps+1];
    }
    
    return vel;
}


bool Simulation::alreadyRun() const{
    return this->runSimulation && this->steps.size()>0 && this->positions.size()==this->steps.size();
}

Corps& Simulation::operator[](const int& pos) const {
    return *this->corps[pos];
}

Simulation& Simulation::operator+=(Corps& corps) {
    this->addCorps(corps);
    return *this;
}

Simulation& Simulation::operator-=(const Corps& corps) {
    this->removeCorps(corps);
    return *this;
}

bool Simulation::operator==(const Simulation& op) const {
    if (this == &op) {
        return true;
    }
    if (!(this->solarSystem==op.solarSystem && this->name == op.name && this->startTime==op.startTime && this->endTime==op.endTime &&this->timeStep == this->timeStep && this->positions == op.positions && this->steps==op.steps)) {
        return false;
    }
    if (this->corps.size() != op.corps.size()) {
        return false;
    }
    for (vector<Corps*>::const_iterator it = op.corps.begin(); it != op.corps.end(); ++it) {
        if (!this->containsCorps(*(*it))) {
            return false;
        }
    }
    return true;
}

void Simulation::writeStream(ostream& out) const{
    out << "Simulation: " << this->name << endl;
    out << "Solar System: " << this->solarSystem << endl;
    out << "Start Time: " << this->startTime << endl;
    out << "Time Step: " << this->timeStep << endl;
    out << "End Time: " << this->endTime << endl;
    out << "RUN: " << ((this->alreadyRun())?"YES":"NO") << endl;
    if(this->runSimulation) {
     out << "Number Records Stored: " << this->positions.size()*this->positions[0].size() << endl;
    }
    out << "Corps (" << this->corps.size() << ")" << endl;
    int i=1;
    for(vector<Corps*>::const_iterator it=this->corps.begin();it!=this->corps.end();++it, i++){
        out << i << " - " << *it << endl;
    }
    if(this->alreadyRun()){
        vector<vector<XYZ>>::const_iterator itp;
        vector<Timer>::const_iterator itt;
        for(itp=this->positions.begin(),itt=this->steps.begin();itt!=this->steps.end();++itp,++itt){
            i=1;
            cout << *itt << " - ";
            vector<XYZ>::const_iterator itp_i;
            for(itp_i=itp->begin();itp_i!=itp->end();++itp_i,i++){
                out << "(" << i << ")"<< "P[" << *itp_i << "] ";
            }
            cout << endl;
        }
    }
    
}

void Simulation::writePointerStream(ostream& out) const{
    cout << typeid(*this).name() << " " << this->name << "(" << ((this->runSimulation)?"RUN":"NORUN") << "," << this->startTime << "," << this->endTime << "," << this->timeStep;
}

void Simulation::readStream(istream &in, ostream& out){
    this->edit=true;
    this->runSimulation=false;
    out << this << endl;
    Utils::getString(this->name,"Name: ","Kept Value Name\n", true,false,in, out);
    out << "Start Time:" << endl;
    in >> this->startTime;
    this->startTime.convertDate(true);
    out << "End Time:"<<endl;
    in >> this->endTime;
    this->endTime.convertDate(true);
    out << "Step Time:" << endl;
    in >> this->timeStep;
    this->timeStep.convertDate(false);
    if(this->solarSystem==NULL){
        out << "Solar System Not Clearcut!" << endl;
        return;
    }
    int option, option1,i;
    do {
        out << "Simulation Corps:" << endl;
        out << "Solar System: " << this->solarSystem << endl;
        out << "1-Add" << endl;
        out << "2-Remove" << endl;
        out << "0-Exit" << endl;
        Utils::getValue<int>(option, "Option: ", "Invalid Option!\n", false, false, cin, cout);
        out << endl;
        switch (option) {
            case 1:
            {
                cout << "Corps to Add" << endl;
                i=1;
                vector<Corps*> all_corps = this->solarSystem->getAllBodys();
                for(vector<Corps*>::const_iterator it=all_corps.begin();it!=all_corps.end();++it, i++){
                    if(!this->containsCorps(**it)){
                        out << i << " - " << *it << endl;
                    }
                }
                out << "0-Back" << endl;
                Utils::getValue<int>(option1, "Option: ", "Invalid Option!\n", false, false, cin, cout);
                out << endl;
                if (option1 > 0 && option1<i) {
                    if(this->addCorps(*all_corps[option1 - 1])){
                        out << "Corps Added" << endl;
                    }else{
                        out << "Corps Not Added" << endl;
                    }
                }
                break;
            }
            case 2:
            {
                i = 1;
                cout << "Corps to Remove" << endl;
                for (vector<Corps*>::const_iterator it = this->corps.begin(); it != this->corps.end(); ++it, i++) {
                    out << i << " - " << *it << endl;
                }
                out << "0-Back" << endl;
                 Utils::getValue<int>(option1, "Option: ", "Invalid Option!\n", false, false, cin, cout);
                out << endl;
                if (option1 > 0 && option1<i) {
                    if(this->removeCorps(option1 - 1)){
                        out << "Corps Removed" << endl;
                    }else{
                        out << "Corps Not Removed" << endl;
                    }
                }
                break;
            }
            case 0:
            {
                out << "Exit Simulation Corps" << endl;
                break;
            }
            default:
            {
                out << "Invalid Option!" << endl;
                break;
            }
        }

    }while(option!=0);
    
    
}

ostream& operator<<(ostream &out, const Simulation &simulation){
    simulation.writeStream(out);
    return out;
}

ostream& operator<<(ostream &out, const Simulation *simulation) {
    simulation->writePointerStream(out);
    out << ")";
    return out;
}


istream& operator>>(istream &in, Simulation &simulation) {
    simulation.readStream(in, cout);
    in.clear();
    return in;
}