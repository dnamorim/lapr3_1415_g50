#include "Star.h"

string Star::NAME = "Sun";
double Star::MASS = 0;
double Star::DIAMETER = 0;

Star::Star(string name, double mass, double diameter) : name(name), mass(mass), diameter(diameter) {
}

Star::Star(const Star& copy) :ID(copy), name(copy.name), mass(copy.mass), diameter(copy.diameter) {
}

Star::~Star() {
}

Star* Star::clone() const{
    return new Star(*this);
}

string Star::getName() const{
    return this->name;
}

void Star::setName(string name){
    this->name=name;
    this->edit=true;
}

double Star::getMass() const{
    return this->mass;
}

void Star::setMass(double mass){
    this->mass=mass;
    this->edit=true;
}

double Star::getDiameter() const{
    return this->diameter;
}

void Star::setDiameter(double diameter){
    this->diameter=diameter;
    this->edit=true;
}

bool Star::validate() const{
    return (this->name!="" && this->mass >=0 && this->diameter >=0);
}

bool Star::operator==(const Star& op) const{
    return (this == &op || (this->name==op.name && this->mass==op.mass && this->diameter==op.diameter));
}

void Star::writeStream(ostream& out) const{
    out << "Name: "<< this->name << endl;
    out << "Mass: " << this->mass << endl;
    out << "Diameter: "<< this->diameter << endl;
}

void Star::writePointerStream(ostream& out) const{
    out << "[Star]" << this->name << "(" << this->mass << "," << this->diameter;
}

void Star::readStream(istream &in, ostream& out){
    this->edit=true;
    out << this << endl;
    Utils::getString(this->name,"Name: ","Kept Value Name\n", true,false,in, out);
    Utils::getValue<double>(this->mass, "Mass: ", "Kept Value Mass\n", true, true, in, out);
    Utils::getValue<double>(this->diameter, "Diameter: ", "Kept Value Diameter\n", true, true, in, out);
}

ostream& operator<<(ostream &out, const Star &star){
    star.writeStream(out);
    return out;
}

ostream& operator<<(ostream &out, const Star *star){
    star->writePointerStream(out);
    out << ")";
    return out;
}

istream& operator>>(istream &in, Star &star){
    star.readStream(in, cout);
    in.clear();
    return in;
}