#ifndef STAR_H
#define	STAR_H

#include <iostream>
#include <string>

using namespace std;

#include "../../utils/Utils.h"
#include "../../utils/ID/ID.h"

class Star: public ID {
private:
    /* Nome da Estrela */
    string name;
    /* Massa da Estrela */
    double mass;
    /* Diâmetro da Estrela */
    double diameter;
public:
    /* Válores Pré-Definidos*/
    /* Nome Pré-Definido */
    static string NAME;
    /* Massa  Pré-Definida */
    static double MASS;
    /* Diâmetro  Pré-Definido */
    static double DIAMETER;
    
    /**
     * Construir Instância de Estrela(COMPLETO)
     */
    Star(string name=Star::NAME, double mass=Star::MASS, double diameter=Star::DIAMETER);
    
    /**
     * Construir Instância de Estrela(CÓPIA)
     * @param copy Instância de Estrela a Cópiar
     */
    Star(const Star& copy);
    
    /**
     * Destrotor da Instância de Estrela
     */
    virtual ~Star();
    
     /**
     * Clone da Instância da Estrela
     * @return Copia da Instância da Estrela
     */
    virtual Star* clone() const;

    /**
     * Consultar Nome da Estrela
     * @return Nome
     */
    string getName() const;
    
    /**
     * Alterar Nome da Estrela
     * @param name Novo Nome
     */
    void setName(string name=Star::NAME);
    
    /**
     * Consultar Massa da Estrela
     * @return Massa
     */
    double getMass() const;
    
    /**
     * Alterar Massa da Estrela
     * @param mass Nova Massa
     */
    void setMass(double mass=Star::MASS);
    
    /**
     * Consultar Diâmetro da Estrela
     * @return Diâmetro
     */
    double getDiameter() const;
    
    /**
     * Alterar Diâmetro da Estrela
     * @param diameter Novo Diâmetro
     */
    void setDiameter(double diameter=Star::DIAMETER);
    
    /**
     * Validar Estrela
     * @return true se for um estrela válido, caso contrário retorna false
     */
    virtual bool validate() const;
    
    /**
     * Sobrecarga de Operador==(Comparação de Instâncias)
     * @param op Corpo a Comparar
     * @return true se forem iguais, caso contrário retorna false
     */
    virtual bool operator==(const Star& op) const;
    
    /**
     * Representação Textual da Instância do Corpo
     * @param out Stream de Output
     */
    virtual void writeStream(ostream& out) const;
    
    /**
     * Representação Textual do Apontador do Corpo
     * @param out Stream de Output
     */
    virtual void writePointerStream(ostream& out) const;

    /**
     * Leitura Textual da Instância do Corpo
     * @param in Stream de Input
     * @param out Stream de Output
     */
    virtual void readStream(istream &in, ostream& out);
    
    /**
     * Sobrecarga de Operador<<(StreamOUT)
     * @param out Stream de Output
     * @param star Estrela para Output
     * @return StreamOUT
     */
    friend ostream& operator<<(ostream &out, const Star &star);

    /**
     * Sobrecarga de Operador<<*(StreamOUT)
     * @param out Stream de Output
     * @param star Estrela para Output
     * @return StreamOUT
     */
    friend ostream& operator<<(ostream &out, const Star *star);

    /**
     * Sobrecarga de Operador>>(StreamIN)
     * @param out Stream de Input
     * @param star Estrela para Input
     * @return StreamIN
     */
    friend istream& operator>>(istream &in, Star &star);
    
};

#endif	/* STAR_H */

