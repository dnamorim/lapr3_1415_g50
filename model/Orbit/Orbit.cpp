#include "Orbit.h"
#include "../Corps/Corps.h"
#include "../OrbitalCorps/OrbitalCorps.h"

vector<OrbitalCorps *> Orbit::BODYS = [] {
    vector<OrbitalCorps *> v;
    return v;
}();

Orbit::Orbit(Corps* center,const vector<OrbitalCorps*>& bodys) : center(center), bodys() {
    this->setBodys(bodys);
}

Orbit::Orbit(const Orbit& copy) :ID(copy), center(copy.center), bodys() {
    this->setBodys(copy.getBodys());
}

Orbit::~Orbit() {
    this->clear();
}

Orbit* Orbit::clone() const {
    return new Orbit(*this);
}

Corps& Orbit::getCenter() const {
    return *this->center;
}

void Orbit::setCenter(Corps* center) {
    this->edit=true;
    if(this->center != center) {
        this->center = center;
    }
}

vector<OrbitalCorps*> Orbit::getBodys() const {
    return this->bodys;
}

vector<OrbitalCorps*> Orbit::getAllBodys() const{
    vector<OrbitalCorps*> all;
    for(vector<OrbitalCorps*>::const_iterator it=this->bodys.begin();it!=this->bodys.end();++it){
        all.push_back(*it);
        vector<OrbitalCorps*> others = (*it)->getOrbit().getAllBodys();
        for(vector<OrbitalCorps*>::const_iterator it_others=others.begin();it_others!=others.end();++it_others){
            all.push_back(*it_others);
        }
    }
    return all;
}

void Orbit::setBodys(const vector<OrbitalCorps*>& vbodys) {
    this->clear();
    this->addOrbits(vbodys);
}

bool Orbit::defined() const {
    if (this->bodys.size() != 0) {
        if (!ID::defined())
            return false;
        for (vector<OrbitalCorps*>::const_iterator it = this->bodys.begin(); it != this->bodys.end(); ++it) {
            if (!(*it)->defined())
                return false;
        }
    }
    return true;
}

bool Orbit::edited() const {
    if (this->bodys.size() != 0) {
        if (ID::edited())
            return true;
        for (vector<OrbitalCorps*>::const_iterator it = this->bodys.begin(); it != this->bodys.end(); ++it) {
            if ((*it)->edited())
                return true;
        }
    }
    return false;
}

double Orbit::calMass() const {
    double tmass = 0;
    for (vector<OrbitalCorps*>::const_iterator it = this->bodys.begin(); it != this->bodys.end(); ++it) {
        tmass += (*it)->calMass();
    }
    return tmass;
}

void Orbit::clear() {
     this->edit=true;
    for (vector<OrbitalCorps*>::iterator it = this->bodys.begin(); it != this->bodys.end(); ++it) {
        delete *it;
    }
    this->bodys.clear();
}

bool Orbit::validateOrbit(const OrbitalCorps& orbitalCorps) const {
    return (!this->containsOrbit(orbitalCorps) && orbitalCorps.validate());
}

long Orbit::numberOrbits() const {
    return this->bodys.size();
}

bool Orbit::containsOrbit(const OrbitalCorps& orbitalCorps) const {
    if (this->center==NULL || *(this->center) == orbitalCorps)
        return false;
    for (vector<OrbitalCorps*>::const_iterator it = this->bodys.begin(); it != this->bodys.end(); ++it) {
        if (*(*it) == orbitalCorps) {
            return true;
        }
    }
    return false;
}

bool Orbit::containsAllOrbit(const OrbitalCorps& orbitalCorps) const{
    vector<OrbitalCorps*> all = this->getAllBodys();
    for (vector<OrbitalCorps*>::const_iterator it = all.begin(); it != all.end(); ++it) {
        if (*(*it) == orbitalCorps) {
            return true;
        }
    }
    return false;
}

bool Orbit::containsOrbit(const int& pos) const{
    return pos>=0 && this->bodys.size()-1 <= pos;
}

int Orbit::indexOrbit(const OrbitalCorps& orbitalCorps) const {
    int pos = 0;
    for (vector<OrbitalCorps*>::const_iterator it = this->bodys.begin(); it != this->bodys.end(); ++it, pos++) {
        if (*(*it) == orbitalCorps) {
            return pos;
        }
    }
    return -1;
}

OrbitalCorps& Orbit::getOrbit(const int& pos) const {
    return *this->bodys[pos];
}

int Orbit::addOrbits(const vector<OrbitalCorps*>& orbits) {
    int sucessos = 0;
    for (vector<OrbitalCorps*>::const_iterator it = orbits.begin(); it != orbits.end(); ++it) {
        if(this->addOrbit((OrbitalCorps&)**it)){
            sucessos++;
        }
    }
    return sucessos;
}

bool Orbit::addOrbit(const OrbitalCorps& orbitalCorps) {
    if (!this->validateOrbit(orbitalCorps))
        return false;
    this->edit=true;
    this->bodys.push_back((OrbitalCorps*) orbitalCorps.clone());
    return true;
}

bool Orbit::addOrbit(const int& pos, const OrbitalCorps& orbitalCorps) {
    if (pos < 0 || pos>this->bodys.size() || !this->validateOrbit(orbitalCorps))
        return false;
    this->edit=true;
    this->bodys[pos] = (OrbitalCorps*) orbitalCorps.clone();
    return true;
}

bool Orbit::removeOrbit(const int& pos) {
    if (pos < 0 || pos>this->bodys.size())
        return false;
    this->edit=true;
    this->bodys.erase(this->bodys.begin() + pos);
    return true;
}

bool Orbit::removeOrbit(const OrbitalCorps& orbitalCorps) {
    if (!this->containsOrbit(orbitalCorps))
        return false;
    this->edit=true;
    this->bodys.erase(this->bodys.begin() + this->indexOrbit(orbitalCorps));
    return true;
}

OrbitalCorps& Orbit::operator[](const int& pos) const {
    return *this->bodys[pos];
}

Orbit& Orbit::operator+=(const OrbitalCorps& orbitalCorps) {
    this->addOrbit(orbitalCorps);
    return *this;
}

Orbit& Orbit::operator-=(const OrbitalCorps& orbitalCorps) {
    this->removeOrbit(orbitalCorps);
    return *this;
}

bool Orbit::operator==(const Orbit &op) const{
    if (this == &op) {
        return true;
    }
    if (this->bodys.size() != op.bodys.size()) {
        return false;
    }
    for (vector<OrbitalCorps*>::const_iterator it = op.bodys.begin(); it != op.bodys.end(); ++it) {
        if (!this->containsOrbit(*(*it))) {
            return false;
        }
    }
    return true;
}

void Orbit::writeStream(ostream& out) const {
    if(this->center!=NULL){
            out << "Center: " << this->center << ")" << endl;
    }
    out << "Number of Bodies in Orbita: " << this->bodys.size() << endl;
    for (vector<OrbitalCorps*>::const_iterator it = this->bodys.begin(); it != this->bodys.end(); ++it) {
        out << *it << endl;
    }
}

void Orbit::readStream(istream &in, ostream& out) {
    this->edit=true;
    int option, option1;
    do {
        out << "Orbit:" << endl;
        if(this->center!=NULL){
            out << "Center: " << this->center << endl;
        }
        out << "1-Add" << endl;
        out << "2-Remove" << endl;
        out << "3-Edit" << endl;
        out << "0-Exit Orbit" << endl;
        Utils::getValue<int>(option, "Option: ", "Invalid Option!\n", false, false, cin, cout);
        out << endl;
        switch (option) {
            case 1:
            {
                OrbitalCorps orbitalCorps;
                in>>orbitalCorps;
                if(this->addOrbit(orbitalCorps)){
                    orbitalCorps.writePointerStream(out);
                    out << endl << "Orbital Corps Added" << endl;
                }else{
                    out << "Orbital Corps Not Added" << endl;
                }
                break;
            }
            case 2:
            {
                int i = 1;
                for (vector<OrbitalCorps*>::const_iterator it = this->bodys.begin(); it != this->bodys.end(); ++it, i++) {
                    out << i << "- " << *it << endl;
                }
                out << "0-Back" << endl;
                Utils::getValue<int>(option1, "Option: ", "Invalid Option!\n", false, false, cin, cout);
                out << endl;
                if (option1 != 0) {
                    if(this->removeOrbit(option1 - 1)){
                        out << "Orbital Corps Removed" << endl;
                    }else{
                        out << "Orbital Corps Not Removed" << endl;
                    }
                }
                break;
            }
            case 3:
            {
                int j = 1;
                for (vector<OrbitalCorps*>::const_iterator it = this->bodys.begin(); it != this->bodys.end(); ++it) {
                    out << j << "- " << *it << endl;
                    j++;
                }
                out << "0- Back" << endl;
               Utils::getValue<int>(option1, "Option: ", "Invalid Option!\n", false, false, cin, cout);
                out << endl;
                if (option1 != 0 && this->containsOrbit(option1 - 1)) {
                    in>>this->getOrbit(option1 - 1);
                    this->getOrbit(option1 - 1).writePointerStream(out);
                    out << ")" << endl << "Orbital Corps Edited" << endl;
                }
                break;
            }
            case 0:
            {
                out << "Exit Orbit" << endl;
                break;
            }
            default:
            {
                out << "Invalid Option!" << endl;
                break;
            }

        }
    } while (option != 0);

}

ostream& operator<<(ostream &out, const Orbit &orbit) {
    orbit.writeStream(out);
    return out;
}

istream& operator>>(istream &in, Orbit &orbit) {
    orbit.readStream(in, cout);
    in.clear();
    return in;
}