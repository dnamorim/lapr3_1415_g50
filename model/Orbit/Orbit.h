#ifndef ORBIT_H
#define	ORBIT_H
/**
 * Representação de um Orbita
 * @file Orbit.h
 * 
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 * 
 * @version 1.1
 * >Criado a 14 de Dezembro de 2014, 09:20 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Defenição de Atributos e Métodos de Acesso/Consulta
 *  -Sobrecarga de Operadores ==;<<;>> 
 * >Modificado a 17 de Dezembro de 2014, 17:30 por André Ferreira <1120243@isep.ipp.pt>
 *  -Adiciona operadores de leitura que permite editar a órbita.
 * >Modifica a 3 de Dezembro de 2015, 18:08 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Adicionar Identificador
 *  -Metodos de Defenição e Edição
 */

#include <iostream>
#include <vector>
#include <iterator>
#include <climits>
//forward declaration para resolver o Cross-Referencing Problem
class Corps;
class OrbitalCorps;
using namespace std;

#include "../../utils/ID/ID.h"

class Orbit: public ID {
private:
    /* Corpo Central da Orbita */
    Corps* center;
    /* Lita de Corpos Orbitais */
    vector<OrbitalCorps*> bodys;
public:
    /*  Valores Pré-Definidos */
    /*  Corpos Orbitais Pré-Definido */
    static vector<OrbitalCorps *> BODYS;
   
    /**
     * Construir Instância de Corpo(COMPLETO)
     */
    Orbit(Corps* center=NULL,const vector<OrbitalCorps*>& bodys = Orbit::BODYS);

    /**
     * Construir Instância da Orbita(CÓPIA)
     * @param copy Instância da Orbita a Cópiar
     */
    Orbit(const Orbit& copy);

    /**
     * Destrotor da Instância da Orbita
     */
    virtual ~Orbit();
    
    /**
     * Clone da Instância da Orbita
     * @return Copia da Instância da Orbita
     */
    virtual Orbit* clone() const;
    
    /**
     * Consultar Corpo Central da Orbita
     * @return 
     */
    Corps& getCenter() const;
    
    /**
     * Alterar Corpo Central da Orbita
     * @param center Novo Corpo Central
     */
    void setCenter(Corps* center);
    
    /**
     * Consultar Lista de Corpos Orbitais da Orbita
     * @return Lista de Corpos Orbitais
     */
    vector<OrbitalCorps*> getBodys() const;
    
    /**
     * Consultar Lista de Todos os Corpos Orbitais da Orbita
     * @return Lista de Todos os Corpos
     */
    vector<OrbitalCorps*> getAllBodys() const;
    
    /**
     * Alterar Lista de Corpos Orbitais da Orbita
     * @param vbodys Nova Lista de Corpos Orbitais
     */
    void setBodys(const vector<OrbitalCorps*>& vbodys = Orbit::BODYS);
    
    /**
     * Verificar se esta Defenido
     * @return true, se estiver defenido, caso contrário retorna false
     */
    virtual bool defined() const;
    
     /**
     * Vereificae se foi Editado
     * @return true, se foi editado, caso contrário retorna false
     */
    virtual bool edited() const;
    
    /**
     * Calcular a Massa Total da Orbita
     * @return Massa Total
     */
    virtual double calMass() const;
    
    /**
     * Limpar Lista de Corpos Orbitais da Orbita
     */
    void clear();
    
    /**
     * Validar o Corpo Orbital na Orbita
     * @param orbitalCorps Corpo Orbital a Verificar
     * @return Sucesso da Operação
     */
    virtual bool validateOrbit(const OrbitalCorps& orbitalCorps) const;

    /**
     * Numero de Corpos Orbitais a Orbitar o Centro da Orbita
     * @return Numero de Corpos Orbitais
     */
    long numberOrbits() const;

    /**
     * Verificar se um Corpo Orbital esta Contido na Lista de Corpos Orbitais da Orbita
     * @param orbitalCorps Corpo Orbital a Verificar
     * @return true caso o Corpo Orbital esteja contido na Lista de Corpos Orbitais, caso contrário retorna false.
     */
    bool containsOrbit(const OrbitalCorps& orbitalCorps) const;
    
    /**
     * Verificar se um Corpo Orbital esta Contido na Lista de Todos os Corpos Orbitais da Orbita
     * @param orbitalCorps Corpo Orbital a Verificar
     * @return true caso o Corpo Orbital esteja contido na Lista de Todos os Corpos Orbitais, caso contrário retorna false.
     */
    bool containsAllOrbit(const OrbitalCorps& orbitalCorps) const;
    
    /**
     * Verificar se existe um Corpo Orbital na Lista de Corpos Orbitais na Posição da Orbita
     * @param pos Posição
     * @return true caso existe um Corpo Orbital na Lista de Corpos Orbitais na Posição, caso contrário retorna false.
     */
    bool containsOrbit(const int& pos) const;

    /**
     * Posição de um Corpo Orbital na Lista de Corpos Orbitais da Orbita
     * @param orbitalCorps Corpo Orbital a Procurar
     * @return Posição, ou -1 caso o Corpo Orbital não exista na Lista de Corpos Orbitais
     */
    int indexOrbit(const OrbitalCorps& orbitalCorps) const;

    /**
     * Consultar um Corpo Orbital da Lista de Corpos Orbitais da Orbita
     * @param pos Posição do Corpo
     * @return Corpo Orbital
     */
    OrbitalCorps& getOrbit(const int& pos) const;
    
    /**
     * Adicionar uma Lista de Corpos Orbitais a Lista de Corpos Orbitais da Orbita
     * @param orbits Lista de Corpos Orbitais 
     * @return Numero de Sucessos da Operação
     */
    int addOrbits(const vector<OrbitalCorps*>& orbits);
    
    /**
     * Adicionar um Corpo Orbital a Lista de Corpos Orbitaisda Orbita
     * @param orbitalCorps Novo Corpo Orbital
     * @return Sucesso da Operação
     */
    bool addOrbit(const OrbitalCorps& orbitalCorps);
    
    /**
     * Adicionar um Corpo Orbital a Lista de Corpos Orbitais da Orbita
     * @param pos Posição para Adicionar Corpo Orbital
     * @param orbitalCorps Novo Corpo Orbital
     * @return Sucesso da Operação
     */
    bool addOrbit(const int& pos, const OrbitalCorps& orbitalCorps);
    
    /**
     * Remover um Corpo Orbital da Lista de Corpos Orbitais da Orbita
     * @param pos Posição do Corpo Orbital a Remover
     * @return Sucesso da Operação
     */
    bool removeOrbit(const int& pos);
    
    /**
     * Remover Corpo Orbital da Lista de Corpos Orbitais da Orbita
     * @param orbitalCorps Corpo Orbital a Remover
     * @return Sucesso da Operação
     */
    bool removeOrbit(const OrbitalCorps& orbitalCorps);

    /**
     * Consultar um Corpo Orbital da Lista de Corpos Orbitais da Orbita
     * @param pos Posição do Corpo
     * @return Corpo Orbital
     */
    OrbitalCorps& operator[](const int& pos) const;
    
    /**
     * Adicionar um Corpo Orbital a Lista de Corpos Orbitais da Orbita
     * @param orbitalCorps Novo Corpo Orbital 
     * @return Orbita
     */
    Orbit& operator+=(const OrbitalCorps& orbitalCorps);
    
    /**
     * Remover um Corpo Orbital da Lista de Corpos Orbitais da Orbita
     * @param orbitalCorps Corpo Orbital
     * @return Orbita 
     */
    Orbit& operator-=(const OrbitalCorps& orbitalCorps);
    
    
    /**
     * Sobrecarga de Operador==(Comparação de Instâncias)
     * @param op Orbita a Comparar
     * @return true se forem iguais, caso contrário retorna false
     */
    virtual bool operator==(const Orbit &op) const;
    
    /**
     * Representação Textual da Instância da Orbita
     * @param out Stream de Output
     */
    virtual void writeStream(ostream& out) const;
    
    /**
     * Leitura textual de istância da órbita.
     * @param in Stream de input
     * @param out Stream de output
     */
    
    virtual void readStream(istream &in, ostream& out);
    
    /**
     * Sobrecarga de Operador<<(StreamOUT)
     * @param out Stream de Output
     * @param corps Orbita para Output
     * @return StreamOUT
     */
    friend ostream& operator<<(ostream &out, const Orbit &orbit);
    
    /**
     * Sobrecarga de Operador>>(StreamIN)
     * @param out Stream de Input
     * @param corps Corpo para Input
     * @return StreamIN
     */
    friend istream& operator>>(istream &in, Orbit &orbit);
};

#endif	/* ORBIT_H */

