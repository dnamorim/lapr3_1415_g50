#ifndef CORPS_H
#define	CORPS_H
/**
 * Representação de um Corpo
 * @file Corps.h
 * 
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 * 
 * @version 1.1
 * Criado a 14 de Dezembro de 2014, 17:05 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Defenição de Atributos e Métodos de Acesso/Consulta
 *  -Sobrecarga de Operadores ==;<<;>>
 *  -Validação de Corpo
 * >Modificado a 15 de Dezembro 2014, 15:40 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Corpos com Orbitas 
 * >Modificado a 16 de Dezembro 2014, 14:10 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Métodos de Acesso/Consulta a Orbita
 *  -Sobrecarga <<*
 * >Modificado a 18 de Dezembro 2014, 20:00 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Coordenadas do Corpo
 *  -Velocidade do Corpo
 * * >Modifica a 2 de Dezembro de 2014, 17:20 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Adicionar Identificador
 * >Modifica a 3 de Dezembro de 2015, 18:08 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Metodos de Defenição e Edição
 * >Modifica a 4 de Dezembro de 2015, 20:15 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Adicionar Class XYZ
 */

#include <iostream>
#include <string>
#include <algorithm>
#include <typeinfo>

using namespace std;

#include "../../utils/ID/ID.h"
#include "../../utils/XYZ/XYZ.h"
#include "../../utils/Timer/Timer.h"

#include "../../utils/Utils.h"

/* Tipos de Origem */
enum class Source {
    Natural, Manmade
};

//forward declaration para resolver o Cross-Referencing Problem
class Orbit;
class OrbitalCorps;

class Corps : public ID{
private:
    /* Nome do Corpo*/
    string name;
    /* Massa do Corpo */
    double mass;
    /* Diametro do Corpo*/
    double diameter;
    /* Densidade do Corpo */
    double density;
    /* Origem do Corpo */
    Source source;
    /* Orbita com centro no Corpo */
    Orbit* orbit;
    /* Coordenadas do Corpo */
    XYZ coordinates;
    /* Velocidade do Corpo */
    XYZ speed;
    /* Data de Referência */
    Timer referenceDate;
public:
    /* Válores Pré-Definidos*/
    /* Nome Pré-Definido */
    static string NAME;
    /* Massa Pré-Definida */
    static double MASS;
    /* Diametro Pré-Definido */
    static double DIAMETER;
    /* Densidade Pré-Definida */
    static double DENSITY;
    /* Origem Pré-Definida */
    static Source SOURCE;
    /* Coordenadas Pré-Definidas*/
    static XYZ COORDINATES;
    /* Velocidade Pré-Definidas*/
    static XYZ SPEED;
    /* Data de Referência Pré-Definida*/
    static Timer REFERENCEDATE;
    /* Lista de Corpos Orbitais Pré-Defenida */
    static vector<OrbitalCorps *> ORBITBODYS;

    /**
     * Construir Instância de Corpo(COMPLETO)
     */
    Corps(double mass = Corps::MASS, double diameter = Corps::DIAMETER, double density = Corps::DENSITY, XYZ coordinates = Corps::COORDINATES, XYZ speed = Corps::SPEED, string name = Corps::NAME, Timer referenceDate=Corps::REFERENCEDATE, Source source = Corps::SOURCE, const vector<OrbitalCorps*>& orbitBodys=Corps::ORBITBODYS);

    /**
     * Construir Instância de Corpo(CÓPIA)
     * @param copy Instância de Corpo a Cópiar
     */
    Corps(const Corps& copy);

    /**
     * Destrotor da Instância de Corpo
     */
    virtual ~Corps();

    /**
     * Clone da Instância do Corpo
     * @return Copia da Instância do Corpo
     */
    virtual Corps* clone() const;

    /**
     * Consultar Nome do Corpo
     * @return Nome
     */
    string getName() const;

    /**
     * Alterar Nome do Corpo
     * @param name Novo Nome
     */
    void setName(string name = Corps::NAME);

    /**
     * Consultar Coordenadas do Corpo
     * @return Coordenadas
     */
    XYZ getCoordinates() const;
    
    /**
     * Consultar Coordenadas do Corpo(ACESSO DIRECTO)
     * @return Coordenadas
     */
    XYZ& getCoordinates();
    
    /**
     * Alterar Coordenadas do Corpo
     * @param coordinates Novas Cordenadas
     */
    void setCoordinates(XYZ coordinates=Corps::COORDINATES);

    /**
     * Consultar Velocidade do Corpo
     * @return Velocidade
     */
    XYZ getSpeed() const;
    
     /**
     * Consultar Velocidade do Corpo(ACESSO DIRECTO)
     * @return Velocidade
     */
    XYZ& getSpeed();

    /**
     * Alterar Velocidade do Corpo
     * @param coordinates Nova Velocidade
     */
    void setSpeed(XYZ speed=Corps::SPEED);

    /**
     * Consultar Massa do Corpo
     * @return Massa
     */
    double getMass() const;

    /**
     * Alterar Massa do Corpo
     * @param mass Nova Massa
     */
    void setMass(double mass = Corps::MASS);

    /**
     * Consultar Diametro do Corpo
     * @return Diametro
     */
    double getDiameter() const;

    /**
     * Alterar Diametro do Corpo
     * @param diameter Novo Diametro
     */
    void setDiameter(double diameter = Corps::DIAMETER);

    /**
     * Cosnutar Densidade do Corpo
     * @return Densidade
     */
    double getDensity() const;

    /**
     * Alterar Densidade do Corpo
     * @param density Nova Densidade
     */
    void setDensity(double density = Corps::DENSITY);

    /**
     * Cosnutar Origem do Corpo
     * @return Origem
     */
    Source getSource() const;

    /**
     * Alterar Origem do Corpo
     * @param source Nova Origem
     */
    void setSource(Source source = Corps::SOURCE);

    /**
     * Consultar Orbita do Corpo
     * @return Orbita
     */
    Orbit& getOrbit() const;
    
    /**
     * Alterar Orbita do Corpo
     * @param orbit Nova Orbita
     */
    void setOrbit(Orbit& orbit);
    
    /**
     * Consultar Data de Referência do Corpo
     * @return Data de Referência
     */
    Timer getReferenceDate() const;
    
    /**
     * Consultar Data de Referência do Corpo
     * @return Data de Referência
     */
    Timer& getReferenceDate();
    
    /**
     * Alterar Data de Referência do Corpo
     * @param orbit Nova Data de Referência
     */
    void setReferenceDate(Timer referenceDate);
    
    /**
     * Verificar se esta Defenido
     * @return true, se estiver defenido, caso contrário retorna false
     */
    virtual bool defined() const;
    
     /**
     * Vereificae se foi Editado
     * @return true, se foi editado, caso contrário retorna false
     */
    virtual bool edited() const;
    
    /**
     * Calcular a Massa Total do Corpo
     * @return Massa Total
     */
    virtual double calMass() const;

    /**
     * Validar Corpo
     * @return true se for um corpo válido, caso contrário retorna false
     */
    virtual bool validate() const;

    /**
     * Sobrecarga de Operador==(Comparação de Instâncias)
     * @param op Corpo a Comparar
     * @return true se forem iguais, caso contrário retorna false
     */
    virtual bool operator==(const Corps& op) const;
    
    /**
     * Representação Textual da Instância do Corpo
     * @param out Stream de Output
     */
    virtual void writeStream(ostream& out) const;

    /**
     * Representação Textual do Apontador do Corpo
     * @param out Stream de Output
     */
    virtual void writePointerStream(ostream& out) const;

    /**
     * Leitura Textual da Instância do Corpo
     * @param in Stream de Input
     * @param out Stream de Output
     */
    virtual void readStream(istream &in, ostream& out);

    /**
     * Sobrecarga de Operador<<(StreamOUT)
     * @param out Stream de Output
     * @param corps Corpo para Output
     * @return StreamOUT
     */
    friend ostream& operator<<(ostream &out, const Corps &corps);

    /**
     * Sobrecarga de Operador<<*(StreamOUT)
     * @param out Stream de Output
     * @param corps Corpo para Output
     * @return StreamOUT
     */
    friend ostream& operator<<(ostream &out, const Corps *corps);

    /**
     * Sobrecarga de Operador>>(StreamIN)
     * @param out Stream de Input
     * @param corps Corpo para Input
     * @return StreamIN
     */
    friend istream& operator>>(istream &in, Corps &corps);
};



#endif /* CORPS_H */