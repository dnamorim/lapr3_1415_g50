#include "Corps.h"
#include "../Orbit/Orbit.h"
#include "../OrbitalCorps/OrbitalCorps.h"


string Corps::NAME = "unknown";
double Corps::MASS = 0;
double Corps::DIAMETER = 0;
double Corps::DENSITY = 0;
Source Corps::SOURCE = Source::Natural;
XYZ Corps::COORDINATES;
XYZ Corps::SPEED;
vector<OrbitalCorps *> Corps::ORBITBODYS = [] {
    vector<OrbitalCorps *> v;
    return v;
}();
Timer Corps::REFERENCEDATE;

Corps::Corps(double mass, double diameter, double density, XYZ coordinates, XYZ speed, string name, Timer referenceDate, Source source,const vector<OrbitalCorps*>& bodys) : mass(mass), diameter(diameter), density(density), coordinates(coordinates), speed(speed), name(name), referenceDate(referenceDate), source(source), orbit(new Orbit(this,bodys)) {

}

Corps::Corps(const Corps& copy) :ID(copy), mass(copy.mass), diameter(copy.diameter), density(copy.density), coordinates(copy.coordinates), speed(copy.speed), name(copy.name), referenceDate(copy.referenceDate), source(copy.source), orbit(new Orbit(this, copy.getOrbit().getBodys())) {

}

Corps::~Corps() {
    delete this->orbit;
}

Corps* Corps::clone() const {
    return new Corps(*this);
}

string Corps::getName() const {
    return this->name;
}

void Corps::setName(string name) {
    this->name = name;
    this->edit = true;
}

XYZ Corps::getCoordinates() const {
    return this->coordinates;
}

XYZ& Corps::getCoordinates() {
    this->edit=true;
    return this->coordinates;
}

void Corps::setCoordinates(XYZ coordinates) {
    this->coordinates = coordinates;
    this->edit = true;
}

XYZ Corps::getSpeed() const {
    return this->speed;
}

XYZ& Corps::getSpeed() {
    this->edit=true;
    return this->speed;
}

void Corps::setSpeed(XYZ speed) {
    this->speed = speed;
    this->edit = true;
}

double Corps::getMass() const {
    return this->mass;
}

void Corps::setMass(double mass) {
    this->mass = mass;
    this->edit = true;
}

double Corps::getDiameter() const {
    return this->diameter;
}

void Corps::setDiameter(double diameter) {
    this->diameter = diameter;
    this->edit = true;
}

double Corps::getDensity() const {
    return this->density;
}

void Corps::setDensity(double density) {
    this->density = density;
    this->edit = true;
}

Source Corps::getSource() const {
    return this->source;
}

void Corps::setSource(Source source) {
    this->source = source;
    this->edit = true;
}

Orbit& Corps::getOrbit() const {
    return *this->orbit;
}

void Corps::setOrbit(Orbit& corpsOrbit) {
    delete this->orbit;
    this->orbit = corpsOrbit.clone();
    this->orbit->setCenter(this);
    this->edit = true;
}

Timer Corps::getReferenceDate() const {
    return this->referenceDate;
}

Timer& Corps::getReferenceDate() {
    this->edit=true;
    return this->referenceDate;
}

void Corps::setReferenceDate(Timer referenceDate) {
    this->referenceDate = referenceDate;
    this->edit = true;
}

bool Corps::defined() const {
    return ID::defined() && this->orbit->defined();
}

bool Corps::edited() const {
    return ID::edited() || this->orbit->edited();
}

double Corps::calMass() const {
    return this->mass + this->orbit->calMass();
}

bool Corps::validate() const {
    return (this->name != "" && this->mass >= 0 && this->diameter >= 0 && this->density >= 0);
}

bool Corps::operator==(const Corps& op) const {
    return (this == &op || (this->source == op.source && this->name == op.name && this->mass == op.mass && this->diameter == op.diameter && this->density == op.density && *this->orbit==*op.orbit));
}

void Corps::writeStream(ostream& out) const {
    out << typeid (*this).name() << endl;
    out << "Source: " << ((this->source == Source::Natural) ? "Natural" : "Manmade") << endl;
    out << "Name: " << this->name << endl;
    out << "Reference Date: " << this->referenceDate << endl;
    out << "Coordinates: " << this->coordinates << endl;
    out << "Mass: " << this->mass << endl;
    out << "Diameter: " << this->diameter << endl;
    out << "Density: " << this->density << endl;
    out << "Speed: " << this->speed << endl;
    out << "Orbit:" << endl << *this->orbit << endl;
}

void Corps::writePointerStream(ostream& out) const {
    out << "[" << typeid (*this).name() << "] " << this->name << "(" << ((this->source == Source::Natural) ? "Natural" : "Manmade") << "," << this->referenceDate <<"," << "{" << this->coordinates << "}" << "," << this->mass << "," << this->diameter << "," << this->density << "," << "{" << this->speed << "}";
}

void Corps::readStream(istream &in, ostream& out) {
    this->edit = true;
    out << this << endl;
    string source=(this->source == Source::Natural)?"natural":"manmade";
    Utils::getString(this->name,"Source(Natural/Manmade): ","Kept Value Source\n", true,false,in, out);
    transform(source.begin(), source.end(), source.begin(), ::tolower);
    if (source == "manmade") {
        this->source = Source::Manmade;
    } else if (source == "natural") {
        this->source = Source::Natural;
    }else{
        out << "Kept Value Source"<< endl;
    }
    Utils::getString(this->name,"Name: ","Kept Value Name\n", true,false,in, out);
    out << "Reference Date: " << endl;
    in >> this->referenceDate;
    out << "Coordinates:" << endl;
    in >> this->coordinates;
    Utils::getValue<double>(this->mass, "Mass: ", "Kept Value Mass\n", true, true, in, out);
    Utils::getValue<double>(this->mass, "Diameter: ", "Kept Value Diameter\n", true, true, in, out);
    Utils::getValue<double>(this->density, "Density: ", "Kept Value Density\n", true, true, in, out);
    out << "Speed: " << endl;
    in >> this->speed;
    in >> *this->orbit;
}

ostream& operator<<(ostream &out, const Corps &corps) {
    corps.writeStream(out);
    return out;
}

ostream& operator<<(ostream &out, const Corps *corps) {
    corps->writePointerStream(out);
    out << ")";
    return out;
}

istream& operator>>(istream &in, Corps &corps) {
    corps.readStream(in, cout);
    in.clear();
    return in;
}