#include "OrbitalCorps.h"

double OrbitalCorps::SEMIMAJORAXIS = 0;
double OrbitalCorps::PERIOD = 0;
double OrbitalCorps::ORBITALSPEED = 0;
double OrbitalCorps::INCLINATIONAXISTOORIBIT = 0;
double OrbitalCorps::INCLINATIONORBIT = 0;
double OrbitalCorps::ECCENTRICITYORBIT = 0;
double OrbitalCorps::ESCAPEVELOCITY = 0;
double OrbitalCorps::PERIHELION = 0;
double OrbitalCorps::APHELION = 0;
double OrbitalCorps::MEANANOMALY = 0;
double OrbitalCorps::LONGITUDEPERIHELION = 0;
double OrbitalCorps::LONGITUDEASCENDINGNODE = 0;

OrbitalCorps::OrbitalCorps(double mass, double diameter, double density, XYZ coordinates, XYZ speed, double semimajoraxis, double period, double orbitalSpeed, double inclinationAxisToOribit, double inclinationOrbit, double eccentricityOrbit, double escapeVelocity, double perihelion, double aphelion, double meanAnomaly, double longitudePerihelion, double longitudeAscendingNode, string name, Timer referenceDate,Source source,const vector<OrbitalCorps*>& orbitBodys) : Corps(mass, diameter, density, coordinates, speed, name, referenceDate,source,orbitBodys), semimajoraxis(semimajoraxis), period(period), orbitalSpeed(orbitalSpeed), inclinationAxisToOribit(inclinationAxisToOribit), inclinationOrbit(inclinationOrbit), eccentricityOrbit(eccentricityOrbit), escapeVelocity(escapeVelocity), perihelion(perihelion), aphelion(aphelion), meanAnomaly(meanAnomaly), longitudePerihelion(longitudePerihelion), longitudeAscendingNode(longitudeAscendingNode)  {

}

OrbitalCorps::OrbitalCorps(const OrbitalCorps& copy) : Corps(copy),semimajoraxis(copy.semimajoraxis), period(copy.period), orbitalSpeed(copy.orbitalSpeed), inclinationAxisToOribit(copy.inclinationAxisToOribit), inclinationOrbit(copy.inclinationOrbit), eccentricityOrbit(copy.eccentricityOrbit), escapeVelocity(copy.escapeVelocity), perihelion(copy.perihelion), aphelion(copy.aphelion), meanAnomaly(copy.meanAnomaly), longitudePerihelion(copy.longitudePerihelion), longitudeAscendingNode(copy.longitudeAscendingNode) {

}

OrbitalCorps::~OrbitalCorps() {

}

Corps* OrbitalCorps::clone() const {
    return new OrbitalCorps(*this);
}

double OrbitalCorps::getSemimajorAxis() const {
    return this->semimajoraxis;
}

void OrbitalCorps::setSemimajorAxis(double semimajoraxis) {
    this->semimajoraxis = semimajoraxis;
    this->edit = true;
}

double OrbitalCorps::getPeriod() const {
    return this->period;
}

void OrbitalCorps::setPeriod(double periodo) {
    this->period = periodo;
    this->edit = true;
}

double OrbitalCorps::getOrbitalSpeed() const {
    return this->orbitalSpeed;
}

void OrbitalCorps::setOrbitalSpeed(double speed) {
    this->orbitalSpeed = speed;
    this->edit = true;
}

double OrbitalCorps::getInclinationAxisToOribit() const {
    return this->inclinationAxisToOribit;
}

void OrbitalCorps::setInclinationAxisToOribit(double inclinacao) {
    this->inclinationAxisToOribit = inclinacao;
    this->edit = true;
}

double OrbitalCorps::getInclinationOrbit() const {
    return this->inclinationOrbit;
}

void OrbitalCorps::setInclinationOrbit(double inclinacao) {
    this->inclinationOrbit = inclinacao;
    this->edit = true;
}

double OrbitalCorps::getEccentricityOrbit() const {
    return this->eccentricityOrbit;
}

void OrbitalCorps::setEccentricityOrbit(double eccentricity) {
    this->eccentricityOrbit = eccentricity;
    this->edit = true;
}

double OrbitalCorps::getEscapeVelocity() const {
    return this->escapeVelocity;
}

void OrbitalCorps::setEscapeVelocity(double velocity) {
    this->escapeVelocity = velocity;
    this->edit = true;
}

double OrbitalCorps::getPerihelion() const {
    return this->perihelion;
}

void OrbitalCorps::setPerihelion(double perihelion) {
    this->perihelion = perihelion;
    this->edit = true;
}

double OrbitalCorps::getAphelion() const {
    return this->aphelion;
}

void OrbitalCorps::setAphelion(double aphelion) {
    this->aphelion = aphelion;
    this->edit = true;
}

double OrbitalCorps::getMeanAnomaly() const {
    return this->meanAnomaly;
}

void OrbitalCorps::setMeanAnomaly(double meanAnomaly) {
    this->meanAnomaly = meanAnomaly;
    this->edit = true;
}

double OrbitalCorps::getLongitudePerihelion() const {
    return this->longitudePerihelion;
}

void OrbitalCorps::setLongitudePerihelion(double longitudePerihelion) {
    this->longitudePerihelion = longitudePerihelion;
    this->edit = true;
}

double OrbitalCorps::getLongitudeAscendingNode() const {
    return this->longitudeAscendingNode;
}

void OrbitalCorps::setLongitudeAscendingNode(double longitudeAscendingNode) {
    this->longitudePerihelion = longitudeAscendingNode;
    this->edit = true;
}


bool OrbitalCorps::validate() const {
    return (Corps::validate() && this->semimajoraxis >= 0 && this->eccentricityOrbit >= 0 && this->escapeVelocity >= 0 && this->inclinationAxisToOribit >= 0
            && this->inclinationOrbit >= 0 && this->perihelion >= 0 && this->aphelion >= 0 && this->orbitalSpeed >= 0 && this->period >= 0);
}

bool OrbitalCorps::operator==(const OrbitalCorps& op) const {
    return Corps::operator==(op) && (this->semimajoraxis == op.semimajoraxis && this->period == op.period && this->orbitalSpeed == op.orbitalSpeed && this->inclinationAxisToOribit == op.inclinationAxisToOribit && this->inclinationOrbit == op.inclinationOrbit && this->eccentricityOrbit == op.eccentricityOrbit && this->escapeVelocity == op.escapeVelocity && this->perihelion == op.perihelion && this->aphelion == op.aphelion && this->meanAnomaly == op.meanAnomaly && this->longitudePerihelion == op.longitudePerihelion && this->longitudeAscendingNode == op.longitudeAscendingNode);
}

void OrbitalCorps::writePointerStream(ostream& out) const {
    Corps::writePointerStream(out);
    out << "," << this->semimajoraxis << "," << this->period << "," << this->orbitalSpeed << "," << this->inclinationAxisToOribit << ", " << this->inclinationOrbit << "," << this->eccentricityOrbit << "," << this->escapeVelocity << "," << this->perihelion << "," << this->aphelion << "," << this->meanAnomaly << "," << this->longitudePerihelion << "," << this->longitudeAscendingNode;
}

void OrbitalCorps::writeStream(ostream& out) const {
    Corps::writeStream(out);
    out << "Semimajor Axis: " << this->semimajoraxis << endl;
    out << "Period: " << this->period << endl;
    out << "Orbital Speed: " << this->orbitalSpeed << endl;
    out << "Inclination Axis To Oribit: " << this->inclinationAxisToOribit << endl;
    out << "Inclination Orbit: " << this->inclinationOrbit << endl;
    out << "Eccentricity Orbit: " << this->eccentricityOrbit << endl;
    out << "Escape Velocity: " << this->escapeVelocity << endl;
    out << "Perihelion: " << this->perihelion << endl;
    out << "Aphelion: " << this->aphelion << endl;
    out << "Mean Anomaly: " << this->meanAnomaly << endl;
    out << "Longitude Perihelion: " << this->longitudePerihelion << endl;
    out << "Longitude Ascending Node: " << this->longitudeAscendingNode << endl;
 }

void OrbitalCorps::readStream(istream &in, ostream& out) {
    Corps::readStream(in, out);
    Utils::getValue<double>(this->semimajoraxis, "Semimajor Axis: ", "Kept Value Semimajor Axis\n", true, true, in, out);
    Utils::getValue<double>(this->period, "Period: ", "Kept Value Period\n", true, true, in, out);
    Utils::getValue<double>(this->orbitalSpeed, "Orbital Speed: ", "Kept Value Orbital Speed\n", true, true, in, out);
    Utils::getValue<double>(this->inclinationAxisToOribit, "Inclination Axis To Oribit: ", "Kept Value Inclination Axis To Oribit\n", true, true, in, out);
    Utils::getValue<double>(this->inclinationOrbit, "Inclination Orbit: ", "Kept Value Inclination Orbit Earth\n", true, true, in, out);
    Utils::getValue<double>(this->eccentricityOrbit, "Eccentricity Orbit: ", "Kept Value Eccentricity Orbit\n", true, true, in, out);
    Utils::getValue<double>(this->escapeVelocity, "Escape Velocity: ", "Kept Value Escape Velocity\n", true, true, in, out);
    Utils::getValue<double>(this->perihelion, "Perihelion: ", "Kept Value Orbit Perihelion\n", true, true, in, out);
    Utils::getValue<double>(this->aphelion, "Aphelion: ", "Kept Value aphelion\n", true, true, in, out);
    Utils::getValue<double>(this->meanAnomaly, "Mean Anomaly: ", "Kept Value Mean Anomaly\n", true, true, in, out);
    Utils::getValue<double>(this->longitudePerihelion, "Longitude Perihelion: ", "Kept Value Longitude Perihelion\n", true, true, in, out);
    Utils::getValue<double>(this->longitudeAscendingNode, "Longitude Ascending Node: ", "Kept Value Longitude Ascending Node\n", true, true, in, out);
}