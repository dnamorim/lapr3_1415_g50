#ifndef ORBITLCORPS_H
#define	ORBITLCORPS_H
/**
 * RepresentaÃ§Ã£o de um Corpo Orbital
 * @file OrbitalCorps.h
 * 
 * @author AndrÃ© Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 * 
 * @version 1.1
 * Criado a 14 de Dezembro de 2014, 21:45 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Sobrecarga de Operadores ==;<<;>> 
 *  -ValidaÃ§Ã£o de Corpo Orbital
 * Criado a 18 de Dezembro de 2014, 15:30 por Sara Freitas <1130489@isep.ipp.pt> 
 * e Ricardo Rodrigues <1120703@isep.ipp.pt>
 *  -get's, set's, metodo validate, metodo escrever e equals 
 * Modificado a 14 de Janeiro de 2014, 20:19 por Duarte Amorim <1130674@isep.ipp.pt>
 * - adicionados novos atributos: eccentricAnomaly, longitudePerihelion, longitudeAscendingNode.
 */


#include <iostream>

using namespace std;

#include "../Corps/Corps.h"

class OrbitalCorps : public Corps {
private:
    double semimajoraxis;
    double period;
    double orbitalSpeed;
    double inclinationAxisToOribit;
    double inclinationOrbit;
    double eccentricityOrbit;
    double escapeVelocity;
    double meanAnomaly;
    double longitudePerihelion;
    double longitudeAscendingNode;
    double perihelion;
    double aphelion;
public:
    static double SEMIMAJORAXIS;
    static double PERIOD;
    static double ORBITALSPEED;
    static double INCLINATIONAXISTOORIBIT;
    static double INCLINATIONORBIT;
    static double ECCENTRICITYORBIT;
    static double ESCAPEVELOCITY;
    static double MEANANOMALY;
    static double LONGITUDEPERIHELION;
    static double LONGITUDEASCENDINGNODE;
    
    static double PERIHELION;
    static double APHELION;
    
    /**
     * Construir InstÃ¢ncia de Corpo Orbital(COMPLETO)
     */
    OrbitalCorps(double mass = Corps::MASS, double diameter = Corps::DIAMETER, double density = Corps::DENSITY, XYZ coordinates= Corps::COORDINATES,XYZ speed = Corps::SPEED,double semimajoraxis=OrbitalCorps::SEMIMAJORAXIS, double period=OrbitalCorps::PERIOD, double orbitalSpeed=OrbitalCorps::ORBITALSPEED, double inclinationAxisToOribit=OrbitalCorps::INCLINATIONAXISTOORIBIT, double inclinationOrbit=OrbitalCorps::INCLINATIONORBIT, double eccentricityOrbit=OrbitalCorps::ECCENTRICITYORBIT, double escapeVelocity=OrbitalCorps::ESCAPEVELOCITY, double perihelion=OrbitalCorps::PERIHELION, double aphelion=OrbitalCorps::APHELION, double meanAnomaly = OrbitalCorps::MEANANOMALY, double longitudePerihelion = OrbitalCorps::LONGITUDEPERIHELION, double longitudeAscendingNode = OrbitalCorps::LONGITUDEASCENDINGNODE,string name = Corps::NAME,  Timer referenceDate=Corps::REFERENCEDATE,Source source = Corps::SOURCE,const vector<OrbitalCorps*>& orbitBodys=Corps::ORBITBODYS);

    /**
     * Construir InstÃ¢ncia de Corpo Orbital(CÃ“PIA)
     * @param copy InstÃ¢ncia de Corpo Orbital a CÃ³piar
     */
    OrbitalCorps(const OrbitalCorps& copy);

    /**
     * Destrotor da InstÃ¢ncia de Corpo Orbital
     */
    virtual ~OrbitalCorps();

    /**
     * Clone da InstÃ¢ncia do Corpo Orbital
     * @return Copia da InstÃ¢ncia do Corpo Orbital
     */
    virtual Corps* clone() const;
    
  /**
   * Consultar Distancia do Corpo Orbital
   * @return Distancia
   */
    double getSemimajorAxis() const;
    /**
     * Modificar Distancia do Corpo Orbital
     * @param distance
     */
    void setSemimajorAxis(double semimajoraxis);
    
    /**
     * Consultar Periodo do Corpo Orbital
     * @return Periodo
     */
    double getPeriod() const;
    
    /**
     * Modificar Periodo do Corpo Orbital
     * @param Periodo
     */
    void setPeriod(double period);
    /**
     * Consultar a Velicidade Orbital do Corpo Orbital
     * @return Velocidade Orbital
     */
    double getOrbitalSpeed() const;
    
    /**
     * Modificar Velicidade Orbital do Corpo Orbital
     * @param Velocidade Orbital
     */
    void setOrbitalSpeed(double velocidade);
    
    /**
     * Consultar a InclinaÃ§Ã£o do Eixo da Orbita do Corpo Orbital
     * @return InclinaÃ§Ã£o do Eixo da Orbita
     */
    double getInclinationAxisToOribit() const;
    
    /**
     * Modificara InclinaÃ§Ã£o do Eixo da Orbita do Corpo Orbital
     * @param InclinaÃ§Ã£o do Eixo da Orbita
     */
    void setInclinationAxisToOribit(double inclinacao);
    
    /**
     * Consultar a InclinaÃ§Ã£o da Orbita
     * @return InclinaÃ§Ã£o da Orbita
     */
    double getInclinationOrbit() const;
    
    /**
     * Modificara InclinaÃ§Ã£o da Orbita
     * @param InclinaÃ§Ã£o da Orbita
     */
    void setInclinationOrbit(double inclinacao);
    
    /**
     * Consultar a Excentricidade da Orbita (Afastamento de uma orbitra circular)
     * @return Excentricidade da Orbita
     */
    double getEccentricityOrbit() const;
    
     /**
     * Modificara a Excentricidade da Orbita (Afastamento de uma orbitra circular)
     * @param Excentricidade da Orbita
     */
    void setEccentricityOrbit(double eccentricity);
    
    /**
     * Consultar a Velocidade de Escape
     * @return Velocidade de Escape
     */
    double getEscapeVelocity() const;
    
     /**
     * Modificara Velocidade de Escape
     * @param Velocidade de Escape
     */
    void setEscapeVelocity(double velocity);
    
    /**
     * Consultar Perihelion do Corpo Orbital
     * @return Perihelion
     */
    double getPerihelion() const;
    
    /**
     * Alterar Perihelion do Corpo Orbital
     * @param Novo Perihelion
     */
    void setPerihelion(double perihelion);
    
    
    /**
     * Consultar Aphelion do Corpo Orbital
     * @return Aphelion
     */
    double getAphelion() const;

    /**
     * Alterar Aphelion do Corpo Orbital
     * @param Nova Aphelion
     */
    void setAphelion(double aphelion);
    
    /**
     *  Obter Mean Anomaly J2000 do Corpo Orbital
     *  @return eccentric anomaly
     */
    double getMeanAnomaly() const;
    
    /**
     *  Definir nova mean Anomaly J2000 do Corpo Orbital
     *  @param nova eccentric anomaly
     */
    void setMeanAnomaly(double meanAnomaly);

    /**
     *  Obter Longitude Perihelion do Corpo Orbital
     *  @return longitude perihelion
     */
    double getLongitudePerihelion() const;
    
    /**
     *  Definir nova Longitude Perihelion do Corpo Orbital
     *  @param nova longitude perihelion
     */
    void setLongitudePerihelion(double longitudePerihelion);
    
    /**
     *  Obter Longitude Ascending Node do Corpo Orbital
     *  @return longitudeAscendingNode
     */
    double getLongitudeAscendingNode() const;
    
    /**
     *  Definir nova Longitude Ascending Node do Corpo Orbital
     *  @param longitudeAscendingNode
     */
    void setLongitudeAscendingNode(double longitudeAscendingNode);
    
    /**
     * Validar Corpo Orbital
     * @return true se for um corpo vÃ¡lido, caso contrÃ¡rio retorna false
     */
    virtual bool validate() const;

    /**
     * Sobrecarga de Operador==(ComparaÃ§Ã£o de InstÃ¢ncias)
     * @param op Corpo Orbital a Comparar
     * @return true se forem iguais, caso contrÃ¡rio retorna false
     */
    virtual bool operator==(const OrbitalCorps& op) const;

    /**
     * RepresentaÃ§Ã£o Textual da InstÃ¢ncia do Corpo Orbital
     * @param out Stream de Output
     */
    virtual void writeStream(ostream& out) const;
    
    /**
     * RepresentaÃ§Ã£o Textual do Apontador do Corpo Orbital
     * @param out Stream de Output
     */
    virtual void writePointerStream(ostream& out) const;

    /**
     * Leitura Textual da InstÃ¢ncia do Corpo Orbital
     * @param in Stream de Input
     * @param out Stream de Output
     */
    virtual void readStream(istream &in, ostream& out);
    
};

#endif	/* ORBITLCORPS_H */