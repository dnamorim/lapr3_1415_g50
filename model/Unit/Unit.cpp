#include "Unit.h"

double Unit::G = 6.674E-11;
double Unit::AU = 149597870700;

double Unit::mg = 0.000001;
double Unit::cg = 0.00001;
double Unit::dg = 0.0001;
double Unit::g  = 0.001;
double Unit::dag= 0.01;
double Unit::hg = 0.1;
double Unit::kg = 1;

double Unit::mm = 0.001;
double Unit::cm = 0.01;
double Unit::dm = 0.1;
double Unit::m = 1;
double Unit::dam = 1;
double Unit::hm = 100;
double Unit::km = 1000;

double Unit::sec = 1;
double Unit::min = 60;
double Unit::hor = 3600;

double Unit::ValueInSi(string unit) {
    double ret=1;
    double exp=1;
    for(int i=0;i<unit.length();i++){
        if(isdigit(unit[i]) || unit[i]=='+' || unit[i]=='-'){
            exp = atof(unit.substr(i).c_str());
            unit= unit.substr(0,i-1);
            break;
        }
    }
    if (unit == "G") {
        ret= Unit::G;
    }
    transform(unit.begin(), unit.end(), unit.begin(), ::tolower);
    if (unit == "km") {
        ret= Unit::km;
    } else if (unit == "hm") {
        ret= Unit::hm;
    } else if (unit == "dam") {
        ret= Unit::dam;
    } else if (unit == "dm") {
        ret= Unit::dm;
    } else if (unit == "cm") {
        ret= Unit::cm;
    } else if (unit == "mm") {
        ret= Unit::mm;
    } else if (unit == "h" || unit=="hr") {
        ret= Unit::hor;
    }else if (unit == "min") {
        ret= Unit::min;
    } else if (unit == "au") {
       //DUVIDA É PARA COMVERTER AU PARA M OU UTILIZAR AU COM UNIDADE
        ret= Unit::AU;
    }
    return pow(ret,exp);
}

double Unit::ConvertToSI(double num, string unit) {
    if (unit == "") {
        return num;
    } else if (unit.find("/") != -1) {
        string unit1 = unit.substr(0, unit.find_last_of("/"));
        string unit2 = unit.substr(unit.find_last_of("/") + 1);
        return num * (Unit::ValueInSi(unit1) * Unit::ValueInSi(unit2));
    } else {
        return num * Unit::ValueInSi(unit);
    }
}

double Unit::convertToAU(double num) {
    return (num / Unit::AU);
}