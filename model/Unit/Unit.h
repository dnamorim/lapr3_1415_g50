#ifndef UNIT_H
#define	UNIT_H

#include <string>
#include <algorithm>
#include <iostream>
#include <cmath>
#include <cctype>

using namespace std;

class Unit {
private:
    static double ValueInSi(string unit);
public:
    static double G;
    static double AU;
    
    static double kg;
    static double hg;
    static double dag;
    static double g;
    static double dg;
    static double cg;
    static double mg;
    
    static double mm;
    static double cm;
    static double dm;
    static double m;
    static double dam;
    static double hm;
    static double km;
    
    static double sec;
    static double min;
    static double hor;

    static double ConvertToSI(double num, string unit);
    static double convertToAU(double num);
};

#endif	/* UNIT_H */

