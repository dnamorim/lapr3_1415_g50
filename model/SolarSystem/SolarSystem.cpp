#include "SolarSystem.h"

string SolarSystem::NAME = "Unknown";
Star SolarSystem::STAR;
vector<Corps *> SolarSystem::BODYS = [] {
    vector<Corps *> v;
    return v;
}();
map<string, Corps*> SolarSystem::supportCorpsTypes = [] {
    map<string, Corps*> m;
    m["Corps"] = new Corps();
    m["Gravitational Corps"] = new GravitationalCorps();
    m["Gravitational with Propulsion Corps"] = new GravitationalwithPropulsionCorps();
    m["Orbital Corps"] = new OrbitalCorps();
    return m;
}();

SolarSystem::SolarSystem(string name, Star star, const vector<Corps*>& bodys) : name(name), star(star), bodys() {
    this->setBodys(bodys);
}

SolarSystem::SolarSystem(const SolarSystem &copy) :ID(copy), name(copy.name), star(copy.star), bodys() {
    this->setBodys(copy.getBodys());
}

SolarSystem::~SolarSystem() {

}

SolarSystem* SolarSystem::clone() const {
    return new SolarSystem(*this);
}

string SolarSystem::getName() const {
    return this->name;
}

void SolarSystem::setName(string name) {
    this->name = name;
    this->edit = true;
}

Star SolarSystem::getStar() const {
    return this->star;
}

Star& SolarSystem::getStar() {
    this->edit = true;
    return this->star;
}

void SolarSystem::setStar(Star star) {
    this->star = star;
    this->edit = true;
}

vector<Corps*> SolarSystem::getBodys() const {
    return this->bodys;
}

vector<Corps*> SolarSystem::getAllBodys() const{
    vector<Corps*> all;
    for(vector<Corps*>::const_iterator it=this->bodys.begin();it!=this->bodys.end();++it){
        all.push_back(*it);
        vector<OrbitalCorps*> orbit = (*it)->getOrbit().getAllBodys();
        for(vector<OrbitalCorps*>::const_iterator it_orbit=orbit.begin();it_orbit!=orbit.end();++it_orbit){
            all.push_back(*it_orbit);
        }
    }
    return all;
}


void SolarSystem::setBodys(const vector<Corps*>& vbodys) {
    this->clear();
    this->addBodys(vbodys);
}

bool SolarSystem::defined() const {
    return ID::defined() && this->star.defined();
}

bool SolarSystem::edited() const {
    return ID::edited() || this->star.edited();
}

bool SolarSystem::validate() const {
    return (this->name != "" && this->star.validate());
}

void SolarSystem::clear() {
    this->edit = true;
    for (vector<Corps*>::iterator it = this->bodys.begin(); it != this->bodys.end(); ++it) {
        delete *it;
    }
    this->bodys.clear();
}

bool SolarSystem::validateBody(const Corps& body) const {
    return (!this->containsBody(body) && body.validate());
}

long SolarSystem::numberBodys() const {
    return this->bodys.size();
}

bool SolarSystem::containsBody(const Corps& body) const {
    for (vector<Corps*>::const_iterator it = this->bodys.begin(); it != this->bodys.end(); ++it) {
        if (*(*it) == body) {
            return true;
        }
    }
    return false;
}

bool SolarSystem::containsAllBody(const Corps& corps) const{
    vector<Corps*> all = this->getAllBodys();
    for (vector<Corps*>::const_iterator it = all.begin(); it != all.end(); ++it) {
        if (*(*it) == corps) {
            return true;
        }
    }
    return false;
}

bool SolarSystem::containsBody(const int& pos) const {
    return pos >= 0 && this->bodys.size() - 1 <= pos;
}

int SolarSystem::indexBody(const Corps& body) const {
    int pos = 0;
    for (vector<Corps*>::const_iterator it = this->bodys.begin(); it != this->bodys.end(); ++it, pos++) {
        if (*(*it) == body) {
            return pos;
        }
    }
    return -1;
}

Corps& SolarSystem::getBody(const int& pos) const {
    return *this->bodys[pos];
}

int SolarSystem::addBodys(const vector<Corps*>& orbits) {
    int sucessos = 0;
    for (vector<Corps*>::const_iterator it = orbits.begin(); it != orbits.end(); ++it) {
        if (this->addBody((Corps&)**it)) {
            sucessos++;
        }
    }
    return sucessos;
}

bool SolarSystem::addBody(const Corps& body) {
    if (!this->validateBody(body))
        return false;
    this->edit = true;
    this->bodys.push_back((Corps*) body.clone());
    return true;
}

bool SolarSystem::addBody(const int& pos, const Corps& body) {
    if (pos < 0 || pos>this->bodys.size() || !this->validateBody(body))
        return false;
    this->edit = true;
    this->bodys[pos] = (Corps*) body.clone();
    return true;
}

bool SolarSystem::removeBody(const int& pos) {
    if (pos < 0 || pos>this->bodys.size())
        return false;
    this->edit = true;
    this->bodys.erase(this->bodys.begin() + pos);
    return true;
}

bool SolarSystem::removeBody(const Corps& body) {
    if (!this->containsBody(body))
        return false;
    this->edit = true;
    this->bodys.erase(this->bodys.begin() + this->indexBody(body));
    return true;
}

Corps& SolarSystem::operator[](const int& pos) const {
    return *this->bodys[pos];
}

SolarSystem& SolarSystem::operator+=(const Corps& body) {
    this->addBody(body);
    return *this;
}

SolarSystem& SolarSystem::operator-=(const Corps& body) {
    this->removeBody(body);
    return *this;
}

bool SolarSystem::operator==(const SolarSystem& op) const {
    if (this == &op) {
        return true;
    }
    if (!(this->name == op.name && this->star == op.star)) {
        return false;
    }
    if (this->bodys.size() != op.bodys.size()) {
        return false;
    }
    for (vector<Corps*>::const_iterator it = op.bodys.begin(); it != op.bodys.end(); ++it) {
        if (!this->containsBody(*(*it))) {
            return false;
        }
    }
    return true;
}

void SolarSystem::writeStream(ostream& out) const {
    out << "Solar System" << endl;
    out << "Name: " << name << endl;
    out << "Star:" << endl << this->star << endl;
    out << "Number of Bodies: " << this->bodys.size() << endl;
    for (vector<Corps*>::const_iterator it = this->bodys.begin(); it != this->bodys.end(); ++it) {
        out << *it << ")" << endl;
    }
}

void SolarSystem::writePointerStream(ostream& out) const {
    out << "[Solar System]" << this->name << "(";
    this->star.writePointerStream(out);
    out << ")";
}

void SolarSystem::readStream(istream &in, ostream& out) {
    this->edit = true;
    out << this << endl;
    Utils::getString(this->name,"Name: ","Kept Value Name\n", true,false,in, out);
    out << "Star: " << endl;
    in >> this->star;
    int option, option1;
    do {
        out << "Center: ";
        out << this << endl;
        out << "Bodys:" << endl;
        out << "1-Add" << endl;
        out << "2-Remove" << endl;
        out << "3-Edit" << endl;
        out << "0-Exit Bodys" << endl;
        Utils::getValue<int>(option, "Option: ", "Invalid Option!\n", false, false, cin, cout);
        out << endl;
        switch (option) {
            case 1:
            {
                out << "Bodys Type:" << endl;
                int i = 1;
                for (map<string, Corps*>::const_iterator it = SolarSystem::supportCorpsTypes.begin(); it != SolarSystem::supportCorpsTypes.end(); ++it) {
                    cout << i << "-" << it->first << endl;
                    i++;
                }
                out << "0-Back" << endl;
                Utils::getValue<int>(option1, "Option: ", "Invalid Option!\n", false, false, cin, cout);
                out << endl;
                i = 1;
                for (map<string, Corps*>::const_iterator it = SolarSystem::supportCorpsTypes.begin(); it != SolarSystem::supportCorpsTypes.end(); ++it) {
                    if (i == option1) {
                        Corps* corps = it->second->clone();
                        in>>*corps;
                        if (this->addBody(*corps)) {
                            out << corps << endl;
                            out << "Body Added" << endl;
                        } else {
                            out << "Body Not Added" << endl;
                        }
                        delete corps;
                        break;
                    }
                    i++;
                }

                break;
            }
            case 2:
            {
                int i = 1;
                for (vector<Corps*>::const_iterator it = this->bodys.begin(); it != this->bodys.end(); ++it, i++) {
                    out << i << "- " << *it << endl;
                }
                out << "0-Back" << endl;
                Utils::getValue<int>(option1, "Option: ", "Invalid Option!\n", false, false, cin, cout);
                out << endl;
                if (option1 != 0) {
                    if (this->removeBody(option1 - 1)) {
                        out << "Body Removed" << endl;
                    } else {
                        out << "Body Not Removed" << endl;
                    }
                }
                break;
            }
            case 3:
            {
                int j = 1;
                for (vector<Corps*>::const_iterator it = this->bodys.begin(); it != this->bodys.end(); ++it) {
                    out << j << "- " << *it << endl;
                    j++;
                }
                out << "0- Back" << endl;
                Utils::getValue<int>(option1, "Option: ", "Invalid Option!\n", false, false, cin, cout);
                out << endl;
                if (option1 != 0 && this->containsBody(option1 - 1)) {
                    in>>this->getBody(option1 - 1);
                    this->getBody(option1 - 1).writePointerStream(out);
                    out << ")" << endl << "Body Edited" << endl;
                }
                break;
            }
            case 0:
            {
                out << "Exit Bodys" << endl;
                break;
            }
            default:
            {
                out << "Invalid Option!" << endl;
                break;
            }

        }
    } while (option != 0);
}

ostream& operator<<(ostream &out, const SolarSystem &solarSystem) {
    solarSystem.writeStream(out);
    return out;
}

ostream& operator<<(ostream &out, const SolarSystem *solarSystem) {
    solarSystem->writePointerStream(out);
    out << ")";
    return out;
}

istream& operator>>(istream &in, SolarSystem &solarSystem) {
    solarSystem.readStream(in, cout);
    in.clear();
    return in;
}