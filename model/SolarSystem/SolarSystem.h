/**
 * Solar Sytstem
 * @file SolarSystem.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * Criado a 18 de Dezembro de 2014, 11:45 por Duarte Amorim <1130674@isep.ipp.pt>
 *  - Definição dos atribuitos e métodos de acesso e consulta;
 *  - Sobrecarga de Operadores
 *  - Validação de Corpo
 *  - Construtores
 * Modificado a 5 de Janeiro de 2015 por Ricardo Rodrigues <1120703@isep.ipp.pt>
 *  - Sobrecarga operador ==
 *  - Correcção método validate (incompleto para já)
 *  */

#ifndef SOLARSYSTEM_H
#define SOLARSYSTEM_H

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <iterator>

using namespace std;

#include "../../utils/ID/ID.h"
#include "../Star/Star.h"
#include "../Corps/Corps.h"
#include "../Orbit/Orbit.h"
#include "../GravitationalCorps/GravitationalCorps.h"
#include "../GravitationalCorps/GravitationalwithPropulsionCorps/GravitationalwithPropulsionCorps.h"
#include "../OrbitalCorps/OrbitalCorps.h"

class SolarSystem: public ID {
private:
    /* Nome do Sistema Solar */
    string name;
    /* Estrela do Sistema Solar*/
    Star star;
    /*Lista de Corpos */
    vector<Corps *> bodys;
public:
    /*  Valores Pré-Definidos */
    /*  Nome Pré-Definido */
    static string NAME;
    /* Estrela Pré-Definidoa */
    static Star STAR;
    /* Corpos Pré-Defenidos */
    static vector<Corps *> BODYS;
    /* Lista de Corpos Suportados*/
    static map<string,Corps*> supportCorpsTypes;

    /**
     * Construir Instância de Corpo(COMPLETO)
     */
    SolarSystem(string name = SolarSystem::NAME, Star star=SolarSystem::STAR,const vector<Corps*>& bodys =SolarSystem::BODYS);

    /**
     * Construir Instância do Sistema Solar(CÓPIA)
     * @param copy Instância de Sistema Solar a Cópiar
     */
    SolarSystem(const SolarSystem &solar);
    
    /**
     * Destrutor de uma Instância do Sistema Solar
     */
    ~SolarSystem();
    
    /**
     * Clone da Instância do Sistema Solar
     * @return Copia da Instância do Sistema Solar
     */
    virtual SolarSystem* clone() const;
    
    /**
     * Consultar Nome do Sistema Solar
     * @return Nome
     */
    string getName() const;
    
    /**
     *  Alterar Nome do Sistema Solar
     *  @param name Novo Nome
     */
    void setName(string name=SolarSystem::NAME);
    
    /**
     * Consultar Estrela do Sistema Solar
     * @return Estrela
     */
    Star getStar() const;
    
    /**
     * Consultar Estrela do Sistema Solar
     * @return Estrela
     */
    Star& getStar();
    
    /**
     * Alterar Estrela do Sistema Solar
     * @param star Nova Estrela
     */
    void setStar(Star star=SolarSystem::STAR);
    
     /**
     * Consultar a Lista de Corpos do Sistema Solar
     * @return Lista de Corpos
     */
    vector<Corps*> getBodys() const;
    
    /**
     * Consultar a Lista de Todos os Corpos do Sistema Solar
     * @return Lista de Todos os Corpos
     */
    vector<Corps*> getAllBodys() const;
    
     /**
     * Alterar Lista de Corpos do Sistema Solar
     * @param vbodys Nova Lista de Corpos
     */
    void setBodys(const vector<Corps*>& vbodys=SolarSystem::BODYS);

     /**
     * Verificar se esta Defenido
     * @return true, se estiver defenido, caso contrário retorna false
     */
    virtual bool defined() const;
    
     /**
     * Vereificae se foi Editado
     * @return true, se foi editado, caso contrário retorna false
     */
    virtual bool edited() const;
    
    /**
     * Validar Corpo
     * @return true se for um Sistema Solar válido, caso contrário retorna false
     */
    virtual bool validate() const;
    
    /**
     * Limpar Lista de Corpos do Sistema Solar
     */
    void clear();
    
     /**
     * Validar o Corpo no Sistema Solar
     * @param body Corpo a Verificar
     * @return Sucesso da Operação
     */
    virtual bool validateBody(const Corps& body) const;

    /**
     * Numero de Corpos no Sistema Solar
     * @return Numero de Corpos
     */
    long numberBodys() const;
    
    /**
     * Verificar se um Corpo esta Contido na Lista de Corpos do Sistema Solar
     * @param body Corpo a Verificar
     * @return true caso o Corpo esteja contido na Lista de Corpos, caso contrário retorna false.
     */
    bool containsBody(const Corps& body) const;
    
    /**
     * Verificar se um Corpo esta Contido na Lista de Todos os Corpos do Sistema Solar
     * @param body Corpo a Verificar
     * @return true caso o Corpo esteja contido na Lista de Todos os Corpos, caso contrário retorna false.
     */
    bool containsAllBody(const Corps& body) const;
    
    /**
     * Verificar se existe um Corpo na Lista de Corpos na Posição do Sistema Solar
     * @param pos Posição
     * @return true caso existe um Corpo na Lista de Corpos Orbitais na Posição, caso contrário retorna false.
     */
    bool containsBody(const int& pos) const;

    /**
     * Posição de um Corpo na Lista de Corpos do Sistema Solar
     * @param body Corpo a Procurar
     * @return Posição, ou -1 caso o Corpo não exista na Lista de Corpos Orbitais
     */
    int indexBody(const Corps& body) const;
    
     /**
     * Consultar um Corpo da Lista de Corpos do Sistema Solar
     * @param pos Posição do Corpo
     * @return Corpo
     */
    Corps& getBody(const int& pos) const;
    
    /**
     * Adicionar uma Lista de Corpos a Lista de Corpos do Sistema Solar
     * @param orbits Lista de Corpos 
     * @return Numero de Sucessos da Operação
     */
    int addBodys(const vector<Corps*>& bodys);
    
    /**
     * Adicionar um Corpo a Lista de Corpos do Sistema Solar
     * @param orbitalCorps Novo Corpo
     * @return Sucesso da Operação
     */
    bool addBody(const Corps& body);
    
    /**
     * Adicionar um Corpo a Lista de Corpos do Sistema Solar
     * @param pos Posição para Adicionar Corpo
     * @param orbitalCorps Novo Corpo
     * @return Sucesso da Operação
     */
    bool addBody(const int& pos, const Corps& body);
    
    /**
     * Remover um Corpo da Lista de Corpos Orbitais do Sistema Solar
     * @param pos Posição do Corpo Orbital a Remover
     * @return Sucesso da Operação
     */
    bool removeBody(const int& pos);
    
    /**
     * Remover Corpo da Lista de Corpos do Sistema Solar
     * @param orbitalCorps Corpo a Remover
     * @return Sucesso da Operação
     */
    bool removeBody(const Corps& body);

    /**
     * Consultar um Corpo da Lista de Corpos do Sistema Solar
     * @param pos Posição do Corpo
     * @return Corpo
     */
    Corps& operator[](const int& pos) const;
    
    /**
     * Adicionar um Corpo a Lista de Corpos do Sistema Solar
     * @param body Novo Corpo 
     * @return Sistema Solar
     */
    SolarSystem& operator+=(const Corps& body);
    
    /**
     * Remover um Corpo da Lista de Corpos do Sistema Solar
     * @param body Corpo
     * @return Sistema Solar 
     */
    SolarSystem& operator-=(const Corps& body);
    
    /**
     * Sobrecarga de Operador==(Comparação de Instâncias)
     * @param op Sistema Solar a Comparar
     * @return true se forem iguais, caso contrário retorna false
     */
    virtual bool operator==(const SolarSystem &op) const;
    
    /**
     * Representação Textual da Instância do Sistema Solar
     * @param out Stream de Output
     */
    virtual void writeStream(ostream& out) const;
    
    /**
     * Representação Textual do Apontador do Sistema Solar
     * @param out Stream de Output
     */
    virtual void writePointerStream(ostream& out) const;
    
    /**
     * Leitura Textual da Instância do Sistema Solar
     * @param in Stream de Input
     * @param out Stream de Output
     */
    virtual void readStream(istream &in, ostream& out);
    
     /**
     * Sobrecarga de Operador<<(StreamOUT)
     * @param out Stream de Output
     * @param solarSystem Sistema Solar para Output
     * @return StreamOUT
     */
    friend ostream& operator<<(ostream &out, const SolarSystem &solarSystem);
    
     /**
     * Sobrecarga de Operador<<*(StreamOUT)
     * @param out Stream de Output
     * @param solarSystem Sistema Solar para Output
     * @return StreamOUT
     */
    friend ostream& operator<<(ostream &out, const SolarSystem *solarSystem);

     /**
     * Sobrecarga de Operador>>(StreamIN)
     * @param out Stream de Input
     * @param solarSystem Sistema Solar para Input
     * @return StreamIN
     */
    friend istream& operator>>(istream &in, SolarSystem &solarSystem);
};

#endif /* SOLARSYSTEM_H */
