#include "Import.h"

vector<ImportPlanets *> Import::typesPlanets = [] {
    vector<ImportPlanets *> v;
    v.push_back(new ImportPlanetsCSV());
    return v;
}();

bool Import::importPlanets(string file, vector<OrbitalCorps*>& listPlanets) {
    string type = Utils::getExtension(file);
    for (vector<ImportPlanets *>::iterator it = Import::typesPlanets.begin(); it != Import::typesPlanets.end(); ++it) {
        if ((*it)->getType() == type) {
            return (*it)->import(file, listPlanets);
        }
    }
    return false;
};

