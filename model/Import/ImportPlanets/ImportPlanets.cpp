#include "ImportPlanets.h"

string ImportPlanets::TYPE = "csv";

ImportPlanets::ImportPlanets(string type) : type(type) {
}

ImportPlanets::ImportPlanets(const ImportPlanets& copy) : type(copy.type) {
}

ImportPlanets::~ImportPlanets() {
}

string ImportPlanets::getType() const {
    return this->type;
}

void ImportPlanets::setType(string type){
    this->type=type;
}

bool ImportPlanets::operator==(const ImportPlanets& op) const {
    return ( this == &op || this->type == op.type);
}