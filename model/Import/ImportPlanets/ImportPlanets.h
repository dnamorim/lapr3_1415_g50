#ifndef IMPORTPLANETS_H
#define	IMPORTPLANETS_H
/**
 * Importar Planetas(ESQUELETO)
 * @file ImportPlanets.h
 * 
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 * 
 * @version 1.0
 * Criado a 18 de Dezembro de 2014, 14:10 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Definição da Estrutura da Importação dos Planetas 
 */

#include <iostream>
#include <vector>
#include <fstream> 
#include <string>

using namespace std;
#include "../../Orbit/Orbit.h"
#include "../../OrbitalCorps/OrbitalCorps.h"

class ImportPlanets {
private:
    string type;
public:
    /* Válores Pré-Definidos*/
    /* Extenção do Tipo de Ficheiro Suportado pelo Importador */
    static string TYPE;
    /**
     * Construir Instância do Importar Planetas(COMPLETO)
     */
    ImportPlanets(string type=ImportPlanets::TYPE);
     /**
     * Construir Instância do Importar Planetas(CÓPIA)
     * @param copy Instância do Importar Planetas a Cópiar
     */
    ImportPlanets(const ImportPlanets& copy);
    /**
     * Destrotor da Instância do Importar Planetas
     */
    virtual ~ImportPlanets();
    
    /**
     * Clone da Instância do Importar Planetas
     * @return Copia da Instância do Importar Planetas
     */
    virtual ImportPlanets * clone() const = 0;
    
    /**
     * Consultar Tipo de Ficheiro Suportado pelo Importador
     * @return 
     */
    string getType() const;
    
    /**
     * Alterar Tipo de Ficheiro Suportado pelo Importador
     * @param type Novo Tipo de Ficheiro
     */
    void setType(string type=ImportPlanets::TYPE);
    
    /**
     * Importar Lista de Planetas
     * @param file Ficheiro para Importar os Planetas
     * @param listPlanets Lista com os Planetas Importados
     * @return Sucesso da Operação
     */
    virtual bool import(string file, vector<OrbitalCorps*>& listPlanets) const = 0;
    
     /**
     * Sobrecarga de Operador==(Comparação de Instâncias)
     * @param op Importar os Planetas a Comparar
     * @return true se forem iguais, caso contrário retorna false
     */
    virtual bool operator==(const ImportPlanets& op) const;
};

#endif	/* IMPORTPLANETS_H */

