#ifndef IMPORTPLANETSCSV_H
#define	IMPORTPLANETSCSV_H
/**
 * Importar Planetas (CSV)
 * @file ImportPlanetsCSV.h
 * 
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 * 
 * @version 1.1
 * >Criado a 18 de Dezembro de 2014, 16:20 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Definição da Estrutura da Importação dos Planetas 
 * >Modificado a 2 de Janeiro de 2015, 14,57 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Tipos de Unidades
 *  -Planetas com Orbitas
 *
 * Modificado a 5 de Janeiro de 2015, 21:24 por Duarte Amorim <1130674@isep.ipp.pt>
 *  - Troca da função stof pela atof(str.c_str()) para compatibilidade de compiladores pré std C++11 (MinGW e libstdC++)
 */

#include <vector>
#include <algorithm>
#include <string>
#include <stdlib.h>     

using namespace std;

#include "ImportPlanets.h"
#include "../../../utils/Utils.h"

class ImportPlanetsCSV : public ImportPlanets {
public:
    /**
     * Construir Instância do Importar CSV Planetas(COMPLETO)
     */
    ImportPlanetsCSV(string type="csv");
    
    /**
     * Construir Instância do Importar Planetas CSV(CÓPIA)
     * @param copy Instância do Importar Planetas CSV a Cópiar
     */
    ImportPlanetsCSV(const ImportPlanetsCSV& copy);
    
    /**
     * Destrotor da Instância do Importar Planetas
     */
    virtual ~ImportPlanetsCSV();

    /**
     * Clone da Instância do Importar Planetas do Tipo CSV
     * @return Copia da Instância do Importar Planetas
     */
    virtual ImportPlanets * clone() const;

    /**
     * Importar Lista de Planetas
     * @param file Ficheiro CSV para Importar os Planetas
     * @param listPlanets Lista com os Planetas Importados
     * @return Sucesso da Operação
     */
    virtual bool import(string file, vector<OrbitalCorps*>& listPlanets) const;
};

#endif	/* IMPORTPLANETSCSV_H */

