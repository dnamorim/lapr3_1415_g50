#include "ImportPlanetsCSV.h"

ImportPlanetsCSV::ImportPlanetsCSV(string type) : ImportPlanets(type) {
}

ImportPlanetsCSV::ImportPlanetsCSV(const ImportPlanetsCSV& copy) : ImportPlanets(copy) {
}

ImportPlanetsCSV::~ImportPlanetsCSV() {
}

ImportPlanets * ImportPlanetsCSV::clone() const {
    return new ImportPlanetsCSV(*this);
}

bool ImportPlanetsCSV::import(string file, vector<OrbitalCorps*>& listPlanets) const {
    ifstream in;
    vector<OrbitalCorps*> list_orbits(listPlanets);
    vector<int> pos_not_orbiting_the_star;
    in.open(file.c_str());
    if (in.is_open()) {
        string buffer;
        while (getline(in, buffer, '\r')) {
            vector<string> line = Utils::split(buffer, ';');

            for (unsigned long i = list_orbits.size(); i < line.size() - 1; i++) {
                list_orbits.push_back(new OrbitalCorps());
            }

            line[0] = Utils::removeAll(line[0]);

            string command = line[0].substr(0, line[0].find_first_of("("));
            transform(command.begin(), command.end(), command.begin(), ::tolower);

            string unit = Utils::getUnit(line[0]);

            if (command == "orbiting") {
                for (int i = 1; i < line.size(); i++) {
                    for (vector<OrbitalCorps*>::iterator it = list_orbits.begin(); it != list_orbits.end(); ++it) {
                        if ((*it)->getName() == line[i]) {
                            if ((*it)->getOrbit().addOrbit(*list_orbits[i - 1])) {
                                list_orbits[i - 1] = &(*it)->getOrbit().getOrbit((int) (*it)->getOrbit().numberOrbits() - 1);
                                pos_not_orbiting_the_star.push_back(i - 1);
                                break;
                            }
                        }
                    }
                }

            } else if (command == "characteristics") {
                for (int i = 1; i < line.size(); i++) {
                    list_orbits[i - 1]->setName(line[i]);
                }
            } else if (command == "referencedate") {
                for (int i = 1; i < line.size(); i++) {
                    list_orbits[i - 1]->getReferenceDate() = line[i];
                }
            } else {
                for (int i = 1; i < line.size(); i++) {
                    replace(line[i].begin(),line[i].end(),',','.');
                }
                if (command == "periodofrevolution") {
                    for (int i = 1; i < line.size(); i++) {
                        list_orbits[i - 1]->setPeriod(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "orbitalspeedm/s" || command == "orbitalspeed") {
                    for (int i = 1; i < line.size(); i++) {
                        list_orbits[i - 1]->setOrbitalSpeed(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "inclinationofaxistoorbit") {
                    for (int i = 1; i < line.size(); i++) {
                        list_orbits[i - 1]->setInclinationAxisToOribit(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "equatorialdiameter") {
                    for (int i = 1; i < line.size(); i++) {
                        list_orbits[i - 1]->setDiameter(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "mass") {
                    for (int i = 1; i < line.size(); i++) {
                        list_orbits[i - 1]->setMass(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "density") {
                    for (int i = 1; i < line.size(); i++) {
                        list_orbits[i - 1]->setDensity(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "escapevelocity,equator") {
                    for (int i = 1; i < line.size(); i++) {
                        list_orbits[i - 1]->setEscapeVelocity(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "semimajoraxis") {
                    for (int i = 1; i < line.size(); i++) {
                        list_orbits[i - 1]->setSemimajorAxis(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "orbiteccentricity") {
                    for (int i = 1; i < line.size(); i++) {
                        list_orbits[i - 1]->setEccentricityOrbit(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }

                } else if (command == "orbitinclination") {
                    for (int i = 1; i < line.size(); i++) { //A conversão garante que o valor fica em degrees?
                        list_orbits[i - 1]->setInclinationOrbit(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "perihelion") {
                    for (int i = 1; i < line.size(); i++) {
                        list_orbits[i - 1]->setPerihelion(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "aphelion") {
                    for (int i = 1; i < line.size(); i++) {
                        list_orbits[i - 1]->setAphelion(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "px") {
                    for (int i = 1; i < line.size(); i++) {
                        list_orbits[i - 1]->getCoordinates().setX(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "py") {
                    for (int i = 1; i < line.size(); i++) {
                        list_orbits[i - 1]->getCoordinates().setY(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "pz") {
                    for (int i = 1; i < line.size(); i++) {
                        list_orbits[i - 1]->getCoordinates().setZ(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "vx") {
                    for (int i = 1; i < line.size(); i++) {
                        list_orbits[i - 1]->getSpeed().setX(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "vy") {
                    for (int i = 1; i < line.size(); i++) {
                        list_orbits[i - 1]->getSpeed().setY(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "vz") {
                    for (int i = 1; i < line.size(); i++) {
                        list_orbits[i - 1]->getSpeed().setZ(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "longitudeofperihelion") {
                    for (int i = 1; i < line.size(); i++) { //A conversão garante que o fica adimensional?
                        list_orbits[i - 1]->setLongitudePerihelion(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "longitudeofascendingnode") {
                    for (int i = 1; i < line.size(); i++) { //A conversão garante que o fica adimensional?
                        list_orbits[i - 1]->setLongitudeAscendingNode(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                } else if (command == "meananomaly") {
                    for (int i = 1; i < line.size(); i++) { //A conversão garante que o valor fica em degrees?
                        list_orbits[i - 1]->setMeanAnomaly(Unit::ConvertToSI(atof(line[i].c_str()), unit));
                    }
                }
            }
        }
        for (int i = 0; i < list_orbits.size(); i++) {
            if (!(find(listPlanets.begin(), listPlanets.end(), list_orbits[i]) != listPlanets.end())
                    && !(find(pos_not_orbiting_the_star.begin(), pos_not_orbiting_the_star.end(), i) != pos_not_orbiting_the_star.end())) {
                listPlanets.push_back(list_orbits[i]);
            }
        }
        return true;
    }
    return false;
}