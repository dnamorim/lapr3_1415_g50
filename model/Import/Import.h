#ifndef IMPORT_H
#define	IMPORT_H
/**
 * Importar
 * @file Import.h
 * 
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 * 
 * @version 1.0
 * >Criado a 3 de Janeiro de 2015, 16:20 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Definição da Estrutura da Importação
 */

#include <vector>
#include <string>

using namespace std;
#include "../../utils/Utils.h"
#include "../OrbitalCorps/OrbitalCorps.h"

#include "ImportPlanets/ImportPlanets.h"
#include "ImportPlanets/ImportPlanetsCSV.h"

class Import {
public:
    /* Lita de Tipos de Importação de Planetas Suportados para Importar*/
    static vector<ImportPlanets *> typesPlanets;
     /**
     * Importar Lista de Planetas
     * @param file Ficheiro CSV para Importar os Planetas
     * @param listPlanets Lista com os Planetas Importados
     * @return Sucesso da Operação
     */
    static bool importPlanets(string file, vector<OrbitalCorps*>& listPlanets);
};

#endif	/* IMPORT_H */

