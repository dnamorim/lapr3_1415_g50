#include "Export.h"

vector<ExportSimulation *> Export::typesSimulations = [] {
    vector<ExportSimulation *> v;
    v.push_back(new ExportSimulationCSV());
    v.push_back(new ExportSimulationHTML());
    v.push_back(new ExportSimulationGIF());
    return v;
}();

bool Export::exportSimulation(string file, const Simulation& simulation){
    string type = Utils::getExtension(file);
    for (vector<ExportSimulation *>::iterator it = Export::typesSimulations.begin(); it != Export::typesSimulations.end(); ++it) {
        if ((*it)->getType() == type) {
            Simulation s = simulation;
            return (*it)->expor(file, s);
        }
    }
    return false;
};

