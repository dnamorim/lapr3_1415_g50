#include "ExportSimulationHTML.h"

ExportSimulationHTML::ExportSimulationHTML(string type):ExportSimulation(type) {
}

ExportSimulationHTML::ExportSimulationHTML(const ExportSimulationHTML& copy):ExportSimulation(copy) {
}

ExportSimulationHTML::~ExportSimulationHTML() {
}

ExportSimulation * ExportSimulationHTML::clone() const{
    return new ExportSimulationHTML(*this);
}

bool ExportSimulationHTML::expor(string file, const Simulation& simulation) const{
    ofstream out;
    out.open(file.c_str());
    if (out.is_open()) {
        
        vector<Corps*> corps = simulation.getCorps();
        out << "<!DOCTYPE html><html><body>";
        out << "<img src='http://www.dei.isep.ipp.pt/images/topo_index.png' style='width:100%'>";
         out << "<h2 style='text-align:center;'>LAPR3 Solar System Simulator (G50)</h2>";
        out << "<table style='width:100%;text-align:center;' border=1>";
        
        out << "<tr>";
        out << "<th>Simulation Name</th>";
        out << "<td colspan='"<<((simulation.numberCorps()+1)*3)<<"'>"<<simulation.getName()<<"</td>";
        out << "</tr>";
        
        out << "<tr>";
        out << "<th>Solar System</th>";
        out << "<td colspan='"<<((simulation.numberCorps()+1)*3)<<"'>"<<&simulation.getSolarSystem() <<"</td>";
        out << "</tr>";
        
        out << "<tr>";
        out << "<th>Start Time</th>";
        out << "<td colspan='"<<((simulation.numberCorps()+1)*3)<<"'>"<<simulation.getStartTime() <<"</td>";
        out << "</tr>";
        
        out << "<tr>";
        out << "<th>Time Step</th>";
        out << "<td colspan='"<<((simulation.numberCorps()+1)*3)<<"'>"<<simulation.getTimeStep() <<"</td>";
        out << "</tr>";
        
        out << "<tr>";
        out << "<th>End Time</th>";
        out << "<td colspan='"<<((simulation.numberCorps()+1)*3)<<"'>"<<simulation.getEndTime() <<"</td>";
        out << "</tr>";
        
        out << "<tr>";
        out << "<th>RUN</th>";
        out << "<td colspan='"<<((simulation.numberCorps()+1)*3)<<"'>"<<((simulation.alreadyRun())?"YES":"NO") <<"</td>";
        out << "</tr>";
        
    
        out << "<tr>";
        out << "<th colspan='"<<((simulation.numberCorps()+1)*3)+1<<"'>"<<"Corps"<<"</th>";
        out << "</tr>";
        for(vector<Corps*>::const_iterator it=corps.begin();it!=corps.end();++it){
            out << "<tr>";
            out << "<td colspan='"<<((simulation.numberCorps()+1)*3)+1<<"'>"<<*it <<"</td>";
            out << "</tr>";
        }
        if(simulation.alreadyRun()){
            out << "<tr>";
            out << "<th colspan='"<<((simulation.numberCorps()+1)*3)+1<<"'>"<<"SIMULATION"<<"</th>";
            out << "</tr>";
        
            out << "<tr>";
            out << "<th>Corps</th>";
            out << "<td colspan='3'>Star</td>";
            int i=1;
            for(vector<Corps*>::const_iterator it=corps.begin();it!=corps.end();++it,i++){
                out << "<td colspan='3' title='"<< (*it) <<"'>" << (*it)->getName() << "</td>";
            }
            out << "</tr>";
            out << "<tr>";
            out << "<th>Time</th>";
            for(int i=0; i<simulation.numberCorps()+1;i++){
                out << "<th>Px</th><th>Py</th><th>Pz</th>";
            }
            out << "</tr>";
            vector<Timer> vtimer = simulation.getSteps();
            int size = (int) vtimer.size();
            for (int i=0; i < size; i++) {
                out << "<tr>";
                out << "<th>" << vtimer[i] << "</th>";
                vector<XYZ> position = simulation.getPositions(i);
                for(vector<XYZ>::const_iterator itp=position.begin();itp!=position.end();++itp,i++){
                    out << "<th>"<< Unit::convertToAU(itp->getX())<<"</th><th>"<<Unit::convertToAU(itp->getY())<<"</th><th>"<<Unit::convertToAU(itp->getZ())<<"</th>";
                }
                out << "</tr>";
            }
        }
        out << "</table>";
       
        out <<"</body></html>";
        return true;
    }
    return false;
}