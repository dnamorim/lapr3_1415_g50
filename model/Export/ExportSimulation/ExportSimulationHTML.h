#ifndef EXPORTSIMULATIONHTML_H
#define	EXPORTSIMULATIONHTML_H

/**
 * Exportar Simulador (HTML)
 * @file ExportSimulationHTML.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 13 de Janeiro de 2015, 17:27 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Definição da Estrutura da Exportador Simulador em HTML
 *
 */

#include <string>

using namespace std;

#include "ExportSimulation.h"

class ExportSimulationHTML : public ExportSimulation {
public:
    /**
     * Construir Instância do ExportSimulationHTML(COMPLETO)
     */
    ExportSimulationHTML(string type="html");
    
    /**
     * Construir Instância do ExportSimulationHTML(CÓPIA)
     * @param copy Instância do ExportSimulation a Cópiar
     */
    ExportSimulationHTML(const ExportSimulationHTML& copy);
    /**
     * Destrotor da Instância do ExportSimulationHTML
     */
    virtual ~ExportSimulationHTML();
    
    /**
     * Clone da Instância do ExportSimulationHTML
     * @return Copia da Instância do ExportSimulationHTML
     */
    virtual ExportSimulation * clone() const;
    
    /**
     * Exportar
     * @param file Ficheiro para Exportar o Simulador em Formato HTML
     * @param simulation Simulador
     * @return Sucesso da Operação
     */
    virtual bool expor(string file, const Simulation& simulation) const;
    
};

#endif	/* EXPORTSIMULATIONHTML_H */

