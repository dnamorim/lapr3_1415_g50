#include "ExportSimulationGIF.h"
#include "../../../utils/Gif/gif.h"
#include "../../../utils/Gif/bwobjects.h"

int ExportSimulationGIF::width = 800;
int ExportSimulationGIF::height = 600;
int ExportSimulationGIF::escale = 1e9;
double ExportSimulationGIF::delay=0.02;
bool ExportSimulationGIF::legend=true;
perspective ExportSimulationGIF::angle = XY;
bw ExportSimulationGIF::sun ={64,64, sun_bw_object};
bw ExportSimulationGIF::corps={16,16, corps_bw_object};
bw ExportSimulationGIF::alfanum={273,9, text_bw_object};
rgba ExportSimulationGIF::color_background = {0,0,0, 255};
rgba ExportSimulationGIF::color_sun = {255, 215, 0, 255};
rgba ExportSimulationGIF::color_corps = {160,82,45, 255};
rgba ExportSimulationGIF::color_corps_text = {112,128,144};

rgba ExportSimulationGIF::color_line = {255, 255, 255, 255};
rgba ExportSimulationGIF::color_text = {255, 255, 255, 255};

ExportSimulationGIF::ExportSimulationGIF(string type):ExportSimulation(type) {
}

ExportSimulationGIF::ExportSimulationGIF(const ExportSimulationGIF& copy):ExportSimulation(copy) {
}

ExportSimulationGIF::~ExportSimulationGIF() {
}

ExportSimulation * ExportSimulationGIF::clone() const{
    return new ExportSimulationGIF(*this);
}

void ExportSimulationGIF::posAngle(int& x,int &y, const XYZ& coor, perspective angle){
    if(angle==YZ){
        x = (int) (coor.getY() / escale)+(width / 2);
        y = (int) (coor.getZ() / escale)+(height / 2);
    }else if(angle==XY){
        x = (int) (coor.getX() / escale)+(width / 2);
        y = (int) (coor.getZ() / escale)+(height / 2);
    }else{
        x = (int) (coor.getX() / escale)+(width / 2);
        y = (int) (coor.getY() / escale)+(height / 2);
    }
}

void ExportSimulationGIF::colorBackground(uint8_t * frame,rgba color,int width, int height) {
    for (int i = 0; i < (width * height * 4); i += 4) {
        frame[i] = color.red;
        frame[i + 1] = color.green;
        frame[i + 2] = color.blue;
        frame[i + 3] = color.alfa;
    }
}

void ExportSimulationGIF::draw(uint8_t * frame,bw * obj,int centerx, int centery, rgba color,int width, int height){
    int pos, x, y;
    int ch = obj->height;
    int cw = obj->width;
    for(int i=0; i<ch; i++){
        for(int j=0; j<cw; j++){
            if(obj->print[i*cw+j] == false){
                x = (centerx-1) - (cw/2) + j;
                y = (centery-1) - (ch/2) + i;
                if(x > 0 && x < width && y > 0 && y < height){
                    pos = 4 * (y*width+x);
                    frame[pos] = color.red;
                    frame[pos+1] = color.green;
                    frame[pos+2] = color.blue;
                    frame[pos+3] = color.alfa;
                }
            }
        }
    }
}

void ExportSimulationGIF::drawText(uint8_t * frame,string text, int posx, int posy,rgba color,int width, int height){
    int ximg, pos, x, y;
    for(int c=0; c<text.length(); c++){
        if( text[c] >= 65 && text[c] <= 90 ) ximg = (text[c]-65)*7;
        else if( text[c] >= 97 && text[c] <= 122 ) ximg = (text[c]-97)*7;
        else if( text[c] >= 48 && text[c] <= 57 ) ximg = (text[c]-48)*7 + 182;
        else if( text[c] == 45 || text[c] == '/' || text[c] == ':') ximg = 252;
        else if( text[c] == 32) ximg = 266;
        else ximg = 259;
        
        for(int i=0; i<alfanum.height; i++){
            for(int j=0; j<7; j++){
                if(alfanum.print[i*alfanum.width+(j+ximg)] == false){
                    x = posx+j+(7*c);
                    y = posy+i;
                    if(x > 0 && x < width && y > 0 && y < height){
                        pos = 4 * (y*width+x);
                        frame[pos] = color.red;
                        frame[pos+1] = color.green;
                        frame[pos+2] = color.blue;
                        frame[pos+3] = color.alfa;
                    }
                }
            }
        }
        
    }
}

void ExportSimulationGIF::drawCorps(uint8_t * frame,vector<XYZ>& position, vector<Corps*>& corpos,perspective angle,rgba color,rgba color_text,int width, int height, int escale,bw * obj,bool legend) {
    int x, y;
    for (int i = 1; i < position.size(); i++) {
        ExportSimulationGIF::posAngle(x,y, position[i], angle);
        if (x >= 0 && x < width && y >= 0 && y < height) {
            ExportSimulationGIF::draw(frame,obj,x,y,color,width,height);
            if(legend){
                ExportSimulationGIF::drawText(frame,corpos[i-1]->getName(),x+15,y-6,color_text,ExportSimulationGIF::width,ExportSimulationGIF::height);
            }
        }
    }
    ExportSimulationGIF::posAngle(x,y, position[0], angle);
    ExportSimulationGIF::draw(frame, &ExportSimulationGIF::sun, x,y,ExportSimulationGIF::color_sun,ExportSimulationGIF::width,ExportSimulationGIF::height);
}

void ExportSimulationGIF::drowDashedCorps(uint8_t * frame,vector< vector<XYZ> > positions,int stop,perspective angle,rgba color,int width, int height, int escale) {
    int x, y, pos;
    for (int k = 1; k < positions[0].size(); k++) {
        for (double i = 0; i <= stop; i++) {
            ExportSimulationGIF::posAngle(x,y, positions[i][k], angle);
            if (x >= 0 && x < width && y >= 0 && y < height) {
                pos = 4 * (y*width+x);
                frame[pos] = color.red;
                frame[pos+1] = color.green;
                frame[pos+2] = color.blue;
                frame[pos+3] = color.alfa;
            }
        }
        
    }
}

bool ExportSimulationGIF::expor(string file, const Simulation& simulation) const{
    if(simulation.alreadyRun()){
        GifWriter * gw = new GifWriter;
        GifBegin(gw, file.c_str(), ExportSimulationGIF::width, ExportSimulationGIF::height, ExportSimulationGIF::delay);
        uint8_t * frame=new uint8_t[ExportSimulationGIF::width * ExportSimulationGIF::height * 4];
        ExportSimulationGIF::colorBackground(frame,ExportSimulationGIF::color_background);
        GifWriteFrame(gw, frame, ExportSimulationGIF::width, ExportSimulationGIF::height, ExportSimulationGIF::delay);
        int pos=0;
        vector<Timer> times=simulation.getSteps();
        for(vector<Timer>::const_iterator itt=times.begin();itt!=times.end();++itt,pos++){
            vector<XYZ> position = simulation.getPositions(pos);
            //LIMPAR FUNDO
            ExportSimulationGIF::colorBackground(frame,ExportSimulationGIF::color_background);
            //NOME DA SIMULAÇÃO
            if(ExportSimulationGIF::legend){
                ExportSimulationGIF::drawText(frame,"Simulation - "+simulation.getName(),15,15,color_text,ExportSimulationGIF::width,ExportSimulationGIF::height);
                stringstream time;
                time << *itt;
                ExportSimulationGIF::drawText(frame,time.str(),15,30,color_text,ExportSimulationGIF::width,ExportSimulationGIF::height);
                if(ExportSimulationGIF::angle==XZ){
                    ExportSimulationGIF::drawText(frame,"Axes XZ",15,45,color_text,ExportSimulationGIF::width,ExportSimulationGIF::height);
                }else if(ExportSimulationGIF::angle==YZ){
                    ExportSimulationGIF::drawText(frame,"Axes YZ",15,30,color_text,ExportSimulationGIF::width,ExportSimulationGIF::height);
                }else if(ExportSimulationGIF::angle==XY){
                    ExportSimulationGIF::drawText(frame,"Axes XY",15,45,color_text,ExportSimulationGIF::width,ExportSimulationGIF::height);
                }
            }
            //RASTO DOS CORPOS
            vector<vector<XYZ>> positions=simulation.getPositions();
            ExportSimulationGIF::drowDashedCorps(frame,positions,pos,ExportSimulationGIF::angle,ExportSimulationGIF::color_line,ExportSimulationGIF::width,ExportSimulationGIF::height,ExportSimulationGIF::escale);
            //CORPOS
            vector<Corps*> vcorps = simulation.getCorps();
            ExportSimulationGIF::drawCorps(frame,position, vcorps,ExportSimulationGIF::angle,ExportSimulationGIF::color_corps,ExportSimulationGIF::color_corps_text,ExportSimulationGIF::width,ExportSimulationGIF::height,ExportSimulationGIF::escale,&ExportSimulationGIF::corps,ExportSimulationGIF::legend);
            GifWriteFrame(gw, frame, ExportSimulationGIF::width, ExportSimulationGIF::height, ExportSimulationGIF::delay);
        }
        GifEnd(gw);
        return true;
    }
    return false;
}









