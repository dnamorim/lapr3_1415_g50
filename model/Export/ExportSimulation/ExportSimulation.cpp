#include "ExportSimulation.h"

string ExportSimulation::TYPE = "csv";

ExportSimulation::ExportSimulation(string type) : type(type) {
}

ExportSimulation::ExportSimulation(const ExportSimulation& copy) : type(copy.type) {
}

ExportSimulation::~ExportSimulation() {
}

string ExportSimulation::getType() const {
    return this->type;
}

void ExportSimulation::setType(string type){
    this->type=type;
}

bool ExportSimulation::operator==(const ExportSimulation& op) const {
    return ( this == &op || this->type == op.type);
}