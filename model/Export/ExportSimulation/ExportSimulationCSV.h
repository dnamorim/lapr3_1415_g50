#ifndef EXPORTSIMULATIONCSV_H
#define	EXPORTSIMULATIONCSV_H
/**
 * Exportar Simulador (CSV)
 * @file ExportSimulationCSV.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 13 de Janeiro de 2015, 17:27 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Definição da Estrutura da Exportador Simulador em CSV
 *
 */

#include <string>

using namespace std;

#include "ExportSimulation.h"

class ExportSimulationCSV : public ExportSimulation {
public:
    /**
     * Construir Instância do ExportSimulationCSV(COMPLETO)
     */
    ExportSimulationCSV(string type="csv");
    
    /**
     * Construir Instância do ExportSimulationCSV(CÓPIA)
     * @param copy Instância do ExportSimulation a Cópiar
     */
    ExportSimulationCSV(const ExportSimulationCSV& copy);
    /**
     * Destrotor da Instância do ExportSimulationCSV
     */
    virtual ~ExportSimulationCSV();
    
    /**
     * Clone da Instância do ExportSimulationCSV
     * @return Copia da Instância do ExportSimulationCSV
     */
    virtual ExportSimulation * clone() const;
    
    /**
     * Exportar
     * @param file Ficheiro para Exportar o Simulador em Formato CSV
     * @param simulation Simulador
     * @return Sucesso da Operação
     */
    virtual bool expor(string file, const Simulation& simulation) const;
    
};


#endif	/* EXPORTSIMULATIONCSV_H */

