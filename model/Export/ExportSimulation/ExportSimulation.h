#ifndef EXPORTSIMULATION_H
#define	EXPORTSIMULATION_H
/**
 * Exportar Simulação(ESQUELETO)
 * @file ExportSimulation.h
 * 
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 * 
 * @version 1.0
 * Criado a 13 de Janeiro de 2015, 17:14 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Definição da Estrutura da Exportador Simulador
 */

#include <fstream> 
#include <string>

using namespace std;

#include "../../Simulation/Simulation.h"

class ExportSimulation {
public:
private:
    /* Tipo do Suportado de Simulação*/
    string type;
public:
    /* Válores Pré-Definidos*/
    /* Extenção do Tipo de Ficheiro Suportado pelo Exportador de Simulador */
    static string TYPE;
    
    /**
     * Construir Instância do ExportSimulation(COMPLETO)
     */
    ExportSimulation(string type=ExportSimulation::TYPE);
    
     /**
     * Construir Instância do ExportSimulation(CÓPIA)
     * @param copy Instância do ExportSimulation a Cópiar
     */
    ExportSimulation(const ExportSimulation& copy);
    /**
     * Destrotor da Instância do ExportSimulation
     */
    virtual ~ExportSimulation();
    
    /**
     * Clone da Instância do ExportSimulation
     * @return Copia da Instância do ExportSimulation
     */
    virtual ExportSimulation * clone() const = 0;
    
    /**
     * Consultar Tipo de Ficheiro Suportado pelo Exportador de Simulador
     * @return Tipo Suportado
     */
    string getType() const;
    
    /**
     * Alterar Tipo de Ficheiro Suportado pelo Exportador de Simulador
     * @param type Novo Tipo de Ficheiro
     */
    void setType(string type=ExportSimulation::TYPE);
    
    /**
     * Exportar 
     * @param file Ficheiro para Exportar o Simulador
     * @param simulation Simulador
     * @return Sucesso da Operação
     */
    virtual bool expor(string file, const Simulation& simulation) const = 0;
    
     /**
     * Sobrecarga de Operador==(Comparação de Instâncias)
     * @param op ExportSimulation a Comparar
     * @return true se forem iguais, caso contrário retorna false
     */
    virtual bool operator==(const ExportSimulation& op) const;
private:

};

#endif	/* EXPORTSIMULATION_H */

