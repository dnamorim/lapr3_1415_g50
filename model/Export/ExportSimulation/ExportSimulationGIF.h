#ifndef EXPORTSIMULATIONGIF_H
#define	EXPORTSIMULATIONGIF_H
/**
 * Exportar Simulador (GIF)
 * @file ExportSimulationGIF.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 13 de Janeiro de 2015, 17:29 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Definição da Estrutura da Exportador Simulador em GIF
 *
 */

#include <string>

using namespace std;

#include "ExportSimulation.h"
#include "../../../utils/Gif/rgba-bw.h"

class ExportSimulationGIF : public ExportSimulation {
protected:
    static void posAngle(int& x,int &y, const XYZ& coor, perspective angle=ExportSimulationGIF::angle);
    static void colorBackground(uint8_t * frame,rgba color=ExportSimulationGIF::color_background,int width=ExportSimulationGIF::width, int height=ExportSimulationGIF::height);
    
    static void draw(uint8_t * frame,bw * obj,int centerx=0, int centery=0, rgba color=ExportSimulationGIF::color_corps,int width=ExportSimulationGIF::width, int height=ExportSimulationGIF::height);
    
    static void drawText(uint8_t * frame,string text, int posx, int posy,rgba color=ExportSimulationGIF::color_text,int width=ExportSimulationGIF::width, int height=ExportSimulationGIF::height);
    static void drawCorps(uint8_t * frame,vector<XYZ>& position, vector<Corps*>& corpos,perspective angle=ExportSimulationGIF::angle,rgba color=ExportSimulationGIF::color_corps,rgba color_text=ExportSimulationGIF::color_corps_text,int width=ExportSimulationGIF::width, int height=ExportSimulationGIF::height,int escale=ExportSimulationGIF::escale,bw * obj=&ExportSimulationGIF::corps,bool legend=ExportSimulationGIF::legend);
    
    static void drowDashedCorps(uint8_t * frame,vector< vector<XYZ> > positions,int stop,perspective angle,rgba color,int width, int height, int escale);
public:
    static int width;
    static double delay;
    static int height;
    static int escale;
    static perspective angle;
    static bool legend;
    static bw sun;
    static bw corps;
    static bw alfanum;
    static rgba color_background;
    static rgba color_sun;
    static rgba color_corps;
    static rgba color_corps_text;
    static rgba color_line;
    static rgba color_text;
    /**
     * Construir Instância do ExportSimulationGIF(COMPLETO)
     */
    ExportSimulationGIF(string type="gif");
    
    /**
     * Construir Instância do ExportSimulationGIF(CÓPIA)
     * @param copy Instância do ExportSimulation a Cópiar
     */
    ExportSimulationGIF(const ExportSimulationGIF& copy);
    /**
     * Destrotor da Instância do ExportSimulationGIF
     */
    virtual ~ExportSimulationGIF();
    
    /**
     * Clone da Instância do ExportSimulationGIF
     * @return Copia da Instância do ExportSimulationGIF
     */
    virtual ExportSimulation * clone() const;
    
    
    /**
     * Exportar
     * @param file Ficheiro para Exportar o Simulador em Formato GIF
     * @param simulation Simulador
     * @return Sucesso da Operação
     */
    virtual bool expor(string file, const Simulation& simulation) const;
    
};

#endif	/* EXPORTSIMULATIONGIF_H */

