#include "ExportSimulationCSV.h"

ExportSimulationCSV::ExportSimulationCSV(string type):ExportSimulation(type) {
}

ExportSimulationCSV::ExportSimulationCSV(const ExportSimulationCSV& copy):ExportSimulation(copy) {
}

ExportSimulationCSV::~ExportSimulationCSV() {
}

ExportSimulation * ExportSimulationCSV::clone() const{
    return new ExportSimulationCSV(*this);
}

bool ExportSimulationCSV::expor(string file, const Simulation& simulation) const{
    ofstream out;
    out.open(file.c_str());
    if (out.is_open()) {
        out << "Simulation;" << simulation.getName() << endl;
        out << "Solar System: " << &simulation.getSolarSystem() << endl;
        out << "Star Time;" << simulation.getStartTime() << endl;
        out << "Time Step;" << simulation.getTimeStep() << endl;
        out << "End Time;" << simulation.getEndTime() << endl;
        out << "RUN;" << ((simulation.alreadyRun())?"YES":"NO") << endl;
        vector<Corps*> corps = simulation.getCorps();
        out << "Corps (" << corps.size() << ")" << endl;
        int i=1;
        for(vector<Corps*>::const_iterator it=corps.begin();it!=corps.end();++it,i++){
            out << i << ";" << *it << ";" << endl;
        }
        if(simulation.alreadyRun()){
            out << "Corps;Star;;;";
            int i=1;
            for(vector<Corps*>::const_iterator it=corps.begin();it!=corps.end();++it,i++){
                out << "(" <<i << ")" << (*it)->getName() << ";;;";
            }
            out << endl;
            out << "Time;";
            for(i=0; i<simulation.numberCorps()+1;i++){
                out << "Px;Py;Pz;";
            }
            out << endl;
            for (int pos = 0; pos < simulation.getSteps().size(); pos++) {
                out << simulation.getSteps()[pos] << ";";
                vector<XYZ> position = simulation.getPositions(pos);
                for(vector<XYZ>::const_iterator itp=position.begin();itp!=position.end();++itp){
                    out << Unit::convertToAU(itp->getX()) << ";" << Unit::convertToAU(itp->getY()) << ";" << Unit::convertToAU(itp->getZ()) << ";";
                }
                out << endl;
            }
        }
        return true;
    }
    
    return false;
}