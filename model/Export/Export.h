#ifndef EXPORT_H
#define	EXPORT_H
/**
 * Exportar
 * @file Import.h
 * 
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 * 
 * @version 1.0
 * >Criado a 13 de Janeiro de 2015, 17:12 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Definição da Estrutura da Exportação
 */

#include <vector>
#include <string>

using namespace std;

#include "../Simulation/Simulation.h"
#include "ExportSimulation/ExportSimulation.h"
#include "ExportSimulation/ExportSimulationCSV.h"
#include "ExportSimulation/ExportSimulationHTML.h"
#include "ExportSimulation/ExportSimulationGIF.h"

class Export {
public:
    /* Lita de Tipos de Exportação de Simuladores Suportados para Exportador */
    static vector<ExportSimulation *> typesSimulations;
     /**
     * Exportar Simulador
     * @param file Ficheiro para Importar os Planetas
     * @param simulation Simulador
     * @return Sucesso da Operação
     */
    static bool exportSimulation(string file, const Simulation& simulation);
};


#endif	/* EXPORT_H */

