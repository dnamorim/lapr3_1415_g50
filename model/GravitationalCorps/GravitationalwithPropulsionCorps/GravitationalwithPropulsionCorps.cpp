#include "GravitationalwithPropulsionCorps.h"

double GravitationalwithPropulsionCorps::PROPULSION = 0;

GravitationalwithPropulsionCorps::GravitationalwithPropulsionCorps(double mass, double diameter, double density, double propulsion, XYZ coordinates, XYZ speed, string name,Timer referenceDate,Source source,const vector<OrbitalCorps*>& orbitBodys) : GravitationalCorps(mass, diameter, density, coordinates, speed, name, referenceDate,source, orbitBodys), propulsion(propulsion) {

}

GravitationalwithPropulsionCorps::GravitationalwithPropulsionCorps(const GravitationalwithPropulsionCorps& copy) : GravitationalCorps(copy), propulsion(copy.propulsion) {

}

GravitationalwithPropulsionCorps::~GravitationalwithPropulsionCorps() {

}

Corps* GravitationalwithPropulsionCorps::clone() const {
    return new GravitationalwithPropulsionCorps(*this);
}

double GravitationalwithPropulsionCorps::getPropulsion() const {
    return this->propulsion;
}

void GravitationalwithPropulsionCorps::setPropulsion(double propulsion) {
    this->propulsion = propulsion;
    this->edit = true;
}

bool GravitationalwithPropulsionCorps::validate() const {
    return (GravitationalCorps::validate() && this->propulsion >= 0);
}

bool GravitationalwithPropulsionCorps::operator==(const GravitationalwithPropulsionCorps& op) const {
    return (GravitationalCorps::operator==(op) && this->propulsion == op.propulsion);
}

void GravitationalwithPropulsionCorps::writePointerStream(ostream& out) const{
    GravitationalCorps::writePointerStream(out);
    out << "," << this->propulsion;
}

void GravitationalwithPropulsionCorps::writeStream(ostream& out) const {
    GravitationalCorps::writeStream(out);
    out << "Propulsion: " << this->propulsion << endl;
}

void GravitationalwithPropulsionCorps::readStream(istream &in, ostream& out) {
    GravitationalCorps::readStream(in, out);
    Utils::getValue<double>(this->propulsion, "Propulsion: ", "Kept Value Propulsion\n", true, true, in, out);
}