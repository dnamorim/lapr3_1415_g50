#ifndef GRAVITATIONALWITHPROPULSIONCORPS_H
#define	GRAVITATIONALWITHPROPULSIONCORPS_H
/**
 * Representação de um Corpo Gravítico com Propulsão
 * @file GravitationalwithPropulsionCorps.h
 * 
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 * 
 * @version 1.0
 * Criado a 14 de Dezembro de 2014, 21:03 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Defenição de Atributos e Métodos de Acesso/Consulta
 *  -Sobrecarga de Operadores ==;<<;>> 
 *  -Validação de Corpo Gravítico com Propulsão
 */

#include <iostream>

using namespace std;

#include "../GravitationalCorps.h"

class GravitationalwithPropulsionCorps : public GravitationalCorps {
private:
    /* Propulsão do Corpo Gravítico com Propulsão*/
    double propulsion;
public:
    /* Válores Pré-Definidos*/
    /* Propulsão Pré-Definida */
    static double PROPULSION;

    /**
     * Construir Instância de Corpo Gravítico com Propulsão(COMPLETO)
     */
    GravitationalwithPropulsionCorps(double mass = Corps::MASS, double diameter = Corps::DIAMETER, double density = Corps::DENSITY, double propulsion = GravitationalwithPropulsionCorps::PROPULSION, XYZ coordinates= Corps::COORDINATES,XYZ speed = Corps::SPEED,string name = Corps::NAME,  Timer referenceDate=Corps::REFERENCEDATE,Source source = Corps::SOURCE,const vector<OrbitalCorps*>& orbitBodys=Corps::ORBITBODYS);

    /**
     * Construir Instância de Corpo Gravítico com Propulsão(CÓPIA)
     * @param copy Instância de Corpo Gravítico com Propulsão a Cópiar
     */
    GravitationalwithPropulsionCorps(const GravitationalwithPropulsionCorps& copy);

    /**
     * Destrotor da Instância de Corpo Gravítico com Propulsão
     */
    virtual ~GravitationalwithPropulsionCorps();

    /**
     * Clone da Instância do Corpo Gravítico com Propulsão
     * @return Copia da Instância do Corpo Gravítico com Propulsão
     */
    virtual Corps* clone() const;

    /**
     * Consultar Propulsão do Corpo Gravítico com Propulsão
     * @return Propulsão
     */
    double getPropulsion() const;

    /**
     * Altear Propulsão do Corpo Gravítico com Propulsão
     * @param propulsion Nova Propulsão
     */
    void setPropulsion(double propulsion = GravitationalwithPropulsionCorps::PROPULSION);

    /**
     * Validar Corpo Gravítico com Propulsão
     * @return true se for um corpo válido, caso contrário retorna false
     */
    virtual bool validate() const;

    /**
     * Sobrecarga de Operador==(Comparação de Instâncias)
     * @param op Corpo Gravítico com Propulsão a Comparar
     * @return true se forem iguais, caso contrário retorna false
     */
    virtual bool operator==(const GravitationalwithPropulsionCorps& op) const;

    /**
     * Representação Textual do Apontador do Corpo Gravítico com Propulsão
     * @param out Stream de Output
     */
    virtual void writePointerStream(ostream& out) const;
    
    /**
     * Representação Textual da Instância do Corpo Gravítico com Propulsão
     * @param out Stream de Output
     */
    virtual void writeStream(ostream& out) const;

    /**
     * Leitura Textual da Instância do Corpo Gravítico com Propulsão
     * @param in Stream de Input
     * @param out Stream de Output
     */
    virtual void readStream(istream &in, ostream& out);


};

#endif	/* GRAVITATIONALWITHPROPULSIONCORPS_H */