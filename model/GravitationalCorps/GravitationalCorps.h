#ifndef GRAVITATIONALCORPS_H
#define	GRAVITATIONALCORPS_H
/**
 * Representação de um Corpo Gravítico 
 * @file GravitationalCorps.h
 * 
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 * 
 * @version 1.0
 * Criado a 14 de Dezembro de 2014, 20:10 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Defenição de Atributos e Métodos de Acesso/Consulta
 *  -Sobrecarga de Operadores ==;<<;>> 
 *  -Validação de Corpo Gravítico
 */

#include <iostream>

using namespace std;

#include "../Corps/Corps.h"

class GravitationalCorps : public Corps {
private:
public:
    /**
     * Construir Instância de Corpo Gravítico(COMPLETO)
     */
    GravitationalCorps(double mass = Corps::MASS, double diameter = Corps::DIAMETER, double density = Corps::DENSITY, XYZ coordinates = Corps::COORDINATES, XYZ speed = Corps::SPEED, string name = Corps::NAME,  Timer referenceDate=Corps::REFERENCEDATE,Source source = Corps::SOURCE, const vector<OrbitalCorps*>& orbitBodys=Corps::ORBITBODYS);

    /**
     * Construir Instância de Corpo Gravítico(CÓPIA)
     * @param copy Instância de Corpo Gravítico a Cópiar
     */
    GravitationalCorps(const GravitationalCorps& copy);

    /**
     * Destrotor da Instância de Corpo Gravítico
     */
    virtual ~GravitationalCorps();

    /**
     * Clone da Instância do Corpo Gravítico
     * @return Copia da Instância do Corpo Gravítico
     */
    virtual Corps* clone() const;

    /**
     * Validar Corpo Gravítico
     * @return true se for um corpo válido, caso contrário retorna false
     */
    virtual bool validate() const;

    /**
     * Sobrecarga de Operador==(Comparação de Instâncias)
     * @param op Corpo Gravítico a Comparar
     * @return true se forem iguais, caso contrário retorna false
     */
    virtual bool operator==(const GravitationalCorps& op) const;

    /**
     * Representação Textual do Apontador do Corpo Gravítico com Propulsão
     * @param out Stream de Output
     */
    virtual void writePointerStream(ostream& out) const;

    /**
     * Representação Textual da Instância do Corpo Gravítico
     * @param out Stream de Output
     */
    virtual void writeStream(ostream& out) const;

    /**
     * Leitura Textual da Instância do Corpo Gravítico
     * @param in Stream de Input
     * @param out Stream de Output
     */
    virtual void readStream(istream &in, ostream& out);

};

#endif	/* GRAVITATIONALCORPS_H */