#include "GravitationalCorps.h"

GravitationalCorps::GravitationalCorps(double mass, double diameter, double density, XYZ coordinates, XYZ speed, string name,  Timer referenceDate,Source source,const vector<OrbitalCorps*>& orbitBodys) : Corps(mass, diameter, density, coordinates, speed, name,referenceDate,source,orbitBodys) {

}

GravitationalCorps::GravitationalCorps(const GravitationalCorps& copy) : Corps(copy) {

}

GravitationalCorps::~GravitationalCorps() {

}

Corps* GravitationalCorps::clone() const {
    return new GravitationalCorps(*this);
}

bool GravitationalCorps::validate() const{
    return Corps::validate();
}

bool GravitationalCorps::operator==(const GravitationalCorps& op) const {
    return Corps::operator==(op);
}

void GravitationalCorps::writePointerStream(ostream& out) const{
    Corps::writePointerStream(out);
}

void GravitationalCorps::writeStream(ostream& out) const {
    Corps::writeStream(out);
}

void GravitationalCorps::readStream(istream &in, ostream& out) {
    Corps::readStream(in, out);
}