#** README ** #

This is the Git Repository of the Group 50 for the development of the LAPR3 project of 2014/2015.

### The Project ###

* Solar System simulator.

### How to set up and use the simulator? ###

* In the future, it will be provided instructions about the set up and use of this simulator.

### Group ###

* [André Ferreira](https://bitbucket.org/1120243) ([1120243@isep.ipp.pt](mailto:1120243@isep.ipp.pt))
* [Duarte Amorim](https://bitbucket.org/1130674) ([1130674@isep.ipp.pt](mailto:1130674@isep.ipp.pt))
* [Gilberto Pereira](https://bitbucket.org/1131251) ([1131251@isep.ipp.pt](mailto:1131251@isep.ipp.pt))
* [Ricardo Rodrigues](https://bitbucket.org/1120703) ([1120703@isep.ipp.pt](mailto:1120703@isep.ipp.pt))
* [Sara Freitas](https://bitbucket.org/1130489) ([1130489@isep.ipp.pt](mailto:1130489@isep.ipp.pt))

### Supervisor ###

* [Prof. Luís Nogueira](https://bitbucket.org/luisnogueira) ([lmn@isep.ipp.pt](mailto:lmn@isep.ipp.pt))

© LAPR3_G50, LEI-ISEP, 2014-2015