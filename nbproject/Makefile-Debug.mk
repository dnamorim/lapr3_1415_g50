#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-MacOSX
CND_DLIB_EXT=dylib
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/controller/Controller.o \
	${OBJECTDIR}/controller/CopySimulationController/CopySimulationController.o \
	${OBJECTDIR}/controller/CopySolarSystemController/CopySolarSystemController.o \
	${OBJECTDIR}/controller/CreateNewSimulationController/CreateNewSimulationController.o \
	${OBJECTDIR}/controller/CreateSolarSystemController/CreateSolarSystemController.o \
	${OBJECTDIR}/controller/DeleteSimulationController/DeleteSimulationController.o \
	${OBJECTDIR}/controller/EditSimulationController/EditSimulationController.o \
	${OBJECTDIR}/controller/EditSolarSystemController/EditSolarSystemController.o \
	${OBJECTDIR}/controller/FilterResultsSimulationController/FilterResultsSimulationController.o \
	${OBJECTDIR}/controller/RunSimulationController/RunSimulationController.o \
	${OBJECTDIR}/controller/SelectSolarSystemController/SelectSolarSystemController.o \
	${OBJECTDIR}/controller/SimulationResultsController/SimulationResultsController.o \
	${OBJECTDIR}/datalayer/DataLayer.o \
	${OBJECTDIR}/datalayer/DataLayerMockObjects/DataLayerMockObjects.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/model/Corps/Corps.o \
	${OBJECTDIR}/model/Export/Export.o \
	${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulation.o \
	${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationCSV.o \
	${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationGIF.o \
	${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationHTML.o \
	${OBJECTDIR}/model/GravitationalCorps/GravitationalCorps.o \
	${OBJECTDIR}/model/GravitationalCorps/GravitationalwithPropulsionCorps/GravitationalwithPropulsionCorps.o \
	${OBJECTDIR}/model/Import/Import.o \
	${OBJECTDIR}/model/Import/ImportPlanets/ImportPlanets.o \
	${OBJECTDIR}/model/Import/ImportPlanets/ImportPlanetsCSV.o \
	${OBJECTDIR}/model/Orbit/Orbit.o \
	${OBJECTDIR}/model/OrbitalCorps/OrbitalCorps.o \
	${OBJECTDIR}/model/Simulation/Simulation.o \
	${OBJECTDIR}/model/SolarSystem/SolarSystem.o \
	${OBJECTDIR}/model/Star/Star.o \
	${OBJECTDIR}/model/Unit/Unit.o \
	${OBJECTDIR}/ui/CopySimulationUI/CopySimulationUI.o \
	${OBJECTDIR}/ui/CopySolarSystemUI/CopySolarSystemUI.o \
	${OBJECTDIR}/ui/CreateNewSimulationUI/CreateNewSimulationUI.o \
	${OBJECTDIR}/ui/CreateSolarSystemUI/CreateSolarSystemUI.o \
	${OBJECTDIR}/ui/DeleteSimulationUI/DeleteSimulationUI.o \
	${OBJECTDIR}/ui/EditSimulationUI/EditSimulationUI.o \
	${OBJECTDIR}/ui/EditSolarSystemUI/EditSolarSystemUI.o \
	${OBJECTDIR}/ui/FilterResultsSimulationUI/FilterResultsSimulationUI.o \
	${OBJECTDIR}/ui/MainUI/MainUI.o \
	${OBJECTDIR}/ui/RunSimulationUI/RunSimulationUI.o \
	${OBJECTDIR}/ui/SelectSolarSystemUI/SelectSolarSystemUI.o \
	${OBJECTDIR}/ui/SimulationResultsUI/SimulationResultsUI.o \
	${OBJECTDIR}/ui/UI.o \
	${OBJECTDIR}/utils/ID/ID.o \
	${OBJECTDIR}/utils/Timer/Timer.o \
	${OBJECTDIR}/utils/Utils.o \
	${OBJECTDIR}/utils/XYZ/XYZ.o

# Test Directory
TESTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}/tests

# Test Files
TESTFILES= \
	${TESTDIR}/TestFiles/f6 \
	${TESTDIR}/TestFiles/f8 \
	${TESTDIR}/TestFiles/f9 \
	${TESTDIR}/TestFiles/f5 \
	${TESTDIR}/TestFiles/f1 \
	${TESTDIR}/TestFiles/f7 \
	${TESTDIR}/TestFiles/f2 \
	${TESTDIR}/TestFiles/f10 \
	${TESTDIR}/TestFiles/f11 \
	${TESTDIR}/TestFiles/f4

# C Compiler Flags
CFLAGS=`cppunit-config --cflags` 

# CC Compiler Flags
CCFLAGS=`cppunit-config --cflags` 
CXXFLAGS=`cppunit-config --cflags` 

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=`cppunit-config --libs` `cppunit-config --libs` `cppunit-config --libs` `cppunit-config --libs` `cppunit-config --libs` `cppunit-config --libs` `cppunit-config --libs` `cppunit-config --libs` `cppunit-config --libs`  

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${TESTDIR}/TestFiles/f3

${TESTDIR}/TestFiles/f3: ${OBJECTFILES}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f3 ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/controller/Controller.o: controller/Controller.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/Controller.o controller/Controller.cpp

${OBJECTDIR}/controller/CopySimulationController/CopySimulationController.o: controller/CopySimulationController/CopySimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/CopySimulationController
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/CopySimulationController/CopySimulationController.o controller/CopySimulationController/CopySimulationController.cpp

${OBJECTDIR}/controller/CopySolarSystemController/CopySolarSystemController.o: controller/CopySolarSystemController/CopySolarSystemController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/CopySolarSystemController
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/CopySolarSystemController/CopySolarSystemController.o controller/CopySolarSystemController/CopySolarSystemController.cpp

${OBJECTDIR}/controller/CreateNewSimulationController/CreateNewSimulationController.o: controller/CreateNewSimulationController/CreateNewSimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/CreateNewSimulationController
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/CreateNewSimulationController/CreateNewSimulationController.o controller/CreateNewSimulationController/CreateNewSimulationController.cpp

${OBJECTDIR}/controller/CreateSolarSystemController/CreateSolarSystemController.o: controller/CreateSolarSystemController/CreateSolarSystemController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/CreateSolarSystemController
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/CreateSolarSystemController/CreateSolarSystemController.o controller/CreateSolarSystemController/CreateSolarSystemController.cpp

${OBJECTDIR}/controller/DeleteSimulationController/DeleteSimulationController.o: controller/DeleteSimulationController/DeleteSimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/DeleteSimulationController
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/DeleteSimulationController/DeleteSimulationController.o controller/DeleteSimulationController/DeleteSimulationController.cpp

${OBJECTDIR}/controller/EditSimulationController/EditSimulationController.o: controller/EditSimulationController/EditSimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/EditSimulationController
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/EditSimulationController/EditSimulationController.o controller/EditSimulationController/EditSimulationController.cpp

${OBJECTDIR}/controller/EditSolarSystemController/EditSolarSystemController.o: controller/EditSolarSystemController/EditSolarSystemController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/EditSolarSystemController
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/EditSolarSystemController/EditSolarSystemController.o controller/EditSolarSystemController/EditSolarSystemController.cpp

${OBJECTDIR}/controller/FilterResultsSimulationController/FilterResultsSimulationController.o: controller/FilterResultsSimulationController/FilterResultsSimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/FilterResultsSimulationController
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/FilterResultsSimulationController/FilterResultsSimulationController.o controller/FilterResultsSimulationController/FilterResultsSimulationController.cpp

${OBJECTDIR}/controller/RunSimulationController/RunSimulationController.o: controller/RunSimulationController/RunSimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/RunSimulationController
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/RunSimulationController/RunSimulationController.o controller/RunSimulationController/RunSimulationController.cpp

${OBJECTDIR}/controller/SelectSolarSystemController/SelectSolarSystemController.o: controller/SelectSolarSystemController/SelectSolarSystemController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/SelectSolarSystemController
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/SelectSolarSystemController/SelectSolarSystemController.o controller/SelectSolarSystemController/SelectSolarSystemController.cpp

${OBJECTDIR}/controller/SimulationResultsController/SimulationResultsController.o: controller/SimulationResultsController/SimulationResultsController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/SimulationResultsController
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/SimulationResultsController/SimulationResultsController.o controller/SimulationResultsController/SimulationResultsController.cpp

${OBJECTDIR}/datalayer/DataLayer.o: datalayer/DataLayer.cpp 
	${MKDIR} -p ${OBJECTDIR}/datalayer
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/datalayer/DataLayer.o datalayer/DataLayer.cpp

${OBJECTDIR}/datalayer/DataLayerMockObjects/DataLayerMockObjects.o: datalayer/DataLayerMockObjects/DataLayerMockObjects.cpp 
	${MKDIR} -p ${OBJECTDIR}/datalayer/DataLayerMockObjects
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/datalayer/DataLayerMockObjects/DataLayerMockObjects.o datalayer/DataLayerMockObjects/DataLayerMockObjects.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/model/Corps/Corps.o: model/Corps/Corps.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Corps
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Corps/Corps.o model/Corps/Corps.cpp

${OBJECTDIR}/model/Export/Export.o: model/Export/Export.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Export
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Export/Export.o model/Export/Export.cpp

${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulation.o: model/Export/ExportSimulation/ExportSimulation.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Export/ExportSimulation
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulation.o model/Export/ExportSimulation/ExportSimulation.cpp

${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationCSV.o: model/Export/ExportSimulation/ExportSimulationCSV.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Export/ExportSimulation
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationCSV.o model/Export/ExportSimulation/ExportSimulationCSV.cpp

${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationGIF.o: model/Export/ExportSimulation/ExportSimulationGIF.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Export/ExportSimulation
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationGIF.o model/Export/ExportSimulation/ExportSimulationGIF.cpp

${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationHTML.o: model/Export/ExportSimulation/ExportSimulationHTML.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Export/ExportSimulation
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationHTML.o model/Export/ExportSimulation/ExportSimulationHTML.cpp

${OBJECTDIR}/model/GravitationalCorps/GravitationalCorps.o: model/GravitationalCorps/GravitationalCorps.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/GravitationalCorps
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/GravitationalCorps/GravitationalCorps.o model/GravitationalCorps/GravitationalCorps.cpp

${OBJECTDIR}/model/GravitationalCorps/GravitationalwithPropulsionCorps/GravitationalwithPropulsionCorps.o: model/GravitationalCorps/GravitationalwithPropulsionCorps/GravitationalwithPropulsionCorps.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/GravitationalCorps/GravitationalwithPropulsionCorps
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/GravitationalCorps/GravitationalwithPropulsionCorps/GravitationalwithPropulsionCorps.o model/GravitationalCorps/GravitationalwithPropulsionCorps/GravitationalwithPropulsionCorps.cpp

${OBJECTDIR}/model/Import/Import.o: model/Import/Import.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Import
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Import/Import.o model/Import/Import.cpp

${OBJECTDIR}/model/Import/ImportPlanets/ImportPlanets.o: model/Import/ImportPlanets/ImportPlanets.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Import/ImportPlanets
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Import/ImportPlanets/ImportPlanets.o model/Import/ImportPlanets/ImportPlanets.cpp

${OBJECTDIR}/model/Import/ImportPlanets/ImportPlanetsCSV.o: model/Import/ImportPlanets/ImportPlanetsCSV.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Import/ImportPlanets
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Import/ImportPlanets/ImportPlanetsCSV.o model/Import/ImportPlanets/ImportPlanetsCSV.cpp

${OBJECTDIR}/model/Orbit/Orbit.o: model/Orbit/Orbit.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Orbit
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Orbit/Orbit.o model/Orbit/Orbit.cpp

${OBJECTDIR}/model/OrbitalCorps/OrbitalCorps.o: model/OrbitalCorps/OrbitalCorps.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/OrbitalCorps
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/OrbitalCorps/OrbitalCorps.o model/OrbitalCorps/OrbitalCorps.cpp

${OBJECTDIR}/model/Simulation/Simulation.o: model/Simulation/Simulation.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Simulation
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Simulation/Simulation.o model/Simulation/Simulation.cpp

${OBJECTDIR}/model/SolarSystem/SolarSystem.o: model/SolarSystem/SolarSystem.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/SolarSystem
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/SolarSystem/SolarSystem.o model/SolarSystem/SolarSystem.cpp

${OBJECTDIR}/model/Star/Star.o: model/Star/Star.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Star
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Star/Star.o model/Star/Star.cpp

${OBJECTDIR}/model/Unit/Unit.o: model/Unit/Unit.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Unit
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Unit/Unit.o model/Unit/Unit.cpp

${OBJECTDIR}/ui/CopySimulationUI/CopySimulationUI.o: ui/CopySimulationUI/CopySimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/CopySimulationUI
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/CopySimulationUI/CopySimulationUI.o ui/CopySimulationUI/CopySimulationUI.cpp

${OBJECTDIR}/ui/CopySolarSystemUI/CopySolarSystemUI.o: ui/CopySolarSystemUI/CopySolarSystemUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/CopySolarSystemUI
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/CopySolarSystemUI/CopySolarSystemUI.o ui/CopySolarSystemUI/CopySolarSystemUI.cpp

${OBJECTDIR}/ui/CreateNewSimulationUI/CreateNewSimulationUI.o: ui/CreateNewSimulationUI/CreateNewSimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/CreateNewSimulationUI
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/CreateNewSimulationUI/CreateNewSimulationUI.o ui/CreateNewSimulationUI/CreateNewSimulationUI.cpp

${OBJECTDIR}/ui/CreateSolarSystemUI/CreateSolarSystemUI.o: ui/CreateSolarSystemUI/CreateSolarSystemUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/CreateSolarSystemUI
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/CreateSolarSystemUI/CreateSolarSystemUI.o ui/CreateSolarSystemUI/CreateSolarSystemUI.cpp

${OBJECTDIR}/ui/DeleteSimulationUI/DeleteSimulationUI.o: ui/DeleteSimulationUI/DeleteSimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/DeleteSimulationUI
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/DeleteSimulationUI/DeleteSimulationUI.o ui/DeleteSimulationUI/DeleteSimulationUI.cpp

${OBJECTDIR}/ui/EditSimulationUI/EditSimulationUI.o: ui/EditSimulationUI/EditSimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/EditSimulationUI
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/EditSimulationUI/EditSimulationUI.o ui/EditSimulationUI/EditSimulationUI.cpp

${OBJECTDIR}/ui/EditSolarSystemUI/EditSolarSystemUI.o: ui/EditSolarSystemUI/EditSolarSystemUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/EditSolarSystemUI
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/EditSolarSystemUI/EditSolarSystemUI.o ui/EditSolarSystemUI/EditSolarSystemUI.cpp

${OBJECTDIR}/ui/FilterResultsSimulationUI/FilterResultsSimulationUI.o: ui/FilterResultsSimulationUI/FilterResultsSimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/FilterResultsSimulationUI
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/FilterResultsSimulationUI/FilterResultsSimulationUI.o ui/FilterResultsSimulationUI/FilterResultsSimulationUI.cpp

${OBJECTDIR}/ui/MainUI/MainUI.o: ui/MainUI/MainUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/MainUI
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/MainUI/MainUI.o ui/MainUI/MainUI.cpp

${OBJECTDIR}/ui/RunSimulationUI/RunSimulationUI.o: ui/RunSimulationUI/RunSimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/RunSimulationUI
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/RunSimulationUI/RunSimulationUI.o ui/RunSimulationUI/RunSimulationUI.cpp

${OBJECTDIR}/ui/SelectSolarSystemUI/SelectSolarSystemUI.o: ui/SelectSolarSystemUI/SelectSolarSystemUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/SelectSolarSystemUI
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/SelectSolarSystemUI/SelectSolarSystemUI.o ui/SelectSolarSystemUI/SelectSolarSystemUI.cpp

${OBJECTDIR}/ui/SimulationResultsUI/SimulationResultsUI.o: ui/SimulationResultsUI/SimulationResultsUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/SimulationResultsUI
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/SimulationResultsUI/SimulationResultsUI.o ui/SimulationResultsUI/SimulationResultsUI.cpp

${OBJECTDIR}/ui/UI.o: ui/UI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/UI.o ui/UI.cpp

${OBJECTDIR}/utils/ID/ID.o: utils/ID/ID.cpp 
	${MKDIR} -p ${OBJECTDIR}/utils/ID
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/utils/ID/ID.o utils/ID/ID.cpp

${OBJECTDIR}/utils/Timer/Timer.o: utils/Timer/Timer.cpp 
	${MKDIR} -p ${OBJECTDIR}/utils/Timer
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/utils/Timer/Timer.o utils/Timer/Timer.cpp

${OBJECTDIR}/utils/Utils.o: utils/Utils.cpp 
	${MKDIR} -p ${OBJECTDIR}/utils
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/utils/Utils.o utils/Utils.cpp

${OBJECTDIR}/utils/XYZ/XYZ.o: utils/XYZ/XYZ.cpp 
	${MKDIR} -p ${OBJECTDIR}/utils/XYZ
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/utils/XYZ/XYZ.o utils/XYZ/XYZ.cpp

# Subprojects
.build-subprojects:

# Build Test Targets
.build-tests-conf: .build-conf ${TESTFILES}
${TESTDIR}/TestFiles/f6: ${TESTDIR}/tests/CorpsTest/CorpsTestClass.o ${TESTDIR}/tests/CorpsTest/CorpsTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f6 $^ ${LDLIBSOPTIONS} 

${TESTDIR}/TestFiles/f8: ${TESTDIR}/tests/GravitationalCorpsTest/GravitationalCorpsTestClass.o ${TESTDIR}/tests/GravitationalCorpsTest/GravitionalCorpsTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f8 $^ ${LDLIBSOPTIONS} `cppunit-config --libs`   

${TESTDIR}/TestFiles/f9: ${TESTDIR}/tests/GravitationalWithPropulsionCorpsTestClass/GravitationalWithPropulsionCorpsTestClass.o ${TESTDIR}/tests/GravitationalWithPropulsionCorpsTestClass/GravitationalWithPropulsionCorpsTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f9 $^ ${LDLIBSOPTIONS} `cppunit-config --libs`   

${TESTDIR}/TestFiles/f5: ${TESTDIR}/tests/IDTest/IDTestClass.o ${TESTDIR}/tests/IDTest/IDTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f5 $^ ${LDLIBSOPTIONS} `cppunit-config --libs`   

${TESTDIR}/TestFiles/f1: ${TESTDIR}/tests/OrbitalCorpsTest/OrbitalCorpsTestClass.o ${TESTDIR}/tests/OrbitalCorpsTest/OrbitalCorpsTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f1 $^ ${LDLIBSOPTIONS} `cppunit-config --libs`   

${TESTDIR}/TestFiles/f7: ${TESTDIR}/tests/OrbitTest/OrbitTestClass.o ${TESTDIR}/tests/OrbitTest/OrbitTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f7 $^ ${LDLIBSOPTIONS} `cppunit-config --libs`   

${TESTDIR}/TestFiles/f2: ${TESTDIR}/tests/StarTest/StarTestClass.o ${TESTDIR}/tests/StarTest/StarTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f2 $^ ${LDLIBSOPTIONS} `cppunit-config --libs`   

${TESTDIR}/TestFiles/f10: ${TESTDIR}/tests/TimerTest/TimerTestClass.o ${TESTDIR}/tests/TimerTest/TimerTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f10 $^ ${LDLIBSOPTIONS} `cppunit-config --libs`   

${TESTDIR}/TestFiles/f11: ${TESTDIR}/tests/UtilsTest/UtilsTestClass.o ${TESTDIR}/tests/UtilsTest/UtilsTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f11 $^ ${LDLIBSOPTIONS} `cppunit-config --libs`   

${TESTDIR}/TestFiles/f4: ${TESTDIR}/tests/XYZTest/XYZTestClass.o ${TESTDIR}/tests/XYZTest/XYZTestRunner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f4 $^ ${LDLIBSOPTIONS} `cppunit-config --libs` `cppunit-config --libs`   


${TESTDIR}/tests/CorpsTest/CorpsTestClass.o: tests/CorpsTest/CorpsTestClass.cpp 
	${MKDIR} -p ${TESTDIR}/tests/CorpsTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -I. -std=c++11 -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/CorpsTest/CorpsTestClass.o tests/CorpsTest/CorpsTestClass.cpp


${TESTDIR}/tests/CorpsTest/CorpsTestRunner.o: tests/CorpsTest/CorpsTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/CorpsTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -I. -std=c++11 -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/CorpsTest/CorpsTestRunner.o tests/CorpsTest/CorpsTestRunner.cpp


${TESTDIR}/tests/GravitationalCorpsTest/GravitationalCorpsTestClass.o: tests/GravitationalCorpsTest/GravitationalCorpsTestClass.cpp 
	${MKDIR} -p ${TESTDIR}/tests/GravitationalCorpsTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/GravitationalCorpsTest/GravitationalCorpsTestClass.o tests/GravitationalCorpsTest/GravitationalCorpsTestClass.cpp


${TESTDIR}/tests/GravitationalCorpsTest/GravitionalCorpsTestRunner.o: tests/GravitationalCorpsTest/GravitionalCorpsTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/GravitationalCorpsTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/GravitationalCorpsTest/GravitionalCorpsTestRunner.o tests/GravitationalCorpsTest/GravitionalCorpsTestRunner.cpp


${TESTDIR}/tests/GravitationalWithPropulsionCorpsTestClass/GravitationalWithPropulsionCorpsTestClass.o: tests/GravitationalWithPropulsionCorpsTestClass/GravitationalWithPropulsionCorpsTestClass.cpp 
	${MKDIR} -p ${TESTDIR}/tests/GravitationalWithPropulsionCorpsTestClass
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/GravitationalWithPropulsionCorpsTestClass/GravitationalWithPropulsionCorpsTestClass.o tests/GravitationalWithPropulsionCorpsTestClass/GravitationalWithPropulsionCorpsTestClass.cpp


${TESTDIR}/tests/GravitationalWithPropulsionCorpsTestClass/GravitationalWithPropulsionCorpsTestRunner.o: tests/GravitationalWithPropulsionCorpsTestClass/GravitationalWithPropulsionCorpsTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/GravitationalWithPropulsionCorpsTestClass
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/GravitationalWithPropulsionCorpsTestClass/GravitationalWithPropulsionCorpsTestRunner.o tests/GravitationalWithPropulsionCorpsTestClass/GravitationalWithPropulsionCorpsTestRunner.cpp


${TESTDIR}/tests/IDTest/IDTestClass.o: tests/IDTest/IDTestClass.cpp 
	${MKDIR} -p ${TESTDIR}/tests/IDTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/IDTest/IDTestClass.o tests/IDTest/IDTestClass.cpp


${TESTDIR}/tests/IDTest/IDTestRunner.o: tests/IDTest/IDTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/IDTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/IDTest/IDTestRunner.o tests/IDTest/IDTestRunner.cpp


${TESTDIR}/tests/OrbitalCorpsTest/OrbitalCorpsTestClass.o: tests/OrbitalCorpsTest/OrbitalCorpsTestClass.cpp 
	${MKDIR} -p ${TESTDIR}/tests/OrbitalCorpsTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/OrbitalCorpsTest/OrbitalCorpsTestClass.o tests/OrbitalCorpsTest/OrbitalCorpsTestClass.cpp


${TESTDIR}/tests/OrbitalCorpsTest/OrbitalCorpsTestRunner.o: tests/OrbitalCorpsTest/OrbitalCorpsTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/OrbitalCorpsTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/OrbitalCorpsTest/OrbitalCorpsTestRunner.o tests/OrbitalCorpsTest/OrbitalCorpsTestRunner.cpp


${TESTDIR}/tests/OrbitTest/OrbitTestClass.o: tests/OrbitTest/OrbitTestClass.cpp 
	${MKDIR} -p ${TESTDIR}/tests/OrbitTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/OrbitTest/OrbitTestClass.o tests/OrbitTest/OrbitTestClass.cpp


${TESTDIR}/tests/OrbitTest/OrbitTestRunner.o: tests/OrbitTest/OrbitTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/OrbitTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/OrbitTest/OrbitTestRunner.o tests/OrbitTest/OrbitTestRunner.cpp


${TESTDIR}/tests/StarTest/StarTestClass.o: tests/StarTest/StarTestClass.cpp 
	${MKDIR} -p ${TESTDIR}/tests/StarTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/StarTest/StarTestClass.o tests/StarTest/StarTestClass.cpp


${TESTDIR}/tests/StarTest/StarTestRunner.o: tests/StarTest/StarTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/StarTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/StarTest/StarTestRunner.o tests/StarTest/StarTestRunner.cpp


${TESTDIR}/tests/TimerTest/TimerTestClass.o: tests/TimerTest/TimerTestClass.cpp 
	${MKDIR} -p ${TESTDIR}/tests/TimerTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/TimerTest/TimerTestClass.o tests/TimerTest/TimerTestClass.cpp


${TESTDIR}/tests/TimerTest/TimerTestRunner.o: tests/TimerTest/TimerTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/TimerTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/TimerTest/TimerTestRunner.o tests/TimerTest/TimerTestRunner.cpp


${TESTDIR}/tests/UtilsTest/UtilsTestClass.o: tests/UtilsTest/UtilsTestClass.cpp 
	${MKDIR} -p ${TESTDIR}/tests/UtilsTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/UtilsTest/UtilsTestClass.o tests/UtilsTest/UtilsTestClass.cpp


${TESTDIR}/tests/UtilsTest/UtilsTestRunner.o: tests/UtilsTest/UtilsTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/UtilsTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/UtilsTest/UtilsTestRunner.o tests/UtilsTest/UtilsTestRunner.cpp


${TESTDIR}/tests/XYZTest/XYZTestClass.o: tests/XYZTest/XYZTestClass.cpp 
	${MKDIR} -p ${TESTDIR}/tests/XYZTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/XYZTest/XYZTestClass.o tests/XYZTest/XYZTestClass.cpp


${TESTDIR}/tests/XYZTest/XYZTestRunner.o: tests/XYZTest/XYZTestRunner.cpp 
	${MKDIR} -p ${TESTDIR}/tests/XYZTest
	${RM} "$@.d"
	$(COMPILE.cc) -g -Werror -std=c++11 `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/XYZTest/XYZTestRunner.o tests/XYZTest/XYZTestRunner.cpp


${OBJECTDIR}/controller/Controller_nomain.o: ${OBJECTDIR}/controller/Controller.o controller/Controller.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller
	@NMOUTPUT=`${NM} ${OBJECTDIR}/controller/Controller.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/Controller_nomain.o controller/Controller.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/controller/Controller.o ${OBJECTDIR}/controller/Controller_nomain.o;\
	fi

${OBJECTDIR}/controller/CopySimulationController/CopySimulationController_nomain.o: ${OBJECTDIR}/controller/CopySimulationController/CopySimulationController.o controller/CopySimulationController/CopySimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/CopySimulationController
	@NMOUTPUT=`${NM} ${OBJECTDIR}/controller/CopySimulationController/CopySimulationController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/CopySimulationController/CopySimulationController_nomain.o controller/CopySimulationController/CopySimulationController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/controller/CopySimulationController/CopySimulationController.o ${OBJECTDIR}/controller/CopySimulationController/CopySimulationController_nomain.o;\
	fi

${OBJECTDIR}/controller/CopySolarSystemController/CopySolarSystemController_nomain.o: ${OBJECTDIR}/controller/CopySolarSystemController/CopySolarSystemController.o controller/CopySolarSystemController/CopySolarSystemController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/CopySolarSystemController
	@NMOUTPUT=`${NM} ${OBJECTDIR}/controller/CopySolarSystemController/CopySolarSystemController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/CopySolarSystemController/CopySolarSystemController_nomain.o controller/CopySolarSystemController/CopySolarSystemController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/controller/CopySolarSystemController/CopySolarSystemController.o ${OBJECTDIR}/controller/CopySolarSystemController/CopySolarSystemController_nomain.o;\
	fi

${OBJECTDIR}/controller/CreateNewSimulationController/CreateNewSimulationController_nomain.o: ${OBJECTDIR}/controller/CreateNewSimulationController/CreateNewSimulationController.o controller/CreateNewSimulationController/CreateNewSimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/CreateNewSimulationController
	@NMOUTPUT=`${NM} ${OBJECTDIR}/controller/CreateNewSimulationController/CreateNewSimulationController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/CreateNewSimulationController/CreateNewSimulationController_nomain.o controller/CreateNewSimulationController/CreateNewSimulationController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/controller/CreateNewSimulationController/CreateNewSimulationController.o ${OBJECTDIR}/controller/CreateNewSimulationController/CreateNewSimulationController_nomain.o;\
	fi

${OBJECTDIR}/controller/CreateSolarSystemController/CreateSolarSystemController_nomain.o: ${OBJECTDIR}/controller/CreateSolarSystemController/CreateSolarSystemController.o controller/CreateSolarSystemController/CreateSolarSystemController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/CreateSolarSystemController
	@NMOUTPUT=`${NM} ${OBJECTDIR}/controller/CreateSolarSystemController/CreateSolarSystemController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/CreateSolarSystemController/CreateSolarSystemController_nomain.o controller/CreateSolarSystemController/CreateSolarSystemController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/controller/CreateSolarSystemController/CreateSolarSystemController.o ${OBJECTDIR}/controller/CreateSolarSystemController/CreateSolarSystemController_nomain.o;\
	fi

${OBJECTDIR}/controller/DeleteSimulationController/DeleteSimulationController_nomain.o: ${OBJECTDIR}/controller/DeleteSimulationController/DeleteSimulationController.o controller/DeleteSimulationController/DeleteSimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/DeleteSimulationController
	@NMOUTPUT=`${NM} ${OBJECTDIR}/controller/DeleteSimulationController/DeleteSimulationController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/DeleteSimulationController/DeleteSimulationController_nomain.o controller/DeleteSimulationController/DeleteSimulationController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/controller/DeleteSimulationController/DeleteSimulationController.o ${OBJECTDIR}/controller/DeleteSimulationController/DeleteSimulationController_nomain.o;\
	fi

${OBJECTDIR}/controller/EditSimulationController/EditSimulationController_nomain.o: ${OBJECTDIR}/controller/EditSimulationController/EditSimulationController.o controller/EditSimulationController/EditSimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/EditSimulationController
	@NMOUTPUT=`${NM} ${OBJECTDIR}/controller/EditSimulationController/EditSimulationController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/EditSimulationController/EditSimulationController_nomain.o controller/EditSimulationController/EditSimulationController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/controller/EditSimulationController/EditSimulationController.o ${OBJECTDIR}/controller/EditSimulationController/EditSimulationController_nomain.o;\
	fi

${OBJECTDIR}/controller/EditSolarSystemController/EditSolarSystemController_nomain.o: ${OBJECTDIR}/controller/EditSolarSystemController/EditSolarSystemController.o controller/EditSolarSystemController/EditSolarSystemController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/EditSolarSystemController
	@NMOUTPUT=`${NM} ${OBJECTDIR}/controller/EditSolarSystemController/EditSolarSystemController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/EditSolarSystemController/EditSolarSystemController_nomain.o controller/EditSolarSystemController/EditSolarSystemController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/controller/EditSolarSystemController/EditSolarSystemController.o ${OBJECTDIR}/controller/EditSolarSystemController/EditSolarSystemController_nomain.o;\
	fi

${OBJECTDIR}/controller/FilterResultsSimulationController/FilterResultsSimulationController_nomain.o: ${OBJECTDIR}/controller/FilterResultsSimulationController/FilterResultsSimulationController.o controller/FilterResultsSimulationController/FilterResultsSimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/FilterResultsSimulationController
	@NMOUTPUT=`${NM} ${OBJECTDIR}/controller/FilterResultsSimulationController/FilterResultsSimulationController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/FilterResultsSimulationController/FilterResultsSimulationController_nomain.o controller/FilterResultsSimulationController/FilterResultsSimulationController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/controller/FilterResultsSimulationController/FilterResultsSimulationController.o ${OBJECTDIR}/controller/FilterResultsSimulationController/FilterResultsSimulationController_nomain.o;\
	fi

${OBJECTDIR}/controller/RunSimulationController/RunSimulationController_nomain.o: ${OBJECTDIR}/controller/RunSimulationController/RunSimulationController.o controller/RunSimulationController/RunSimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/RunSimulationController
	@NMOUTPUT=`${NM} ${OBJECTDIR}/controller/RunSimulationController/RunSimulationController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/RunSimulationController/RunSimulationController_nomain.o controller/RunSimulationController/RunSimulationController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/controller/RunSimulationController/RunSimulationController.o ${OBJECTDIR}/controller/RunSimulationController/RunSimulationController_nomain.o;\
	fi

${OBJECTDIR}/controller/SelectSolarSystemController/SelectSolarSystemController_nomain.o: ${OBJECTDIR}/controller/SelectSolarSystemController/SelectSolarSystemController.o controller/SelectSolarSystemController/SelectSolarSystemController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/SelectSolarSystemController
	@NMOUTPUT=`${NM} ${OBJECTDIR}/controller/SelectSolarSystemController/SelectSolarSystemController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/SelectSolarSystemController/SelectSolarSystemController_nomain.o controller/SelectSolarSystemController/SelectSolarSystemController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/controller/SelectSolarSystemController/SelectSolarSystemController.o ${OBJECTDIR}/controller/SelectSolarSystemController/SelectSolarSystemController_nomain.o;\
	fi

${OBJECTDIR}/controller/SimulationResultsController/SimulationResultsController_nomain.o: ${OBJECTDIR}/controller/SimulationResultsController/SimulationResultsController.o controller/SimulationResultsController/SimulationResultsController.cpp 
	${MKDIR} -p ${OBJECTDIR}/controller/SimulationResultsController
	@NMOUTPUT=`${NM} ${OBJECTDIR}/controller/SimulationResultsController/SimulationResultsController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/controller/SimulationResultsController/SimulationResultsController_nomain.o controller/SimulationResultsController/SimulationResultsController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/controller/SimulationResultsController/SimulationResultsController.o ${OBJECTDIR}/controller/SimulationResultsController/SimulationResultsController_nomain.o;\
	fi

${OBJECTDIR}/datalayer/DataLayer_nomain.o: ${OBJECTDIR}/datalayer/DataLayer.o datalayer/DataLayer.cpp 
	${MKDIR} -p ${OBJECTDIR}/datalayer
	@NMOUTPUT=`${NM} ${OBJECTDIR}/datalayer/DataLayer.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/datalayer/DataLayer_nomain.o datalayer/DataLayer.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/datalayer/DataLayer.o ${OBJECTDIR}/datalayer/DataLayer_nomain.o;\
	fi

${OBJECTDIR}/datalayer/DataLayerMockObjects/DataLayerMockObjects_nomain.o: ${OBJECTDIR}/datalayer/DataLayerMockObjects/DataLayerMockObjects.o datalayer/DataLayerMockObjects/DataLayerMockObjects.cpp 
	${MKDIR} -p ${OBJECTDIR}/datalayer/DataLayerMockObjects
	@NMOUTPUT=`${NM} ${OBJECTDIR}/datalayer/DataLayerMockObjects/DataLayerMockObjects.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/datalayer/DataLayerMockObjects/DataLayerMockObjects_nomain.o datalayer/DataLayerMockObjects/DataLayerMockObjects.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/datalayer/DataLayerMockObjects/DataLayerMockObjects.o ${OBJECTDIR}/datalayer/DataLayerMockObjects/DataLayerMockObjects_nomain.o;\
	fi

${OBJECTDIR}/main_nomain.o: ${OBJECTDIR}/main.o main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/main.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main_nomain.o main.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/main.o ${OBJECTDIR}/main_nomain.o;\
	fi

${OBJECTDIR}/model/Corps/Corps_nomain.o: ${OBJECTDIR}/model/Corps/Corps.o model/Corps/Corps.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Corps
	@NMOUTPUT=`${NM} ${OBJECTDIR}/model/Corps/Corps.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Corps/Corps_nomain.o model/Corps/Corps.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/model/Corps/Corps.o ${OBJECTDIR}/model/Corps/Corps_nomain.o;\
	fi

${OBJECTDIR}/model/Export/Export_nomain.o: ${OBJECTDIR}/model/Export/Export.o model/Export/Export.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Export
	@NMOUTPUT=`${NM} ${OBJECTDIR}/model/Export/Export.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Export/Export_nomain.o model/Export/Export.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/model/Export/Export.o ${OBJECTDIR}/model/Export/Export_nomain.o;\
	fi

${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulation_nomain.o: ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulation.o model/Export/ExportSimulation/ExportSimulation.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Export/ExportSimulation
	@NMOUTPUT=`${NM} ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulation.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulation_nomain.o model/Export/ExportSimulation/ExportSimulation.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulation.o ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulation_nomain.o;\
	fi

${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationCSV_nomain.o: ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationCSV.o model/Export/ExportSimulation/ExportSimulationCSV.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Export/ExportSimulation
	@NMOUTPUT=`${NM} ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationCSV.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationCSV_nomain.o model/Export/ExportSimulation/ExportSimulationCSV.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationCSV.o ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationCSV_nomain.o;\
	fi

${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationGIF_nomain.o: ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationGIF.o model/Export/ExportSimulation/ExportSimulationGIF.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Export/ExportSimulation
	@NMOUTPUT=`${NM} ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationGIF.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationGIF_nomain.o model/Export/ExportSimulation/ExportSimulationGIF.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationGIF.o ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationGIF_nomain.o;\
	fi

${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationHTML_nomain.o: ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationHTML.o model/Export/ExportSimulation/ExportSimulationHTML.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Export/ExportSimulation
	@NMOUTPUT=`${NM} ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationHTML.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationHTML_nomain.o model/Export/ExportSimulation/ExportSimulationHTML.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationHTML.o ${OBJECTDIR}/model/Export/ExportSimulation/ExportSimulationHTML_nomain.o;\
	fi

${OBJECTDIR}/model/GravitationalCorps/GravitationalCorps_nomain.o: ${OBJECTDIR}/model/GravitationalCorps/GravitationalCorps.o model/GravitationalCorps/GravitationalCorps.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/GravitationalCorps
	@NMOUTPUT=`${NM} ${OBJECTDIR}/model/GravitationalCorps/GravitationalCorps.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/GravitationalCorps/GravitationalCorps_nomain.o model/GravitationalCorps/GravitationalCorps.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/model/GravitationalCorps/GravitationalCorps.o ${OBJECTDIR}/model/GravitationalCorps/GravitationalCorps_nomain.o;\
	fi

${OBJECTDIR}/model/GravitationalCorps/GravitationalwithPropulsionCorps/GravitationalwithPropulsionCorps_nomain.o: ${OBJECTDIR}/model/GravitationalCorps/GravitationalwithPropulsionCorps/GravitationalwithPropulsionCorps.o model/GravitationalCorps/GravitationalwithPropulsionCorps/GravitationalwithPropulsionCorps.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/GravitationalCorps/GravitationalwithPropulsionCorps
	@NMOUTPUT=`${NM} ${OBJECTDIR}/model/GravitationalCorps/GravitationalwithPropulsionCorps/GravitationalwithPropulsionCorps.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/GravitationalCorps/GravitationalwithPropulsionCorps/GravitationalwithPropulsionCorps_nomain.o model/GravitationalCorps/GravitationalwithPropulsionCorps/GravitationalwithPropulsionCorps.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/model/GravitationalCorps/GravitationalwithPropulsionCorps/GravitationalwithPropulsionCorps.o ${OBJECTDIR}/model/GravitationalCorps/GravitationalwithPropulsionCorps/GravitationalwithPropulsionCorps_nomain.o;\
	fi

${OBJECTDIR}/model/Import/Import_nomain.o: ${OBJECTDIR}/model/Import/Import.o model/Import/Import.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Import
	@NMOUTPUT=`${NM} ${OBJECTDIR}/model/Import/Import.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Import/Import_nomain.o model/Import/Import.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/model/Import/Import.o ${OBJECTDIR}/model/Import/Import_nomain.o;\
	fi

${OBJECTDIR}/model/Import/ImportPlanets/ImportPlanets_nomain.o: ${OBJECTDIR}/model/Import/ImportPlanets/ImportPlanets.o model/Import/ImportPlanets/ImportPlanets.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Import/ImportPlanets
	@NMOUTPUT=`${NM} ${OBJECTDIR}/model/Import/ImportPlanets/ImportPlanets.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Import/ImportPlanets/ImportPlanets_nomain.o model/Import/ImportPlanets/ImportPlanets.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/model/Import/ImportPlanets/ImportPlanets.o ${OBJECTDIR}/model/Import/ImportPlanets/ImportPlanets_nomain.o;\
	fi

${OBJECTDIR}/model/Import/ImportPlanets/ImportPlanetsCSV_nomain.o: ${OBJECTDIR}/model/Import/ImportPlanets/ImportPlanetsCSV.o model/Import/ImportPlanets/ImportPlanetsCSV.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Import/ImportPlanets
	@NMOUTPUT=`${NM} ${OBJECTDIR}/model/Import/ImportPlanets/ImportPlanetsCSV.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Import/ImportPlanets/ImportPlanetsCSV_nomain.o model/Import/ImportPlanets/ImportPlanetsCSV.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/model/Import/ImportPlanets/ImportPlanetsCSV.o ${OBJECTDIR}/model/Import/ImportPlanets/ImportPlanetsCSV_nomain.o;\
	fi

${OBJECTDIR}/model/Orbit/Orbit_nomain.o: ${OBJECTDIR}/model/Orbit/Orbit.o model/Orbit/Orbit.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Orbit
	@NMOUTPUT=`${NM} ${OBJECTDIR}/model/Orbit/Orbit.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Orbit/Orbit_nomain.o model/Orbit/Orbit.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/model/Orbit/Orbit.o ${OBJECTDIR}/model/Orbit/Orbit_nomain.o;\
	fi

${OBJECTDIR}/model/OrbitalCorps/OrbitalCorps_nomain.o: ${OBJECTDIR}/model/OrbitalCorps/OrbitalCorps.o model/OrbitalCorps/OrbitalCorps.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/OrbitalCorps
	@NMOUTPUT=`${NM} ${OBJECTDIR}/model/OrbitalCorps/OrbitalCorps.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/OrbitalCorps/OrbitalCorps_nomain.o model/OrbitalCorps/OrbitalCorps.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/model/OrbitalCorps/OrbitalCorps.o ${OBJECTDIR}/model/OrbitalCorps/OrbitalCorps_nomain.o;\
	fi

${OBJECTDIR}/model/Simulation/Simulation_nomain.o: ${OBJECTDIR}/model/Simulation/Simulation.o model/Simulation/Simulation.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Simulation
	@NMOUTPUT=`${NM} ${OBJECTDIR}/model/Simulation/Simulation.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Simulation/Simulation_nomain.o model/Simulation/Simulation.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/model/Simulation/Simulation.o ${OBJECTDIR}/model/Simulation/Simulation_nomain.o;\
	fi

${OBJECTDIR}/model/SolarSystem/SolarSystem_nomain.o: ${OBJECTDIR}/model/SolarSystem/SolarSystem.o model/SolarSystem/SolarSystem.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/SolarSystem
	@NMOUTPUT=`${NM} ${OBJECTDIR}/model/SolarSystem/SolarSystem.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/SolarSystem/SolarSystem_nomain.o model/SolarSystem/SolarSystem.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/model/SolarSystem/SolarSystem.o ${OBJECTDIR}/model/SolarSystem/SolarSystem_nomain.o;\
	fi

${OBJECTDIR}/model/Star/Star_nomain.o: ${OBJECTDIR}/model/Star/Star.o model/Star/Star.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Star
	@NMOUTPUT=`${NM} ${OBJECTDIR}/model/Star/Star.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Star/Star_nomain.o model/Star/Star.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/model/Star/Star.o ${OBJECTDIR}/model/Star/Star_nomain.o;\
	fi

${OBJECTDIR}/model/Unit/Unit_nomain.o: ${OBJECTDIR}/model/Unit/Unit.o model/Unit/Unit.cpp 
	${MKDIR} -p ${OBJECTDIR}/model/Unit
	@NMOUTPUT=`${NM} ${OBJECTDIR}/model/Unit/Unit.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/model/Unit/Unit_nomain.o model/Unit/Unit.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/model/Unit/Unit.o ${OBJECTDIR}/model/Unit/Unit_nomain.o;\
	fi

${OBJECTDIR}/ui/CopySimulationUI/CopySimulationUI_nomain.o: ${OBJECTDIR}/ui/CopySimulationUI/CopySimulationUI.o ui/CopySimulationUI/CopySimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/CopySimulationUI
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ui/CopySimulationUI/CopySimulationUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/CopySimulationUI/CopySimulationUI_nomain.o ui/CopySimulationUI/CopySimulationUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ui/CopySimulationUI/CopySimulationUI.o ${OBJECTDIR}/ui/CopySimulationUI/CopySimulationUI_nomain.o;\
	fi

${OBJECTDIR}/ui/CopySolarSystemUI/CopySolarSystemUI_nomain.o: ${OBJECTDIR}/ui/CopySolarSystemUI/CopySolarSystemUI.o ui/CopySolarSystemUI/CopySolarSystemUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/CopySolarSystemUI
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ui/CopySolarSystemUI/CopySolarSystemUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/CopySolarSystemUI/CopySolarSystemUI_nomain.o ui/CopySolarSystemUI/CopySolarSystemUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ui/CopySolarSystemUI/CopySolarSystemUI.o ${OBJECTDIR}/ui/CopySolarSystemUI/CopySolarSystemUI_nomain.o;\
	fi

${OBJECTDIR}/ui/CreateNewSimulationUI/CreateNewSimulationUI_nomain.o: ${OBJECTDIR}/ui/CreateNewSimulationUI/CreateNewSimulationUI.o ui/CreateNewSimulationUI/CreateNewSimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/CreateNewSimulationUI
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ui/CreateNewSimulationUI/CreateNewSimulationUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/CreateNewSimulationUI/CreateNewSimulationUI_nomain.o ui/CreateNewSimulationUI/CreateNewSimulationUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ui/CreateNewSimulationUI/CreateNewSimulationUI.o ${OBJECTDIR}/ui/CreateNewSimulationUI/CreateNewSimulationUI_nomain.o;\
	fi

${OBJECTDIR}/ui/CreateSolarSystemUI/CreateSolarSystemUI_nomain.o: ${OBJECTDIR}/ui/CreateSolarSystemUI/CreateSolarSystemUI.o ui/CreateSolarSystemUI/CreateSolarSystemUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/CreateSolarSystemUI
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ui/CreateSolarSystemUI/CreateSolarSystemUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/CreateSolarSystemUI/CreateSolarSystemUI_nomain.o ui/CreateSolarSystemUI/CreateSolarSystemUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ui/CreateSolarSystemUI/CreateSolarSystemUI.o ${OBJECTDIR}/ui/CreateSolarSystemUI/CreateSolarSystemUI_nomain.o;\
	fi

${OBJECTDIR}/ui/DeleteSimulationUI/DeleteSimulationUI_nomain.o: ${OBJECTDIR}/ui/DeleteSimulationUI/DeleteSimulationUI.o ui/DeleteSimulationUI/DeleteSimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/DeleteSimulationUI
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ui/DeleteSimulationUI/DeleteSimulationUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/DeleteSimulationUI/DeleteSimulationUI_nomain.o ui/DeleteSimulationUI/DeleteSimulationUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ui/DeleteSimulationUI/DeleteSimulationUI.o ${OBJECTDIR}/ui/DeleteSimulationUI/DeleteSimulationUI_nomain.o;\
	fi

${OBJECTDIR}/ui/EditSimulationUI/EditSimulationUI_nomain.o: ${OBJECTDIR}/ui/EditSimulationUI/EditSimulationUI.o ui/EditSimulationUI/EditSimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/EditSimulationUI
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ui/EditSimulationUI/EditSimulationUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/EditSimulationUI/EditSimulationUI_nomain.o ui/EditSimulationUI/EditSimulationUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ui/EditSimulationUI/EditSimulationUI.o ${OBJECTDIR}/ui/EditSimulationUI/EditSimulationUI_nomain.o;\
	fi

${OBJECTDIR}/ui/EditSolarSystemUI/EditSolarSystemUI_nomain.o: ${OBJECTDIR}/ui/EditSolarSystemUI/EditSolarSystemUI.o ui/EditSolarSystemUI/EditSolarSystemUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/EditSolarSystemUI
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ui/EditSolarSystemUI/EditSolarSystemUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/EditSolarSystemUI/EditSolarSystemUI_nomain.o ui/EditSolarSystemUI/EditSolarSystemUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ui/EditSolarSystemUI/EditSolarSystemUI.o ${OBJECTDIR}/ui/EditSolarSystemUI/EditSolarSystemUI_nomain.o;\
	fi

${OBJECTDIR}/ui/FilterResultsSimulationUI/FilterResultsSimulationUI_nomain.o: ${OBJECTDIR}/ui/FilterResultsSimulationUI/FilterResultsSimulationUI.o ui/FilterResultsSimulationUI/FilterResultsSimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/FilterResultsSimulationUI
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ui/FilterResultsSimulationUI/FilterResultsSimulationUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/FilterResultsSimulationUI/FilterResultsSimulationUI_nomain.o ui/FilterResultsSimulationUI/FilterResultsSimulationUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ui/FilterResultsSimulationUI/FilterResultsSimulationUI.o ${OBJECTDIR}/ui/FilterResultsSimulationUI/FilterResultsSimulationUI_nomain.o;\
	fi

${OBJECTDIR}/ui/MainUI/MainUI_nomain.o: ${OBJECTDIR}/ui/MainUI/MainUI.o ui/MainUI/MainUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/MainUI
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ui/MainUI/MainUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/MainUI/MainUI_nomain.o ui/MainUI/MainUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ui/MainUI/MainUI.o ${OBJECTDIR}/ui/MainUI/MainUI_nomain.o;\
	fi

${OBJECTDIR}/ui/RunSimulationUI/RunSimulationUI_nomain.o: ${OBJECTDIR}/ui/RunSimulationUI/RunSimulationUI.o ui/RunSimulationUI/RunSimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/RunSimulationUI
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ui/RunSimulationUI/RunSimulationUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/RunSimulationUI/RunSimulationUI_nomain.o ui/RunSimulationUI/RunSimulationUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ui/RunSimulationUI/RunSimulationUI.o ${OBJECTDIR}/ui/RunSimulationUI/RunSimulationUI_nomain.o;\
	fi

${OBJECTDIR}/ui/SelectSolarSystemUI/SelectSolarSystemUI_nomain.o: ${OBJECTDIR}/ui/SelectSolarSystemUI/SelectSolarSystemUI.o ui/SelectSolarSystemUI/SelectSolarSystemUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/SelectSolarSystemUI
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ui/SelectSolarSystemUI/SelectSolarSystemUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/SelectSolarSystemUI/SelectSolarSystemUI_nomain.o ui/SelectSolarSystemUI/SelectSolarSystemUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ui/SelectSolarSystemUI/SelectSolarSystemUI.o ${OBJECTDIR}/ui/SelectSolarSystemUI/SelectSolarSystemUI_nomain.o;\
	fi

${OBJECTDIR}/ui/SimulationResultsUI/SimulationResultsUI_nomain.o: ${OBJECTDIR}/ui/SimulationResultsUI/SimulationResultsUI.o ui/SimulationResultsUI/SimulationResultsUI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui/SimulationResultsUI
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ui/SimulationResultsUI/SimulationResultsUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/SimulationResultsUI/SimulationResultsUI_nomain.o ui/SimulationResultsUI/SimulationResultsUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ui/SimulationResultsUI/SimulationResultsUI.o ${OBJECTDIR}/ui/SimulationResultsUI/SimulationResultsUI_nomain.o;\
	fi

${OBJECTDIR}/ui/UI_nomain.o: ${OBJECTDIR}/ui/UI.o ui/UI.cpp 
	${MKDIR} -p ${OBJECTDIR}/ui
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ui/UI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ui/UI_nomain.o ui/UI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ui/UI.o ${OBJECTDIR}/ui/UI_nomain.o;\
	fi

${OBJECTDIR}/utils/ID/ID_nomain.o: ${OBJECTDIR}/utils/ID/ID.o utils/ID/ID.cpp 
	${MKDIR} -p ${OBJECTDIR}/utils/ID
	@NMOUTPUT=`${NM} ${OBJECTDIR}/utils/ID/ID.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/utils/ID/ID_nomain.o utils/ID/ID.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/utils/ID/ID.o ${OBJECTDIR}/utils/ID/ID_nomain.o;\
	fi

${OBJECTDIR}/utils/Timer/Timer_nomain.o: ${OBJECTDIR}/utils/Timer/Timer.o utils/Timer/Timer.cpp 
	${MKDIR} -p ${OBJECTDIR}/utils/Timer
	@NMOUTPUT=`${NM} ${OBJECTDIR}/utils/Timer/Timer.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/utils/Timer/Timer_nomain.o utils/Timer/Timer.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/utils/Timer/Timer.o ${OBJECTDIR}/utils/Timer/Timer_nomain.o;\
	fi

${OBJECTDIR}/utils/Utils_nomain.o: ${OBJECTDIR}/utils/Utils.o utils/Utils.cpp 
	${MKDIR} -p ${OBJECTDIR}/utils
	@NMOUTPUT=`${NM} ${OBJECTDIR}/utils/Utils.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/utils/Utils_nomain.o utils/Utils.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/utils/Utils.o ${OBJECTDIR}/utils/Utils_nomain.o;\
	fi

${OBJECTDIR}/utils/XYZ/XYZ_nomain.o: ${OBJECTDIR}/utils/XYZ/XYZ.o utils/XYZ/XYZ.cpp 
	${MKDIR} -p ${OBJECTDIR}/utils/XYZ
	@NMOUTPUT=`${NM} ${OBJECTDIR}/utils/XYZ/XYZ.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Werror -std=c++11 -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/utils/XYZ/XYZ_nomain.o utils/XYZ/XYZ.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/utils/XYZ/XYZ.o ${OBJECTDIR}/utils/XYZ/XYZ_nomain.o;\
	fi

# Run Test Targets
.test-conf:
	@if [ "${TEST}" = "" ]; \
	then  \
	    ${TESTDIR}/TestFiles/f6 || true; \
	    ${TESTDIR}/TestFiles/f8 || true; \
	    ${TESTDIR}/TestFiles/f9 || true; \
	    ${TESTDIR}/TestFiles/f5 || true; \
	    ${TESTDIR}/TestFiles/f1 || true; \
	    ${TESTDIR}/TestFiles/f7 || true; \
	    ${TESTDIR}/TestFiles/f2 || true; \
	    ${TESTDIR}/TestFiles/f10 || true; \
	    ${TESTDIR}/TestFiles/f11 || true; \
	    ${TESTDIR}/TestFiles/f4 || true; \
	else  \
	    ./${TEST} || true; \
	fi

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${TESTDIR}/TestFiles/f3

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
