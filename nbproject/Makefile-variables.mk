#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-MacOSX
CND_ARTIFACT_DIR_Debug=build/Debug/GNU-MacOSX/tests/TestFiles
CND_ARTIFACT_NAME_Debug=f3
CND_ARTIFACT_PATH_Debug=build/Debug/GNU-MacOSX/tests/TestFiles/f3
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-MacOSX/package
CND_PACKAGE_NAME_Debug=lapr31415g50.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-MacOSX/package/lapr31415g50.tar
# Release configuration
CND_PLATFORM_Release=GNU-MacOSX
CND_ARTIFACT_DIR_Release=build/Release/GNU-MacOSX/tests/TestFiles
CND_ARTIFACT_NAME_Release=f3
CND_ARTIFACT_PATH_Release=build/Release/GNU-MacOSX/tests/TestFiles/f3
CND_PACKAGE_DIR_Release=dist/Release/GNU-MacOSX/package
CND_PACKAGE_NAME_Release=lapr31415g50.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-MacOSX/package/lapr31415g50.tar
# GilbertoMac configuration
CND_PLATFORM_GilbertoMac=GNU-MacOSX
CND_ARTIFACT_DIR_GilbertoMac=dist/GilbertoMac/GNU-MacOSX
CND_ARTIFACT_NAME_GilbertoMac=lapr3_1415_g50
CND_ARTIFACT_PATH_GilbertoMac=dist/GilbertoMac/GNU-MacOSX/lapr3_1415_g50
CND_PACKAGE_DIR_GilbertoMac=dist/GilbertoMac/GNU-MacOSX/package
CND_PACKAGE_NAME_GilbertoMac=lapr31415g50.tar
CND_PACKAGE_PATH_GilbertoMac=dist/GilbertoMac/GNU-MacOSX/package/lapr31415g50.tar
# mbp-dnamorim configuration
CND_PLATFORM_mbp-dnamorim=GNU-MacOSX
CND_ARTIFACT_DIR_mbp-dnamorim=build/mbp-dnamorim/GNU-MacOSX/tests/TestFiles
CND_ARTIFACT_NAME_mbp-dnamorim=f3
CND_ARTIFACT_PATH_mbp-dnamorim=build/mbp-dnamorim/GNU-MacOSX/tests/TestFiles/f3
CND_PACKAGE_DIR_mbp-dnamorim=dist/mbp-dnamorim/GNU-MacOSX/package
CND_PACKAGE_NAME_mbp-dnamorim=lapr31415g50.tar
CND_PACKAGE_PATH_mbp-dnamorim=dist/mbp-dnamorim/GNU-MacOSX/package/lapr31415g50.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
