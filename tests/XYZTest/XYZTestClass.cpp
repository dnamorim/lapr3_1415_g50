/*
 * File:   XYZTestClass.cpp
 * Author: dnamorim
 *
 * Created on 12/jan/2015, 0:18:35
 */

#include "XYZTestClass.h"
#include "../../utils/XYZ/XYZ.h"


CPPUNIT_TEST_SUITE_REGISTRATION(XYZTestClass);

XYZTestClass::XYZTestClass() {
}

XYZTestClass::~XYZTestClass() {
}

void XYZTestClass::setUp() {
}

void XYZTestClass::tearDown() {
}

void XYZTestClass::testClone() {
    XYZ p(4, 1, 3);
    XYZ* result = p.clone();
    CPPUNIT_ASSERT(p == *result);
}

void XYZTestClass::testGetSetX() {
    XYZ p;
    double result = -0.213136685094;
    p.setX(result);
    CPPUNIT_ASSERT(result == p.getX());
    
}

void XYZTestClass::testGetSetY() {
    XYZ p;
    double result = 0.880776815814;
    p.setY(result);
    CPPUNIT_ASSERT(result == p.getY());
}

void XYZTestClass::testGetSetZ() {
    XYZ p;
    double result = 0.381786157002;
    p.setZ(result);
    CPPUNIT_ASSERT(result == p.getZ());
}

void XYZTestClass::testWriteStream() {
    XYZ p(-0.213136685094, 0.880776815814, 0.381786157002);
    stringstream out;
    stringstream result;
    result << p.getX() << "," << p.getY() << "," << p.getZ();
    p.writeStream(out);
    CPPUNIT_ASSERT(out.str() == result.str());
}

void XYZTestClass::testEqualsTrue() {
    XYZ p1(-0.213136685094, 0.880776815814, 0.381786157002);
    XYZ p2(p1);
    
    CPPUNIT_ASSERT(p1 == p2);
}

void XYZTestClass::testEqualsFalse() {
    XYZ p1(-0.213136685094, 0.880776815814, 0.381786157002);    
    XYZ p2(p1);
    p2.setZ(0.387);
    
    CPPUNIT_ASSERT(!(p1 == p2));
}

void XYZTestClass::testAdd() {
    XYZ p(4, -1, 3);
    XYZ add(1, 2, 1);
    XYZ expected(5, 1, 4);
    
    XYZ result = p + add;
    CPPUNIT_ASSERT(result == expected);
}

void XYZTestClass::testAddAttribuiton() {
    XYZ p(4, -1, 3);
    XYZ add(1, 2, 1);
    XYZ expected(5, 1, 4);
    
    p += add;
    CPPUNIT_ASSERT(p == expected);
}

void XYZTestClass::testSub() {
    XYZ p(4, -1, 3);
    XYZ sub(1, 2, 1);
    XYZ expected(3, -3, 2);
    
    XYZ result = p - sub;
    CPPUNIT_ASSERT(result == expected);
}

void XYZTestClass::testSubAttribuiton() {
    XYZ p(4, -1, 3);
    XYZ sub(1, 2, 1);
    XYZ expected(3, -3, 2);
    
    p -= sub;
    CPPUNIT_ASSERT(p == expected);
}

