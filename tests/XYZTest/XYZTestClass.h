/*
 * File:   XYZTestClass.h
 * Author: dnamorim
 *
 * Created on 12/jan/2015, 0:18:35
 */

#ifndef XYZTESTCLASS_H
#define	XYZTESTCLASS_H

#include <cppunit/extensions/HelperMacros.h>

class XYZTestClass : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(XYZTestClass);

    CPPUNIT_TEST(testClone);
    CPPUNIT_TEST(testGetSetX);
    CPPUNIT_TEST(testGetSetY);
    CPPUNIT_TEST(testGetSetZ);
    CPPUNIT_TEST(testWriteStream);
    CPPUNIT_TEST(testEqualsTrue);
    CPPUNIT_TEST(testEqualsFalse);
    CPPUNIT_TEST(testAdd);
    CPPUNIT_TEST(testSub);
    CPPUNIT_TEST(testAddAttribuiton);
    CPPUNIT_TEST(testSubAttribuiton);

    CPPUNIT_TEST_SUITE_END();

public:
    XYZTestClass();
    virtual ~XYZTestClass();
    void setUp();
    void tearDown();

private:
    void testClone();
    void testGetSetX();
    void testGetSetY();
    void testGetSetZ();
    void testWriteStream();
    void testEqualsTrue();
    void testEqualsFalse();
    void testAdd();
    void testSub();
    void testAddAttribuiton();
    void testSubAttribuiton();

};

#endif	/* XYZTESTCLASS_H */

