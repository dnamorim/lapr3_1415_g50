/*
 * File:   GravitionalWithPropulsionCorpsTestClass.h
 * Author: dnamorim
 *
 * Created on 12/jan/2015, 22:49:17
 */

#ifndef GRAVITIONALWITHPROPULSIONCORPSTESTCLASS_H
#define	GRAVITIONALWITHPROPULSIONCORPSTESTCLASS_H

#include <cppunit/extensions/HelperMacros.h>

class GravitationalWithPropulsionCorpsTestClass : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(GravitationalWithPropulsionCorpsTestClass);

    CPPUNIT_TEST(testClone);
    CPPUNIT_TEST(testGetSetPropulsion);
    CPPUNIT_TEST(testValidateTrue);
    CPPUNIT_TEST(testValidateFalse);
    CPPUNIT_TEST(testWritePointerStream);
    CPPUNIT_TEST(testWriteStream);
    CPPUNIT_TEST(testEqualsTrue);
    CPPUNIT_TEST(testEqualsFalse);

    CPPUNIT_TEST_SUITE_END();

public:
    GravitationalWithPropulsionCorpsTestClass();
    virtual ~GravitationalWithPropulsionCorpsTestClass();
    void setUp();
    void tearDown();

private:
    void testClone();
    void testGetSetPropulsion();
    void testValidateTrue();
    void testValidateFalse();
    void testWritePointerStream();
    void testWriteStream();
    void testEqualsTrue();
    void testEqualsFalse();

};

#endif	/* GRAVITIONALWITHPROPULSIONCORPSTESTCLASS_H */

