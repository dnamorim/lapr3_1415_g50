/*
 * File:   GravitionalWithPropulsionCorpsTestClass.cpp
 * Author: dnamorim
 *
 * Created on 12/jan/2015, 22:49:17
 */

#include "GravitationalWithPropulsionCorpsTestClass.h"
#include "../../model/Orbit/Orbit.h"
#include "../../model/GravitationalCorps/GravitationalwithPropulsionCorps/GravitationalwithPropulsionCorps.h"


CPPUNIT_TEST_SUITE_REGISTRATION(GravitationalWithPropulsionCorpsTestClass);

GravitationalWithPropulsionCorpsTestClass::GravitationalWithPropulsionCorpsTestClass() {
}

GravitationalWithPropulsionCorpsTestClass::~GravitationalWithPropulsionCorpsTestClass() {
}

void GravitationalWithPropulsionCorpsTestClass::setUp() {
}

void GravitationalWithPropulsionCorpsTestClass::tearDown() {
}

void GravitationalWithPropulsionCorpsTestClass::testClone() {
    GravitationalwithPropulsionCorps gpc(5.97694449E+24, 12800, 5.52, 25, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Spaceship", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    GravitationalwithPropulsionCorps* result = (GravitationalwithPropulsionCorps*) (gpc.clone());
    CPPUNIT_ASSERT(gpc == (*result));
}

void GravitationalWithPropulsionCorpsTestClass::testGetSetPropulsion() {
    GravitationalwithPropulsionCorps gpc;
    double value = 25.32;
    gpc.setPropulsion(value);
    CPPUNIT_ASSERT(value == gpc.getPropulsion());
}

void GravitationalWithPropulsionCorpsTestClass::testValidateTrue() {
    GravitationalwithPropulsionCorps gpc(5.97694449E+24, 12800, 5.52, 40, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Spaceship", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    CPPUNIT_ASSERT(gpc.validate());
}

void GravitationalWithPropulsionCorpsTestClass::testValidateFalse() {
    GravitationalwithPropulsionCorps gpc(5.97694449E+24, 12800, 5.52, -40, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Spaceship", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    CPPUNIT_ASSERT(!gpc.validate());
}

void GravitationalWithPropulsionCorpsTestClass::testWritePointerStream() {
    GravitationalwithPropulsionCorps gpc(5.97694449E+24, 12800, 5.52, 35, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Spaceship", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    stringstream out;
    stringstream expected;
    expected << "[" << typeid (gpc).name() << "] " << gpc.getName() << "(" << ((gpc.getSource() == Source::Natural) ? "Natural" : "Manmade") << "," << gpc.getReferenceDate() <<"," << "{" << gpc.getCoordinates() << "}" << "," << gpc.getMass() << "," << gpc.getDiameter() << "," << gpc.getDensity() << "," << "{" << gpc.getSpeed() << "}," << gpc.getPropulsion();
    gpc.writePointerStream(out);
    CPPUNIT_ASSERT(out.str() == expected.str());
}

void GravitationalWithPropulsionCorpsTestClass::testWriteStream() {
    GravitationalwithPropulsionCorps gpc(5.97694449E+24, 12800, 5.52, 35, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Spaceship", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    stringstream out;
    stringstream expected;
    expected << typeid (gpc).name() << endl;
    expected << "Source: " << ((gpc.getSource() == Source::Natural) ? "Natural" : "Manmade") << endl;
    expected << "Name: " << gpc.getName() << endl;
    expected << "Reference Date: " << gpc.getReferenceDate() << endl;
    expected << "Coordinates: " << gpc.getCoordinates() << endl;
    expected << "Mass: " << gpc.getMass() << endl;
    expected << "Diameter: " << gpc.getDiameter() << endl;
    expected << "Density: " << gpc.getDensity() << endl;
    expected << "Speed: " << gpc.getSpeed() << endl;
    expected << "Orbit:" << endl;
    expected << gpc.getOrbit() << endl;
    expected << "Propulsion: " << gpc.getPropulsion() << endl;
    
    gpc.writeStream(out);
    CPPUNIT_ASSERT(out.str() == expected.str());
}

void GravitationalWithPropulsionCorpsTestClass::testEqualsTrue() {
    GravitationalwithPropulsionCorps gpc1(5.97694449E+24, 12800, 5.52, 432,  XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Spaceship", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    GravitationalwithPropulsionCorps gpc2(5.97694449E+24, 12800, 5.52, 432, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Spaceship", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    CPPUNIT_ASSERT(gpc1 == gpc2);
}

void GravitationalWithPropulsionCorpsTestClass::testEqualsFalse() {
    GravitationalwithPropulsionCorps gpc1(5.97694449E+24, 12800, 5.52, 423,XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Spaceship", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    GravitationalwithPropulsionCorps gpc2(5.97694449E+24, 12800, 5.52, 324,XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Spaceship", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    CPPUNIT_ASSERT(!(gpc1 == gpc2));
}

