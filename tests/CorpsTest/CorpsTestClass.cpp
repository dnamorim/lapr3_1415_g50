/*
 * File:   CorpsTestClass.cpp
 * Author: dnamorim
 *
 * Created on 12/jan/2015, 14:44:07
 */

#include <iostream>
#include <string>
#include <vector>

#include "CorpsTestClass.h"
#include "../../model/Orbit/Orbit.h"
#include "../../model/OrbitalCorps/OrbitalCorps.h"
#include "../../model/Corps/Corps.h"


CPPUNIT_TEST_SUITE_REGISTRATION(CorpsTestClass);

CorpsTestClass::CorpsTestClass() {
}

CorpsTestClass::~CorpsTestClass() {
}

void CorpsTestClass::setUp() {
}

void CorpsTestClass::tearDown() {
}

void CorpsTestClass::testCalMass() {
    OrbitalCorps moon(5.97694449E+24, 1738.14, 3.3464, XYZ(1.25071836853357E-3, -1.90882209607546E-3, -0.598143502900807E-3), XYZ(0.917775808693E+4, 0.554012078890E+4, 0.241820062466E+4), 3.84E+05, 2360580, 1022, 0, 5.145, 0.0549, 2.38, 362600, 405400, 123, 2381, 0, "Moon", Timer(2014, 1, 3, 0, 0, 0), Source::Natural, Corps::ORBITBODYS);
    Corps earth(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Earth", Timer(14, 1, 3), Source::Natural, Corps::ORBITBODYS);    
    Orbit o(&earth, Orbit::BODYS);
    vector<OrbitalCorps*> lst = o.getBodys();
    lst.push_back(&moon);
    o.addOrbits(lst);
    Orbit& endOrbit = o;
    earth.setOrbit(endOrbit);
    
    double expected = moon.getMass() + earth.getMass();
    CPPUNIT_ASSERT(expected == earth.calMass());
}

void CorpsTestClass::testClone() {
    Corps corps(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Earth", Timer(14, 1, 3), Source::Natural, Corps::ORBITBODYS);
    Corps* result = corps.clone();
    CPPUNIT_ASSERT(corps == (*result));
}

void CorpsTestClass::testDefinedTrue() {
    Corps corps(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Earth", Timer(14, 1, 3), Source::Natural, Corps::ORBITBODYS);
    corps.setID(1234);
    CPPUNIT_ASSERT(corps.defined());
}

void CorpsTestClass::testDefinedFalse() {
    Corps corps(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Earth", Timer(14, 1, 3), Source::Natural, Corps::ORBITBODYS);
    CPPUNIT_ASSERT(!corps.defined());
}

void CorpsTestClass::testEditedTrue() {
    Corps corps(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Earth", Timer(14, 1, 3), Source::Natural, Corps::ORBITBODYS);    
    corps.setEdit(true);
    CPPUNIT_ASSERT(corps.edited());
}

void CorpsTestClass::testEditedFalse() {
    Corps corps(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Earth", Timer(14, 1, 3), Source::Natural, Corps::ORBITBODYS);    
    CPPUNIT_ASSERT(!corps.edited());
}

void CorpsTestClass::testGetSetCoordinates() {
    Corps corps;
    XYZ coord(-0.213136685094, 0.880776815814, 0.381786157002);
    corps.setCoordinates(coord);
    CPPUNIT_ASSERT(coord == corps.getCoordinates());
}

void CorpsTestClass::testGetSetCoordinatesReference() {
    Corps corps;
    XYZ value(-0.213136685094, 0.880776815814, 0.381786157002);
    XYZ& expected = value;
    corps.setCoordinates(value);
    CPPUNIT_ASSERT(corps.getCoordinates() == expected);
}

void CorpsTestClass::testGetSetDensity() {
    Corps corps;
    double value = 5.52;
    corps.setDensity(value);
    CPPUNIT_ASSERT(value == corps.getDensity());
}

void CorpsTestClass::testGetSetDiameter() {
    Corps corps;
    double value = 12800;
    corps.setDiameter(value);
    CPPUNIT_ASSERT(corps.getDiameter() == value);
}

void CorpsTestClass::testGetSetMass() {
    Corps corps;
    double value = 5.97694449E+24;
    corps.setMass(value);
    CPPUNIT_ASSERT(corps.getMass() == value);
}

void CorpsTestClass::testGetSetName() {
    Corps corps;
    string value = "Earth";
    corps.setName(value);
    CPPUNIT_ASSERT(value == corps.getName());
}

void CorpsTestClass::testGetSetOrbit() {
    OrbitalCorps moon(19407924, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), 1.08E+08, 31557600, 29786.7868, 23.5, 1.85, 0.0934, 5.0, 206.655215E+6, 259.232432E+6, 0, 0, 0, "Moon", Timer(2015, 01, 3, 0, 0, 0, true), Source::Natural, OrbitalCorps::ORBITBODYS); 
    Corps earth(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Earth", Timer(14, 1, 3, true), Source::Natural, Corps::ORBITBODYS);    
    Orbit o(&earth, Orbit::BODYS);
    vector<OrbitalCorps*> lst = o.getBodys();
    lst.push_back(&moon);
    o.addOrbits(lst);
    Orbit& endOrbit = o;
    earth.setOrbit(endOrbit);
    CPPUNIT_ASSERT(earth.getOrbit() == endOrbit);
}

void CorpsTestClass::testGetSetReferenceDate() {
    Corps corps;
    Timer value(2014, 1, 3, 0, 0, 0);
    corps.setReferenceDate(value);
    CPPUNIT_ASSERT(value == corps.getReferenceDate());
}

void CorpsTestClass::testGetSetReferenceDateReference() {
    Corps corps;
    Timer value(2014, 1, 3, 0, 0, 0, true);
    Timer& expected = value;
    corps.setReferenceDate(value);
    CPPUNIT_ASSERT(corps.getReferenceDate() == expected);
}

void CorpsTestClass::testGetSetSource() {
    Corps corps;
    Source value = Source::Natural;
    corps.setSource(value);
    CPPUNIT_ASSERT(value == corps.getSource());
}

void CorpsTestClass::testGetSetSpeed() {
    Corps corps;
    XYZ value(0.917775808693E+4, 0.554012078890E+4, 0.241820062466E+4);
    corps.setSpeed(value);
    CPPUNIT_ASSERT(value == corps.getSpeed());
}

void CorpsTestClass::testGetSetSpeedReference() {
    Corps corps;
    XYZ value(0.917775808693E+4, 0.554012078890E+4, 0.241820062466E+4);
    XYZ& expected = value;
    corps.setSpeed(value);
    CPPUNIT_ASSERT(corps.getSpeed() == expected);
}

void CorpsTestClass::testValidateTrue() {
    Corps corps(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Earth", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    CPPUNIT_ASSERT(corps.validate());
}

void CorpsTestClass::testValidateFalse() {
    Corps corps(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Earth", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    corps.setMass(-2);
    CPPUNIT_ASSERT(!corps.validate());
}

void CorpsTestClass::testWritePointerStream() {
    Corps earth(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Earth", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    stringstream out;
    stringstream expected;
    expected << "[" << typeid (earth).name() << "] " << earth.getName() << "(" << ((earth.getSource() == Source::Natural) ? "Natural" : "Manmade") << "," << earth.getReferenceDate() <<"," << "{" << earth.getCoordinates() << "}" << "," << earth.getMass() << "," << earth.getDiameter() << "," << earth.getDensity() << "," << "{" << earth.getSpeed() << "}";
    
    earth.writePointerStream(out);
    CPPUNIT_ASSERT(out.str() == expected.str());
}

void CorpsTestClass::testWriteStream() {
    Corps earth(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Earth", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    stringstream out;
    stringstream expected;
    expected << typeid (earth).name() << endl;
    expected << "Source: " << ((earth.getSource() == Source::Natural) ? "Natural" : "Manmade") << endl;
    expected << "Name: " << earth.getName() << endl;
    expected << "Reference Date: " << earth.getReferenceDate() << endl;
    expected << "Coordinates: " << earth.getCoordinates() << endl;
    expected << "Mass: " << earth.getMass() << endl;
    expected << "Diameter: " << earth.getDiameter() << endl;
    expected << "Density: " << earth.getDensity() << endl;
    expected << "Speed: " << earth.getSpeed() << endl;
    expected << "Orbit:" << endl;
    expected << earth.getOrbit();
    expected << endl;
    
    earth.writeStream(out);
    CPPUNIT_ASSERT(out.str() == expected.str());
}

void CorpsTestClass::testEqualsTrue() {
    Corps c1(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Earth", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    Corps c2(c1);
    CPPUNIT_ASSERT(c1 == c2);
}

void CorpsTestClass::testEqualsFalse() {
    Corps c1(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Earth", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    Corps c2(c1);
    c2.setMass(1234);
    CPPUNIT_ASSERT(!(c1 == c2));
}