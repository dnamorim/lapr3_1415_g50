/*
 * File:   CorpsTestClass.h
 * Author: dnamorim
 *
 * Created on 12/jan/2015, 14:44:07
 */

#ifndef CORPSTESTCLASS_H
#define	CORPSTESTCLASS_H

#include <cppunit/extensions/HelperMacros.h>

class CorpsTestClass : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(CorpsTestClass);

    CPPUNIT_TEST(testCalMass);
    CPPUNIT_TEST(testClone);
    CPPUNIT_TEST(testDefinedTrue);
    CPPUNIT_TEST(testDefinedFalse);
    CPPUNIT_TEST(testEditedTrue);
    CPPUNIT_TEST(testEditedFalse);
    CPPUNIT_TEST(testGetSetCoordinates);
    CPPUNIT_TEST(testGetSetCoordinatesReference);
    CPPUNIT_TEST(testGetSetDensity);
    CPPUNIT_TEST(testGetSetDiameter);
    CPPUNIT_TEST(testGetSetMass);
    CPPUNIT_TEST(testGetSetName);
    CPPUNIT_TEST(testGetSetOrbit);
    CPPUNIT_TEST(testGetSetReferenceDate);
    CPPUNIT_TEST(testGetSetReferenceDateReference);
    CPPUNIT_TEST(testGetSetSource);
    CPPUNIT_TEST(testGetSetSpeed);
    CPPUNIT_TEST(testGetSetSpeedReference);
    CPPUNIT_TEST(testValidateTrue);
    CPPUNIT_TEST(testValidateFalse);
    CPPUNIT_TEST(testWritePointerStream);
    CPPUNIT_TEST(testWriteStream);
    CPPUNIT_TEST(testEqualsTrue);
    CPPUNIT_TEST(testEqualsFalse);

    CPPUNIT_TEST_SUITE_END();

public:
    CorpsTestClass();
    virtual ~CorpsTestClass();
    void setUp();
    void tearDown();

private:
    void testCalMass();
    void testClone();
    void testDefinedTrue();
    void testDefinedFalse();
    void testEditedTrue();
    void testEditedFalse();
    void testGetSetCoordinates();
    void testGetSetCoordinatesReference();
    void testGetSetDensity();
    void testGetSetDiameter();
    void testGetSetMass();
    void testGetSetName();
    void testGetSetOrbit();
    void testGetSetReferenceDate();
    void testGetSetReferenceDateReference();
    void testGetSetSource();
    void testGetSetSpeed();
    void testGetSetSpeedReference();
    void testValidateTrue();
    void testValidateFalse();
    void testWritePointerStream();
    void testWriteStream();
    void testEqualsTrue();
    void testEqualsFalse();

};

#endif	/* CORPSTESTCLASS_H */

