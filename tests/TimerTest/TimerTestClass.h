/*
 * File:   TimerTestClass.h
 * Author: dnamorim
 *
 * Created on 12/jan/2015, 22:51:20
 */

#ifndef TIMERTESTCLASS_H
#define	TIMERTESTCLASS_H

#include <cppunit/extensions/HelperMacros.h>

class TimerTestClass : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(TimerTestClass);

    CPPUNIT_TEST(testClear);
    CPPUNIT_TEST(testGetDay);
    CPPUNIT_TEST(testGetHour);
    CPPUNIT_TEST(testGetMin);
    CPPUNIT_TEST(testGetMonth);
    CPPUNIT_TEST(testGetSec);
    CPPUNIT_TEST(testGetTimer);
    CPPUNIT_TEST(testGetYear);
    CPPUNIT_TEST(testNow);
    CPPUNIT_TEST(testWriteStream);

    CPPUNIT_TEST_SUITE_END();

public:
    TimerTestClass();
    virtual ~TimerTestClass();
    void setUp();
    void tearDown();

private:
    void testClear();
    void testGetDay();
    void testGetHour();
    void testGetMin();
    void testGetMonth();
    void testGetSec();
    void testGetTimer();
    void testGetYear();
    void testNow();
    void testWriteStream();

};

#endif	/* TIMERTESTCLASS_H */

