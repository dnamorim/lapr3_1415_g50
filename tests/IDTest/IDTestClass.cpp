/*
 * File:   IDTestClass.cpp
 * Author: dnamorim
 *
 * Created on 12/jan/2015, 11:44:08
 */

#include "IDTestClass.h"
#include "../../utils/ID/ID.h"


CPPUNIT_TEST_SUITE_REGISTRATION(IDTestClass);

IDTestClass::IDTestClass() {
}

IDTestClass::~IDTestClass() {
}

void IDTestClass::setUp() {
}

void IDTestClass::tearDown() {
}

void IDTestClass::testClone() {
    ID id(1234, false, false, false);
    ID* cloned = id.clone();
    CPPUNIT_ASSERT(id == *cloned);
}

void IDTestClass::testDefinedTrue() {
    ID id;
    id.setDefine(true);
    CPPUNIT_ASSERT(id.defined());
}

void IDTestClass::testDefinedFalse() {
    ID id;
    id.setDefine(false);
    CPPUNIT_ASSERT(!id.defined());
}

void IDTestClass::testEditedTrue() {
    ID id;
    id.setEdit(true);
    CPPUNIT_ASSERT(id.edited());
}

void IDTestClass::testEditedFalse() {
    ID id; 
    id.setEdit(false);
    CPPUNIT_ASSERT(!id.edited());
}

void IDTestClass::testActiveTrue() {
    ID id;
    id.setActive(true);
    CPPUNIT_ASSERT(id.isactive());
}

void IDTestClass::testActiveFalse() {
    ID id; 
    id.setActive(false);
    CPPUNIT_ASSERT(!id.isactive());
}

void IDTestClass::testGetSetDefine() {
    ID id;
    id.setDefine(true);
    CPPUNIT_ASSERT(id.getDefine());
}

void IDTestClass::testGetSetEdit() {
    ID id;
    id.setEdit(true);
    CPPUNIT_ASSERT(id.getEdit());
}

void IDTestClass::testGetSetActive() {
    ID id;
    id.setActive(true);
    CPPUNIT_ASSERT(id.getActive());
}

void IDTestClass::testGetSetID() {
    ID id;
    int idn = 8295;
    id.setID(idn);
    CPPUNIT_ASSERT(idn == id.getID());
}

void IDTestClass::testEqualsTrue() {
    ID id1(1234, true, false, false);
    ID id2(id1);
    
    CPPUNIT_ASSERT(id1 == id2);
}

void IDTestClass::testEqualsFalse() {
    ID id1(1234, true, false, false);
    ID id2(2134, true, false, false);
    
    CPPUNIT_ASSERT(!(id1 == id2));
}

