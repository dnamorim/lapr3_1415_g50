/*
 * File:   IDTestClass.h
 * Author: dnamorim
 *
 * Created on 12/jan/2015, 11:44:07
 */

#ifndef IDTESTCLASS_H
#define	IDTESTCLASS_H

#include <cppunit/extensions/HelperMacros.h>

class IDTestClass : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(IDTestClass);

    CPPUNIT_TEST(testClone);
    CPPUNIT_TEST(testDefinedTrue);
    CPPUNIT_TEST(testDefinedFalse);
    CPPUNIT_TEST(testEditedTrue);
    CPPUNIT_TEST(testEditedFalse);
    CPPUNIT_TEST(testActiveTrue);
    CPPUNIT_TEST(testActiveFalse);
    CPPUNIT_TEST(testGetSetDefine);
    CPPUNIT_TEST(testGetSetEdit);
    CPPUNIT_TEST(testGetSetActive);
    CPPUNIT_TEST(testGetSetID);
    CPPUNIT_TEST(testEqualsTrue);
    CPPUNIT_TEST(testEqualsFalse);

    CPPUNIT_TEST_SUITE_END();

public:
    IDTestClass();
    virtual ~IDTestClass();
    void setUp();
    void tearDown();

private:
    void testClone();
    void testDefinedTrue();
    void testDefinedFalse();
    void testEditedTrue();
    void testEditedFalse();
    void testActiveTrue();
    void testActiveFalse();
    void testGetSetDefine();
    void testGetSetEdit();
    void testGetSetActive();
    void testGetSetID();
    void testEqualsTrue();
    void testEqualsFalse();

};

#endif	/* IDTESTCLASS_H */

