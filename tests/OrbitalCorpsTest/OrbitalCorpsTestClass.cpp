/*
 * File:   OrbitalCorpsClass.cpp
 * Author: dnamorim
 *
 * Created on 11/jan/2015, 20:29:04
 */

#include <iostream>
#include <string>
#include "OrbitalCorpsTestClass.h"
#include "../../model/Orbit/Orbit.h"
#include "../../model/OrbitalCorps/OrbitalCorps.h"

CPPUNIT_TEST_SUITE_REGISTRATION(OrbitalCorpsTestClass);

OrbitalCorpsTestClass::OrbitalCorpsTestClass() {
}

OrbitalCorpsTestClass::~OrbitalCorpsTestClass() {
}

void OrbitalCorpsTestClass::setUp() {
}

void OrbitalCorpsTestClass::tearDown() {
}

void OrbitalCorpsTestClass::testClone() {
    OrbitalCorps earth(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), 1.50E+08, 31557600, 29786.7868, 23.5, 0, 0.0167, 11.2, 147.098291E+6, 152.098233E+6, 0.33842279, -23.94362959, 49.55953891, "Earth", Timer(2014, 1, 3, 0, 0 ,0), Source::Natural, Corps::ORBITBODYS);
    Corps* c = earth.clone();
    OrbitalCorps* result = (OrbitalCorps*) c;
    
    CPPUNIT_ASSERT(earth == (*result));
}

/**
 * Test of Getter and Setter Aphelion from Orbital Corps Class
 */
void OrbitalCorpsTestClass::testGetSetAphelion() {
    OrbitalCorps corps;
    double value = 152.098233E+6; 
    corps.setAphelion(value);
    CPPUNIT_ASSERT(corps.getAphelion() == value);
}

/**
 * Test of Getter and Setter Eccentricity Orbit from Orbital Corps Class
 */
void OrbitalCorpsTestClass::testGetSetEccentricityOrbit() {
    OrbitalCorps orbitalCorps;
    double value = 0.0167;
    orbitalCorps.setEccentricityOrbit(value);
    CPPUNIT_ASSERT(value == orbitalCorps.getEccentricityOrbit());
}

/**
 * Test of Getter and Setter Escape Velocity from Orbital Corps Class
 */
void OrbitalCorpsTestClass::testGetSetEscapeVelocity() {
    OrbitalCorps orbitalCorps;
    double value = 11.2;
    orbitalCorps.setEscapeVelocity(11.2);
    CPPUNIT_ASSERT(value == orbitalCorps.getEscapeVelocity());
}
/**
 * Test of Getter and Setter Inclination Axis To Orbit from Orbital Corps Class
 */
void OrbitalCorpsTestClass::testGetSetInclinationAxisToOribit() {
    OrbitalCorps orbitalCorps;
    double value = 23.5;
    orbitalCorps.setInclinationAxisToOribit(value);
    CPPUNIT_ASSERT(orbitalCorps.getInclinationAxisToOribit() == value);
}

/**
 * Test of Getter and Setter Inclination Orbit from Orbital Corps Class
 */
void OrbitalCorpsTestClass::testGetSetInclinationOrbit() {
    OrbitalCorps orbitalCorps;
    double result = 3.394;
    orbitalCorps.setInclinationOrbit(result);
    CPPUNIT_ASSERT(result == orbitalCorps.getInclinationOrbit());
}

/**
 * Test of Getter and Setter Orbital Speed from Orbital Corps Class
 */
void OrbitalCorpsTestClass::testGetSetOrbitalSpeed() {
    OrbitalCorps orbitalCorps;
    double result = 29786.7868;
    orbitalCorps.setOrbitalSpeed(result);
    CPPUNIT_ASSERT(result == orbitalCorps.getOrbitalSpeed());
}

/**
 * Test of Getter and Setter Pherihelion from Orbital Corps Class
 */
void OrbitalCorpsTestClass::testGetSetPerihelion() {
    OrbitalCorps orbitalCorps;
    double result = 147.098291E+6;
    orbitalCorps.setPerihelion(result);
    CPPUNIT_ASSERT(orbitalCorps.getPerihelion() == result);
}

/**
 * Test of Getter and Setter Period from Orbital Corps Class
 */
void OrbitalCorpsTestClass::testGetSetPeriod() {
    OrbitalCorps orbitalCorps;
    double result = 31557600;
    orbitalCorps.setPeriod(result);
    CPPUNIT_ASSERT(orbitalCorps.getPeriod() == result);
}

/**
 * Test of Getter and Setter Semimajor Axis from Orbital Corps Class
 */
void OrbitalCorpsTestClass::testGetSetSemimajorAxis() {
    OrbitalCorps orbitalCorps;
    double result = 1.50E+8;
    orbitalCorps.setSemimajorAxis(result);
    CPPUNIT_ASSERT(orbitalCorps.getSemimajorAxis() == result);
}

void OrbitalCorpsTestClass::testGetSetMeanAnomaly() {
    OrbitalCorps orbitalCorps;
    double result = 1.50E+8;
    orbitalCorps.setMeanAnomaly(result);
    CPPUNIT_ASSERT(orbitalCorps.getMeanAnomaly() == result);
}

void OrbitalCorpsTestClass::testGetSetLongitudePerihelion() {
    OrbitalCorps orbitalCorps;
    double result = 49.55953891;
    orbitalCorps.setLongitudePerihelion(result);
    CPPUNIT_ASSERT(orbitalCorps.getLongitudePerihelion()== result);
}

void OrbitalCorpsTestClass::testGetSetLongitudeAscendingNode() {
    OrbitalCorps orbitalCorps;
    double result = 5.3453234;
    orbitalCorps.setLongitudeAscendingNode(result);
    CPPUNIT_ASSERT(orbitalCorps.getLongitudeAscendingNode() == result);
}


void OrbitalCorpsTestClass::testValidateTrue() {
    OrbitalCorps earth(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), 1.50E+08, 31557600, 29786.7868, 23.5, 0, 0.0167, 11.2, 147.098291E+6, 152.098233E+6, 0.33842279, -23.94362959, 49.55953891,  "Earth", Timer(2014, 1, 3, 0, 0 ,0), Source::Natural, Corps::ORBITBODYS);
    CPPUNIT_ASSERT(earth.validate());
}

void OrbitalCorpsTestClass::testValidateFalse() {
    OrbitalCorps earth(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), 1.50E+08, 31557600, 29786.7868, 23.5, 0, 0.0167, 11.2, 147.098291E+6, 152.098233E+6, 0.33842279, -23.94362959, 49.55953891, "Earth", Timer(2014, 1, 3, 0, 0 ,0), Source::Natural, Corps::ORBITBODYS);
    earth.setPeriod(-21);
    CPPUNIT_ASSERT(!earth.validate());
}

void OrbitalCorpsTestClass::testWritePointerStream() {
    OrbitalCorps c(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), 1.50E+08, 31557600, 29786.7868, 23.5, 0, 0.0167, 11.2, 147.098291E+6, 152.098233E+6, 0.33842279, -23.94362959, 49.55953891, "Earth", Timer(2014, 1, 3, 0, 0 ,0), Source::Natural, Corps::ORBITBODYS);
    stringstream expected;
    stringstream out;
    
    //  PointerStreamOut Corps
    expected << "["<< typeid(c).name() << "] " << c.getName() << "(" << ((c.getSource() == Source::Natural) ? "Natural" : "Manmade") << "," << c.getReferenceDate() <<"," << "{" << c.getCoordinates() << "}" << "," << c.getMass() << "," << c.getDiameter() << "," << c.getDensity() << "," << "{" << c.getSpeed() << "}";
    //  PointerStreamOut OrbitalCorps
    expected << "," << c.getSemimajorAxis() << "," << c.getPeriod() << "," << c.getOrbitalSpeed() << "," << c.getInclinationAxisToOribit() << ", " << c.getInclinationOrbit() << "," << c.getEccentricityOrbit() << "," << c.getEscapeVelocity() << "," << c.getPerihelion() << "," << c.getAphelion() << "," << c.getMeanAnomaly() << "," << c.getLongitudePerihelion() << "," << c.getLongitudeAscendingNode();
    
    c.writePointerStream(out);
    CPPUNIT_ASSERT(out.str() == expected.str());
}

void OrbitalCorpsTestClass::testWriteStream() {
    OrbitalCorps c(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), 1.50E+08, 31557600, 29786.7868, 23.5, 0, 0.0167, 11.2, 147.098291E+6, 152.098233E+6, 0.33842279, -23.94362959, 49.55953891, "Earth", Timer(2014, 1, 3, 0, 0 ,0), Source::Natural, Corps::ORBITBODYS);
    stringstream expected;
    stringstream out;
    
    expected << typeid (c).name() << endl;
    expected << "Source: " << ((c.getSource() == Source::Natural) ? "Natural" : "Manmade") << endl;
    expected << "Name: " << c.getName() << endl;
    expected << "Reference Date: " << c.getReferenceDate()<< endl;
    expected << "Coordinates: " << c.getCoordinates() << endl;
    expected << "Mass: " << c.getMass() << endl;
    expected << "Diameter: " <<c.getDiameter() << endl;
    expected << "Density: " << c.getDensity() << endl;
    expected << "Speed: " << c.getSpeed() << endl;
    expected << "Orbit:" << endl;
    expected << c.getOrbit();
    expected << endl;
    expected << "Semimajor Axis: " << c.getSemimajorAxis() << endl;
    expected << "Period: " << c.getPeriod() << endl;
    expected << "Orbital Speed: " << c.getOrbitalSpeed() << endl;
    expected << "Inclination Axis To Oribit: " << c.getInclinationAxisToOribit() << endl;
    expected << "Inclination Orbit: " << c.getInclinationOrbit() << endl;
    expected << "Eccentricity Orbit: " << c.getEccentricityOrbit() << endl;
    expected << "Escape Velocity: " << c.getEscapeVelocity() << endl;
    expected << "Perihelion: " << c.getPerihelion() << endl;
    expected << "Aphelion: " << c.getAphelion() << endl;
    expected << "Mean Anomaly: " << c.getMeanAnomaly() << endl;
    expected << "Longitude Perihelion: " << c.getLongitudePerihelion() << endl;
    expected << "Longitude Ascending Node: " << c.getLongitudeAscendingNode() << endl;

    c.writeStream(out);
    CPPUNIT_ASSERT(out.str() == expected.str());
}

void OrbitalCorpsTestClass::testEqualsTrue() {
    OrbitalCorps c1(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), 1.50E+08, 31557600, 29786.7868, 23.5, 0, 0.0167, 11.2, 147.098291E+6, 152.098233E+6, 1232.29378, 283, 0, "Earth", Timer(2014, 1, 3, 0, 0 ,0), Source::Natural, Corps::ORBITBODYS);
    OrbitalCorps c2(c1);
    CPPUNIT_ASSERT(c1 == c2);
}

void OrbitalCorpsTestClass::testEqualsFalse() {
    OrbitalCorps c1(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), 1.50E+08, 31557600, 29786.7868, 23.5, 0, 0.0167, 11.2, 147.098291E+6, 152.098233E+6, 1232.29378, 283, 0, "Earth", Timer(2014, 1, 3, 0, 0 ,0), Source::Natural, Corps::ORBITBODYS);
    OrbitalCorps c2(c1);
    c2.setMass(1234);
    CPPUNIT_ASSERT(!(c1 == c2));
}

