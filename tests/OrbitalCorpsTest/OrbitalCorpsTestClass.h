/*
 * File:   OrbitalCorpsClass.h
 * Author: dnamorim
 *
 * Created on 11/jan/2015, 20:29:03
 */

#ifndef ORBITALCORPSCLASS_H
#define	ORBITALCORPSCLASS_H

#include <cppunit/extensions/HelperMacros.h>

class OrbitalCorpsTestClass : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(OrbitalCorpsTestClass);

    CPPUNIT_TEST(testClone);
    CPPUNIT_TEST(testGetSetAphelion);
    CPPUNIT_TEST(testGetSetEccentricityOrbit);
    CPPUNIT_TEST(testGetSetEscapeVelocity);
    CPPUNIT_TEST(testGetSetInclinationAxisToOribit);
    CPPUNIT_TEST(testGetSetInclinationOrbit);
    CPPUNIT_TEST(testGetSetOrbitalSpeed);
    CPPUNIT_TEST(testGetSetPerihelion);
    CPPUNIT_TEST(testGetSetPeriod);
    CPPUNIT_TEST(testGetSetSemimajorAxis);
    CPPUNIT_TEST(testGetSetMeanAnomaly);
    CPPUNIT_TEST(testGetSetMeanAnomaly);
    CPPUNIT_TEST(testGetSetLongitudePerihelion);
    CPPUNIT_TEST(testValidateTrue);
    CPPUNIT_TEST(testValidateFalse);
    CPPUNIT_TEST(testWritePointerStream);
    CPPUNIT_TEST(testWriteStream);
    CPPUNIT_TEST(testEqualsTrue);
    CPPUNIT_TEST(testEqualsFalse);

    CPPUNIT_TEST_SUITE_END();

public:
    OrbitalCorpsTestClass();
    virtual ~OrbitalCorpsTestClass();
    void setUp();
    void tearDown();

private:
    void testClone();
    void testGetSetAphelion();
    void testGetSetEccentricityOrbit();
    void testGetSetEscapeVelocity();
    void testGetSetInclinationAxisToOribit();
    void testGetSetInclinationOrbit();
    void testGetSetOrbitalSpeed();
    void testGetSetPerihelion();
    void testGetSetPeriod();
    void testGetSetSemimajorAxis();
    void testGetSetMeanAnomaly();
    void testGetSetLongitudePerihelion();
    void testGetSetLongitudeAscendingNode();
    void testValidateTrue();
    void testValidateFalse();
    void testWritePointerStream();
    void testWriteStream();
    void testEqualsTrue();
    void testEqualsFalse();

};

#endif	/* ORBITALCORPSCLASS_H */

