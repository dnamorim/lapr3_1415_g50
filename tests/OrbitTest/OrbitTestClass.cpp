/*
 * File:   OrbitTestClass.cpp
 * Author: dnamorim
 *
 * Created on 12/jan/2015, 22:46:09
 */

#include "OrbitTestClass.h"
#include "../../model/OrbitalCorps/OrbitalCorps.h"
#include "../../model/Orbit/Orbit.h"


CPPUNIT_TEST_SUITE_REGISTRATION(OrbitTestClass);

OrbitTestClass::OrbitTestClass() {
}

OrbitTestClass::~OrbitTestClass() {
}

void OrbitTestClass::setUp() {
}

void OrbitTestClass::tearDown() {
}

void OrbitTestClass::testAddOrbit() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testAddOrbit2() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testAddOrbits() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testCalMass() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testClear() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testClone() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testContainsAllOrbit() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testContainsOrbit() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testContainsOrbit2() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testDefined() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testEdited() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testGetAllBodys() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testGetBodys() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testGetCenter() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testGetOrbit() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testIndexOrbit() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testNumberOrbits() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testRemoveOrbit() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testRemoveOrbit2() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testValidateOrbit() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

void OrbitTestClass::testWriteStream() {
    if (true /*check result*/) {
        CPPUNIT_ASSERT(false);
    }
}

