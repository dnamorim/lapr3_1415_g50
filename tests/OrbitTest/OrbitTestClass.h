/*
 * File:   OrbitTestClass.h
 * Author: dnamorim
 *
 * Created on 12/jan/2015, 22:46:09
 */

#ifndef ORBITTESTCLASS_H
#define	ORBITTESTCLASS_H

#include <cppunit/extensions/HelperMacros.h>

class OrbitTestClass : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(OrbitTestClass);

    CPPUNIT_TEST(testAddOrbit);
    CPPUNIT_TEST(testAddOrbit2);
    CPPUNIT_TEST(testAddOrbits);
    CPPUNIT_TEST(testCalMass);
    CPPUNIT_TEST(testClear);
    CPPUNIT_TEST(testClone);
    CPPUNIT_TEST(testContainsAllOrbit);
    CPPUNIT_TEST(testContainsOrbit);
    CPPUNIT_TEST(testContainsOrbit2);
    CPPUNIT_TEST(testDefined);
    CPPUNIT_TEST(testEdited);
    CPPUNIT_TEST(testGetAllBodys);
    CPPUNIT_TEST(testGetBodys);
    CPPUNIT_TEST(testGetCenter);
    CPPUNIT_TEST(testGetOrbit);
    CPPUNIT_TEST(testIndexOrbit);
    CPPUNIT_TEST(testNumberOrbits);
    CPPUNIT_TEST(testRemoveOrbit);
    CPPUNIT_TEST(testRemoveOrbit2);
    CPPUNIT_TEST(testValidateOrbit);
    CPPUNIT_TEST(testWriteStream);

    CPPUNIT_TEST_SUITE_END();

public:
    OrbitTestClass();
    virtual ~OrbitTestClass();
    void setUp();
    void tearDown();

private:
    void testAddOrbit();
    void testAddOrbit2();
    void testAddOrbits();
    void testCalMass();
    void testClear();
    void testClone();
    void testContainsAllOrbit();
    void testContainsOrbit();
    void testContainsOrbit2();
    void testDefined();
    void testEdited();
    void testGetAllBodys();
    void testGetBodys();
    void testGetCenter();
    void testGetOrbit();
    void testIndexOrbit();
    void testNumberOrbits();
    void testRemoveOrbit();
    void testRemoveOrbit2();
    void testValidateOrbit();
    void testWriteStream();

};

#endif	/* ORBITTESTCLASS_H */

