/*
 * File:   UtilsTestClass.h
 * Author: dnamorim
 *
 * Created on 12/jan/2015, 22:52:22
 */

#ifndef UTILSTESTCLASS_H
#define	UTILSTESTCLASS_H

#include <cppunit/extensions/HelperMacros.h>

class UtilsTestClass : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(UtilsTestClass);

    CPPUNIT_TEST(testGetExtension);
    CPPUNIT_TEST(testGetString);
    CPPUNIT_TEST(testGetUnit);
    CPPUNIT_TEST(testIs_file_exist);
    CPPUNIT_TEST(testRemoveAll);
    CPPUNIT_TEST(testSplit);

    CPPUNIT_TEST_SUITE_END();

public:
    UtilsTestClass();
    virtual ~UtilsTestClass();
    void setUp();
    void tearDown();

private:
    void testGetExtension();
    void testGetString();
    void testGetUnit();
    void testIs_file_exist();
    void testRemoveAll();
    void testSplit();

};

#endif	/* UTILSTESTCLASS_H */

