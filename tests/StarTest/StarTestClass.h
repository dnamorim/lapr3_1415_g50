/*
 * File:   StarTestClass.h
 * Author: dnamorim
 *
 * Created on 11/jan/2015, 19:34:12
 */

#ifndef STARTESTCLASS_H
#define	STARTESTCLASS_H

#include <cppunit/extensions/HelperMacros.h>

class StarTestClass : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(StarTestClass);

    CPPUNIT_TEST(testClone);
    CPPUNIT_TEST(testGetSetDiameter);
    CPPUNIT_TEST(testGetSetMass);
    CPPUNIT_TEST(testGetSetName);
    CPPUNIT_TEST(testValidateTrue);
    CPPUNIT_TEST(testValidateFalse);
    CPPUNIT_TEST(testWritePointerStream);
    CPPUNIT_TEST(testWriteStream);
    CPPUNIT_TEST(testEqualsTrue);
    CPPUNIT_TEST(testEqualsFalse);

    CPPUNIT_TEST_SUITE_END();

public:
    StarTestClass();
    virtual ~StarTestClass();
    void setUp();
    void tearDown();

private:
    void testClone();
    void testGetSetDiameter();
    void testGetSetMass();
    void testGetSetName();
    void testValidateTrue();
    void testValidateFalse();
    void testWritePointerStream();
    void testWriteStream();
    void testEqualsTrue();
    void testEqualsFalse();

};

#endif	/* STARTESTCLASS_H */

