/*
 * File:   StarTestClass.cpp
 * Author: dnamorim
 *
 * Created on 11/jan/2015, 19:34:12
 */

#include <iostream>
#include <string>

#include "StarTestClass.h"
#include "../../model/Star/Star.h"


CPPUNIT_TEST_SUITE_REGISTRATION(StarTestClass);

StarTestClass::StarTestClass() {
}

StarTestClass::~StarTestClass() {
}

void StarTestClass::setUp() {
}

void StarTestClass::tearDown() {
}

void StarTestClass::testClone() {
    Star s("Sun", 1.970e+30, 1.440e+6);
    
    Star* result = s.clone();
    
    CPPUNIT_ASSERT(s == (*result)); //  check if the pointer of the clone matches the Star replicated
}

/**
 * Test of Getter and Setter Diameter of Star class
 */
void StarTestClass::testGetSetDiameter() {
    Star star;
    
    double value = 1.440E+6;
    star.setDiameter(value);
    
    CPPUNIT_ASSERT(star.getDiameter() == value);
}

/**
 * Test of Getter and Setter Mass of Star class
 */
void StarTestClass::testGetSetMass() {
    Star star;
    
    double value = 1.97076E+30; 
    star.setMass(value);
    
    CPPUNIT_ASSERT(star.getMass() == value);
}

/**
 * Test of Getter and Setter Name of Star class
 */
void StarTestClass::testGetSetName() {
    Star star;
    
    string value = "Sun";
    star.setName(value);
    
    CPPUNIT_ASSERT(star.getName() == value);
}

/**
 * Test of Validate of Star class - Success Scenario
 */
void StarTestClass::testValidateTrue() {
    Star star("Sun", 1.970e+30, 1.440e+6);
    
    CPPUNIT_ASSERT(star.validate());
}

/**
 * Test of Validate of Star class - Insuccess Scenario
 */
void StarTestClass::testValidateFalse() {
    Star star;
    star.setDiameter(-23);
    
    CPPUNIT_ASSERT(!star.validate());
}

/**
 * Test of writePointerStream of class Star
 */
void StarTestClass::testWritePointerStream() {
    Star s("Sun", 1.970e+30, 1.440e+6);
    
    stringstream expected;
    expected << "[Star]" << s.getName() << "(" << s.getMass() << "," << s.getDiameter();
    
    stringstream out;
    s.writePointerStream(out);
    
    CPPUNIT_ASSERT(out.str() == expected.str());
}

/**
 * Test of writeStream of class Star
 */
void StarTestClass::testWriteStream() {
    Star s("Sun", 1.970e+30, 1.440e+6);
    
    stringstream expected;
    expected << "Name: "<< s.getName() << endl;
    expected << "Mass: " << s.getMass() << endl;
    expected << "Diameter: "<< s.getDiameter() << endl;
    
    stringstream out;
    s.writeStream(out);
    
    CPPUNIT_ASSERT(out.str() == expected.str());
}

/**
 * Test of operator == of Star Class - Success Scenario (and also Copy Constructor)
 */
void StarTestClass::testEqualsTrue() {
    Star s1("Sun", 1.970e+30, 1.440e+6);
    Star s2(s1);
    
    CPPUNIT_ASSERT(s1 == s2);
}

/**
 * Test of operator == of Star Class - Insuccess Scenario
 */
void StarTestClass::testEqualsFalse() {
    Star s1, s2;
    s2.setDiameter(1.432e+4);
    
    CPPUNIT_ASSERT(!(s1 == s2));
}

