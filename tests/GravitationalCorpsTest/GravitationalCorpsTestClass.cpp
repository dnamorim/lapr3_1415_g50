/*
 * File:   GravitationalCorpsTest.cpp
 * Author: dnamorim
 *
 * Created on 12/jan/2015, 22:47:38
 */

#include "GravitationalCorpsTestClass.h"
#include "../../model/Orbit/Orbit.h"
#include "../../model/GravitationalCorps/GravitationalCorps.h"


CPPUNIT_TEST_SUITE_REGISTRATION(GravitationalCorpsTestClass);

GravitationalCorpsTestClass::GravitationalCorpsTestClass() {
}

GravitationalCorpsTestClass::~GravitationalCorpsTestClass() {
}

void GravitationalCorpsTestClass::setUp() {
}

void GravitationalCorpsTestClass::tearDown() {
}

void GravitationalCorpsTestClass::testClone() {
    GravitationalCorps gravCorps(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Satellite", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    GravitationalCorps* result = (GravitationalCorps*) (gravCorps.clone());
    CPPUNIT_ASSERT(gravCorps == (*result));
}

void GravitationalCorpsTestClass::testValidateTrue() {
    GravitationalCorps corps(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Satellite", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    CPPUNIT_ASSERT(corps.validate());
}

void GravitationalCorpsTestClass::testValidateFalse() {
    GravitationalCorps corps(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Satellite", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    corps.setMass(-2);
    CPPUNIT_ASSERT(!corps.validate());
}

void GravitationalCorpsTestClass::testWritePointerStream() {
    GravitationalCorps sat(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Satellite", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    stringstream out;
    stringstream expected;
    expected << "[" << typeid (sat).name() << "] " << sat.getName() << "(" << ((sat.getSource() == Source::Natural) ? "Natural" : "Manmade") << "," << sat.getReferenceDate() <<"," << "{" << sat.getCoordinates() << "}" << "," << sat.getMass() << "," << sat.getDiameter() << "," << sat.getDensity() << "," << "{" << sat.getSpeed() << "}";
    
    sat.writePointerStream(out);
    CPPUNIT_ASSERT(out.str() == expected.str());
}

void GravitationalCorpsTestClass::testWriteStream() {
    GravitationalCorps sat(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Satellite", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    stringstream out;
    stringstream expected;
    expected << typeid (sat).name() << endl;
    expected << "Source: " << ((sat.getSource() == Source::Natural) ? "Natural" : "Manmade") << endl;
    expected << "Name: " << sat.getName() << endl;
    expected << "Reference Date: " << sat.getReferenceDate() << endl;
    expected << "Coordinates: " << sat.getCoordinates() << endl;
    expected << "Mass: " << sat.getMass() << endl;
    expected << "Diameter: " << sat.getDiameter() << endl;
    expected << "Density: " << sat.getDensity() << endl;
    expected << "Speed: " << sat.getSpeed() << endl;
    expected << "Orbit:" << endl;
    expected << sat.getOrbit();
    expected << endl;
    
    sat.writeStream(out);
    CPPUNIT_ASSERT(out.str() == expected.str());
}

void GravitationalCorpsTestClass::testEqualsTrue() {
    GravitationalCorps c1(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Satellite", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    GravitationalCorps c2(c1);
    CPPUNIT_ASSERT(c1 == c2);
}

void GravitationalCorpsTestClass::testEqualsFalse() {
    GravitationalCorps c1(5.97694449E+24, 12800, 5.52, XYZ(-0.213136685094, 0.880776815814, 0.381786157002), XYZ(-2.555128185578E+4, -0.522029758323E+4, -0.226102251360E+4), "Satellite", Timer(2014, 1, 3, 0, 0, 0, true), Source::Natural, Corps::ORBITBODYS);    
    GravitationalCorps c2(c1);
    c2.setMass(1234);
    CPPUNIT_ASSERT(!(c1 == c2));
}