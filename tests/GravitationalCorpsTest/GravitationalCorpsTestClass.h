/*
 * File:   GravitationalCorpsTest.h
 * Author: dnamorim
 *
 * Created on 12/jan/2015, 22:47:38
 */

#ifndef GRAVITATIONALCORPSTEST_H
#define	GRAVITATIONALCORPSTEST_H

#include <cppunit/extensions/HelperMacros.h>

class GravitationalCorpsTestClass : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(GravitationalCorpsTestClass);

    CPPUNIT_TEST(testClone);
    CPPUNIT_TEST(testValidateTrue);
    CPPUNIT_TEST(testValidateFalse);
    CPPUNIT_TEST(testEqualsTrue);
    CPPUNIT_TEST(testEqualsFalse);
    CPPUNIT_TEST(testWritePointerStream);
    CPPUNIT_TEST(testWriteStream);

    CPPUNIT_TEST_SUITE_END();

public:
    GravitationalCorpsTestClass();
    virtual ~GravitationalCorpsTestClass();
    void setUp();
    void tearDown();

private:
    void testClone();
    void testValidateTrue();
    void testValidateFalse();
    void testEqualsTrue();
    void testEqualsFalse();
    void testWritePointerStream();
    void testWriteStream();

};

#endif	/* GRAVITATIONALCORPSTEST_H */

