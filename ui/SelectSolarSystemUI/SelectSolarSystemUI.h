#ifndef SELECTSOLARSYSTEMUI_H
#define	SELECTSOLARSYSTEMUI_H
/**
 * Selecionar Sistema Solar UI - UC1
 * @file SelectSolarSystemUI.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 10 de Janeiro de 2015, às 13:10 por Ricardo Rodrigues <1120703@isep.ipp.pt>
 *  -Estrutura Basica do UI
 *  -Método do RUN
 */

#include <iostream>
#include <map>
#include <iterator>
#include <string>

using namespace std;

#include "../UI.h"
#include "../../controller/SelectSolarSystemController/SelectSolarSystemController.h"

class SelectSolarSystemUI : public UI {
private:
    SelectSolarSystemController controller;
public:
    
    /**
     * Construir Instância de SelectSolarSystemUI(COMPLETO)
     */
    SelectSolarSystemUI();
    
    /**
     * Construir Instância de SelectSolarSystemUI(CÓPIA)
     * @param copy Instância de SelectSolarSystemUI a Cópiar
     */
    SelectSolarSystemUI(const SelectSolarSystemUI& copy);
    
    /**
     * Destrotor da Instância de SelectSolarSystemUI
     */
    virtual ~SelectSolarSystemUI();
    
    /**
     * Verificar se é Possivel Executou UI
     * @return true se for possivel executou UI, caso contrário retorna false
     */
    virtual bool canRun() const;
    
    /**
    * Executar UI
    * @return Sucesso da Operação
    */
    virtual bool run();

};

#endif	/* SELECTSOLARSYSTEMUI_H */

