#include "SelectSolarSystemUI.h"

SelectSolarSystemUI::SelectSolarSystemUI() : UI(), controller() {
}

SelectSolarSystemUI::SelectSolarSystemUI(const SelectSolarSystemUI& copy) : UI(copy), controller(copy.controller) {
}

SelectSolarSystemUI::~SelectSolarSystemUI() {
}

bool SelectSolarSystemUI::canRun() const {
    return this->controller.canRun();
}

bool SelectSolarSystemUI::run() {
    if (!this->canRun()) {
        return false;
    }
    this->uirun = true;
    cout << "Select Solar Systems" << endl;
    map<int, string> list = this->controller.getSolarSystems();
    if (list.size() <= 0) {
        cout << "There is no Solar Systems to Select!" << endl;
        return false;
    }
    cout << "List of Solar Systems to Select:" << endl;
    int i=1;
    for (map<int, string>::iterator it = list.begin(); it != list.end(); ++it,i++) {
        cout << i << " - " << it->second << endl;
    }
    cout << "0 - Back" << endl;
    int op = 0;
    do{
        if (!Utils::getValue<int>(op, "Select Solar System: ", "Invalid Solar System!\n", false, false, cin, cout) || op == 0) {
            return true;
        }
    }while(op<0 && op>i);
    i=1;
    for(map<int, string>::iterator it = list.begin(); it != list.end(); ++it,i++){
        if(op==i){
            op=it->first;
            break;
        }
    }
    if (this->controller.loadSolarSystem(op)) {
        cout << this->controller.getSolarSystem();
        if (this->confirm()) {
            return this->controller.save();
        }
    } else {
        cout << "Invalid Solar System!" << endl;
    }
    return false;
}