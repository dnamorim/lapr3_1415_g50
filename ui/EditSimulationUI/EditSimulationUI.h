#ifndef EDITSIMULATIONUI_H
#define	EDITSIMULATIONUI_H
/**
 * Editar Simulação UI - UC7
 * @file EditSimulationUI.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 10 de Janeiro de 2015, 15:20 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Estrutura Basica do UI
 *  -Método do RUN
 */

#include <iostream>
#include <string>

using namespace std;

#include "../UI.h"
#include "../../controller/EditSimulationController/EditSimulationController.h"

class EditSimulationUI:public UI {
private:
    EditSimulationController controller;
public:
    /**
     * Construir Instância de EditSimulationUI(COMPLETO)
     */
    EditSimulationUI();
    
    /**
     * Construir Instância de EditSimulationUI(CÓPIA)
     * @param copy Instância de EditSimulationUI a Cópiar
     */
    EditSimulationUI(const EditSimulationUI& copy);
    
    /**
     * Destrotor da Instância de EditSimulationUI
     */
    virtual ~EditSimulationUI();
    
    /**
     * Verificar se é Possivel Executou UI
     * @return true se for possivel executou UI, caso contrário retorna false
     */
    virtual bool canRun() const;
    
    /**
     * Executar UI
     * @return Sucesso da Operação
     */
    virtual bool run();


};

#endif	/* EDITSIMULATIONUI_H */

