#include "EditSimulationUI.h"

EditSimulationUI::EditSimulationUI():UI(), controller() {
}

EditSimulationUI::EditSimulationUI(const EditSimulationUI& copy):UI(copy), controller(copy.controller)  {
}

EditSimulationUI::~EditSimulationUI() {
}

bool EditSimulationUI::canRun() const{
    return this->controller.canRun();
}

bool EditSimulationUI::run(){
    if (!this->canRun()) {
        return false;
    }
    cout << "Edit Simulation" << endl;
    this->uirun = true;
    map<int, string> list = this->controller.getSimulations();
    if (list.size() <= 0) {
        cout << "There is no Simulation to Edit!" << endl;
        return false;
    }
    cout << "List of Simulation to Edit:" << endl;
    int i=1;
    for (map<int, string>::iterator it = list.begin(); it != list.end(); ++it,i++) {
        cout << i << " - " << it->second << endl;
    }
    cout << "0 - Back" << endl;
    int op = 0;
    do{
        if (!Utils::getValue<int>(op, "Select Simulation: ", "Invalid Simulation!\n", false, false, cin, cout) || op == 0) {
            return true;
        }
    }while(op<0 && op>i);
    i=1;
    for(map<int, string>::iterator it = list.begin(); it != list.end(); ++it,i++){
        if(op==i){
            op=it->first;
            break;
        }
    }
    if (this->controller.loadSimulation(op)) {
        cin >> this->controller.getSimulation();
        cout << this->controller.getSimulation();
        if (this->confirm()) {
            return this->controller.save();
        }
    } else {
        cout << "Invalid Simulation!" << endl;
    }
    return false;
}