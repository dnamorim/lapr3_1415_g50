#ifndef CREATESOLARSYSTEMUI_H
#define CREATESOLARSYSTEMUI_H
/**
 * Criar Sistema Solar UI - UC2
 * @file CreateSolarSystemUI.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 18 de Dezembro de 2014, às 01:45 por Duarte Amorim <1130674@isep.ipp.pt>
 *  -Estrutura Basica do UI
 * 
 * >Modificado a 8 de Janeiro de 2015, às 21:04 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Métodos do RUN
 */

#include <iostream>

using namespace std;

#include "../UI.h"
#include "../../controller/CreateSolarSystemController/CreateSolarSystemController.h"

class CreateSolarSystemUI : public UI {
private:
    CreateSolarSystemController controller;
public:
    /**
     * Construir Instância de CreateSolarSystemUI(COMPLETO)
     */
    CreateSolarSystemUI();
    
    /**
     * Construir Instância de CreateSolarSystemUI(CÓPIA)
     * @param copy Instância de CreateSolarSystemUI a Cópiar
     */
    CreateSolarSystemUI(const CreateSolarSystemUI& copy);
    
    /**
     * Destrotor da Instância de CreateSolarSystemUI
     */
    ~CreateSolarSystemUI();
    
    /**
     * Verificar se é Possivel Executou UI
     * @return true se for possivel executou UI, caso contrário retorna false
     */
    virtual bool canRun() const;
    
    /**
    * Executar UI
    * @return Sucesso da Operação
    */
    virtual bool run();
};

#endif /* CREATESOLARSYSTEMUI_H */
