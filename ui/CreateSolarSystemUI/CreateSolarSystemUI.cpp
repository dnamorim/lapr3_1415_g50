#include "CreateSolarSystemUI.h"

CreateSolarSystemUI::CreateSolarSystemUI() : UI(), controller() {
}

CreateSolarSystemUI::CreateSolarSystemUI(const CreateSolarSystemUI& copy): UI(copy),controller(copy.controller){
    
}

CreateSolarSystemUI::~CreateSolarSystemUI() {
    
}
bool CreateSolarSystemUI::canRun() const{
    return this->controller.canRun();
}

bool CreateSolarSystemUI::run() {
    if(!this->canRun()){
       return false;
    }
    this->uirun=true;
    cout << "Create a new Solar System" << endl;
    
    string name=this->controller.getNameSolarSystem();
    Utils::getString(name,"Name: ","Kept Value Name\n", true,false,cin,cout);
    this->controller.setNameSolarSystem(name);
            
    cin >> this->controller.getStar();
    
    string file;
    Utils::getString(file,"Provide the full/relative path to the file with Solar System Planets Information:\n> ","File Not Found!\n", false,true);
    if(!this->controller.importPlanets(file)){
        cout << "\nSomething went wrong when importing the file. Check if has a supported extension and format." << endl;
        return false;
    }
    cout << this->controller.getSolarSystem();
    if(UI::confirm()){
        return this->controller.save();
    }
    return false;
}

