#ifndef COPYSOLARSYSTEMUI_H
#define	COPYSOLARSYSTEMUI_H
/**
 * Criar Nova Sistema Solar a partir de Existente UI - UC3
 * @file CopySolarSystemUI.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 10 de Janeiro de 2015, 21:45 por Sara Freitas <1130489@isep.ipp.pt>
 *  -Estrutura Basica do UI
 *  -Método do RUN
 */

#include <iostream>
#include <map>
#include <iterator>
#include <string>
using namespace std;

#include "../UI.h"
#include "../../controller/CopySolarSystemController/CopySolarSystemController.h"

class CopySolarSystemUI : public UI {
private:
	CopySolarSystemController controller;
public:

    /**
     * Construir Instância de CopySolarSystemUI(COMPLETO)
     */
    CopySolarSystemUI();
    
    /**
     * Construir Instância de CopySolarSystemUI(CÓPIA)
     * @param copy Instância de CopySolarSystemUI a Cópiar
     */
    CopySolarSystemUI(const CopySolarSystemUI& copy);
	
    /**
	* Destrotor da Instância de CopySolarSystemUI
	*/
    virtual ~CopySolarSystemUI();
    
	/**
	* Verificar se é Possivel Executou UI
	* @return true se for possivel executou UI, caso contrário retorna false
	*/
	virtual bool canRun() const;
	
	/**
	* Executar UI
	* @return Sucesso da Operação
	*/
	virtual bool run();


};

#endif	/* COPYSOLARSYSTEMUI_H */

