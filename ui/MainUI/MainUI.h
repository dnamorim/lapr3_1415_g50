#ifndef MAINUI_H
#define MAINUI_H
/**
 * UI Principal do Simulador
 * @file MainUI.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 17 de Dezembro de 2014, 17:14 por Duarte Amorim <1130674@isep.ipp.pt>
 *  -Criação da classe principal da Aplicação
 *  -Definição do Menu Principal da Aplicação
 */

#include <string>
#include <ctime>
#include <sstream>

using namespace std;

//comentar include DataLayerBD para fazer testes unitários
#include "../../datalayer/DataLayerBD/DataLayerBD.h"
#include "../../datalayer/DataLayerMockObjects/DataLayerMockObjects.h"

#include "../UI.h"
#include "../SelectSolarSystemUI/SelectSolarSystemUI.h"
#include "../CreateSolarSystemUI/CreateSolarSystemUI.h"
#include "../CopySolarSystemUI/CopySolarSystemUI.h"
#include "../EditSolarSystemUI/EditSolarSystemUI.h"
#include "../CreateNewSimulationUI/CreateNewSimulationUI.h"
#include "../CopySimulationUI/CopySimulationUI.h"
#include "../EditSimulationUI/EditSimulationUI.h"
#include "../DeleteSimulationUI/DeleteSimulationUI.h"
#include "../RunSimulationUI/RunSimulationUI.h"
#include "../SimulationResultsUI/SimulationResultsUI.h"
#include "../FilterResultsSimulationUI/FilterResultsSimulationUI.h"

class MainUI : public UI {
private:
    void showMenu();
    bool load();
    bool save();
    bool datastore();
public:
    /**
     * Construir Instância de MainUI(COMPLETO)
     */
    MainUI();
    
    /**
     * Construir Instância de MainUI(CÓPIA)
     * @param copy Instância de MainUI a Cópiar
     */
    MainUI(const MainUI& copy);
    
    /**
     * Destrotor da Instância de MainUI
     */
    ~MainUI();
    
    /**
     * Verificar se é Possivel Executou UI
     * @return true se for possivel executou UI, caso contrário retorna false
     */
    virtual bool canRun() const;
    
    /**
     * Executar UI
     * @return Sucesso da Operação
     */
    virtual bool run();
};

#endif /* MAINUI_H */
