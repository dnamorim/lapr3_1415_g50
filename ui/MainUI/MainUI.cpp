#include "MainUI.h"

MainUI::MainUI() : UI() {
    if(!this->run()){
        cout << "[ERROR in Main App]" << endl;
    }
}

MainUI::MainUI(const MainUI& copy) : UI(copy) {
}

MainUI::~MainUI() {
}

bool MainUI::canRun() const {
    return Controller::dataLayer != NULL;
}

bool MainUI::run() {
    datastore();
    if (!this->canRun() || !this->load()) {
        return false;
    }
    int opt;
    this->uirun = true;
    do {
        
        this->showMenu();
        Utils::getValue<int>(opt, "Insert an option: ", "The option inserted cannot be recognized.\n ", false, false, cin, cout);
        cout << endl;
        UI* run = NULL;
        switch (opt) {
            case 1:
                run = new SelectSolarSystemUI();
                break;
            case 2:
                run = new CreateSolarSystemUI();
                break;
            case 3:
                run = new CopySolarSystemUI();
                break;
            case 4:
                run = new EditSolarSystemUI();
                break;
            case 5:
                run = new CreateNewSimulationUI();
                break;
            case 6:
                run = new CopySimulationUI();
                break;
            case 7:
                run = new EditSimulationUI();
                break;
            case 8:
                run = new DeleteSimulationUI();
                break;
            case 9:
                run = new RunSimulationUI();
                break;
            case 10:
                run = new SimulationResultsUI();
                break;
            case 11:
                run = new FilterResultsSimulationUI();
                break;
#ifdef DEBUG_CONTROLLER_VAR
            case 1235987:
                //[MODO DEBUG DAS VARIÁVEIS DE AMBIENTE]
                Controller::showData(cout);
                this->pause();
                break;
#endif
            case 0:
                cout << "Quitting the Simulator..." << endl;
                break;
            default:
                cout << "Please, provide a valid option within the menu." << endl;
                UI::pause();
                break;
        }
        if (run != NULL) {
            clock_t t = clock();
            if (run->run()) {
                cout << "[Successfully in ";
            } else {
                cout << "[Unsuccessfully in ";
            }
            t -= clock();
            delete run;
            
            streamsize ss = cout.precision();
            cout.precision(2);
            cout << (((float) t) / CLOCKS_PER_SEC) << " seconds]" << endl;
            cout.precision(ss);
            UI::pause();
            cout << endl;
        }
    } while (opt != 0);
    return this->save();
}

void MainUI::showMenu() {
    cout << "LAPR3 Solar System Simulator (G54)\n" << endl;
    
    cout << "SOLAR SYSTEM OPERATIONS" << endl;
    cout << "1. Select a Solar System to become the active one" << endl;
    cout << "2. Create a new Solar System" << endl;
    cout << "3. Create a Solar System from an existing one" << endl;
    cout << "4. Edit a Solar System data" << endl;
    cout << endl;
    
    cout << "SIMULATION OPERATIONS" << endl;
    cout << "5. Create a new Simulation from scratch" << endl;
    cout << "6. Create a new Simulation from an existing one" << endl;
    cout << "7. Edit a Simulation" << endl;
    cout << "8. Delete a Simulation" << endl;
    cout << "9. Run a Simulation" << endl;
    cout << endl;
    
    cout << "RESULTS OPERATIONS" << endl;
    cout << "10. Show and export the global results of a simulation" << endl;
    cout << "11. Filter resuts from simulation" << endl;
    cout << endl;
    cout << "0. Quit Simulator" << endl;
    cout << endl;
}

bool MainUI::datastore(){
    int opt=0;
    do{
        cout << "DATASTORE" << endl;
        cout << "1. Local" << endl;
        cout << "2. Remote" << endl;
        cout << "0. Exit" << endl;
        Utils::getValue<int>(opt, "Option: ", "The option inserted cannot be recognized.\n", false, false, cin, cout);
    }while(opt<0 || opt>2);
    switch (opt) {
        case 1:
            Controller::dataLayer=new DataLayerMockObjects();
            break;
        case 2:
            //comentar bloco try...catch nos testes unitários. Correr testes em modo Debug
            try {
                string user="lapr3_050",password="lapr3g502015",host="gandalf.dei.isep.ipp.pt/pdborcl";
                Utils::getString(user,"Host: ",host+"\n", true,false,cin, cout);
                Utils::getString(user,"User: ",user+"\n", true,false,cin, cout);
                Utils::getString(password,"Password: ","*****\n", true,false,cin, cout);
                Controller::dataLayer = new DataLayerBD(user, password, host);
            } catch (SQLException error) {
                cerr << "Error from Database: " << error.getMessage();
                cout << "This application will terminate." << endl;
                Controller::dataLayer=NULL;
                return false;
            }
            break;
        case 0:
            exit(-1);
        default:
            return false;
    }
    return true;
}

bool MainUI::load() {
    cout << "Load Data..." << endl;
    bool ret = Controller::dataLayer->load();
    if (ret) {
        cout << "[OK] Load Data" << endl;
    } else {
        cout << "[ERROR] Load Data" << endl;
    }
    return ret;
}

bool MainUI::save() {
    cout << "Save Data..." << endl;
    bool ret = Controller::dataLayer->save();
    if (ret) {
        cout << "[OK] Save Data" << endl;
    } else {
        cout << "[ERROR] Save Data" << endl;
    }
    delete(Controller::dataLayer);
    return ret;
}