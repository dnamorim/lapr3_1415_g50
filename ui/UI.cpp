#include "UI.h"

UI::UI():uirun(false) {
}

UI::UI(const UI& copy): uirun(copy.uirun){
    
}
UI::~UI() {
}

bool UI::alreadyRan() const{
    return this->uirun;
}

void UI::clean(){
    if (!cin) {
        cin.clear();
        cin.ignore(cin.rdbuf()->in_avail());
    }
}
bool UI::confirm(const string msg,string conf, bool caseSensitive){
    UI::clean();
    cout << msg;
    string op;
    getline(cin,op);
    if(!caseSensitive){
        transform(op.begin(), op.end(), op.begin(), ::tolower);
        transform(conf.begin(), conf.end(), conf.begin(), ::tolower);
    }
    if(op==conf){
        return true;
    }
    return false;
}

void UI::pause(const string msg) {
    UI::clean();
    cout << msg;
    cin.get();
}
