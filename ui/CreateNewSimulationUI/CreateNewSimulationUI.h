#ifndef CREATENEWSIMULATIONUI_H
#define CREATENEWSIMULATIONUI_H

/**
 * Criar Nova Simulação UI - UC5
 * @file CreateNewSimulationUI.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * Criado a 18 de Dezembro de 2014, às 01:45 por Duarte Amorim <1130674@isep.ipp.pt>
 *  -Estrutura Basica do UI
 *
 * >Modificado a 12 de Janeiro de 2015, às 15:19 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Métodos do RUN
 */

#include <iostream>
#include <map>
#include <iterator>
#include <string>

using namespace std;

#include "../UI.h"
#include "../../controller/CreateNewSimulationController/CreateNewSimulationController.h"

class CreateNewSimulationUI : public UI {
private:
    CreateNewSimulationControlller controller;
public:
    /**
     * Construir Instância de CreateNewSimulationUI(COMPLETO)
     */
    CreateNewSimulationUI();
    
    /**
     * Construir Instância de CreateNewSimulationUI(CÓPIA)
     * @param copy Instância de CreateNewSimulationUI a Cópiar
     */
    CreateNewSimulationUI(const CreateNewSimulationUI& copy);
    
    /**
     * Destrotor da Instância de CreateNewSimulationUI
     */
    ~CreateNewSimulationUI();
    
     /**
     * Verificar se é Possivel Executou UI
     * @return true se for possivel executou UI, caso contrário retorna false
     */
    virtual bool canRun() const;
    
   /**
    * Executar UI
    * @return Sucesso da Operação
    */
    virtual bool run();
    
};

#endif /* CREATENEWSIMULATIONUI_H */
