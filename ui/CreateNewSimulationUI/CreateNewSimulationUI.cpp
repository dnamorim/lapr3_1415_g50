#include "CreateNewSimulationUI.h"

CreateNewSimulationUI::CreateNewSimulationUI() : UI(), controller(){
    
}

CreateNewSimulationUI::CreateNewSimulationUI(const CreateNewSimulationUI& copy): UI(copy), controller(copy.controller){
    
}

CreateNewSimulationUI::~CreateNewSimulationUI() {
    
}
bool CreateNewSimulationUI::canRun() const{
    return this->controller.canRun();
}

bool CreateNewSimulationUI::run() {
    if(!this->canRun()){
       return false;
    }
    this->uirun=true;
    cout << "Create New Simulation" << endl;
    cin >> this->controller.getSimulation();
    cout << this->controller.getSimulation();
    if(UI::confirm()){
        return this->controller.save();
    }
    return false;
}