#ifndef UI_H
#define	UI_H

/**
 * (ESQUELETO)User Interface
 * @file UI.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 16 de Dezembro de 2014 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Esqueleto Básico da UI
 *
 * >Modificado a 17 de Dezembro de 2014, 00:00 por Duarte Amorim <1130674@isep.ipp.pt>
 * - Adicionado método virtual da interface
 * - Adicionado método getValue para garantir a inserção de um tipo primitivo válido por parte do utilizador.
 * 
 * >Modificado a 7 de Janeiro de 2015, 21:54 por Gilberto Pereira<1131251@isep.ipp.pt>
 *  -getValue foi substituído por Utils::getValue<TIPO>
 *  -canRun(), alreadyRan()
 */

#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>

using namespace std;

#include "../controller/Controller.h"
#include "../utils/Utils.h"

class UI {
protected:
    bool uirun;
public:
    
    /**
     * Construir Instância de MainUI(COMPLETO)
     */
    UI();
    
    /**
     * Construir Instância de UI(CÓPIA)
     * @param copy Instância de UI a Cópiar
     */
    UI(const UI& copy);
    
    /**
     * Destrotor da Instância de UI
     */
    virtual ~UI();
    
    /**
     * Verificar se é Possivel Correr UI
     * @return true se for possivel correr UI, caso contrário retorna false
     */
    virtual bool canRun() const=0;
    
    /**
     * Verificar se já Executou UI
     * @return true se já executou UI, caso contrário retorna false
     */
    bool alreadyRan() const;
    
   /**
    * Executar UI
    * @return Sucesso da Operação
    */
    virtual bool run() = 0;
    
    /**
     * Limpar Buffer
     */
    static void clean();
    
    /**
     * Pedir Confirmação
     * @param msg Mensagem de Confirmação
     * @param conf Valor para Confirmar
     * @param caseSensitive Case Sensitive
     * @return Confirmação
     */
    static bool confirm(const string msg="Confirm?(Y/N)", string conf="Y",bool caseSensitive=false);
    
    /**
     * Pausar Programa
     * @param msg Mensagem da Pausa
     */
    static void pause(const string msg="Press <ENTER> to continue...");
};


#endif	/* UI_H */

