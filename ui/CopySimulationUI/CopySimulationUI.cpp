
#include "CopySimulationUI.h"

CopySimulationUI::CopySimulationUI() : UI(), controller() {
}

CopySimulationUI::CopySimulationUI(const CopySimulationUI& copy) : UI(copy), controller(copy.controller) {
}

CopySimulationUI::~CopySimulationUI() {
}

bool CopySimulationUI::canRun() const {
    return this->controller.canRun();
}

bool CopySimulationUI::run() {
    if (!this->canRun()) {
        return false;
    }
    this->uirun = true;
    cout << "Copy Simulation" << endl;
    map<int, string> list = this->controller.getSimulations();
    if (list.size() <= 0) {
        cout << "There is no Simulations to Copy!" << endl;
        return false;
    }
    int i=1;
    for (map<int, string>::iterator it = list.begin(); it != list.end(); ++it,i++) {
        cout << i << " - " << it->second << endl;
    }
    cout << "0 - Back" << endl;
    int op = 0;
    do{
        if (!Utils::getValue<int>(op, "Select Simulation: ", "Invalid Simulation!\n", false, false, cin, cout) || op == 0) {
            return true;
        }
    }while(op<0 && op>i);
    if (this->controller.loadSimulation(op)) {
        cout << this->controller.getSimulation();
        if (this->confirm()) {
            return this->controller.save();
        }
    } else {
        cout << "Invalid Simulation!" << endl;
    }
    return false;
}
