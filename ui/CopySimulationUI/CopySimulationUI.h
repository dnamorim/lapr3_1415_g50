#ifndef COPYSIMULATIONUI_H
#define	COPYSIMULATIONUI_H
/**
 * Criar Nova Simulação a partir de Existente UI - UC6
 * @file CopySimulationUI.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 10 de Janeiro de 2015, 17:45 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Estrutura Basica do UI
 *  -Método do RUN
 */

#include <iostream>

using namespace std;

#include "../UI.h"
#include "../../controller/CopySimulationController/CopySimulationController.h"


class CopySimulationUI: public UI {
private:
    CopySimulationController controller;
public:
    /**
     * Construir Instância de CopySimulationUI(COMPLETO)
     */
    CopySimulationUI();
    
    /**
     * Construir Instância de CopySimulationUI(CÓPIA)
     * @param copy Instância de CopySimulationUI a Cópiar
     */
    CopySimulationUI(const CopySimulationUI& copy);
    
     /**
     * Destrotor da Instância de CopySimulationUI
     */
    virtual ~CopySimulationUI();
    
    /**
     * Verificar se é Possivel Executou UI
     * @return true se for possivel executou UI, caso contrário retorna false
     */
    virtual bool canRun() const;
    
    /**
    * Executar UI
    * @return Sucesso da Operação
    */
    virtual bool run();
    
};

#endif	/* COPYSIMULATIONUI_H */

