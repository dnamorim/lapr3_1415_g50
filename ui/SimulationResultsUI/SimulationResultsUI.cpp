#include "SimulationResultsUI.h"

SimulationResultsUI::SimulationResultsUI() : UI(), controller() {
}

SimulationResultsUI::SimulationResultsUI(const SimulationResultsUI& copy) : UI(copy), controller(copy.controller) {
}

SimulationResultsUI::~SimulationResultsUI() {
}

bool SimulationResultsUI::canRun() const {
    return this->controller.canRun();
}

bool SimulationResultsUI::run() {
    if (!this->canRun()) {
        return false;
    }
    cout << "Export Global Results of a Simulation" << endl;
    map<int, string> list = this->controller.getSimulations();
    if (list.size() <= 0) {
        cout << "There is no Simulation to Export!" << endl;
        return false;
    }
    cout << "List of Simulation to Export:" << endl;
    int i=1;
    for (map<int, string>::iterator it = list.begin(); it != list.end(); ++it,i++) {
        cout << i << " - " << it->second << endl;
    }
    cout << "0 - Back" << endl;
    int op = 0;
    do{
        if (!Utils::getValue<int>(op, "Select Simulation: ", "Invalid Simulation!\n", false, false, cin, cout) || op == 0) {
            return true;
        }
    }while(op<0 && op>i);
    i=1;
    for(map<int, string>::iterator it = list.begin(); it != list.end(); ++it,i++){
        if(op==i){
            op=it->first;
            break;
        }
    }
    if (this->controller.loadSimulation(op)) {
        cout << this->controller.getSimulation();
        if (UI::confirm()) {
            string file;
             Utils::getString(file,"Provide the full/relative path to export the simulation results:\n> ","", false,false);
            if(Utils::getExtension(file) == "gif") {
                    Utils::getValue<int>(op, "Define GIF perspective to export (default XY):\n1 - XY\n2 - XZ\n3 - YZ\nPlan Perspective: ", "Invalid Plan Perspective!\n", false, false, cin, cout);
                    switch (op) {
                        case 2:{
                            ExportSimulationGIF::angle = YZ;
                            break;
                        }
                        case 3: {
                            ExportSimulationGIF::angle = XZ;
                            break;
                        }
                        default: {
                            ExportSimulationGIF::angle = XY;
                            break;
                        }
                    }
            }
            return this->controller.expor(file);
        }
    } else {
        cout << "Invalid Simulation!" << endl;
    }

    return false;
}