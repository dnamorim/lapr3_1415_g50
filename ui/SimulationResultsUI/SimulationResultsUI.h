#ifndef SIMULATIONRESULTSUI_H
#define	SIMULATIONRESULTSUI_H
/**
 * Aprecentar Resultados da Simulação - UC10
 * @file SimulationResultsUI.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 10 de Janeiro de 2015, às 13:10 por Gilberto Pereira <1120703@isep.ipp.pt>
 *  -Estrutura Basica do UI
 *  -Método do RUN
 */

#include <iostream>
#include <map>
#include <iterator>
#include <string>

using namespace std;

#include "../UI.h"
#include "../../controller/SimulationResultsController/SimulationResultsController.h"

class SimulationResultsUI : public UI {
private:
    SimulationResultsController controller;
public:
    
    /**
     * Construir Instância de SelectSolarSystemUI(COMPLETO)
     */
    SimulationResultsUI();
    
    /**
     * Construir Instância de SelectSolarSystemUI(CÓPIA)
     * @param copy Instância de SelectSolarSystemUI a Cópiar
     */
    SimulationResultsUI(const SimulationResultsUI& copy);
    
    /**
     * Destrotor da Instância de SelectSolarSystemUI
     */
    virtual ~SimulationResultsUI();
    
    /**
     * Verificar se é Possivel Executou UI
     * @return true se for possivel executou UI, caso contrário retorna false
     */
    virtual bool canRun() const;
    
    /**
     * Executar UI
     * @return Sucesso da Operação
     */
    virtual bool run();
    
};

#endif	/* SIMULATIONRESULTSUI_H */

