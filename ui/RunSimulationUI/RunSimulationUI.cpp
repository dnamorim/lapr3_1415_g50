#include "RunSimulationUI.h"

RunSimulationUI::RunSimulationUI() : UI() {
}

RunSimulationUI::RunSimulationUI(const RunSimulationUI& copy) : UI(copy), controller(copy.controller) {
}

RunSimulationUI::~RunSimulationUI() {
}

bool RunSimulationUI::canRun() const {
    return this->controller.canRun();
}

bool RunSimulationUI::run() {
    if (!this->canRun()) {
        return false;
    }
    this->uirun = true;
    cout << "Run Simulation" << endl;
    map<int, string> list = this->controller.getSimulations();
    if (list.size() <= 0) {
        cout << "There is no Simulation to Run!" << endl;
        return false;
    }
    cout << "List of Simulation to Run:" << endl;
    int i=1;
    for (map<int, string>::iterator it = list.begin(); it != list.end(); ++it,i++) {
        cout << i << " - " << it->second << endl;
    }
    cout << "0 - Back" << endl;
    int op = 0;
    do{
        if (!Utils::getValue<int>(op, "Select Simulation: ", "Invalid Simulation!\n", false, false, cin, cout) || op == 0) {
            return true;
        }
    }while(op<0 && op>i);
    i=1;
    for(map<int, string>::iterator it = list.begin(); it != list.end(); ++it,i++){
        if(op==i){
            op=it->first;
            break;
        }
    }
    if (this->controller.loadSimulation(op)) {
        if (this->controller.run()) {
            cout << this->controller.getSimulation();
            if (this->confirm()) {
                return this->controller.save();
            }
            return true;
        } else {
            cout << "An error occurred while running the simulation" << endl;
        }
    } else {
        cout << "Invalid Simulation!" << endl;
    }
    return false;
}