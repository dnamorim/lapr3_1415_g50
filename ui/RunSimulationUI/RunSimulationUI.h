#ifndef RUNSIMULATIONUI_H
#define	RUNSIMULATIONUI_H
/**
 * Correr Simulação UI - UC9
 * @file RunSimulationUI.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 10 de Janeiro de 2015, 01:45 por Gilberto Pereira <1131251@isep.ipp.pt>2
 *  -Estrutura Basica do UI
 *  -Método do RUN
 */

#include <iostream>

using namespace std;

#include "../UI.h"
#include "../../controller/RunSimulationController/RunSimulationController.h"

class RunSimulationUI: public UI {
    private:
        RunSimulationController controller;
public:
    /**
     * Construir Instância de SelectSolarSystemUI(COMPLETO)
     */
    RunSimulationUI();
    
     /**
     * Construir Instância de RunSimulationUI(CÓPIA)
     * @param copy Instância de RunSimulationUI a Cópiar
     */
    RunSimulationUI(const RunSimulationUI& copy);
    
    /**
     * Destrotor da Instância de RunSimulationUI
     */
    virtual ~RunSimulationUI();
    
    /**
     * Verificar se é Possivel Executou UI
     * @return true se for possivel executou UI, caso contrário retorna false
     */
    virtual bool canRun() const;
    
    /**
    * Executar UI
    * @return Sucesso da Operação
    */
    virtual bool run();

};

#endif	/* RUNSIMULATIONUI_H */

