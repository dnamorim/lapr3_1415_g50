#include "FilterResultsSimulationUI.h"

FilterResultsSimulationUI::FilterResultsSimulationUI():UI(),controller() {
}

FilterResultsSimulationUI::FilterResultsSimulationUI(const FilterResultsSimulationUI& copy):UI(copy),controller(copy.controller) {
}

FilterResultsSimulationUI::~FilterResultsSimulationUI() {
}

bool FilterResultsSimulationUI::canRun() const {
    return this->controller.canRun();
}

bool FilterResultsSimulationUI::run(){
    if (!this->canRun()) {
        return false;
    }
    cout << "Export Results of a Simulation" << endl;
    map<int, string> list = this->controller.getSimulations();
    if (list.size() <= 0) {
        cout << "There is no Simulation to Export!" << endl;
        return false;
    }
    cout << "List of Simulation to Export:" << endl;
    int i=1;
    for (map<int, string>::iterator it = list.begin(); it != list.end(); ++it,i++) {
        cout << i << " - " << it->second << endl;
    }
    cout << "0 - Back" << endl;
    int op = 0;
    do{
        if (!Utils::getValue<int>(op, "Select Simulation: ", "Invalid Simulation!\n", false, false, cin, cout) || op == 0) {
            return true;
        }
    }while(op<0 && op>i);
    i=1;
    for(map<int, string>::iterator it = list.begin(); it != list.end(); ++it,i++){
        if(op==i){
            op=it->first;
            break;
        }
    }
    if (!this->controller.loadSimulation(op)) {
        cout << "Invalid Simulation!" << endl;
        return false;
    }
    
    Timer startTime,endTime, timeStep;
    startTime.convertDate(true);
    endTime.convertDate(true);
    timeStep.convertDate(false);
    cout << "Start Time:" << endl;
    cin >> startTime;
    cout << "End Time:"<<endl;
    cin >> endTime;
    cout << "Step Time:" << endl;
    cin >> timeStep;
    cout << "Choose the Corps to filter:" << endl;
    vector<int> addCorps;
    vector<int> existsCorps;
    vector<int>::iterator it_f;
    Simulation simulation=this->controller.getSimulation();
    vector<Corps*> corps=simulation.getCorps();
    op=0;
    do{
        int pos=1;
        existsCorps.clear();
        for(vector<Corps*>::const_iterator it=corps.begin();it!=corps.end();++it,pos++){
            it_f = find (addCorps.begin(), addCorps.end(), pos);
            if (it_f == addCorps.end()){
                cout << pos << " - " << (*it)->getName() << endl;
                existsCorps.push_back(pos);
            }
        }
        cout << "0-Back" << endl;
        Utils::getValue<int>(op, "Option: ", "Invalid Option!\n", false, false, cin, cout);
        op=(existsCorps.size()==0)?0:op;
        if(op!=0){
            it_f = find (existsCorps.begin(), existsCorps.end(), op);
            if(it_f!=existsCorps.end()){
                addCorps.push_back(op);
            }
        }
    }while(op!=0);
    if(this->controller.config(startTime,endTime,timeStep,addCorps)){
        cout << this->controller.getSimulation();
        if (UI::confirm()) {
            string file;
            Utils::getString(file,"Provide the full/relative path to export the simulation results:\n> ","", false,false);
            if(Utils::getExtension(file) == "gif") {
                Utils::getValue<int>(op, "Define GIF perspective to export (default XY):\n1 - XY\n2 - XZ\n3 - YZ\nPlan Perspective: ", "Invalid Plan Perspective!\n", false, false, cin, cout);
                switch (op) {
                    case 2:{
                        ExportSimulationGIF::angle = YZ;
                        break;
                    }
                    case 3: {
                        ExportSimulationGIF::angle = XZ;
                        break;
                    }
                    default: {
                        ExportSimulationGIF::angle = XY;
                        break;
                    }
                }
            }
            return this->controller.expor(file);
        }
    }else{
        cout << "An unexpected error ocourred when exporting the simulation results." << endl;
    }
    
    
    return false;
}