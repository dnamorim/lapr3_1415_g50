#ifndef FILTERRESULTSSIMULATIONUI_H
#define	FILTERRESULTSSIMULATIONUI_H
/**
 * Aprecentar e Filtrar Resultados da Simulação UI - UC11
 * @file FilterResultsSimulationUI.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 10 de Janeiro de 2015, às 13:10 por Gilberto Pereira <1120703@isep.ipp.pt>
 *  -Estrutura Basica do UI
 *  -Método do RUN
 */

#include <iostream>
#include <map>
#include <iterator>
#include <string>

using namespace std;

#include "../UI.h"
#include "../../utils/Timer/Timer.h"
#include "../../model/Simulation/Simulation.h"
#include "../../controller/FilterResultsSimulationController/FilterResultsSimulationController.h"

class FilterResultsSimulationUI: public UI {
private:
    FilterResultsSimulationController controller;
public:
    FilterResultsSimulationUI();
    FilterResultsSimulationUI(const FilterResultsSimulationUI& copy);
    virtual ~FilterResultsSimulationUI();
    
    /**
     * Verificar se é Possivel Executou UI
     * @return true se for possivel executou UI, caso contrário retorna false
     */
    virtual bool canRun() const;
    
    /**
     * Executar UI
     * @return Sucesso da Operação
     */
    virtual bool run();
};

#endif	/* FILTERRESULTSSIMULATIONUI_H */

