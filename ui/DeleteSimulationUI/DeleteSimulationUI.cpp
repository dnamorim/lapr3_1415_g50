#include "DeleteSimulationUI.h"

DeleteSimulationUI::DeleteSimulationUI():UI(),controller() {
}

DeleteSimulationUI::DeleteSimulationUI(const DeleteSimulationUI& copy): UI(copy),controller(copy.controller) {
}

DeleteSimulationUI::~DeleteSimulationUI() {
}

bool DeleteSimulationUI::canRun() const {
    return this->controller.canRun();
}

bool DeleteSimulationUI::run() {
    if (!this->canRun()) {
        return false;
    }
    this->uirun = true;
    cout << "Delete Simulation" << endl;
     map<int, string> list = this->controller.getSimulations();
    if (list.size() <= 0) {
        cout << "There is no Simulations to Delete!" << endl;
        return false;
    }
    cout << "List of Simulations to Delete:" << endl;
    int i=1;
    for (map<int, string>::iterator it = list.begin(); it != list.end(); ++it,i++) {
        cout << i << " - " << it->second << endl;
    }
    cout << "0 - Back" << endl;
    int op = 0;
    do{
        if (!Utils::getValue<int>(op, "Select Simulation: ", "Invalid Simulation!\n", false, false, cin, cout) || op == 0) {
            return true;
        }
    }while(op<0 && op>i);
    if (this->controller.loadSimulation(op)) {
        cout << this->controller.getSimulation();
        if (this->confirm()) {
            return this->controller.del();
        }
    } else {
        cout << "Invalid Simulation!" << endl;
    }
    return false;
}

