#ifndef DELETESIMULATIONUI_H
#define	DELETESIMULATIONUI_H
/**
 * Apagar Simulação UI - UC8
 * @file SelectSolarSystemUI.h
 *
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 *
 * @version 1.0
 * >Criado a 10 de Janeiro de 2015, às 15:16 por Ricardo Rodrigues <1120703@isep.ipp.pt>
 *  -Estrutura Basica do UI
 *  -Método do RUN
 */

#include <iostream>
#include <map>
#include <iterator>
#include <string>

using namespace std;

#include "../UI.h"
#include "../../controller/DeleteSimulationController/DeleteSimulationController.h"

class DeleteSimulationUI: public UI {
private:
    DeleteSimulationController controller;
public:
    /**
     * Construir Instância de DeleteSimulationUI(COMPLETO)
     */
    DeleteSimulationUI();
    
    /**
     * Construir Instância de DeleteSimulationUI(CÓPIA)
     * @param copy Instância de DeleteSimulationUI a Cópiar
     */
    DeleteSimulationUI(const DeleteSimulationUI& copy);
    
     /**
     * Destrotor da Instância de DeleteSimulationUI
     */
    virtual ~DeleteSimulationUI();
    
    /**
     * Verificar se é Possivel Executou UI
     * @return true se for possivel executou UI, caso contrário retorna false
     */
    virtual bool canRun() const;
    
    /**
    * Executar UI
    * @return Sucesso da Operação
    */
    virtual bool run();
private:

};

#endif	/* DELETESIMULATIONUI_H */

