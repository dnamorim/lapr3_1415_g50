#include "EditSolarSystemUI.h"

EditSolarSystemUI::EditSolarSystemUI(): UI(),controller() {
}

EditSolarSystemUI::EditSolarSystemUI(const EditSolarSystemUI& copy):UI(copy), controller(copy.controller) {
}

EditSolarSystemUI::~EditSolarSystemUI() {
}

bool EditSolarSystemUI::canRun() const{
    return this->controller.canRun();
}

bool EditSolarSystemUI::run(){
    if (!this->canRun()) {
        return false;
        
    }
    cout << "Edit Solar System" << endl;
    this->uirun = true;
    cin >> this->controller.getSolarSystem();
    cout << this->controller.getSolarSystem();
    if (this->confirm()) {
        return this->controller.save();
        
    }
    return false;
}