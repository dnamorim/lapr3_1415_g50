#ifndef DATALAYER_H
#define	DATALAYER_H
/**
 * (ESQUELETO)Camada de Acesso
 * @file DataLayer.cpp
 * 
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 * 
 * @version 1.0
 * Criado a 7 de Janeiro de 2015, 22:49 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Metodos de Carregar e Guardar de Sistema Solares e Simulações
 */
#include <string>
#include <map>

using namespace std;

#include "../model/SolarSystem/SolarSystem.h"
#include "../model/Simulation/Simulation.h"

class DataLayer{
private:
    bool autodestroy;
public:
    static bool AUTODESTROY;
    /**
     * Construir Instância de DataLayer(COMPLETO)
     */
    DataLayer(bool autodestroy=DataLayer::AUTODESTROY);
    
    /**
     * Construir Instância da DataLayer(CÓPIA)
     * @param copy Instância da DataLayer a Cópiar
     */
    DataLayer(const DataLayer& copy);
    
    /**
     * Destrotor da Instância da DataLayer
     */
    virtual ~DataLayer();
    
    /**
     * Verificar se a DataLayer e Responsavel por Destruir o Objectos Criados
     */
    bool autoDestroy() const;
    
    /**
     * Carregar DataLayer
     * @return Sucesso da Operação
     */
    virtual bool load()=0;
    
    /**
     * Guardar DataLayer
     * @return Sucesso da Operação
     */
    virtual bool save()=0;
    
    /**
     * Verificar se existe o Sistema Solar pelo seu ID
     * @param id ID do Sistema Solar
     * @return Sucesso da Operação
     */
    virtual bool containsSolarSystem(const int& id) const=0;
    
    /**
     * Verificar se existe a Simulação pelo seu ID
     * @param id ID da Simulação
     * @return Sucesso da Operação
     */
    virtual bool containsSimulation(const int& id) const=0;
    
    /**
     * Verificar se existe a Simulação pelo ID do Sistema Solar
     * @param id ID do Sistema Solar
     * @return Sucesso da Operação
     */
    virtual bool containsSimulationByIDSolarSystem(const int& id) const=0;
    
    /**
     * Consultar todos os Sistemas Solares
     * @return Lista de Ids e Nome dos Sistemas Solares
     */
    virtual map<int, string> getSolarSystems()const=0;
    
    /**
     * Consultar todos os Sistemas Solares Activos
     * @return Lista de Ids e Nome dos Sistemas Solares
     */
    virtual map<int, string> getSolarSystemsActive() const=0;
    
    /**
     * Carregar Sistema Solar pelo ID
     * @param id ID do Sistema Solar a Carregar
     * @return Sistema Solar, em Caso de Sucesso. Caso contrario retrona NULL
     */
    virtual SolarSystem* loadSolarSystem(const int& id) const=0;
    
    /**
     * Guardar Sistema Solar
     * @param solarsystem Sistema Solar a Guardar
     * @return Sistema Solar, em Caso de Sucesso. Caso contrario retrona NULL
     */
    virtual SolarSystem* saveSolarSystem(SolarSystem& solarsystem)=0;
    
    /**
     * Apagar Sistema Solar
     * @param id ID da Sistema Solar a Apagar
     * @return Sucesso da Operação
     */
    virtual bool deleteSolarSystem(const int& id)=0;
    
    /**
     * Consultar todos as Simulações
     * @return Lista de Ids e Nome das Simulações
     */
    virtual map<int, string> getSimulations() const=0;
    
    /**
     * Consultar todos as Simulações Activas
     * @return Lista de Ids e Nome das Simulações
     */
    virtual map<int, string>  getSimulationsActive() const=0;
    
    /**
     * Consultar todos as Simulações por Corridas na Base de Dados
     * @return Lista de Ids e Nome das Simulações
     */
    virtual map<int, string> getSimulationsNotRun() const=0;
    
    /**
     * Consultar todos as Simulações já Corridas na Base de Dados
     * @return Lista de Ids e Nome das Simulações
     */
    virtual map<int, string> getSimulationsRun() const=0;
    
     /**
     * Carregar Simulação pelo ID
     * @param id ID da Simulação a Carregar
     * @param simulation Simulação Carregado
     * @return Simulação, em Caso de Sucesso. Caso contrario retrona NULL
     */
    virtual Simulation* loadSimulation(const int& id)const=0;
    
    /**
     * Guardar Simulação
     * @param Simulation Simulação a Guardar
     * @return Simulação, em Caso de Sucesso. Caso contrario retrona NULL
     */
    virtual Simulation* saveSimulation(Simulation& simulation)=0;
    
    /**
     * Guardar Resultados da Simulação
     * @param Simulation Simulação com os Resultados a Guardar
     * @return Simulação, em Caso de Sucesso. Caso contrario retrona NULL
     */
    virtual Simulation* saveSimulationResults(Simulation& simulation)=0;
    
    /**
     * Apagar Simulação
     * @param id ID da Simulação a Apagar
     * @return Sucesso da Operação
     */
    virtual bool deleteSimulation(const int& id)=0;
    
    /**
     * Representação Textual da Instância da DataLayer
     * @param out Stream de Output
     */
    virtual void writeStream(ostream& out) const=0;
    
    /**
     * Sobrecarga de Operador>>(StreamIN)
     * @param out Stream de Input
     * @param dataLayer DataLayer para Input
     * @return StreamIN
     */
    friend ostream& operator<<(ostream &out, const DataLayer &dataLayer);

};

#endif	/* DATALAYER_H */

