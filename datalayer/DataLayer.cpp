#include "DataLayer.h"

bool DataLayer::AUTODESTROY=false;
DataLayer::DataLayer(bool autodestroy): autodestroy(autodestroy) {

}

DataLayer::DataLayer(const DataLayer& copy) {

}

DataLayer::~DataLayer() {

}

bool DataLayer::autoDestroy() const{
    return this->autodestroy;
}

ostream& operator<<(ostream &out, const DataLayer &dataLayer) {
    dataLayer.writeStream(out);
    return out;
}