#include "DataLayerMockObjects.h"

map<int, SolarSystem*> DataLayerMockObjects::SOLARSYSTEMS = [] {
    map<int, SolarSystem*> m;
    return m;
}();

int DataLayerMockObjects::IDSOLARSYSTEMS = 0;
map<int, Simulation*> DataLayerMockObjects::SIMULATIONS = [] {
    map<int, Simulation*> m;
    return m;
}();
int DataLayerMockObjects::IDSIMULATIONS = 0;

DataLayerMockObjects::DataLayerMockObjects(const int& IDSolarSystems, const int& IDSimulations, const map<int, SolarSystem*>& solarSystems, const map<int, Simulation*>& simulations) : DataLayer(true), solarSystems(), IDSolarSystems(IDSolarSystems), simulations(), IDSimulations(IDSimulations) {
    this->setListSolarSystems(solarSystems);
    this->setListSimulations(simulations);
}

DataLayerMockObjects::DataLayerMockObjects(const DataLayerMockObjects& copy) : DataLayer(copy), solarSystems(), IDSolarSystems(copy.IDSolarSystems), simulations(), IDSimulations(copy.IDSimulations) {
    this->setListSolarSystems(copy.solarSystems);
    this->setListSimulations(copy.simulations);
}

DataLayerMockObjects::~DataLayerMockObjects() {
    this->clearSolarSystems();
    this->clearSimulations();
}

bool DataLayerMockObjects::load() {
    return true;
}

bool DataLayerMockObjects::save() {
    return true;
}

map<int, SolarSystem*> DataLayerMockObjects::getListSolarSystems() const {
    return this->solarSystems;
}

void DataLayerMockObjects::setListSolarSystems(const map<int, SolarSystem*>& solarSystems) {
    this->clearSolarSystems();
    for (map<int, SolarSystem*>::const_iterator it = solarSystems.begin(); it != solarSystems.end(); ++it) {
        this->saveSolarSystem(*it->second);
    }
}

map<int, Simulation*> DataLayerMockObjects::getListSimulations() const {
    return this->simulations;
}

void DataLayerMockObjects::setListSimulations(const map<int, Simulation*>& simulations) {
    this->clearSimulations();
    for (map<int, Simulation*>::const_iterator it = simulations.begin(); it != simulations.end(); ++it) {
        this->saveSimulation(*it->second);
    }
}

int DataLayerMockObjects::getIDSolarSystems() const {
    return this->IDSolarSystems;
}

void DataLayerMockObjects::setIDSolarSystems(const int& IDSolarSystems) {
    this->IDSolarSystems = IDSolarSystems;
}

int DataLayerMockObjects::getIDSimulations() const {
    return this->IDSimulations;
}

void DataLayerMockObjects::setIDSimulations(const int& IDSimulations) {
    this->IDSimulations = IDSimulations;
}

void DataLayerMockObjects::clearSolarSystems() {
    for (map<int, SolarSystem*>::const_iterator it = this->solarSystems.begin(); it != this->solarSystems.end(); ++it) {
        delete it->second;
    }
    this->solarSystems.clear();
}

void DataLayerMockObjects::clearSimulations() {
    for (map<int, Simulation*>::const_iterator it = this->simulations.begin(); it != this->simulations.end(); ++it) {
        delete it->second;
    }
    this->simulations.clear();
}

bool DataLayerMockObjects::containsSolarSystem(const int& id) const {
    return (this->solarSystems.find(id) != this->solarSystems.end());
}

bool DataLayerMockObjects::containsSimulation(const int& id) const {
    return (this->simulations.find(id) != this->simulations.end());
}

bool DataLayerMockObjects::containsSimulationByIDSolarSystem(const int& id) const{
    if(this->containsSolarSystem(id)){
        for(map<int, Simulation*>::const_iterator it=this->simulations.begin();it!=this->simulations.end();++it){
            if(it->first==id){
                return true;
            }
        }
    }
    return false;
}

map<int, string> DataLayerMockObjects::getSolarSystems() const {
    map<int, string> solarsystems;
    for (map<int, SolarSystem*>::const_iterator it = this->solarSystems.begin(); it != this->solarSystems.end(); ++it) {
        solarsystems[it->first] = it->second->getName();
    }
    return solarsystems;
}

map<int, string> DataLayerMockObjects::getSolarSystemsActive() const {
    map<int, string> solarsystems;
    for (map<int, SolarSystem*>::const_iterator it = this->solarSystems.begin(); it != this->solarSystems.end(); ++it) {
        if(it->second->isactive()){
            solarsystems[it->first] = it->second->getName();
        }
    }
    return solarsystems;
}

SolarSystem* DataLayerMockObjects::loadSolarSystem(const int& id) const {
    if (this->containsSolarSystem(id)) {
        return this->solarSystems.at(id);
    }
    return NULL;
}

SolarSystem* DataLayerMockObjects::saveSolarSystem(SolarSystem& solarsystem) {
    if(solarsystem.validate()){
        this->IDSolarSystems++;
        solarsystem.setActive(false);
        this->solarSystems[this->IDSolarSystems] = solarsystem.clone();
        this->solarSystems[this->IDSolarSystems]->setID(this->IDSolarSystems);
        this->solarSystems[this->IDSolarSystems]->setActive(true);
        this->solarSystems[this->IDSolarSystems]->setEdit(false);
        return this->solarSystems[this->IDSolarSystems];
    }
    return NULL;
}

bool DataLayerMockObjects::deleteSolarSystem(const int& id) {
    if (this->containsSolarSystem(id)) {
        for (map<int, Simulation*>::const_iterator it = this->simulations.begin(); it != this->simulations.end(); ++it) {
            if(it->second->getSolarSystem().getID()==id){
                this->deleteSimulation(it->first);
            }
        }
        delete this->solarSystems[id];
        this->solarSystems.erase(id);
        return true;
    }
    return false;
}

map<int, string> DataLayerMockObjects::getSimulations() const {
    map<int, string> simulations;
    for (map<int, Simulation*>::const_iterator it = this->simulations.begin(); it != this->simulations.end(); ++it) {
        simulations[it->first] = it->second->getName();
    }
    return simulations;
}

map<int, string> DataLayerMockObjects::getSimulationsActive() const {
    map<int, string> simulations;
    for (map<int, Simulation*>::const_iterator it = this->simulations.begin(); it != this->simulations.end(); ++it) {
        if(!it->second->isactive()){
            simulations[it->first] = it->second->getName();
        }
    }
    return simulations;
}


map<int, string> DataLayerMockObjects::getSimulationsNotRun() const{
    map<int, string> simulations;
    for (map<int, Simulation*>::const_iterator it = this->simulations.begin(); it != this->simulations.end(); ++it) {
        if(!it->second->alreadyRun()){
            simulations[it->first] = it->second->getName();
        }
    }
    return simulations;
}

map<int, string> DataLayerMockObjects::getSimulationsRun() const{
    map<int, string> simulations;
    for (map<int, Simulation*>::const_iterator it = this->simulations.begin(); it != this->simulations.end(); ++it) {
        if(it->second->alreadyRun()){
            simulations[it->first] = it->second->getName();
        }
    }
    return simulations;
}



Simulation* DataLayerMockObjects::loadSimulation(const int& id) const {
    if (this->containsSimulation(id)) {
        return this->simulations.at(id);
    }
    return NULL;
}

Simulation* DataLayerMockObjects::saveSimulation(Simulation& simulation) {
    if(simulation.validate()){
        this->IDSimulations++;
        this->simulations[this->IDSimulations] = simulation.clone();
        simulation.setActive(false);
        if(this->containsSimulationByIDSolarSystem(simulation.getSolarSystem().getID()) || !this->containsSolarSystem(simulation.getSolarSystem().getID())){
            SolarSystem* temp;
            if((temp=this->saveSolarSystem(simulation.getSolarSystem()))!=NULL){
                temp->setActive(false);
                this->simulations[this->IDSimulations]->setSolarSystem(temp);
            }else{
                this->deleteSimulation(this->IDSimulations);
                return NULL;
            }
        }
        this->simulations[this->IDSimulations]->setID(this->IDSimulations);
        this->simulations[this->IDSimulations]->setActive(true);
        this->simulations[this->IDSimulations]->setEdit(false);
        return this->simulations[this->IDSimulations];
    }
    return NULL;
}

Simulation* DataLayerMockObjects::saveSimulationResults(Simulation& simulation){
    return &simulation;
}

bool DataLayerMockObjects::deleteSimulation(const int& id) {
    if (this->containsSimulation(id)) {
        delete this->simulations[id];
        this->simulations.erase(id);
        return true;
    }
    return false;
}

void DataLayerMockObjects::writeStream(ostream& out) const {
    long num = this->solarSystems.size();
    cout << "Number of Solar Systems in DataLayer: " << num << endl;
    if (num > 0) {
        cout << "Solar Systems:" << endl;
        for (map<int, SolarSystem*>::const_iterator it = this->solarSystems.begin(); it != this->solarSystems.end(); ++it) {
            cout << "ID: " << it->first << endl;
            cout << "EDITED: "<< ((it->second->edited()==true)?"TRUE":"FALSE") << endl;
            cout << *it->second << endl;
        }
    }
    num=this->simulations.size();
    cout << "Number of Simulation in DataLayer: " << this->simulations.size() << endl;
    if (num > 0) {
        cout << "Simulation:" << endl;
        for (map<int, Simulation*>::const_iterator it = this->simulations.begin(); it != this->simulations.end(); ++it) {
            cout << "ID: " << it->first << endl;
            cout << "EDITED: "<< ((it->second->edited()==true)?"TRUE":"FALSE") << endl;
            cout << *it->second << endl;
        }
    }
}