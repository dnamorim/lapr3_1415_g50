#ifndef DATALAYERMOCKOBJECTS_H
#define	DATALAYERMOCKOBJECTS_H

#include <iostream>
#include <iterator>
#include <map>

using namespace std;

//FALTA GET e SET aos maps e valores predefenidos cleam para cada map

#include "../DataLayer.h"

class DataLayerMockObjects:public DataLayer {
protected:
    /* Lista de Sistemas Solares */
    map<int, SolarSystem*> solarSystems;
    /* Contador de IDs dos Sistemas Solares*/
    int IDSolarSystems;
    /* Lista de Simulações */
    map<int, Simulation*> simulations;
    /* Contador de IDs das Simulações */
    int IDSimulations;
   
public:
    /* Válores Pré-Definidos Pré-Defenida */
    /* Lista de Sistemas Solares */
    static map<int, SolarSystem*> SOLARSYSTEMS;
    /* Contador de IDs dos Sistemas Solares Pré-Defenido*/
    static int IDSOLARSYSTEMS;
    /* Lista de Simulações Pré-Defenida */
    static map<int, Simulation*> SIMULATIONS;
    /* Contador de IDs das Simulações Pré-Defenido*/
    static int IDSIMULATIONS;
    
    /**
     * Construir Instância de DataLayerMockObjects(COMPLETO)
     */
    DataLayerMockObjects(const int& IDSolarSystems=DataLayerMockObjects::IDSOLARSYSTEMS,const int& IDSimulations=DataLayerMockObjects::IDSIMULATIONS,const map<int, SolarSystem*>& solarSystems=DataLayerMockObjects::SOLARSYSTEMS,const map<int, Simulation*>& simulations=DataLayerMockObjects::SIMULATIONS);
    
    /**
     * Construir Instância da DataLayerMockObjects(CÓPIA)
     * @param copy Instância da DataLayerMockObjects a Cópiar
     */
    DataLayerMockObjects(const DataLayerMockObjects& copy);
    
    /**
     * Destrotor da Instância da DataLayerMockObjects
     */
    virtual ~DataLayerMockObjects();
    
    /**
     * Carregar DataLayer
     * @return Sucesso da Operação
     */
    virtual bool load();
    
    /**
     * Guardar DataLayer
     * @return Sucesso da Operação
     */
    virtual bool save();
    
    /**
     * Consultar Lista de Sistemas Solares da DataLayer
     * @return Lista de Sistemas Solares 
     */
    map<int, SolarSystem*> getListSolarSystems() const;
   
    /**
     * Alterar Lista de Sistemas Solares da DataLayer
     * @param solarSystems Nova Lista de Sistemas Solares
     */
    void setListSolarSystems(const map<int, SolarSystem*>& solarSystems);
    
    /**
     * Consultar Lista de Simulações da DataLayer
     * @return Lista de Simulações
     */
    map<int, Simulation*> getListSimulations() const;
    
    /**
     * Alterar Lista de Simulações da DataLayer
     * @param solarSystems Nova Lista de Simulações
     */
    void setListSimulations(const map<int, Simulation*>& simulations);
    
    /**
     * Consultar Contador de IDs dos Sistemas Solares da DataLayer
     * @return Contador de IDs dos Sistemas Solares
     */
    int getIDSolarSystems() const;
    
    /**
     * Alterar Contador de IDs dos Sistemas Solares da DataLayer
     * @param IDSolarSystems Contador de IDs dos Sistemas Solares
     */
    void setIDSolarSystems(const int& IDSolarSystems);
    
    /**
     * Consultar Contador de IDs das Simulações da DataLayer
     * @return Contador de IDs das Simulações
     */
    int getIDSimulations() const;
    
    /**
     * Alterar Contador de IDs das Simulações da Data Layer
     * @param IDSimulations Novo Contador de IDs das Simulações
     */
    void setIDSimulations(const int& IDSimulations);
    
    /**
     * Limpar Lista dos Sistemas Solares
     */
    void clearSolarSystems();
    /**
     * Limpar Lista das Simulações
     */
    void clearSimulations();
    
    /**
     * Verificar se existe o Sistema Solar pelo seu ID
     * @param id ID do Sistema Solar
     * @return Sucesso da Operação
     */
    virtual bool containsSolarSystem(const int& id) const;
    
    /**
     * Verificar se existe a Simulação pelo seu ID
     * @param id ID da Simulação
     * @return Sucesso da Operação
     */
    virtual bool containsSimulation(const int& id) const;
    
    /**
     * Verificar se existe a Simulação pelo ID do Sistema Solar
     * @param id ID do Sistema Solar
     * @return Sucesso da Operação
     */
    virtual bool containsSimulationByIDSolarSystem(const int& id) const;
    
    /**
     * Consultar todos os Sistemas Solares na Base de Dados
     * @return Lista de Ids e Nome dos Sistemas Solares
     */
    virtual map<int, string> getSolarSystems() const;
    
    /**
     * Consultar todos as Simulações Activas
     * @return Lista de Ids e Nome das Simulações
     */
    virtual map<int, string>  getSimulationsActive() const;

    
    /**
     * Carregar da Base de Dados o Sistema Solar pelo ID
     * @param id ID do Sistema Solar a Carregar
     * @return Sistema Solar, em Caso de Sucesso. Caso contrario retrona NULL
     */
    virtual SolarSystem* loadSolarSystem(const int& id) const;
    
    /**
     * Guardar na Base de Dados o Sistema Solar
     * @param solarsystem Sistema Solar a Guardar
     * @return Sistema Solar, em Caso de Sucesso. Caso contrario retrona NULL
     */
    virtual SolarSystem* saveSolarSystem(SolarSystem& solarsystem);
    
     /**
     * Apagar Sistema Solar
     * @param id ID da Sistema Solar a Apagar
     * @return Sucesso da Operação
     */
    virtual bool deleteSolarSystem(const int& id);
    
    /**
     * Consultar todos as Simulações
     * @return Lista de Ids e Nome das Simulações
     */
    virtual map<int, string> getSimulations() const;
    
    /**
     * Consultar todos os Sistemas Solares Activos
     * @return Lista de Ids e Nome dos Sistemas Solares
     */
    virtual map<int, string> getSolarSystemsActive() const;
    
    /**
     * Consultar todos as Simulações por Corridas na Base de Dados
     * @return Lista de Ids e Nome das Simulações
     */
    virtual map<int, string> getSimulationsNotRun() const;
    
    /**
     * Consultar todos as Simulações já Corridas na Base de Dados
     * @return Lista de Ids e Nome das Simulações
     */
    virtual map<int, string> getSimulationsRun() const;
    
     /**
     * Carregar da Base de Dados a Simulação pelo ID
     * @param id ID da Simulação a Carregar
     * @return Simulação, em Caso de Sucesso. Caso contrario retrona NULL
     */
    virtual Simulation* loadSimulation(const int& id) const;
    
    /**
     * Guardar na Base de Dados a Simulação
     * @param Simulation Simulação a Guardar
     * @return Simulação, em Caso de Sucesso. Caso contrario retrona NULL
     */
    virtual Simulation* saveSimulation(Simulation& simulation);
    
    /**
     * Guardar Resultados da Simulação
     * @param Simulation Simulação com os Resultados a Guardar
     * @return Simulação, em Caso de Sucesso. Caso contrario retrona NULL
     */
    virtual Simulation* saveSimulationResults(Simulation& simulation);
    
     /**
     * Apagar Simulação
     * @param id ID da Simulação a Apagar
     * @return Sucesso da Operação
     */
    virtual bool deleteSimulation(const int& id);
    
     /**
     * Representação Textual da Instância da DataLayerMockObjects
     * @param out Stream de Output
     */
    virtual void writeStream(ostream& out) const;
};

#endif	/* DATALAYERMOCKOBJECTS_H */

