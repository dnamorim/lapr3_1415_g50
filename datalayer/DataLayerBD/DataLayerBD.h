#ifndef DATALAYERBD_H
#define	DATALAYERBD_H

#include <string>
#include <map>

using namespace std;

#include "../DataLayer.h"
#include "../../utils/DatabaseConnection/DatabaseConnection.h"
#include "../../model/SolarSystem/SolarSystem.h"
#include "../../model/Simulation/Simulation.h"

class DataLayerBD : public DataLayer, public DatabaseConnection {
private:

public:
    /**
     * Construir Instância de DataLayerBD(COMPLETO)
     */
    DataLayerBD(string usr, string pwd, string dbloc);
    
    /**
     * Construir Instância da DataLayerBD(CÓPIA)
     * @param copy Instância da DataLayerBD a Cópiar
     */
    DataLayerBD(const DataLayerBD& copy);
    
    /**
     * Destrotor da Instância da DataLayerBD
     */
    virtual ~DataLayerBD();
    
    /**
     * Carregar DataLayer
     * @return Sucesso da Operação
     */
    virtual bool load();
    
    /**
     * Guardar DataLayer
     * @return Sucesso da Operação
     */
    virtual bool save();
    
    /**
     * Verificar se existe o Sistema Solar pelo seu ID
     * @param id ID do Sistema Solar
     * @return Sucesso da Operação
     */
    virtual bool containsSolarSystem(const int& id) const;
    
    /**
     * Verificar se existe a Simulação pelo seu ID
     * @param id ID da Simulação
     * @return Sucesso da Operação
     */
    virtual bool containsSimulation(const int& id) const;
    
    /**
     * Verificar se existe a Simulação pelo ID do Sistema Solar
     * @param id ID do Sistema Solar
     * @return Sucesso da Operação
     */
    virtual bool containsSimulationByIDSolarSystem(const int& id) const;
    
    /**
     * Consultar todos os Sistemas Solares
     * @return Lista de Ids e Nome dos Sistemas Solares
     */
    virtual map<int, string> getSolarSystems() const;
    
    /**
     * Consultar todos os Sistemas Solares Activos
     * @return Lista de Ids e Nome dos Sistemas Solares
     */
    virtual map<int, string> getSolarSystemsActive() const;
    
    /**
     * Carregar Sistema Solar pelo ID
     * @param id ID do Sistema Solar a Carregar
     * @return Sistema Solar, em Caso de Sucesso. Caso contrario retrona NULL
     */
    virtual SolarSystem* loadSolarSystem(const int& id) const;
    
    /**
     * Guardar Sistema Solar
     * @param solarsystem Sistema Solar a Guardar
     * @return Sistema Solar, em Caso de Sucesso. Caso contrario retrona NULL
     */
    virtual SolarSystem* saveSolarSystem(SolarSystem& solarsystem);
    
    /**
     * Apagar Sistema Solar
     * @param id ID da Sistema Solar a Apagar
     * @return Sucesso da Operação
     */
    virtual bool deleteSolarSystem(const int& id);
    
    /**
     * Consultar todos as Simulações
     * @return Lista de Ids e Nome das Simulações
     */
    virtual map<int, string> getSimulations() const;
    
    /**
     * Consultar todos as Simulações Activas
     * @return Lista de Ids e Nome das Simulações
     */
    virtual map<int, string>  getSimulationsActive() const;
    
    /**
     * Consultar todos as Simulações por Corridas na Base de Dados
     * @return Lista de Ids e Nome das Simulações
     */
    virtual map<int, string> getSimulationsNotRun() const;
    
    /**
     * Consultar todos as Simulações já Corridas na Base de Dados
     * @return Lista de Ids e Nome das Simulações
     */
    virtual map<int, string> getSimulationsRun() const;
    
    /**
     * Carregar Simulação pelo ID
     * @param id ID da Simulação a Carregar
     * @param simulation Simulação Carregado
     * @return Simulação, em Caso de Sucesso. Caso contrario retrona NULL
     */
    virtual Simulation* loadSimulation(const int& id)const;
    
    /**
     * Guardar Simulação
     * @param Simulation Simulação a Guardar
     * @return Simulação, em Caso de Sucesso. Caso contrario retrona NULL
     */
    virtual Simulation* saveSimulation(Simulation& simulation);
    
    /**
     * Guardar Resultados da Simulação
     * @param Simulation Simulação com os Resultados a Guardar
     * @return Simulação, em Caso de Sucesso. Caso contrario retrona NULL
     */
    virtual Simulation* saveSimulationResults(Simulation& simulation);
    
    /**
     * Apagar Simulação
     * @param id ID da Simulação a Apagar
     * @return Sucesso da Operação
     */
    virtual bool deleteSimulation(const int& id);
    
    /**
     * Representação Textual da Instância da DataLayer
     * @param out Stream de Output
     */
    virtual void writeStream(ostream& out) const;
};

#endif	/* DATALAYERBD_H */

