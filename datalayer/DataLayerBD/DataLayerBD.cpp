#include "DataLayerBD.h"

DataLayerBD::DataLayerBD(string usr, string pwd, string dbloc) : DataLayer(false), DatabaseConnection(usr, pwd, dbloc) {
}

DataLayerBD::DataLayerBD(const DataLayerBD& copy) : DataLayer(copy), DatabaseConnection(copy) {
}

DataLayerBD::~DataLayerBD() {
}

bool DataLayerBD::load(){
    Statement *query;
    query = this->con->createStatement("begin :1 := loadAPP(); end;");
    query->registerOutParam(1, OCCIINT);
    query->executeUpdate();
    bool ret=(query->getInt(1)>0)?true:false;
    con->terminateStatement(query);
    return ret;
}

bool DataLayerBD::save(){
    Statement *query;
    query = this->con->createStatement("begin :1 := saveAPP(); end;");
    query->registerOutParam(1, OCCIINT);
    query->executeUpdate();
    bool ret=(query->getInt(1)>0)?true:false;
    con->terminateStatement(query);
    return ret;
}

bool DataLayerBD::containsSolarSystem(const int& id) const{
    Statement *query;
    query = this->con->createStatement("begin :1 := containsSolarSystem(:2); end;");
    query->registerOutParam(1, OCCIINT);
    query->setInt(2, id);
    query->executeUpdate();
    bool ret=(query->getInt(1)>0)?true:false;
    con->terminateStatement(query);
    return ret;
}

bool DataLayerBD::containsSimulation(const int& id) const{
    Statement *query;
    query = this->con->createStatement("begin :1 := containsSimulation(:2); end;");
    query->registerOutParam(1, OCCIINT);
    query->setInt(2, id);
    query->executeUpdate();
    bool ret=(query->getInt(1)>0)?true:false;
    con->terminateStatement(query);
    return ret;
}

bool DataLayerBD::containsSimulationByIDSolarSystem(const int& id) const{
    Statement *query;
    query = this->con->createStatement("begin :1 := containsSimulationByIDSSystem(:2); end;");
    query->registerOutParam(1, OCCIINT);
    query->setInt(2, id);
    query->executeUpdate();
    bool ret=(query->getInt(1)>0)?true:false;
    con->terminateStatement(query);
    return ret;
}

map<int, string> DataLayerBD::getSolarSystems() const {
    map<int, string> solarsystems;
    Statement *query;
    query = this->con->createStatement("begin getSolarSystems(:1); end;");
    query->registerOutParam(1, OCCICURSOR);
    query->executeUpdate();
    ResultSet *rset = query->getCursor(1);
    while (rset->next()){
        solarsystems[rset->getInt(1)]=rset->getString(3);
    }
    con->terminateStatement(query);
    return solarsystems;
}

map<int, string> DataLayerBD::getSolarSystemsActive() const {
    map<int, string> solarsystems;
    Statement *query;
    query = this->con->createStatement("begin getSolarSystems(:1); end;");
    query->registerOutParam(1, OCCICURSOR);
    query->executeUpdate();
    ResultSet *rset = query->getCursor(1);
    while (rset->next()){
        if(rset->getString(2)=="1"){
            solarsystems[rset->getInt(1)]=rset->getString(3);
        }
    }
    con->terminateStatement(query);
    return solarsystems;
}

SolarSystem* DataLayerBD::loadSolarSystem(const int& id) const{
    if(this->containsSolarSystem(id)){
        SolarSystem* newSolarSystem = new SolarSystem();
        vector<Corps*> allCorps;
        Statement *query;
        //CARREGAR SISTEMA SOLAR
        query = this->con->createStatement("begin getSolarSystem(:1,:2); end;");
        query->setInt(1, id);
        query->registerOutParam(2, OCCICURSOR);
        query->executeUpdate();
        ResultSet *rset = query->getCursor(2);
        if(!rset->next()){
            return NULL;
        }
        newSolarSystem->setID(rset->getInt(1));
        newSolarSystem->setActive((rset->getString(2)=="1")?true:false);
        newSolarSystem->setName(rset->getString(3));
        con->terminateStatement(query);
        //CAREGAR ESTRELA
        query = this->con->createStatement("begin getStar(:1,:2); end;");
        query->setInt(1, rset->getInt(4));
        query->registerOutParam(2, OCCICURSOR);
        query->executeUpdate();
        rset = query->getCursor(2);
        if(!rset->next()){
            return NULL;
        }
        newSolarSystem->getStar().setID(rset->getInt(1));
        newSolarSystem->getStar().setName(rset->getString(2));
        newSolarSystem->getStar().setMass(rset->getDouble(3));
        newSolarSystem->getStar().setDiameter(rset->getDouble(4));
        con->terminateStatement(query);
        // CARREGAR CORPS
        query = this->con->createStatement("begin getCorps(:1,:2); end;");
        query->setInt(1, id);
        query->registerOutParam(2, OCCICURSOR);
        query->executeUpdate();
        rset = query->getCursor(2);
        while (rset->next()){
            Corps* corps = new Corps();
            corps->setID(rset->getInt(1));
            corps->setName(rset->getString(2));
            corps->setSource((rset->getInt(3)==1)?Source::Manmade:Source::Natural);
            corps->setMass(rset->getDouble(4));
            corps->setDiameter(rset->getDouble(5));
            corps->setDensity(rset->getDouble(6));
            corps->getCoordinates()=rset->getString(7);
            corps->getSpeed()=rset->getString(8);
            corps->getReferenceDate()=rset->getString(9);
            allCorps.push_back(corps);
        }
        con->terminateStatement(query);
        //CARREGAR ORBITALCORPS
        query = this->con->createStatement("begin getOrbitCorps(:1,:2); end;");
        query->setInt(1, id);
        query->registerOutParam(2, OCCICURSOR);
        query->executeUpdate();
        rset = query->getCursor(2);
        while (rset->next()){
            OrbitalCorps* orbitalcorps = new OrbitalCorps();
            orbitalcorps->setID(rset->getInt(1));
            orbitalcorps->setName(rset->getString(2));
            orbitalcorps->setSource((rset->getInt(3)==1)?Source::Manmade:Source::Natural);
            orbitalcorps->setMass(rset->getDouble(4));
            orbitalcorps->setDiameter(rset->getDouble(5));
            orbitalcorps->setDensity(rset->getDouble(6));
            orbitalcorps->getCoordinates()=rset->getString(7);
            orbitalcorps->getSpeed()=rset->getString(8);
            orbitalcorps->getReferenceDate()=rset->getString(9);
            orbitalcorps->setSemimajorAxis(rset->getDouble(10));
            orbitalcorps->setPeriod(rset->getDouble(11));
            orbitalcorps->setOrbitalSpeed(rset->getDouble(12));
            orbitalcorps->setInclinationAxisToOribit(rset->getDouble(13));
            orbitalcorps->setInclinationOrbit(rset->getDouble(14));
            orbitalcorps->setEccentricityOrbit(rset->getDouble(15));
            orbitalcorps->setEscapeVelocity(rset->getDouble(16));
            orbitalcorps->setPerihelion(rset->getDouble(17));
            orbitalcorps->setAphelion(rset->getDouble(18));
            orbitalcorps->setMeanAnomaly(rset->getDouble(19));
            orbitalcorps->setLongitudePerihelion(rset->getDouble(20));
            orbitalcorps->setLongitudeAscendingNode(rset->getDouble(21));
            allCorps.push_back(orbitalcorps);
        }
        con->terminateStatement(query);
        //CAREGAR GRAVITARIONALCORPS
        query = this->con->createStatement("begin getGravitationalCorps(:1,:2); end;");
        query->setInt(1, id);
        query->registerOutParam(2, OCCICURSOR);
        query->executeUpdate();
        rset = query->getCursor(2);
        while (rset->next()){
            GravitationalCorps* gravitationalcorps = new GravitationalCorps();
            gravitationalcorps->setID(rset->getInt(1));
            gravitationalcorps->setName(rset->getString(2));
            gravitationalcorps->setSource((rset->getInt(3)==1)?Source::Manmade:Source::Natural);
            gravitationalcorps->setMass(rset->getDouble(4));
            gravitationalcorps->setDiameter(rset->getDouble(5));
            gravitationalcorps->setDensity(rset->getDouble(6));
            gravitationalcorps->getCoordinates()=rset->getString(7);
            gravitationalcorps->getSpeed()=rset->getString(8);
            gravitationalcorps->getReferenceDate()=rset->getString(9);
            allCorps.push_back(gravitationalcorps);
        }
        con->terminateStatement(query);
        //CARREGAR GRAVITARIONALWITHPROPULSIONCORPS
        query = this->con->createStatement("begin getGravitationalPropCorps(:1,:2); end;");
        query->setInt(1, id);
        query->registerOutParam(2, OCCICURSOR);
        query->executeUpdate();
        rset = query->getCursor(2);
        while (rset->next()){
            GravitationalwithPropulsionCorps* gravitationalwithpropulsioncorps = new GravitationalwithPropulsionCorps();
            gravitationalwithpropulsioncorps->setID(rset->getInt(1));
            gravitationalwithpropulsioncorps->setName(rset->getString(2));
            gravitationalwithpropulsioncorps->setSource((rset->getInt(3)==1)?Source::Manmade:Source::Natural);
            gravitationalwithpropulsioncorps->setMass(rset->getDouble(4));
            gravitationalwithpropulsioncorps->setDiameter(rset->getDouble(5));
            gravitationalwithpropulsioncorps->setDensity(rset->getDouble(6));
            gravitationalwithpropulsioncorps->getCoordinates()=rset->getString(7);
            gravitationalwithpropulsioncorps->getSpeed()=rset->getString(8);
            gravitationalwithpropulsioncorps->getReferenceDate()=rset->getString(9);
            gravitationalwithpropulsioncorps->setPropulsion(rset->getDouble(10));
            allCorps.push_back(gravitationalwithpropulsioncorps);
        }
        con->terminateStatement(query);
        //CARREGAR ORBITA
        vector<int> not_add_solarSystem;
        query = this->con->createStatement("begin getOrbit(:1,:2); end;");
        query->setInt(1, id);
        query->registerOutParam(2, OCCICURSOR);
        query->executeUpdate();
        rset = query->getCursor(2);
        while (rset->next()){
            Corps *center;
            for(vector<Corps*>::iterator it=allCorps.begin();it!=allCorps.end();++it){
                if((*it)->getID()==rset->getInt(1)){
                    center=(*it);
                    for(int i=0;i<allCorps.size();i++){
                        if(allCorps[i]->getID()==rset->getInt(2)){
                            if(center->getOrbit().addOrbit((OrbitalCorps&)*allCorps[i])){
                                delete allCorps[i];
                                allCorps[i]=&center->getOrbit().getOrbit((int)center->getOrbit().numberOrbits()-1);
                                not_add_solarSystem.push_back(i);
                                break;
                            }
                        }
                    }
                    break;
                }
            }
            
        }
        con->terminateStatement(query);
        for(int i=0;i<allCorps.size();i++){
            vector<int>::iterator it_add = find (not_add_solarSystem.begin(), not_add_solarSystem.end(), i);
            if(it_add==not_add_solarSystem.end()){
                newSolarSystem->addBody(*allCorps[i]);
                delete allCorps[i];
            }
        }
        return newSolarSystem;
    }
    return NULL;
}

SolarSystem* DataLayerBD::saveSolarSystem(SolarSystem& solarsystem){
    if(solarsystem.validate()){
        if(solarsystem.defined() && this->containsSolarSystem(solarsystem.getID())){
            this->query = this->con->createStatement("begin :1 := updateStateSolarSystem(:2,0); end;");
            this->query->registerOutParam(1, OCCIINT);
            this->query->setInt(2, solarsystem.getID());
            this->query->executeUpdate();
            con->terminateStatement(this->query);
            solarsystem.setActive(false);
        }
        SolarSystem* newSolarSystem =solarsystem.clone();
        newSolarSystem->setActive(true);
        newSolarSystem->setEdit(false);
        stringstream ss;
        //GUARDAR ESTRELA
        this->query = this->con->createStatement("begin :1 := insertStar(AUTO_ID_STAR.nextval,:2,:3,:4); end;");
        this->query->registerOutParam(1, OCCIINT);
        this->query->setString(2, newSolarSystem->getStar().getName());
        this->query->setDouble(3, newSolarSystem->getStar().getMass());
        this->query->setDouble(4, newSolarSystem->getStar().getDiameter());
        this->query->executeUpdate();
        newSolarSystem->getStar().setID(this->query->getInt(1));
        con->terminateStatement(this->query);
        //GUARDAR SISTEMA SOLAR
        this->query = this->con->createStatement("begin :1 := insertSolarSystem(AUTO_ID_SOLARSYSTEM.nextval,1,:2,:3); end;");
        this->query->registerOutParam(1, OCCIINT);
        this->query->setString(2, newSolarSystem->getName());
        this->query->setInt(3,newSolarSystem->getStar().getID());
        this->query->executeUpdate();
        newSolarSystem->setID(this->query->getInt(1));
        con->terminateStatement(this->query);
        //GUARDAR CORPOS DO SISTEMA SOLAR
        vector<Corps*> allCorps = newSolarSystem->getAllBodys();
        int i=1;
        for(vector<Corps*>::iterator it=allCorps.begin();it!=allCorps.end();++it,i++){
            (*it)->setID(i);
            this->query = this->con->createStatement("begin :1 := insertCorps(:2, :3, :4, :5, :6, :7, :8, :9, :10, :11); end;");
            this->query->registerOutParam(1, OCCIINT);
            this->query->setInt(2,(*it)->getID());
            this->query->setInt(3,newSolarSystem->getID());
            this->query->setString(4,(*it)->getName());
            this->query->setInt(5,((*it)->getSource()==Source::Manmade)?1:0);
            this->query->setDouble(6,(*it)->getMass());
            this->query->setDouble(7,(*it)->getDiameter());
            this->query->setDouble(8,(*it)->getDensity());
            ss<<(*it)->getCoordinates();
            this->query->setString(9,ss.str());
            ss.str("");
            ss<<(*it)->getSpeed();
            this->query->setString(10,ss.str());
            ss.str("");
            (*it)->getReferenceDate().writePointerStream(ss);
            this->query->setString(11,ss.str());
            ss.str("");
            this->query->executeUpdate();
            con->terminateStatement(this->query);
            //GUARDAR POR TIPO DE CORPO
            if(typeid(**it)==typeid(GravitationalCorps) || typeid(**it)==typeid(GravitationalwithPropulsionCorps)){
                this->query = this->con->createStatement("begin :1 := insertGravitationalCorps(:2, :3); end;");
                this->query->registerOutParam(1, OCCIINT);
                this->query->setInt(2,(*it)->getID());
                this->query->setInt(3,newSolarSystem->getID());
                this->query->executeUpdate();
                con->terminateStatement(this->query);
            }
            if(typeid(**it)==typeid(GravitationalwithPropulsionCorps)){
                this->query = this->con->createStatement("begin :1 := insertGravitationalPropCorps(:2, :3, :4); end;");
                this->query->registerOutParam(1, OCCIINT);
                this->query->setInt(2,(*it)->getID());
                this->query->setInt(3,newSolarSystem->getID());
                this->query->setDouble(4,((GravitationalwithPropulsionCorps*)(*it))->getPropulsion());
                this->query->executeUpdate();
                con->terminateStatement(this->query);
                
            }
            if(typeid(**it)==typeid(OrbitalCorps)){
                this->query = this->con->createStatement("begin :1 := insertOrbitalCorps(:2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15); end;");
                this->query->registerOutParam(1, OCCIINT);
                this->query->setInt(2,(*it)->getID());
                this->query->setInt(3,newSolarSystem->getID());
                this->query->setDouble(4,((OrbitalCorps*)(*it))->getSemimajorAxis());
                this->query->setDouble(5,((OrbitalCorps*)(*it))->getPeriod());
                this->query->setDouble(6,((OrbitalCorps*)(*it))->getOrbitalSpeed());
                this->query->setDouble(7,((OrbitalCorps*)(*it))->getInclinationAxisToOribit());
                this->query->setDouble(8,((OrbitalCorps*)(*it))->getInclinationOrbit());
                this->query->setDouble(9,((OrbitalCorps*)(*it))->getEccentricityOrbit());
                this->query->setDouble(10,((OrbitalCorps*)(*it))->getEscapeVelocity());
                this->query->setDouble(11,((OrbitalCorps*)(*it))->getPerihelion());
                this->query->setDouble(12,((OrbitalCorps*)(*it))->getAphelion());
                this->query->setDouble(13,((OrbitalCorps*)(*it))->getMeanAnomaly());
                this->query->setDouble(14,((OrbitalCorps*)(*it))->getLongitudePerihelion());
                this->query->setDouble(15,((OrbitalCorps*)(*it))->getLongitudeAscendingNode());
                this->query->executeUpdate();
                con->terminateStatement(this->query);
            }
        }
        //GUARDAR ORBITAS
        for(vector<Corps*>::iterator it=allCorps.begin();it!=allCorps.end();++it){
            vector<OrbitalCorps*> orbitalCorps=(*it)->getOrbit().getBodys();
            for(vector<OrbitalCorps*>::iterator it_o=orbitalCorps.begin();it_o!=orbitalCorps.end();++it_o){
                this->query = this->con->createStatement("begin :1 := insertOrbit(:2, :3, :4); end;");
                this->query->registerOutParam(1, OCCIINT);
                this->query->setInt(2,(*it)->getID());
                this->query->setInt(3,newSolarSystem->getID());
                this->query->setInt(4,(*it_o)->getID());
                this->query->executeUpdate();
                con->terminateStatement(this->query);
            }
        }
        this->con->commit();
        return newSolarSystem;
    }
    return NULL;
}

bool DataLayerBD::deleteSolarSystem(const int& id){
    this->query = this->con->createStatement("begin :1 := deleteSolarSystem(:2); end;");
    this->query->registerOutParam(1, OCCIINT);
    this->query->setInt(2, id);
    this->query->executeUpdate();
    bool ret=(this->query->getInt(1)>0)?true:false;
    con->terminateStatement(query);
    this->con->commit();
    return ret;
    
}


map<int, string> DataLayerBD::getSimulations() const {
    map<int, string> simulations;
    Statement *query;
    query = this->con->createStatement("begin getSimulations(:1); end;");
    query->registerOutParam(1, OCCICURSOR);
    query->executeUpdate();
    ResultSet *rset = query->getCursor(1);
    while (rset->next()){
        simulations[rset->getInt(1)]=rset->getString(5);
    }
    con->terminateStatement(query);
    return simulations;
}

map<int, string> DataLayerBD::getSimulationsActive() const {
    map<int, string> simulations;
    Statement *query;
    query = this->con->createStatement("begin getSimulations(:1); end;");
    query->registerOutParam(1, OCCICURSOR);
    query->executeUpdate();
    ResultSet *rset = query->getCursor(1);
    while (rset->next()){
        if(rset->getString(3)=="1"){
            simulations[rset->getInt(1)]=rset->getString(5);
        }
    }
    con->terminateStatement(query);
    return simulations;
}

map<int, string> DataLayerBD::getSimulationsNotRun() const {
    map<int, string> simulations;
    Statement *query;
    query = this->con->createStatement("begin getSimulations(:1); end;");
    query->registerOutParam(1, OCCICURSOR);
    query->executeUpdate();
    ResultSet *rset = query->getCursor(1);
    while (rset->next()){
        if(rset->getString(3)=="1" && rset->getString(4)=="0"){
            simulations[rset->getInt(1)]=rset->getString(5);
        }
    }
    con->terminateStatement(query);
    return simulations;
}

map<int, string> DataLayerBD::getSimulationsRun() const {
    map<int, string> simulations;
    Statement *query;
    query = this->con->createStatement("begin getSimulations(:1); end;");
    query->registerOutParam(1, OCCICURSOR);
    query->executeUpdate();
    ResultSet *rset = query->getCursor(1);
    while (rset->next()){
        if(rset->getString(4)=="1"){
            simulations[rset->getInt(1)]=rset->getString(5);
        }
    }
    con->terminateStatement(query);
    return simulations;
}

Simulation* DataLayerBD::loadSimulation(const int &id) const{
    if(this->containsSimulation(id)){
        SolarSystem* tempSolarSystem;
        Simulation* newSimulation;
        Statement *query;
        ResultSet *rset;
        Statement *query1;
        ResultSet *rset1;
        //CARREGAR SIMULAÇÃO
        query = this->con->createStatement("begin getSimulation(:1,:2); end;");
        query->setInt(1, id);
        query->registerOutParam(2, OCCICURSOR);
        query->executeUpdate();
        rset = query->getCursor(2);
        if(!rset->next()){
            return NULL;
        }
        //CARREGAR SISTEMA SOLAR
        if((tempSolarSystem=this->loadSolarSystem(rset->getInt(2)))==NULL){
            return NULL;
        }
        newSimulation = new Simulation(tempSolarSystem);
        newSimulation->setID(rset->getInt(1));
        newSimulation->setActive((rset->getString(3)=="1")?true:false);
        newSimulation->setRun((rset->getString(4)=="1")?true:false);
        newSimulation->setName(rset->getString(5));
        newSimulation->getStartTime()=rset->getString(6);
        newSimulation->getTimeStep()=rset->getString(7);
        newSimulation->getEndTime()=rset->getString(8);
        con->terminateStatement(query);
        //CARREGAR CORPOS
        vector<Corps*> allCorps=tempSolarSystem->getAllBodys();
        query = this->con->createStatement("begin getSimulationCorps(:1,:2); end;");
        query->setInt(1, id);
        query->registerOutParam(2, OCCICURSOR);
        query->executeUpdate();
        rset = query->getCursor(2);
        while (rset->next()){
            for(vector<Corps*>::const_iterator it=allCorps.begin();it!=allCorps.end();++it){
                if((*it)->getID()==rset->getInt(3)){
                    newSimulation->addCorps(**it);
                }
            }
        }
        con->terminateStatement(query);
         //CAREGAR COORDENADAS
        if(newSimulation->getRun()){
            vector<vector<XYZ>> coordinates;
            vector<Timer> steps;
            int pos=1;
            query = this->con->createStatement("begin getResultsSimulationStar(:1,:2); end;");
            query->setInt(1, id);
            query->registerOutParam(2, OCCICURSOR);
            query->executeUpdate();
            rset = query->getCursor(2);
            while (rset->next()){
                vector<XYZ> xyzs;
                XYZ xyz_star;
                xyz_star=rset->getString(4);
                xyzs.push_back(xyz_star);
                query1=this->con->createStatement("begin getResultsSimulationCorpsPos(:1,:2,:3); end;");
                query1->setInt(1, id);
                query1->setInt(2, pos);
                query1->registerOutParam(3, OCCICURSOR);
                query1->executeUpdate();
                rset1 = query1->getCursor(3);
                bool addTime=false;
                while(rset1->next()){
                    if(!addTime){
                        Timer timer;
                        timer=rset1->getString(5);
                        steps.push_back(timer);
                        addTime=true;
                    }
                    XYZ xyz_corps;
                    xyz_corps =rset1->getString(6);
                    xyzs.push_back(xyz_corps);
                }
                con->terminateStatement(query1);
                coordinates.push_back(xyzs);
                pos++;
            }
            con->terminateStatement(query);
            newSimulation->setPositions(coordinates);
            newSimulation->setSteps(steps);
        }
        newSimulation->setEdit(false);
        return newSimulation;
    }
    return NULL;
}

Simulation* DataLayerBD::saveSimulation(Simulation& simulation){
    if(simulation.validate()){
        if(!simulation.getSolarSystem().defined() || this->containsSolarSystem(simulation.getSolarSystem().getID())){
            SolarSystem* tempSolarSystem=NULL;
            tempSolarSystem=this->saveSolarSystem(simulation.getSolarSystem());
            if(tempSolarSystem!=NULL){
                simulation.setSolarSystem(tempSolarSystem);
            }else{
                return NULL;
            }
        }
        if(simulation.defined() && this->containsSimulation(simulation.getID())){
            this->query = this->con->createStatement("begin :1 := updateStateSimulation(:2,0); end;");
            this->query->registerOutParam(1, OCCIINT);
            this->query->setInt(2, simulation.getID());
            this->query->executeUpdate();
            con->terminateStatement(this->query);
            simulation.setActive(false);
        }
        Simulation* newSimulation =simulation.clone();
        vector<Corps*> allCorps = newSimulation->getCorps();
        stringstream ss;
        int idSolarSystem=simulation.getSolarSystem().getID();
        newSimulation->setActive(true);
        //GUARDAR SIMULAÇÃO
        this->query = this->con->createStatement("begin :1 := insertSimulation(AUTO_ID_SIMULATION.nextval,:2,1,:3,:4,:5,:6,:7); end;");
        this->query->registerOutParam(1, OCCIINT);
        this->query->setInt(2,idSolarSystem);
        this->query->setInt(3, newSimulation->alreadyRun()?1:0);
        this->query->setString(4,simulation.getName());
        ss.str("");
        newSimulation->getStartTime().writePointerStream(ss);
        this->query->setString(5,ss.str());
        ss.str("");
        newSimulation->getTimeStep().writePointerStream(ss);
        this->query->setString(6,ss.str());
        ss.str("");
        newSimulation->getEndTime().writePointerStream(ss);
        this->query->setString(7,ss.str());
        this->query->executeUpdate();
        newSimulation->setID(this->query->getInt(1));
        con->terminateStatement(this->query);
        //GUARDAR CORPOS
        for(vector<Corps*>::const_iterator it=allCorps.begin();it!=allCorps.end();++it){
            this->query = this->con->createStatement("begin :1 := insertSimulationCorps(:2,:3,:4); end;");
            this->query->registerOutParam(1, OCCIINT);
            this->query->setInt(2,newSimulation->getID());
            this->query->setInt(3, idSolarSystem);
            this->query->setInt(4, (*it)->getID());
            this->query->executeUpdate();
            con->terminateStatement(this->query);
        }
        this->con->commit();
        //GUARDAR RESULTADOS
        Simulation* tempSimularion;
        if((tempSimularion=this->saveSimulationResults(*newSimulation))!=NULL){
            newSimulation=tempSimularion;
        }
        newSimulation->setEdit(false);
        return newSimulation;
    }
    return NULL;
}

Simulation* DataLayerBD::saveSimulationResults(Simulation& simulation){
    int idSimulation = simulation.getID();
    int idSolarSystem = simulation.getSolarSystem().getID();
    
    if(this->containsSimulation(idSimulation) && this->containsSolarSystem(idSolarSystem)){
        if(simulation.alreadyRun()){
            
            if(simulation.defined()){
                this->query = this->con->createStatement("begin :1 := updateSimulationRun(:2,1); end;");
                this->query->registerOutParam(1, OCCIINT);
                this->query->setInt(2, simulation.getID());
                this->query->executeUpdate();
                con->terminateStatement(this->query);
            }

            int pos=0;
            stringstream ss;
            vector<Timer> times=simulation.getSteps();
            vector<Corps*> corps = simulation.getCorps();
            for(vector<Timer>::const_iterator itt=times.begin();itt!=times.end();++itt,pos++){
                vector<XYZ> position = simulation.getPositions(pos);
                vector<XYZ>::const_iterator it_p;
                vector<Corps*>::const_iterator it_c;
                this->query = this->con->createStatement("begin :1 := insertResultsSimulationStar(:2,:3,:4,:5); end;");
                this->query->registerOutParam(1, OCCIINT);
                this->query->setInt(2,pos+1);
                this->query->setInt(3,idSimulation);
                this->query->setInt(4, idSolarSystem);
                ss.str("");
                ss<<position[0];
                this->query->setString(5,ss.str());
                this->query->executeUpdate();
                con->terminateStatement(this->query);
                for(it_p=position.begin()+1,it_c=corps.begin();it_p!=position.end();++it_p, ++it_c){
                    this->query = this->con->createStatement("begin :1 := insertResultsSimulationCorps(:2,:3,:4,:5,:6,:7); end;");
                    this->query->registerOutParam(1, OCCIINT);
                    this->query->setInt(2,pos+1);
                    this->query->setInt(3,idSimulation);
                    this->query->setInt(4, idSolarSystem);
                    this->query->setInt(5, (*it_c)->getID());
                    ss.str("");
                    (*itt).writePointerStream(ss);
                    this->query->setString(6,ss.str());
                    ss.str("");
                    ss<<*(it_p);
                    this->query->setString(7,ss.str());
                    this->query->executeUpdate();
                    con->terminateStatement(this->query);
                }
            }
            this->con->commit();
        }
        return &simulation;
    }
    return NULL;
}

bool DataLayerBD::deleteSimulation(const int& id){
    this->query = this->con->createStatement("begin :1 := deleteSimulation(:2); end;");
    this->query->registerOutParam(1, OCCIINT);
    this->query->setInt(2, id);
    this->query->executeUpdate();
    bool ret=(this->query->getInt(1)>0)?true:false;
    con->terminateStatement(query);
    this->con->commit();
    return ret;
}

void DataLayerBD::writeStream(ostream& out) const{
    
}

