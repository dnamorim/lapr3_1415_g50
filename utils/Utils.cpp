#include "Utils.h"

vector<string> &Utils::split(const string &s, char delim, vector<string> &elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

vector<string> Utils::split(const string &s, char delim) {
    vector<string> elems;
    split(s, delim, elems);
    return elems;
}

string Utils::getUnit(const string &s) {
    return (s.find("(") != -1 || s.find(")") != -1) ? s.substr(s.find_last_of("(") + 1, s.find_last_of(")") - s.find_last_of("(") - 1) : "";
}

string Utils::getExtension(const string &s) {
    string ext = (s.find(".") != -1) ? s.substr(s.find_last_of(".") + 1) : "";
    transform(ext.begin(), ext.end(), ext.begin(), ::tolower);
    return ext;
}

string Utils::removeAll(const string &str, char del) {
    string temp = str;
    temp.erase(remove(temp.begin(), temp.end(), del), temp.end());
    return temp;
}

bool Utils::is_file_exist(const string file)
{

    std::ifstream infile(file.c_str());
    return infile.good();   
}


bool Utils::getString(string&old, string prompt,string fail,bool retOld,bool validateFile, istream &in, ostream &out){
    do {
    if (!in) {
            in.clear();
            in.ignore(in.rdbuf()->in_avail());
        }
      out << prompt;
      string str;
      getline(in, str);
      if (str.empty() || (validateFile && !Utils::is_file_exist(str))){
          out << fail;
      }else{
          old=str;
          return true;
      }
      } while (!retOld);
    return false;
}
