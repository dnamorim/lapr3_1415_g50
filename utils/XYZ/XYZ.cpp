#include "XYZ.h"

double XYZ::X = 0;
double XYZ::Y = 0;
double XYZ::Z = 0;

XYZ::XYZ(double x, double y, double z) : x(x), y(y), z(z) {
}

XYZ::XYZ(const XYZ& copy) : x(copy.x), y(copy.y), z(copy.z) {
}

XYZ::~XYZ() {
}

XYZ* XYZ::clone() const {
    return new XYZ(*this);
}

double XYZ::getX() const {
    return this->x;
}

void XYZ::setX(double x) {
    this->x = x;
}

double XYZ::getY() const {
    return this->y;
}

void XYZ::setY(double y) {
    this->y = y;
}

double XYZ::getZ() const {
    return this->z;
}

void XYZ::setZ(double z) {
    this->z = z;
}

bool XYZ::operator==(const XYZ& op) const {
    return this->x == op.x && this->y == op.y && this->z == op.z;
}

void XYZ::writeStream(ostream& out) const {
    out << this->x << "," << this->y << "," << this->z;
}

void XYZ::readStream(istream &in, ostream& out) {
    Utils::getValue<double>(this->x, "X=", "Kept Value X\n", true, true, in, out);
    Utils::getValue<double>(this->y, "Y=", "Kept Value Y\n", true, true, in, out);
    Utils::getValue<double>(this->z, "Z=", "Kept Value Z\n", true, true, in, out);
}

XYZ& XYZ::operator=(const string& xyz) {
    vector<string> date = Utils::split(xyz, ',');
    if (date.size() >= 1) {
        this->x = atof(date[0].c_str());
    }
    if (date.size() >= 2) {
        this->y = atof(date[1].c_str());
    }
    if (date.size() >= 3) {
        this->z = atof(date[2].c_str());
    }
    return *this;
}


XYZ& XYZ::operator+=(const XYZ& op) {
    this->x += op.x;
    this->y += op.y;
    this->z += op.z;
    return *this;
}

XYZ XYZ::operator+(const XYZ& op2) {
    XYZ temp(*this);
    temp += op2;
    return temp;
}

XYZ& XYZ::operator-=(const XYZ& op) {
    this->x -= op.x;
    this->y -= op.y;
    this->z -= op.z;
    return *this;
}

XYZ XYZ::operator-(const XYZ& op2) {
    XYZ temp(*this);
    temp -= op2;
    return temp;
}

ostream& operator<<(ostream &out, const XYZ &xyz) {
    xyz.writeStream(out);
    return out;
}

istream& operator>>(istream &in, XYZ &xyz) {
    xyz.readStream(in, cout);
    return in;
}