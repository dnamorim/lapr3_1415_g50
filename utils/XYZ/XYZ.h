#ifndef XYZ_H
#define	XYZ_H
/**
 * Representação das Coordenadas
 * @file XYZ.h
 * 
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 * 
 * @version 1.1
 * >Criado a 4 de Janeiro de 2015, 19:24 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Defenição de Atributos e Métodos de Acesso/Consulta
*/

#include <iostream>

using namespace std;

#include "../../utils/Utils.h"


class XYZ {
protected:
    /* Coordenada X*/
    double x;
    /* Coordenada Y*/
    double y;
    /* Coordenada Z */
    double z;
public:
    /* Válores Pré-Definidos*/
    /* X Pré-Definido */
    static double X;
    /* Y Pré-Definido */
    static double Y;
    /* Z Pré-Definido */
    static double Z;
    
     /**
     * Construir Instância de XYZ(COMPLETO)
     */
    XYZ(double x = XYZ::X, double y = XYZ::Y, double z = XYZ::Z);
    
    /**
     * Construir Instância de XYZ(CÓPIA)
     * @param copy Instância de XYZ a Cópiar
     */
    XYZ(const XYZ& copy);
    
    /**
     * Destrotor da Instância de XYZ
     */
    virtual ~XYZ();

    /**
     * Clone da Instância do XYZ
     * @return Copia da Instância do XYZ
     */
    virtual XYZ* clone() const;
    
    /**
     * Consultar X do XYZ
     * @return X
     */
    double getX() const;
    
    /**
     * Alterar X do XYZ
     * @param x Novo X
     */
    void setX(double x=XYZ::X);
    
    /**
     * Consultar Y do XYZ
     * @return Y
     */
    double getY() const;
    
    /**
     * Alterar Y do XYZ
     * @param Y Novo X
     */
    void setY(double y=XYZ::Y);
    
    /**
     * Consultar Z do XYZ
     * @return Z
     */
    double getZ() const;
    
    /**
     * Alterar Z do XYZ
     * @param Z Novo X
     */
    void setZ(double z=XYZ::Z);
    
    /**
     * Sobrecarga de Operador==(Comparação de Instâncias)
     * @param op XYZ a Comparar
     * @return true se forem iguais, caso contrário retorna false
     */
    virtual bool operator==(const XYZ& op) const;
    
    /**
     * Representação Textual da Instância do XYZ
     * @param out Stream de Output
     */
    virtual void writeStream(ostream& out) const;

    /**
     * Leitura Textual da Instância do XYZ
     * @param in Stream de Input
     * @param out Stream de Output
     */
    virtual void readStream(istream &in, ostream& out);
    
    /**
     * Sobrecarga de Operador=(CONVERÇÃO)
     * @param xyz String para Converter
     * @return XYZ
     */
    XYZ& operator=(const string& xyz);
    
    /**
     * Sobrecarga de Operador+=(StreamOUT)
     * @param op Operador para Somar
     * @return Resultado
     */
    virtual XYZ& operator+=(const XYZ& op);
    
    /**
     * Sobrecarga de Operador+
     * @param op Operador para Somar
     * @return Resultado
     */
    virtual inline XYZ operator+( const XYZ& op2);
    
    /**
     * Sobrecarga de Operador+=
     * @param op Operador para Subtrair
     * @return Resultado
     */
    virtual XYZ& operator-=(const XYZ& op);
    
    /**
     * Sobrecarga de Operador-
     * @param op Operador para Subtrair
     * @return Resultado
     */
    virtual inline XYZ operator-( const XYZ& op2);
    
    /**
     * Sobrecarga de Operador<<(StreamOUT)
     * @param out Stream de Output
     * @param xyz XYZ para Output
     * @return StreamOUT
     */
    friend ostream& operator<<(ostream &out, const XYZ &xyz);

    /**
     * Sobrecarga de Operador>>(StreamIN)
     * @param out Stream de Input
     * @param xyz XYZ para Input
     * @return StreamIN
     */
    friend istream& operator>>(istream &in, XYZ &xyz);
    
};

typedef vector<XYZ> XYZs;

#endif	/* XYZ_H */

