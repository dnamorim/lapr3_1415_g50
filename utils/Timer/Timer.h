#ifndef TIMER_H
#define	TIMER_H

#include <iostream>
#include <iomanip>
#include <ctime>
#include <vector>
#include <string>
#include <stdlib.h>
#include <string.h>

using namespace std;

#include "../Utils.h"

class Timer {
private:
    struct tm timer;
    /* Converter para Data */
    bool convert;
public:
    static struct tm TIMER;
    static bool CONVERT;
    Timer(int  year=Timer::TIMER.tm_year, int month=Timer::TIMER.tm_mon, int day=Timer::TIMER.tm_mday,int sec=Timer::TIMER.tm_sec, int min=Timer::TIMER.tm_min, int hour=Timer::TIMER.tm_hour,bool convert=Timer::CONVERT);
    Timer(const Timer& copy);
    virtual ~Timer();
    
    virtual void clear();
    virtual void now();
    
    struct tm getTimer() const;
    void setTimer(struct tm time);
    
    int getYear() const;
    void setYear(int year);
    
    int getMonth() const;
    void setMonth(int month);
    
    int getDay() const;
    void setDay(int day);
    
    int getSec() const;
    void setSec(int sec);
    
    int getMin() const;
    void setMin(int min);
    
    int getHour() const;
    void setHour(int hour);
    
    long getSecondsFromTime() const;
    
    void convertDate(bool b);
    
    virtual Timer& operator=(const string& timer);
    
    virtual bool operator==(const Timer& op) const;
    
    virtual bool operator>(const Timer& op) const;
    
    virtual bool operator<(const Timer& op) const;
    
    virtual Timer operator+(const Timer& op);
    
    virtual Timer& operator+=(const Timer& op);
    /**
     * Representação Textual do Apontador do Corpo
     * @param out Stream de Output
     */
    virtual void writePointerStream(ostream& out) const;
    
    void writeStream(ostream& out) const;
    void readStream(istream &in, ostream& out);
    
    /**
     * Sobrecarga de Operador<<(StreamOUT)
     * @param out Stream de Output
     * @param timer Timer para Output
     * @return StreamOUT
     */
    friend ostream& operator<<(ostream &out, const Timer &timer);
    
    /**
     * Sobrecarga de Operador<<*(StreamOUT)
     * @param out Stream de Output
     * @param timer Timer para Output
     * @return StreamOUT
     */
    friend ostream& operator<<(ostream &out, const Timer *timer);
    
    /**
     * Sobrecarga de Operador>>(StreamIN)
     * @param out Stream de Input
     * @param timer Timer para Input
     * @return StreamIN
     */
    friend istream& operator>>(istream &in, Timer &timer);
};

#endif	/* TIMER_H */

