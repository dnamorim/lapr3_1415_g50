#include "Timer.h"

//  Desactivado devido aos erros de Compilação em Ambientes Windows (07/01/2014)
//struct tm Timer::TIMER = {.tm_sec = 0, .tm_hour = 0, .tm_min = 0, .tm_year = 1900, .tm_mon = 1, .tm_mday = 1};
struct tm Timer::TIMER = {0, 0, 0, 1, 1, 1900, 0, 0, 0};
bool Timer::CONVERT=true;

Timer::Timer(int year, int month, int day, int sec, int min, int hour,bool convert) : timer(),convert(convert) {
    this->setSec(sec);
    this->setMin(min);
    this->setHour(hour);
    this->setMonth(month);
    this->setDay(day);
    this->setYear(year);
}

Timer::Timer(const Timer& copy) : timer(copy.timer), convert(copy.convert) {
}

Timer::~Timer() {
}

void Timer::convertDate(bool b) {
    this->convert = b;
    if(b) {
        this->timer.tm_year -= 1900;
        this->timer.tm_mon -= 1;
        mktime(&this->timer);
    }
}

void Timer::clear() {
    this->setSec(Timer::TIMER.tm_sec);
    this->setMin(Timer::TIMER.tm_min);
    this->setHour(Timer::TIMER.tm_hour);
    this->setDay(Timer::TIMER.tm_wday);
    this->setMonth(Timer::TIMER.tm_mon);
    this->setYear(Timer::TIMER.tm_year);
}

void Timer::now() {
    time_t now;
    time(&now);
    this->timer = *localtime(&now);
}

struct tm Timer::getTimer() const {
    return this->timer;
}

void Timer::setTimer(struct tm time) {
    this->timer = time;
    if (this->convert) {
        mktime(&this->timer);
    }
}

int Timer::getYear() const {
    return this->timer.tm_year + 1900;
}

void Timer::setYear(int year) {
    this->timer.tm_year = year - ((this->convert) ? 1900 : 0);
    if (this->convert) {
        mktime(&this->timer);
    }
}

int Timer::getMonth() const {
    return this->timer.tm_mon + 1;
}

void Timer::setMonth(int month) {
    this->timer.tm_mon = month - ((this->convert) ? 1 : 0);
    if (this->convert) {
        mktime(&this->timer);
    }
}

int Timer::getDay() const {
    return this->timer.tm_mday;
}

void Timer::setDay(int day) {
    this->timer.tm_mday = day;
    if (this->convert) {
        mktime(&this->timer);
    }
}

int Timer::getSec() const {
    return this->timer.tm_sec;
}

void Timer::setSec(int sec) {
    this->timer.tm_sec = sec;
    if (this->convert) {
        mktime(&this->timer);
    }
}

int Timer::getMin() const {
    return this->timer.tm_min;
}

void Timer::setMin(int min) {
    this->timer.tm_min = min;
    if (this->convert) {
        mktime(&this->timer);
    }
}

int Timer::getHour() const {
    return this->timer.tm_hour;
}

void Timer::setHour(int hour) {
    this->timer.tm_hour = hour;
    if (this->convert) {
        mktime(&this->timer);
    }
}

bool Timer::operator>(const Timer& op) const {
    if (this->timer.tm_year > op.timer.tm_year) {
        return true;
    } else if (this->timer.tm_mon > op.timer.tm_mon) {
        return true;
    } else if (this->timer.tm_mday > op.timer.tm_mday) {
        return true;
    } else if (this->timer.tm_hour > op.timer.tm_hour) {
        return true;
    } else if (this->timer.tm_min > op.timer.tm_min) {
        return true;
    } else if (this->timer.tm_sec > op.timer.tm_sec) {
        return true;
    }
    
    return false;
}

bool Timer::operator<(const Timer& op) const {
    if (this->timer.tm_year < op.timer.tm_year) {
        return true;
    } else if (this->timer.tm_mon < op.timer.tm_mon) {
        return true;
    } else if (this->timer.tm_mday < op.timer.tm_mday) {
        return true;
    } else if (this->timer.tm_hour < op.timer.tm_hour) {
        return true;
    } else if (this->timer.tm_min < op.timer.tm_min) {
        return true;
    } else if (this->timer.tm_sec < op.timer.tm_sec) {
        return true;
    }
    
    return false;
    
}

long Timer::getSecondsFromTime() const {
    long seconds = 0;
    seconds += this->timer.tm_sec;
    seconds += this->timer.tm_min * 60;
    seconds += this->timer.tm_hour * 3600;
    seconds += this->timer.tm_mday * 24 * 3600;
    seconds +=((this->convert) ? this->timer.tm_mon + 1 : this->timer.tm_mon) * (2629743); //1 month (30.44 days) = 2629743 seconds (Unix time)
    seconds += ((this->convert) ? this->timer.tm_year + 1900 : this->timer.tm_year) * 31556926;// 1 year (365.24 days) = 31556926 seconds (Unix time)
    
    return seconds;
}

Timer Timer::operator+(const Timer& op) {
    Timer temp(*this);
    temp += op;
    return temp;
}

Timer& Timer::operator+=(const Timer& op) {
    this->timer.tm_year += op.timer.tm_year;
    this->timer.tm_mon += op.timer.tm_mon;
    this->timer.tm_mday += op.timer.tm_mday;
    this->timer.tm_hour += op.timer.tm_hour;
    this->timer.tm_min += op.timer.tm_min;
    this->timer.tm_sec += op.timer.tm_sec;
    mktime(&this->timer);
    return *this;
}


Timer& Timer::operator=(const string& timer) {
    this->clear();
    vector<string> date = Utils::split(timer, ' ');
    vector<string> date_split;
    if (date.size() >= 1) {
        date_split = Utils::split(date[0], '/');
        this->timer.tm_year = (date_split.size() >= 1) ? atoi(date_split[0].c_str()) - 1900 : 0;
        this->timer.tm_mon = (date_split.size() >= 2) ? atoi(date_split[1].c_str()) - 1 : 0;
        this->timer.tm_mday = (date_split.size() >= 3) ? atoi(date_split[2].c_str()) : 1;
    }
    if (date.size() >= 2) {
        date_split = Utils::split(date[1], ':');
        this->timer.tm_hour = (date_split.size() >= 1) ? atoi(date_split[0].c_str()) : 0;
        this->timer.tm_min = (date_split.size() >= 2) ? atoi(date_split[1].c_str()) : 0;
        this->timer.tm_sec = (date_split.size() >= 3) ? atoi(date_split[2].c_str()) : 0;
    }
    mktime(&this->timer);
    return *this;
}

bool Timer::operator==(const Timer& op) const {
    return (this == &op || (this->timer.tm_year == op.timer.tm_year && this->timer.tm_mon == op.timer.tm_mon && this->timer.tm_mday == op.timer.tm_mday && this->timer.tm_hour == op.timer.tm_hour && this->timer.tm_min == op.timer.tm_min && this->timer.tm_sec == op.timer.tm_sec));
}

void Timer::writeStream(ostream& out) const {
    if (this->convert) {
        char* time = asctime(&this->timer);
        time[strlen(time)-1]=(time[strlen(time)-1]=='\n')?'\0':time[strlen(time)-1];
        out << time;
    } else {
        out << this->timer.tm_year << " yrs, " << this->timer.tm_mon << " mon, " << this->timer.tm_mday << " days, " << this->timer.tm_hour << " hrs, " << this->timer.tm_min << " min, " << this->timer.tm_sec << " sec";
    }
}

void Timer::writePointerStream(ostream& out) const{
    out << setfill('0');
    out << this->timer.tm_year + 1900 << "/" << setw(2) << this->timer.tm_mon + 1 << "/" << setw(2) << this->timer.tm_mday << " " << setw(2) << this->timer.tm_hour << ":" << setw(2) << this->timer.tm_min << ":" << setw(2) << this->timer.tm_sec;
}

void Timer::readStream(istream &in, ostream& out) {
    out << "Current Date: ";
    this->writePointerStream(out);
    out << endl;
    Utils::getValue<int>(this->timer.tm_year, "Year=", "Kept Value Year\n", true, false, in, out);
    Utils::getValue<int>(this->timer.tm_mon, "Month=", "Kept Value Month\n", true, false, in, out);
    Utils::getValue<int>(this->timer.tm_mday, "Day=", "Kept Value Day\n", true, false, in, out);
    Utils::getValue<int>(this->timer.tm_hour, "Hour=", "Kept Value Hour\n", true, false, in, out);
    Utils::getValue<int>(this->timer.tm_min, "Min=", "Kept Value Min\n", true, false, in, out);
    Utils::getValue<int>(this->timer.tm_sec, "Sec=", "Kept Value Sec\n", true, false, in, out);
}

ostream& operator<<(ostream &out, const Timer &timer) {
    timer.writeStream(out);
    return out;
}

ostream& operator<<(ostream &out, const Timer *timer){
    timer->writePointerStream(out);
    return out;
}

istream& operator>>(istream &in, Timer &timer) {
    timer.readStream(in, cout);
    in.clear();
    return in;
}
