#ifndef UTILS_H
#define	UTILS_H

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>

using namespace std;

#include "../model/Unit/Unit.h"

class Utils {
private:
    static vector<string> &split(const string &s, char delim, vector<string> &elems);
public:
    static vector<string> split(const string &s, char delim = ';');
    static string getUnit(const string &s);
    static string getExtension(const string &s);
    static string removeAll(const string &str, char del = ' ');
    static bool is_file_exist(const string file);
    static bool getString(string& old, string prompt="",string fail = "",bool retOld = true,bool validateFile = false, istream &in = cin, ostream &out = cout);
    
    template< typename T>
    static bool getValue(T& old, string prompt = "", string fail = "", bool retOld = true, bool convUnit = false, istream &in = cin, ostream &out = cout);
};



template< typename T>
bool Utils::getValue(T& old, string prompt, string fail, bool retOld, bool convUnit, istream &in, ostream &out) {
    do {
        if (!in) {
            in.clear();
            in.ignore(in.rdbuf()->in_avail());
        }
        out << prompt;
        string str;
        string unit = "";
        getline(in, str);
        if (convUnit && str.find(" ")) {
            unit = str.substr(str.find_last_of(" ") + 1);
            unit=Utils::removeAll(unit);
            str = str.substr(0, str.find_last_of(" "));
        }
        istringstream ss(str);
        T val;
        ss >> val;
        if (str.empty() || !ss.eof()) {
            out << fail;
        } else {
            old = (convUnit) ? Unit::ConvertToSI((double) val, unit) : val;
            return true;
        }
    } while (!retOld);
    return false;
}

#endif	/* UTILS_H */

