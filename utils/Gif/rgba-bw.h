#ifndef RGBA_BW_H
#define RGBA_BW_H

typedef struct{
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    uint8_t alfa;
}rgba;

typedef struct{
    int width;
    int height;
    bool * print;
} bw;

enum perspective { XY,YZ, XZ };


#endif  /* RGBA_BW_H */
