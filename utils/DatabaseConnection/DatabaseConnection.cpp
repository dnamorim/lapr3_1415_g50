//
//  DatabaseConnection.cpp
//  LAPR3_1415_G54
//
//  Created by Duarte Nuno Amorim on 19/12/14.
//  Copyright (c) 2014 Duarte Nuno Amorim. All rights reserved.
//

#include "DatabaseConnection.h"

DatabaseConnection::DatabaseConnection(string usr, string pwd, string dbloc) {
    this->env = Environment::createEnvironment(Environment::DEFAULT);
    this->con = this->env->createConnection(usr, pwd, dbloc);
}

DatabaseConnection::DatabaseConnection(const DatabaseConnection& copy) : env(copy.env),con(copy.con){
    
}

DatabaseConnection::~DatabaseConnection() {
    this->env->terminateConnection(this->con);
    Environment::terminateEnvironment(this->env);
}