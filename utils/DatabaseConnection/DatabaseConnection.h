#ifndef DATABASECONNECTION_H
#define DATABASECONNECTION_H

#include <occi.h>
#include <string>

using namespace std;

using namespace oracle::occi;

class DatabaseConnection {
private:
    /* Environment da Base de Dados*/
    Environment *env;
protected:
    /* Coneção a Base de Dados*/
    Connection *con;
    /* Stream de Query a Base de Dados */
    Statement *query;
public:
    
    /**
     * Construir Instância de DatabaseConnection(COMPLETO)
     */
    DatabaseConnection(string usr, string pwd, string dbloc);
    
    /**
     * Construir Instância da DatabaseConnection(CÓPIA)
     * @param copy Instância da DatabaseConnection a Cópiar
     */
    DatabaseConnection(const DatabaseConnection& copy);
    
    /**
     * Destrotor da Instância da DatabaseConnection
     */
    ~DatabaseConnection();
};

#endif /* DATABASECONNECTION_H */
