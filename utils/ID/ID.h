#ifndef ID_H
#define	ID_H
/**
 * Identificador
 * @file ID.h
 * 
 * @author André Ferreira <1120243@isep.ipp.pt>
 * @author Duarte Amorim <1130674@isep.ipp.pt>
 * @author Gilberto Pereira <1131251@isep.ipp.pt>
 * @author Ricardo Rodrigues <1120703@isep.ipp.pt>
 * @author Sara Freitas <1130489@isep.ipp.pt>
 * 
 * @version 1.0
 * Criado a 2 de Janeiro de 2015, 15:05 por Gilberto Pereira <1131251@isep.ipp.pt>
 *  -Defenição de Atributos e Métodos de Acesso/Consulta
 *  -Sobrecarga de Operadores ==
 */

class ID {
private:
    /* Identificação */
    int id;
protected:
    /* Defenido*/
    bool define;
    /* Editado */
    bool edit;
    /* Activo */
    bool active;
public:
    /* Válores Pré-Definidos*/
    /* Identificação Pré-Definida */
    static int Id;
    /* Defenido Pré-Definido */
    static bool DEFINE;
    /* Editado Pré-Definido */
    static bool EDIT;
    /* Activo Pré-Definido */
    static bool ACTIVE;
    /**
     * Construir Instância ID(COMPLETO)
     */
    ID(int id=ID::Id,bool define=ID::DEFINE, bool edit=ID::EDIT, bool active=ID::ACTIVE);
    
    /**
     * Construir Instância ID(CÓPIA)
     * @param copy Instância de ID a Cópiar
     */
    ID(const ID& copy);
    
    /**
     * Destrotor da Instância ID
     */
    virtual ~ID();
    
     /**
     * Clone da Instância do Identificador
     * @return Copia da Instância do Identificador
     */
    virtual ID* clone() const;
    
    /**
     * Verificar se esta Defenido
     * @return true, se estiver defenido, caso contrário retorna false
     */
    virtual bool defined() const;
    
    /**
     * Vereificae se foi Editado
     * @return true, se foi editado, caso contrário retorna false
     */
    virtual bool edited() const;
    
    /**
     * Vereificae se esta activo
     * @return true, se foi activo, caso contrário retorna false
     */
    virtual bool isactive() const;
    
    /**
     * Consultar ID
     * @return ID
     */
    int getID() const;
    
    /**
     * Alterar ID
     * @param id Novo ID
     */
    void setID(int id);
    
    /**
     * Consultar Defenição
     * @return Defenição
     */
    bool getDefine() const;
    
    /**
     * Alterar Defenição
     * @param define Nova Defenição
     */
    void setDefine(bool define);
    
    /**
     * Consultar Edição
     * @return Edição
     */
    bool getEdit() const;
    
    /**
     * Alterar Edição
     * @param edit Nova Edição
     */
    void setEdit(bool edit);
    
    /**
     * Consultar Actividade
     * @return Actividade
     */
    bool getActive() const;
    
    /**
     * Alterar Actividade
     * @param active Nova Actividade
     */
    void setActive(bool active);
    
    /**
     * Sobrecarga de Operador==(Comparação de Instâncias)
     * @param op ID a Comparar
     * @return true se forem iguais, caso contrário retorna false
     */
    bool operator==(const ID& op) const;

};

#endif	/* ID_H */

