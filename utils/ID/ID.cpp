#include "ID.h"

int ID::Id = 0;
bool ID::DEFINE = false;
bool ID::EDIT = false;
bool ID::ACTIVE = true;

ID::ID(int id, bool define, bool edit, bool active) : id(id), define(define), edit(edit), active(active) {
}

ID::ID(const ID& copy) : id(copy.id), define(copy.define), edit(copy.edit), active(copy.active) {
}

ID::~ID() {
}

ID* ID::clone() const {
    return new ID(*this);
}

bool ID::defined() const {
    return this->define;
}

bool ID::edited() const {
    return this->edit;
}

bool ID::isactive() const{
    return this->active;
}

int ID::getID() const {
    return this->id;
}

void ID::setID(int id) {
    this->id = id;
    this->define = true;
}

bool ID::getDefine() const {
    return this->define;
}

void ID::setDefine(bool define) {
    this->define = define;
}

bool ID::getEdit() const {
    return this->edit;
}

void ID::setEdit(bool edit) {
    this->edit = edit;
}

bool ID::getActive() const{
    return this->active;
}


void ID::setActive(bool active){
    this->active=active;
}

bool ID::operator==(const ID& op) const {
    return (this->id == op.id);
}